var path = location.pathname.split('/');
if ( path[path.length-1].indexOf('.html') > -1 ) {
  path.length = path.length - 1;
}
var app_path = path[path.length-3]; // if you just want 'three'
// var app = path.join('/'); //  if you want the whole thing like '/one/two/three'

var root2 = app_path;
var root = app_path;

var loc = window.location;
var relativeURL = loc.protocol + '//' + loc.host + '/' + app_path;


$(document).ready(function () {
  IncludeDatepicker();
	autocompletePenyuluh();
  clearValIfTextEmpty();
  
});

function IncludeDatepicker(){
  $('#ektp-tgl-lahir').datepicker({format:'yyyy-mm-dd'});
  $('#ektp-tgl-terbit').datepicker({format:'yyyy-mm-dd'});
  $('#ektp-tgl-berakhir').datepicker({format:'yyyy-mm-dd'});
}

function autocompletePenyuluh(){
	var dataarray = [];
	var urlApi = relativeURL+"/petani/getEktp.json";
    var jsonData = $.ajax({
      url: urlApi,
      dataType:"json",
      async: false
      }).responseText;

    var dataJson = $.parseJSON(jsonData);
    var data = dataJson.data;
    $( "#no_ektp" ).autocomplete({
      source: dataarray,
      select: function(event , ui) {
          //$("#ektp-nama").val(ui.item.value);
          getEktpInfoPenyuluh(ui.item.value);
      },
      response: function(event, ui) {
            // ui.content is the array that's about to be sent to the response callback.
            if (ui.content.length === 0) {
                $("#empty-message").text("No results found");
                clearVal();
            } else {
                $("#empty-message").empty();
                clearVal();
            }
        }
    });

	$.each(data, function(i, val) {
	    $.each(val, function(index, val) {
	        dataarray.push(val);
    	})
	})
}

function getEktpInfoPenyuluh(ektpId) {
	//alert(ektpId);
	var urlApi = relativeURL+"/petani/getEktpDetail/"+ektpId+".json";
    var jsonData = $.ajax({
      url: urlApi,
      dataType:"json",
      async: false
      }).responseText;
    var dataJson = $.parseJSON(jsonData);
    var data = dataJson.data;
    
    $('#ektp-status-perkawinan').replaceWith('<input class="form-control" type="text" readonly name="ektp[status_perkawinan]" id="ektp-status-perkawinan">');
    $("#ektp-status-perkawinan").val(data.status_perkawinan);
    $('#ektp-gol-darah').replaceWith('<input class="form-control" type="text" readonly name="ektp[gol_darah]" id="ektp-gol-darah">');
    $("#ektp-gol-darah").val(data.gol_darah);
    $("#ektp-nama").val(data.nama);
    $('#ektp-nama').prop('readonly', true);
    $("#ektp-alamat").val(data.alamat);
    $('#ektp-alamat').prop('readonly', true);

    var tglLahir = new Date(data.tanggal_lahir);
    var tglLhr = tglLahir.getDate();
	if (tglLhr<10) tglLhr= '0'+tglLhr
	var blnLhr = data.tanggal_lahir.substr(5, 2);
	var thnLhr = tglLahir.getFullYear();
	var newDateLahir = thnLhr+"-"+blnLhr+"-"+tglLhr;
    $("#ektp-tgl-lahir").val(newDateLahir);
    $("#ektp-tgl-lahir").attr('readonly',true);
    $("#ektp-tgl-lahir").unbind();

    var tglTerbit = new Date(data.tanggal_terbit);
    var tglTbt = tglTerbit.getDate();
	if (tglTbt<10) tglTbt= '0'+tglTbt
	var blnTbt = data.tanggal_terbit.substr(5, 2);
	var thnLhr = tglTerbit.getFullYear();
	var newDateTerbit = thnLhr+"-"+blnTbt+"-"+tglTbt;
    $("#ektp-tgl-terbit").val(newDateTerbit);
    $("#ektp-tgl-terbit").attr('readonly',true);
    $("#ektp-tgl-terbit").unbind();

    var tglBerakhir = new Date(data.tanggal_berakhir);
    var tglBhr = tglBerakhir.getDate();
	if (tglBhr<10) tglBhr= '0'+tglBhr
	var blnBhr = data.tanggal_berakhir.substr(5, 2);
	var thnBhr = tglBerakhir.getFullYear();
	var newDateBerakhir = thnBhr+"-"+blnBhr+"-"+tglBhr;
    $("#ektp-tgl-berakhir").val(newDateBerakhir);
    $("#ektp-tgl-berakhir").attr('readonly',true);
    $("#ektp-tgl-berakhir").unbind();

    $("#ektp-rt-rw").val(data.rt_rw);
    $('#ektp-rt-rw').prop('readonly', true);
    $("#ektp-kecamatan").val(data.kecamatan);
    $('#ektp-kecamatan').prop('readonly', true);
    $("#ektp-kabupaten-kota").val(data.kabupaten_kota);
    $('#ektp-kabupaten-kota').prop('readonly', true);
}

function clearVal() {
	$('#ektp-nama').val('');
	$('#ektp-nama').prop('readonly', false);
	$('#ektp-alamat').val('');
	$('#ektp-alamat').prop('readonly', false);
	$('#ektp-tgl-lahir').val('');
	$('#ektp-tgl-lahir').prop('readonly', false);
	$('#ektp-tgl-lahir').on("click", function(){ $('#ektp-tgl-lahir').datepicker('show'); } );
	$('#ektp-tgl-terbit').val('');
	$('#ektp-tgl-terbit').prop('readonly', false);
	$('#ektp-tgl-terbit').on("click", function(){ $('#ektp-tgl-terbit').datepicker('show'); } );
	$('#ektp-tgl-berakhir').val('');
	$('#ektp-tgl-berakhir').prop('readonly', false);
	$('#ektp-tgl-berakhir').on("click", function(){ $('#ektp-tgl-berakhir').datepicker('show'); } );
	$('#ektp-rt-rw').val('');
	$('#ektp-rt-rw').prop('readonly', false);
  $('#ektp-kecamatan').val('');
  $('#ektp-kecamatan').prop('readonly', false);
  $('#ektp-kabupaten-kota').val('');
  $('#ektp-kabupaten-kota').prop('readonly', false);
  $('#ektp-status-perkawinan').replaceWith('<select name="ektp[status_perkawinan]" required="required" id="ektp-status-perkawinan" class="form-control"><option value="">Please Select One</option><option value="Kawin">Kawin</option><option value="Belum Kawin">Belum Kawin</option></select>');
  $('#ektp-gol-darah').replaceWith('<select name="ektp[gol_darah]" required="required" id="ektp-gol-darah" class="form-control"><option value="">please select one</option><option value="A">A</option><option value="B">B</option><option value="AB">AB</option><option value="O">O</option></select>');
}

function clearValIfTextEmpty() {
  $('#no_ektp').focusout(function() {
      if ($(this).val()===null || $(this).val()==="") {
            clearVal();
        } 
  })
}