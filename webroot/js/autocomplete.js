var today = new Date();
var path = location.pathname.split('/');
if ( path[path.length-1].indexOf('.html') > -1 ) {
  path.length = path.length - 1;
}

var app_path = path[1]; // if you just want 'three'
// var app = path.join('/'); //  if you want the whole thing like '/one/two/three'

var root = location.protocol + '//' + location.host + '/' + app_path;


var classlahanSize = $('.classlahan').size();

$(document).ready(function () {
	IncludeDatepicker();
	autocompletePetani();
	clearValIfTextEmpty();
  //addLahan();
  hideLahan();
  addTab();
  seumurHidup();
  hitungUmur();
});

function hitungUmur(){
  var lahir = new Date($('#lahir').val());
  var age = Math.floor((today-lahir) / (365.25 * 24 * 60 * 60 * 1000));
  var strAge = String(age);
  console.log(strAge);
  if (strAge == "NaN"){
    var newAge = "";
  }else{
    var newAge = age+" Tahun";
  }
  $('#umur').val(newAge);
  $("#umur").attr('readonly',true);
  $("#umur").unbind();
}

function seumurHidup(){
  $('#ektp-seumur-hidup').change(function(){
      if($(this).val() == 1){
        $('#ektp-tgl-berakhir').val('Seumur Hidup').attr('readonly',true);
        $("#ektp-tgl-berakhir").unbind();
      }
      else{
        $('#ektp-tgl-berakhir').val('').attr('readonly',false);        
        $('#ektp-tgl-berakhir').on("click", function(){ $('#ektp-tgl-berakhir').datepicker('show'); } );
      }
  });
}

$(document).on('change', '.lahan-jenis_tanaman_id', function () {
    if (this.value == 3)
      {
        console.log($(this).parents('.lahan_content_form').find('.dataLahanDetil').hide());

      }else{
        console.log($(this).parents('.lahan_content_form').find('.dataLahanDetil').show());
        var idKomoditas =   $(this).parents('.lahan_content_form').find('.petaniKomoditas').val();
        $(this).parents('.lahan_content_form').find('.komoditasPetani').val(idKomoditas);
      }
  });

  $(document).on('change','.lahan-komoditas_id',function(){
      $(this).parents('.lahanForm').find('.komoditasPetani').val($(this).val());
  });

  $(document).on('click','.addAllLahan',function(event){
    event.preventDefault();
    id = '#'+($(this).parents('.tab-pane').attr('id'));
    idData = $(this).parents('.actionButton').data('id');
    addLahan(idData,'.lahanForm',id);
  });

  $(document).on('click','.tabSubSektor li a',function(e){
    e.preventDefault();
    $(this).tab('show');

  });

  $(document).on('click','.delLahan',function(event){
      event.preventDefault();
      sizeOfPanel = $(this).parents('.tab-pane').find('.lahanForm').size();
      if(sizeOfPanel>1){
        $(this).parents('.lahanForm').remove();
      }

  });

function hideLahan(){
  $('.lahan-jenis_tanaman_id').on('change', function() {
      if (this.value == 3)
      {
        console.log($(this).parents('.lahan_content_form').find('.dataLahanDetil').hide());

      }
      else
      {
        console.log($(this).parents('.lahan_content_form').find('.dataLahanDetil').show());
      }
  });
}

function deleteLahan(){
  //var i = $('.classlahan').size();
  $("#lahankita"+(classlahanSize)).remove();
  classlahanSize = $('.lahan_content_form').size();
  //var yg_dihapus = i - 1;
  console.log("lahankita"+(classlahanSize));
  //i = i - 1;
}

function addTab(){
  $('.subSectorSelect').click(function(){
    var tabSubSektor = $('.tabSubSektor');
    var classTab = 'tab'+$(this).val();
    console.log($(this).is(':checked'));
    if($(this).is(':checked')){
      tabs = $('.tabSubSektorTemplate li').clone();
      label = $(this).next('label').find('.subSektorLabel').text();
      idTab = '#tab'+$(this).val();

      tabs.find('a').text(label);
      tabs.find('a').attr('href',idTab);
      //bersihkan kelas aktif
      $('.active').removeClass('active');
      //tambahkan kelas active
      tabs.addClass('active');
      tabs.addClass(classTab);
      tabs.appendTo(tabSubSektor);
      addLahan($(this).val(),'.tab-pane','#add_lahan',label);
    }else{
      if (confirm('Are you sure ?')) {
        $('.'+classTab).remove();
      }else{
        $(this).prop('checked', true);
      }
    }

  });
}

function addLahan(id,classTabs,tempatNambah){
  // ini id div nya
  var var_addLahan = $(tempatNambah);
  var var_addLahanContent = $('#template '+classTabs);
  //i = i + 1;

  //$('#addAllLahan').click(function() {
    classlahanSize = $('.tab-content').data('size');
    $('.tab-content').data('size',parseInt(classlahanSize)+1);
    var klonenganInputanLahan = var_addLahanContent.clone();
    klonenganInputanLahan.addClass('tab'+id);
    console.log(classlahanSize);
    var idLahan= 'tab' + (id);
    klonenganInputanLahan.attr('id', idLahan);
    //klonenganInputanLahan.find("legend").html('Data Lahan ' + (classlahanSize + 0));
    klonenganInputanLahan.find(".lahan-luas").attr('name', 'komoditas_petani[' + classlahanSize + '][lahan][luas]').attr('id', 'lahan-' + classlahanSize + '-luas');
    klonenganInputanLahan.find(".lahan-daerah_id").attr('name', 'komoditas_petani[' + classlahanSize + '][lahan][daerah_id]').attr('id', 'lahan-' + classlahanSize + '-daerah_id');
    klonenganInputanLahan.find(".lahan-alamat").attr('name', 'komoditas_petani[' + classlahanSize + '][lahan][alamat]').attr('id', 'lahan-' + classlahanSize + '-alamat');
    klonenganInputanLahan.find(".lahan-subsektor_id").attr('name', 'komoditas_petani[' + classlahanSize + '][lahan][subsektor_id]').attr('id', 'lahan-' + classlahanSize + '-subsektor_id');
    klonenganInputanLahan.find(".lahan-komoditas_id").attr('name', 'komoditas_petani[' + classlahanSize + '][lahan][komoditas_id]').attr('id', 'lahan-' + classlahanSize + '-komoditas_id');
    klonenganInputanLahan.find(".komoditasPetani").attr('name', 'komoditas_petani[' + classlahanSize + '][id_komoditas]');
    klonenganInputanLahan.find(".lahan-jenis_tanaman_id").attr('name', 'komoditas_petani[' + classlahanSize + '][id_jenis_petani]').attr('id', 'komoditas-petani-' + classlahanSize + '-id-jenis-petani');
    klonenganInputanLahan.find(".JenisTanaman").attr('name', 'komoditas_petani[' + classlahanSize + '][lahan][jenis_tanaman_id]').attr('id', 'komoditas-petani-' + classlahanSize + '-id-jenis-tanaman');
    klonenganInputanLahan.find(".namaAsset").attr('name', 'komoditas_petani[' + classlahanSize + '][lahan][assets][0][nama_asset]');
    klonenganInputanLahan.find(".jumlahAsset").attr('name', 'komoditas_petani[' + classlahanSize + '][lahan][assets][0][jumlah]');
    klonenganInputanLahan.find(".namaAsset2").attr('name', 'komoditas_petani[' + classlahanSize + '][lahan][assets][1][nama_asset]');
    klonenganInputanLahan.find(".jumlahAsset2").attr('name', 'komoditas_petani[' + classlahanSize + '][lahan][assets][1][jumlah]');
    klonenganInputanLahan.find(".actionButton").data('id',id);
    klonenganInputanLahan.find(".subsektor_id_lahan").attr('name', 'komoditas_petani[' + classlahanSize + '][subsektor_id]');
    klonenganInputanLahan.find(".subsektor_id_lahan").val(id);

    //console.log( klonengan.find(".lahan-luas").get() );

    if(id!=10){
      klonenganInputanLahan.find('.peternakan').remove();
    }

    //var klonengan = $('<div>').append( klonenganAwal).remove().html();
    //klonenganInputanLahan.find(".lahan-subsektor_id").change(function (){

        //console.log( $(this));
        var $komoditasEL = klonenganInputanLahan.find('.komoditasEl');
        console.log( $komoditasEL );
        klonenganInputanLahan.find('.dataSubsektor').append('<span class="semi-bold">Sedang mengambil data</div>');
        klonenganInputanLahan.find('.jenisLahan').append('<span class="semi-bold">Sedang mengambil data</div>');
        $.ajax({
            type: "POST",
            url: root + "/komoditas/getKomoditas/",
            data: { subsektor_id:  id, inpNumber: classlahanSize },
            success: function (data) {
                //console.log( $(this));
                console.log( data );
                //klonenganInputanLahan.find('.dataSubsektor').html();
                klonenganInputanLahan.find('.dataSubsektor').html(data);
                //$komoditasEL.remove();
            }
        });

        $.ajax({
            type: "POST",
            url: root + "/subsektor/getJenisLahan/",
            data: { subsektor_id:  id, inpNumber: classlahanSize },
            success: function (dataSubsektor) {
                //console.log( $(this));
                console.log( dataSubsektor );
                //klonenganInputanLahan.find('.dataSubsektor').html();
                klonenganInputanLahan.find('.jenisLahan').html(dataSubsektor);
                //$komoditasEL.remove();
            }
        }).done(function(){
          klonenganInputanLahan.find("select").select2();
        });

        if(id==11){
          klonenganInputanLahan.find('.jenisLahan').find('label').text('Jenis Kapal');
          klonenganInputanLahan.find('.luasLahan').find('label').text('Bobot Kapal(GT)');
        }else if (id==10 || id== 12) {
          klonenganInputanLahan.find('.jenisLahan').find('label').text('Jenis Usaha');
        }

    //});
    klonenganInputanLahan.addClass('active');
    klonenganInputanLahan.appendTo(var_addLahan);
    //$('#'+idLahan).find('.dataLahanDetil').show();
    console.log(classlahanSize);
    //return false;
  //});
}

function IncludeDatepicker(){
  $('#lahir').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
  }).on('changeDate', function(ev) {
    // $("#makes_me").val(calcAge(ev.date));
    var today = new Date(),
        lahir = new Date(this.value);
    var age = Math.floor((today-lahir) / (365.25 * 24 * 60 * 60 * 1000));
    var newAge = age+" Tahun";
    $("#umur").val(newAge);
    $("#umur").attr('readonly',true);
    $("#umur").unbind();
  });

	$('#ektp-tgl-terbit').datepicker({format:'yyyy-mm-dd',autoclose: true});
	$('#ektp-tgl-berakhir').datepicker({format:'yyyy-mm-dd',autoclose: true});
}

function autocompletePetani(){
	var dataarray = [];
	var urlApi = root+"/petani/getEktp.json";
    var jsonData = $.ajax({
      url: urlApi,
      dataType:"json",
      async: false
      }).responseText;

    var dataJson = $.parseJSON(jsonData);
    var data = dataJson.data;
    $( "#no_ektp" ).autocomplete({
      source: dataarray,
      select: function(event , ui) {
          //$("#ektp-nama").val(ui.item.value);
          getEktpInfoPetani(ui.item.value);
      },
      response: function(event, ui) {
            // ui.content is the array that's about to be sent to the response callback.
            if (ui.content.length === 0) {
                $("#empty-message").text("No results found");
                clearVal();
            } else {
                $("#empty-message").empty();
                clearVal();
            }
        }
    });

	$.each(data, function(i, val) {
	    $.each(val, function(index, val) {
	        dataarray.push(val);
    	})
	})
}

function getEktpInfoPetani(ektpId) {
	//alert(ektpId);
	var urlApi = root+"/petani/getEktpDetail/"+ektpId+".json";
    var jsonData = $.ajax({
      url: urlApi,
      dataType:"json",
      async: false
      }).responseText;
    var dataJson = $.parseJSON(jsonData);
    var data = dataJson.data;
	//console.log(data[0].nama);
    // $("#ektp-status-perkawinan option[value='" + data[0].status_perkawinan + "']").attr('selected','selected');
    $('#ektp-status-perkawinan').replaceWith('<input class="form-control" type="text" readonly name="ektp[status_perkawinan]" id="ektp-status-perkawinan">');
    $("#ektp-status-perkawinan").val(data.status_perkawinan);
    $('#ektp-gol-darah').replaceWith('<input class="form-control" type="text" readonly name="ektp[gol_darah]" id="ektp-gol-darah">');
    $("#ektp-gol-darah").val(data.gol_darah);
    $("#ektp-nama").val(data.nama);
    $('#ektp-nama').prop('readonly', true);
    $("#ektp-alamat").val(data.alamat);
    $('#ektp-alamat').prop('readonly', true);

    var tglLahir = new Date(data.tanggal_lahir);
    var tglLhr = tglLahir.getDate();
	  if (tglLhr<10) tglLhr= '0'+tglLhr
	  var blnLhr = data.tanggal_lahir.substr(5, 2);
	  var thnLhr = tglLahir.getFullYear();
	  var newDateLahir = thnLhr+"-"+blnLhr+"-"+tglLhr;
    $("#lahir").val(newDateLahir);
    $("#lahir").attr('readonly',true);
    $("#lahir").unbind();

    var age = Math.floor((today-tglLahir) / (365.25 * 24 * 60 * 60 * 1000));
    var newAge = age+" Tahun";
    $("#umur").val(newAge);
    $("#umur").attr('readonly',true);
    $("#umur").unbind();

    var tglTerbit = new Date(data.tanggal_terbit);
    var tglTbt = tglTerbit.getDate();
	  if (tglTbt<10) tglTbt= '0'+tglTbt
	  var blnTbt = data.tanggal_terbit.substr(5, 2);
	  var thnLhr = tglTerbit.getFullYear();
	  var newDateTerbit = thnLhr+"-"+blnTbt+"-"+tglTbt;
    $("#ektp-tgl-terbit").val(newDateTerbit);
    $("#ektp-tgl-terbit").attr('readonly',true);
    $("#ektp-tgl-terbit").unbind();

    var tglBerakhir = new Date(data.tanggal_berakhir);
    var tglBhr = tglBerakhir.getDate();
	  if (tglBhr<10) tglBhr= '0'+tglBhr
	  var blnBhr = data.tanggal_berakhir.substr(5, 2);
	  var thnBhr = tglBerakhir.getFullYear();
	  var newDateBerakhir = thnBhr+"-"+blnBhr+"-"+tglBhr;
    $("#ektp-tgl-berakhir").val(newDateBerakhir);
    $("#ektp-tgl-berakhir").attr('readonly',true);
    $("#ektp-tgl-berakhir").unbind();

    $("#ektp-rt-rw").val(data.rt_rw);
    $('#ektp-rt-rw').prop('readonly', true);

    $("#ektp-daerah-id").val(data.daerah_id);
    $('#ektp-daerah-id').prop('readonly', true);

    $("#lahan-0-daerah-id").val(data.daerah_id);
    $("#lahan-0-daerah-id option[value=" + data.daerah_id + "]").attr('selected', 'selected');
}

function clearVal() {
	$('#ektp-nama').val('');
	$('#ektp-nama').prop('readonly', false);
	$('#ektp-alamat').val('');
	$('#ektp-alamat').prop('readonly', false);
	$('#lahir').val('');
	$('#lahir').prop('readonly', false);
  $('#lahir').on("click", function(){ $('#lahir').datepicker('show'); } );
  $('#lahir').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
  }).on('changeDate', function(ev) {
    // $("#makes_me").val(calcAge(ev.date));
    var today = new Date(),
        lahir = new Date(this.value);
    var age = Math.floor((today-lahir) / (365.25 * 24 * 60 * 60 * 1000));
    var newAge = age+" Tahun";
    $("#umur").val(newAge);
    $("#umur").attr('readonly',true);
    $("#umur").unbind();
  });
  $('#umur').val('');
  $('#umur').prop('readonly', false);	
  $("#umur").unbind();
	$('#ektp-tgl-terbit').val('');
	$('#ektp-tgl-terbit').prop('readonly', false);
	$('#ektp-tgl-terbit').on("click", function(){ $('#ektp-tgl-terbit').datepicker('show'); } );
	$('#ektp-tgl-berakhir').val('');
	$('#ektp-tgl-berakhir').prop('readonly', false);
	$('#ektp-tgl-berakhir').on("click", function(){ $('#ektp-tgl-berakhir').datepicker('show'); } );
	$('#ektp-rt-rw').val('');
	$('#ektp-rt-rw').prop('readonly', false);
  $('#ektp-status-perkawinan').replaceWith('<select name="ektp[status_perkawinan]" required="required" id="ektp-status-perkawinan" class="form-control"><option value="">Pilih Status Perkawinan</option><option value="KAWIN">Kawin</option><option value="BELUM KAWIN">Belum Kawin</option></select>');
  $('#ektp-gol-darah').replaceWith('<select name="ektp[gol_darah]" required="required" id="ektp-gol-darah" class="form-control"><option value="">Pilih Golongan Darah</option><option value="A">A</option><option value="B">B</option><option value="AB">AB</option><option value="O">O</option><option value="Tidak Tahu">Tidak Tahu</option></select>');
}

function clearValIfTextEmpty() {
	$('#no_ektp').focusout(function() {
	    if ($(this).val()===null || $(this).val()==="") {
            clearVal();
        }
	})
}
