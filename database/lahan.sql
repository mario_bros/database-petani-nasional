-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 13, 2016 at 11:07 AM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_petani`
--

-- --------------------------------------------------------

--
-- Table structure for table `lahan`
--

CREATE TABLE `lahan` (
  `id` int(11) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `daerah_id` int(11) NOT NULL,
  `luas` decimal(8,2) NOT NULL,
  `jenis_tanaman_id` int(50) NOT NULL,
  `petani_id` int(11) NOT NULL,
  `jenis_tanah_id` int(11) NOT NULL,
  `komoditas_id` int(11) NOT NULL DEFAULT '0',
  `jenis_lahan_id` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lahan`
--

INSERT INTO `lahan` (`id`, `alamat`, `daerah_id`, `luas`, `jenis_tanaman_id`, `petani_id`, `jenis_tanah_id`, `komoditas_id`, `jenis_lahan_id`) VALUES
(1, 'PAINGAN', 9, 1.00, 1, 131, 0, 1, 2),
(2, 'PAINGAN', 9, 0.00, 2, 56, 0, 2, 2),
(3, 'PAINGAN', 9, 1.00, 1, 57, 0, 1, 2),
(4, 'PAINGAN', 9, 0.00, 1, 58, 0, 1, 2),
(5, 'PAINGAN', 9, 0.00, 1, 59, 0, 1, 2),
(6, 'PAINGAN', 9, 0.00, 1, 60, 0, 1, 2),
(7, 'PAINGAN', 9, 0.00, 1, 61, 0, 1, 2),
(8, 'PAINGAN', 9, 0.00, 1, 62, 0, 1, 2),
(9, 'PAINGAN', 9, 0.00, 1, 63, 0, 1, 2),
(10, 'PAINGAN', 9, 0.00, 1, 64, 0, 1, 2),
(11, 'PAINGAN', 9, 0.00, 1, 65, 0, 1, 2),
(12, 'PAINGAN', 9, 0.00, 3, 66, 0, 3, 1),
(13, 'PAINGAN', 9, 0.00, 3, 67, 0, 3, 1),
(14, 'PAINGAN', 9, 0.00, 2, 68, 0, 2, 1),
(15, 'PAINGAN', 9, 1.00, 2, 69, 0, 2, 1),
(16, 'PAINGAN', 9, 0.00, 1, 70, 0, 1, 1),
(17, 'PAINGAN', 9, 0.00, 1, 71, 0, 1, 2),
(18, 'PAINGAN', 9, 0.00, 1, 72, 0, 1, 2),
(19, 'PAINGAN', 9, 0.00, 1, 73, 0, 1, 2),
(20, 'PAINGAN', 9, 0.00, 1, 74, 0, 1, 2),
(21, 'JENGGRIK', 9, 1.00, 1, 75, 0, 1, 2),
(22, 'JENGGRIK', 9, 0.00, 1, 76, 0, 1, 2),
(23, 'JENGGRIK', 9, 0.00, 1, 77, 0, 1, 2),
(24, 'JENGGRIK', 9, 0.00, 1, 78, 0, 1, 2),
(25, 'JENGGRIK', 9, 0.00, 1, 79, 0, 1, 2),
(26, 'JENGGRIK', 9, 0.00, 1, 80, 0, 1, 1),
(27, 'JENGGRIK', 9, 1.00, 1, 81, 0, 1, 1),
(28, 'JENGGRIK', 9, 0.00, 1, 82, 0, 1, 1),
(29, 'JENGGRIK', 9, 0.00, 1, 83, 0, 1, 1),
(30, 'JENGGRIK', 9, 0.00, 1, 84, 0, 1, 1),
(31, 'JENGGRIK', 9, 0.00, 2, 85, 0, 2, 1),
(32, 'JENGGRIK', 9, 0.00, 2, 86, 0, 2, 1),
(33, 'JENGGRIK', 9, 0.00, 2, 87, 0, 2, 2),
(34, 'JENGGRIK', 9, 0.00, 2, 88, 0, 2, 2),
(35, 'JENGGRIK', 9, 0.00, 2, 89, 0, 2, 2),
(36, 'JENGGRIK', 9, 0.00, 1, 90, 0, 1, 2),
(37, 'JENGGRIK', 9, 1.00, 1, 91, 0, 1, 2),
(38, 'JENGGRIK', 9, 0.00, 1, 92, 0, 1, 2),
(39, 'JENGGRIK', 9, 0.00, 3, 93, 0, 3, 2),
(40, 'JENGGRIK', 9, 0.00, 3, 94, 0, 3, 2),
(41, 'JENGGRIK', 9, 0.00, 3, 95, 0, 3, 2),
(42, 'JENGGRIK', 9, 0.00, 1, 96, 0, 1, 2),
(43, 'JENGGRIK', 9, 0.00, 1, 97, 0, 1, 2),
(44, 'JENGGRIK', 9, 0.00, 1, 98, 0, 1, 2),
(45, 'JENGGRIK', 9, 0.00, 1, 99, 0, 1, 2),
(46, 'MOJOBAYI', 9, 1.00, 1, 100, 0, 1, 2),
(47, 'MOJOBAYI', 9, 0.00, 1, 101, 0, 1, 2),
(48, 'MOJOBAYI', 9, 0.00, 1, 102, 0, 1, 2),
(49, 'MOJOBAYI', 9, 0.00, 1, 103, 0, 1, 2),
(50, 'MOJOBAYI', 9, 0.00, 1, 104, 0, 1, 2),
(51, 'MOJOBAYI', 9, 0.00, 2, 105, 0, 2, 2),
(52, 'MOJOBAYI', 9, 0.00, 2, 106, 0, 2, 2),
(53, 'MOJOBAYI', 9, 0.00, 2, 107, 0, 2, 2),
(54, 'MOJOBAYI', 9, 0.00, 2, 108, 0, 2, 2),
(55, 'MOJOBAYI', 9, 0.00, 2, 109, 0, 2, 2),
(56, 'MOJOBAYI', 9, 0.00, 2, 110, 0, 2, 2),
(57, 'MOJOBAYI', 9, 1.00, 2, 111, 0, 2, 2),
(58, 'MOJOBAYI', 9, 0.00, 1, 112, 0, 1, 2),
(59, 'MOJOBAYI', 9, 0.00, 1, 113, 0, 1, 2),
(60, 'MOJOBAYI', 9, 0.00, 1, 114, 0, 1, 2),
(61, 'MOJOBAYI', 9, 0.00, 1, 115, 0, 1, 1),
(62, 'MOJOBAYI', 9, 0.00, 1, 116, 0, 1, 1),
(63, 'MOJOBAYI', 9, 0.00, 1, 117, 0, 1, 1),
(64, 'MOJOBAYI', 9, 0.00, 1, 118, 0, 1, 1),
(65, 'MOJOBAYI', 9, 0.00, 1, 119, 0, 1, 1),
(66, 'MOJOBAYI', 9, 0.00, 1, 120, 0, 1, 1),
(67, 'MOJOBAYI', 9, 0.00, 3, 121, 0, 3, 1),
(68, 'MOJOBAYI', 9, 0.00, 3, 122, 0, 3, 1),
(69, 'MOJOBAYI', 9, 0.00, 3, 123, 0, 3, 1),
(70, 'MOJOBAYI', 9, 0.00, 3, 124, 0, 3, 1),
(71, 'MOJOBAYI', 9, 1.00, 3, 125, 0, 3, 1),
(72, 'MOJOBAYI', 9, 0.00, 3, 126, 0, 3, 2),
(73, 'MOJOBAYI', 9, 0.00, 1, 132, 0, 1, 2),
(74, 'MOJOBAYI', 9, 0.00, 1, 128, 0, 1, 2),
(75, 'MOJOBAYI', 9, 0.00, 1, 129, 0, 1, 2),
(76, 'MOJOBAYI', 9, 2.00, 1, 130, 0, 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lahan`
--
ALTER TABLE `lahan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXFK_lahan_petani` (`petani_id`),
  ADD KEY `IXFK_lahan_daerah` (`daerah_id`),
  ADD KEY `jenis_tanaman_id` (`jenis_tanaman_id`),
  ADD KEY `jenis_tanah_id` (`jenis_tanah_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lahan`
--
ALTER TABLE `lahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `lahan`
--
ALTER TABLE `lahan`
  ADD CONSTRAINT `fk_lahan_daerah1` FOREIGN KEY (`daerah_id`) REFERENCES `daerah` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lahan_jenis_tanaman1` FOREIGN KEY (`jenis_tanaman_id`) REFERENCES `jenis_tanaman` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lahan_petani1` FOREIGN KEY (`petani_id`) REFERENCES `petani` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
