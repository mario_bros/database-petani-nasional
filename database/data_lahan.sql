-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 13, 2016 at 10:24 AM
-- Server version: 5.5.49
-- PHP Version: 5.5.35-1+donate.sury.org~precise+4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_petani`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_lahan`
--

CREATE TABLE IF NOT EXISTS `data_lahan` (
  `petani_id` bigint(20) NOT NULL,
  `alamat` varchar(11) NOT NULL,
  `luas` varchar(4) NOT NULL,
  `subsektor_id` bit(1) NOT NULL,
  `jenis_tanaman_id` int(11) NOT NULL,
  `jenis_lahan_id` int(11) NOT NULL,
  `daerah_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_lahan`
--

INSERT INTO `data_lahan` (`petani_id`, `alamat`, `luas`, `subsektor_id`, `jenis_tanaman_id`, `jenis_lahan_id`, `daerah_id`) VALUES
(131, 'PAINGAN', '1', b'1', 1, 2, 9),
(134, 'PAINGAN', '0,5', b'1', 2, 2, 9),
(56, 'PAINGAN', '0,4', b'1', 2, 2, 9),
(57, 'PAINGAN', '1', b'1', 1, 2, 9),
(58, 'PAINGAN', '0,7', b'1', 1, 2, 9),
(59, 'PAINGAN', '0,85', b'1', 1, 2, 9),
(60, 'PAINGAN', '0,5', b'1', 1, 2, 9),
(61, 'PAINGAN', '0,4', b'1', 1, 2, 9),
(62, 'PAINGAN', '0,75', b'1', 1, 2, 9),
(63, 'PAINGAN', '0,7', b'1', 1, 2, 9),
(64, 'PAINGAN', '0,8', b'1', 1, 2, 9),
(65, 'PAINGAN', '0,5', b'1', 1, 2, 9),
(66, 'PAINGAN', '0,65', b'1', 3, 1, 9),
(67, 'PAINGAN', '0,6', b'1', 3, 1, 9),
(68, 'PAINGAN', '0,7', b'1', 2, 1, 9),
(69, 'PAINGAN', '1,1', b'1', 2, 1, 9),
(70, 'PAINGAN', '0,5', b'1', 1, 1, 9),
(71, 'PAINGAN', '0,4', b'1', 1, 2, 9),
(72, 'PAINGAN', '0,6', b'1', 1, 2, 9),
(73, 'PAINGAN', '0,7', b'1', 1, 2, 9),
(74, 'PAINGAN', '0,8', b'1', 1, 2, 9),
(75, 'JENGGRIK', '1,5', b'1', 1, 2, 9),
(76, 'JENGGRIK', '0,9', b'1', 1, 2, 9),
(77, 'JENGGRIK', '0,85', b'1', 1, 2, 9),
(78, 'JENGGRIK', '0,45', b'1', 1, 2, 9),
(79, 'JENGGRIK', '0,75', b'1', 1, 2, 9),
(80, 'JENGGRIK', '0,3', b'1', 1, 1, 9),
(81, 'JENGGRIK', '1,1', b'1', 1, 1, 9),
(82, 'JENGGRIK', '0,85', b'1', 1, 1, 9),
(83, 'JENGGRIK', '0,45', b'1', 1, 1, 9),
(84, 'JENGGRIK', '0,5', b'1', 1, 1, 9),
(85, 'JENGGRIK', '0,65', b'1', 2, 1, 9),
(86, 'JENGGRIK', '0,9', b'1', 2, 1, 9),
(87, 'JENGGRIK', '0,85', b'1', 2, 2, 9),
(88, 'JENGGRIK', '0,45', b'1', 2, 2, 9),
(89, 'JENGGRIK', '0,4', b'1', 2, 2, 9),
(90, 'JENGGRIK', '0,65', b'1', 1, 2, 9),
(91, 'JENGGRIK', '1', b'1', 1, 2, 9),
(92, 'JENGGRIK', '0,85', b'1', 1, 2, 9),
(93, 'JENGGRIK', '0,45', b'1', 3, 2, 9),
(94, 'JENGGRIK', '0,6', b'1', 3, 2, 9),
(95, 'JENGGRIK', '0,65', b'1', 3, 2, 9),
(96, 'JENGGRIK', '0,9', b'1', 1, 2, 9),
(97, 'JENGGRIK', '0,85', b'1', 1, 2, 9),
(98, 'JENGGRIK', '0,45', b'1', 1, 2, 9),
(99, 'JENGGRIK', '0,75', b'1', 1, 2, 9),
(100, 'MOJOBAYI', '1,75', b'1', 1, 2, 9),
(101, 'MOJOBAYI', '0,8', b'1', 1, 2, 9),
(102, 'MOJOBAYI', '0,9', b'1', 1, 2, 9),
(103, 'MOJOBAYI', '0,4', b'1', 1, 2, 9),
(104, 'MOJOBAYI', '0,5', b'1', 1, 2, 9),
(105, 'MOJOBAYI', '0,7', b'1', 2, 2, 9),
(106, 'MOJOBAYI', '0,65', b'1', 2, 2, 9),
(107, 'MOJOBAYI', '0,55', b'1', 2, 2, 9),
(108, 'MOJOBAYI', '0,8', b'1', 2, 2, 9),
(109, 'MOJOBAYI', '0,65', b'1', 2, 2, 9),
(110, 'MOJOBAYI', '0,4', b'1', 2, 2, 9),
(111, 'MOJOBAYI', '1', b'1', 2, 2, 9),
(112, 'MOJOBAYI', '0,7', b'1', 1, 2, 9),
(113, 'MOJOBAYI', '0,65', b'1', 1, 2, 9),
(114, 'MOJOBAYI', '0,55', b'1', 1, 2, 9),
(115, 'MOJOBAYI', '0,8', b'1', 1, 1, 9),
(116, 'MOJOBAYI', '0,9', b'1', 1, 1, 9),
(117, 'MOJOBAYI', '0,4', b'1', 1, 1, 9),
(118, 'MOJOBAYI', '0,65', b'1', 1, 1, 9),
(119, 'MOJOBAYI', '0,7', b'1', 1, 1, 9),
(120, 'MOJOBAYI', '0,65', b'1', 1, 1, 9),
(121, 'MOJOBAYI', '0,35', b'1', 3, 1, 9),
(122, 'MOJOBAYI', '0,8', b'1', 3, 1, 9),
(123, 'MOJOBAYI', '0,9', b'1', 3, 1, 9),
(124, 'MOJOBAYI', '0,65', b'1', 3, 1, 9),
(125, 'MOJOBAYI', '1', b'1', 3, 1, 9),
(126, 'MOJOBAYI', '0,7', b'1', 3, 2, 9),
(132, 'MOJOBAYI', '0,65', b'1', 1, 2, 9),
(128, 'MOJOBAYI', '0,55', b'1', 1, 2, 9),
(129, 'MOJOBAYI', '0,8', b'1', 1, 2, 9),
(130, 'MOJOBAYI', '2,5', b'1', 1, 2, 9),
(0, 'DAWANGAN', '0,5', b'1', 1, 2, 9),
(0, 'DAWANGAN', '0,7', b'1', 1, 2, 9),
(0, 'DAWANGAN', '0,65', b'1', 1, 2, 9),
(0, 'DAWANGAN', '0,55', b'1', 1, 2, 9),
(0, 'DAWANGAN', '0,5', b'1', 1, 2, 9),
(0, 'DAWANGAN', '0,7', b'1', 2, 2, 9),
(0, 'DAWANGAN', '0,65', b'1', 2, 2, 9),
(0, 'DAWANGAN', '0,55', b'1', 2, 2, 9),
(0, 'BANJAR', '0,5', b'1', 2, 2, 9),
(0, 'BANJAR', '0,7', b'1', 2, 2, 9),
(0, 'BANJAR', '0,65', b'1', 2, 2, 9),
(0, 'BANJAR', '0,55', b'1', 1, 2, 9),
(0, 'BANJAR', '0,5', b'1', 1, 2, 9),
(0, 'BANJAR', '0,7', b'1', 1, 2, 9),
(0, 'BANJAR', '0,65', b'1', 1, 2, 9),
(0, 'BANJAR', '0,55', b'1', 1, 2, 9),
(0, 'JONGGRANGAN', '0,5', b'1', 1, 2, 9),
(0, 'JONGGRANGAN', '0,7', b'1', 1, 2, 9),
(0, 'JONGGRANGAN', '0,65', b'1', 3, 2, 9),
(0, 'JONGGRANGAN', '0,55', b'1', 3, 2, 9),
(0, 'JONGGRANGAN', '0,5', b'1', 1, 2, 9),
(0, 'JONGGRANGAN', '0,7', b'1', 1, 2, 9),
(0, 'JONGGRANGAN', '0,65', b'1', 1, 2, 9);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
