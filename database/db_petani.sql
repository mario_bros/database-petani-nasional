-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 11, 2016 at 06:03 PM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_petani`
--

-- --------------------------------------------------------

--
-- Table structure for table `aprroves`
--

CREATE TABLE `aprroves` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `note` text,
  `rekap_rdkk_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bps`
--

CREATE TABLE `bps` (
  `id` int(11) NOT NULL,
  `no_ektp` varchar(20) NOT NULL,
  `no_telp` varchar(255) DEFAULT NULL,
  `daerah_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bumdes`
--

CREATE TABLE `bumdes` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `lat` float NOT NULL,
  `lon` float NOT NULL,
  `daerah_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bumdes`
--

INSERT INTO `bumdes` (`id`, `nama`, `alamat`, `lat`, `lon`, `daerah_id`) VALUES
(3, 'Sukses Mandiri', 'Sidoharjo, Sragen, Jawa Tengah', 100, 100, 11);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Pupuk'),
(2, 'Benih'),
(3, 'Pestisida');

-- --------------------------------------------------------

--
-- Table structure for table `daerah`
--

CREATE TABLE `daerah` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `lat` float NOT NULL,
  `lon` float NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `jenis_daerah_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daerah`
--

INSERT INTO `daerah` (`id`, `nama`, `lat`, `lon`, `note`, `parent_id`, `jenis_daerah_id`, `lft`, `rght`) VALUES
(2, 'Purworejo', -7.71375, 110.009, '', 0, 3, NULL, NULL),
(3, 'Lampung', 106.676, 234235, 'test', 0, 4, NULL, NULL),
(4, 'Lampung selatan', 106.676, 10234200, '', 3, 3, NULL, NULL),
(6, 'Kelurahan lampung selatan', 106.676, 103242000, '', 5, 2, NULL, NULL),
(8, 'JAWA TENGAH', -7.13683, 110.145, NULL, 0, 4, NULL, NULL),
(9, 'SRAGEN', -7.42894, 111.011, NULL, 8, 3, NULL, NULL),
(10, 'SIDOHARJO', -7.42354, 110.968, NULL, 9, 2, NULL, NULL),
(11, 'PURWOSUMAN', -7.44996, 110.952, NULL, 10, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ektp`
--

CREATE TABLE `ektp` (
  `id` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `rt_rw` varchar(50) NOT NULL,
  `pekerjaan` varchar(150) NOT NULL,
  `kewarganegaraan` varchar(150) NOT NULL,
  `status_perkawinan` varchar(150) NOT NULL,
  `gol_darah` varchar(150) NOT NULL,
  `tanggal_terbit` date NOT NULL,
  `daerah_id` int(11) NOT NULL,
  `tanggal_berakhir` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ektp`
--

INSERT INTO `ektp` (`id`, `nama`, `alamat`, `tanggal_lahir`, `rt_rw`, `pekerjaan`, `kewarganegaraan`, `status_perkawinan`, `gol_darah`, `tanggal_terbit`, `daerah_id`, `tanggal_berakhir`) VALUES
('3302444433330001', 'NGATNO BINTORO', 'PAINGAN', '1950-01-01', '01/010', 'Petani', 'WNI', 'Kawin', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330002', 'SUWARTO', 'PAINGAN', '1960-01-02', '01/010', 'Petani', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330003', 'JOKO LAMUR', 'PAINGAN', '1970-01-03', '01/010', 'Petani', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330004', 'MULYONO', 'PAINGAN', '1980-01-04', '01/010', 'Petani', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330005', 'SANGAJI', 'PAINGAN', '1951-01-05', '01/010', 'Petani', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330006', 'SULAMIN', 'PAINGAN', '1961-01-06', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330007', 'NGATIMIN', 'PAINGAN', '1971-01-07', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330008', 'PONIMAN', 'PAINGAN', '1981-01-08', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330009', 'SUTARJO', 'PAINGAN', '1952-01-09', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330010', 'SUTARMIN', 'PAINGAN', '1962-01-10', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330011', 'SUTARMAN', 'PAINGAN', '1972-01-11', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330012', 'NGATIJO', 'PAINGAN', '1982-01-12', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330013', 'BAMBANG', 'PAINGAN', '1953-01-13', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330014', 'NUGROHO', 'PAINGAN', '1963-01-14', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330015', 'TONI ADI', 'PAINGAN', '1973-01-15', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330016', 'BUNALI', 'PAINGAN', '1983-01-16', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330017', 'PAIMIN', 'PAINGAN', '1954-01-17', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'B', '2016-01-12', 10, '2016-12-12'),
('3302444433330018', 'MARYONO', 'PAINGAN', '1964-01-18', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330019', 'BADRUN', 'PAINGAN', '1974-01-19', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330020', 'MARKUN', 'PAINGAN', '1984-01-20', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'BELUM KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330021', 'AHMADI', 'PAINGAN', '1955-01-21', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'AB', '2016-01-12', 10, '2016-12-12'),
('3302444433330022', 'KARNO', 'JENGGRIK', '1965-01-22', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330023', 'MARMAN', 'JENGGRIK', '1975-01-23', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330024', 'SUGIARTO', 'JENGGRIK', '1985-01-24', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330025', 'SARJONO', 'JENGGRIK', '1956-01-25', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330026', 'MARJO', 'JENGGRIK', '1966-01-26', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330027', 'NGADI', 'JENGGRIK', '1976-01-27', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330028', 'EKO RAHARJO', 'JENGGRIK', '1986-01-28', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'BELUM KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330029', 'JATI SUKMONO', 'JENGGRIK', '1957-01-29', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330030', 'BAGIO', 'JENGGRIK', '1967-01-30', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330031', 'AMIN SUYITNO', 'JENGGRIK', '1977-01-31', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'B', '2016-01-12', 10, '2016-12-12'),
('3302444433330032', 'SALMAN', 'JENGGRIK', '1987-02-01', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'B', '2016-01-12', 10, '2016-12-12'),
('3302444433330033', 'EDI PITOYO', 'JENGGRIK', '1958-02-02', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'AB', '2016-01-12', 10, '2016-12-12'),
('3302444433330034', 'MADIKIN', 'JENGGRIK', '1968-02-03', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'AB', '2016-01-12', 10, '2016-12-12'),
('3302444433330035', 'AMAN MARYANTO', 'JENGGRIK', '1978-02-04', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330036', 'BASUKI', 'JENGGRIK', '1988-02-05', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330037', 'LATIF', 'JENGGRIK', '1959-02-06', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330038', 'MASKUR', 'JENGGRIK', '1969-02-07', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330039', 'HERI CAHYONO', 'JENGGRIK', '1979-02-08', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330040', 'BAGUS SUTOPO', 'JENGGRIK', '1989-02-09', '01/010', 'WIRASWASTA', 'WNI', 'BELUM KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330041', 'BAHTIAR', 'JENGGRIK', '1951-02-10', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330042', 'LILIK', 'JENGGRIK', '1961-02-11', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330043', 'AGUS TEDI', 'JENGGRIK', '1971-02-12', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330044', 'BRAHMANTO', 'JENGGRIK', '2081-02-13', '01/010', 'WIRASWASTA', 'WNI', 'BELUM KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330045', 'MASHURI', 'JENGGRIK', '1952-02-14', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330046', 'MARKJUKI', 'JENGGRIK', '1962-02-15', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330047', 'SUTARTO', 'MOJOBAYI', '1972-02-16', '01/010', 'Petani', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330048', 'MARSUDI', 'MOJOBAYI', '1982-02-17', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'B', '2016-01-12', 10, '2016-12-12'),
('3302444433330049', 'MARYANTO', 'MOJOBAYI', '1953-02-18', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'B', '2016-01-12', 10, '2016-12-12'),
('3302444433330050', 'RUDI SALAM', 'MOJOBAYI', '1963-02-19', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'B', '2016-01-12', 10, '2016-12-12'),
('3302444433330051', 'SUDIRO', 'MOJOBAYI', '1973-02-20', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'AB', '2016-01-12', 10, '2016-12-12'),
('3302444433330052', 'BASMAN', 'MOJOBAYI', '1983-02-21', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'AB', '2016-01-12', 10, '2016-12-12'),
('3302444433330053', 'SUMARYONO', 'MOJOBAYI', '1954-02-22', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330054', 'PARTONO', 'MOJOBAYI', '1964-02-23', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330055', 'HASAN', 'MOJOBAYI', '1974-02-24', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330056', 'ROBERT?', 'MOJOBAYI', '1984-02-25', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330057', 'BUDI PRAPTONO', 'MOJOBAYI', '1955-02-26', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330058', 'PUGUH', 'MOJOBAYI', '1965-02-27', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330059', 'WARSITO', 'MOJOBAYI', '1975-02-28', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330060', 'SUHARTO', 'MOJOBAYI', '1985-03-01', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330061', 'SUKARNO', 'MOJOBAYI', '1956-03-02', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330062', 'GUNAWAN', 'MOJOBAYI', '1966-03-03', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330063', 'DENNA LISA SILALAHI', 'MOJOBAYI', '1976-03-04', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330064', 'EMSURIANI', 'MOJOBAYI', '1977-03-05', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330065', 'Y E L F I  SH', 'MOJOBAYI', '1957-03-06', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330066', 'RIRI INDRIANI SH,SPN', 'MOJOBAYI', '1967-03-07', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330067', 'YUYUN', 'MOJOBAYI', '1977-03-08', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330068', 'MILSON JONI', 'MOJOBAYI', '1987-03-09', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'BELUM KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330069', 'WELLY BATHESTA', 'MOJOBAYI', '1958-03-10', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330070', 'WIWID FERYANTO RAHARDIAN', 'MOJOBAYI', '1968-03-11', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330071', 'HAERMAN', 'MOJOBAYI', '1978-03-12', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'B', '2016-01-12', 10, '2016-12-12'),
('3302444433330072', 'MARGONO', 'MOJOBAYI', '1988-03-13', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'B', '2016-01-12', 10, '2016-12-12'),
('3302444433330073', 'AKADINA', 'MOJOBAYI', '1959-03-14', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330074', 'STEFAN', 'MOJOBAYI', '1969-03-15', '01/010', 'Petani', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330075', 'MAHMUD', 'MOJOBAYI', '1979-03-16', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330076', 'OKTOVERI', 'MOJOBAYI', '1989-03-17', '01/010', 'WIRASWASTA', 'WNI', 'BELUM KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330077', 'H. SUMARNO', 'MOJOBAYI', '1951-03-18', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330078', 'SAT LANTAS 2', 'DAWANGAN', '1961-03-19', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330079', 'UMMI AZIZAH', 'DAWANGAN', '1971-03-20', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330080', 'AISYAH MEI SAPUTRI', 'DAWANGAN', '1981-03-21', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330081', 'RUDI SAKYAKIRTI', 'DAWANGAN', '1952-03-22', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330082', 'HERYANTO', 'DAWANGAN', '1962-03-23', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330083', 'WISNU PUTRA ALMUTTAQIN', 'DAWANGAN', '1972-03-24', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330084', 'FU TSHOI PHIONG', 'DAWANGAN', '1982-03-25', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330085', 'ROHMAN', 'DAWANGAN', '1953-03-26', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330086', 'ACHSAN SAJRI', 'BANJAR', '1963-03-27', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'B', '2016-01-12', 10, '2016-12-12'),
('3302444433330087', 'SHOLIKUL HUDA', 'BANJAR', '1973-03-28', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'B', '2016-01-12', 10, '2016-12-12'),
('3302444433330088', 'POI TJAN', 'BANJAR', '1983-03-29', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'AB', '2016-01-12', 10, '2016-12-12'),
('3302444433330089', 'JOHNI TAN', 'BANJAR', '1954-03-30', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'AB', '2016-01-12', 10, '2016-12-12'),
('3302444433330090', 'ANDI FAISAL', 'BANJAR', '1964-03-31', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'AB', '2016-01-12', 10, '2016-12-12'),
('3302444433330091', 'AGUS LEONARD PRASETYO', 'BANJAR', '1974-04-01', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330092', 'YULIATI', 'BANJAR', '1984-04-02', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330093', 'KAHARUDDIN', 'BANJAR', '1955-04-03', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330094', 'SURIYATI', 'JONGGRANGAN', '1965-04-04', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330095', 'IBNU RUSHD', 'JONGGRANGAN', '1975-04-05', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'AB', '2016-01-12', 10, '2016-12-12'),
('3302444433330096', 'JIMMY WIJAYA', 'JONGGRANGAN', '1985-04-06', '01/010', 'WIRASWASTA', 'WNI', 'BELUM KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330097', 'MULYADI', 'JONGGRANGAN', '1956-04-07', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330098', 'R ISKANDAR', 'JONGGRANGAN', '1966-04-08', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330099', 'IRIANTO RUSMAN', 'JONGGRANGAN', '1976-04-09', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330100', 'ANTI DINILLAH', 'JONGGRANGAN', '1986-04-10', '01/010', 'WIRASWASTA', 'WNI', 'BELUM KAWIN', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433332222', 'BUDI RAHARJO', 'MOJOBAYI', '2016-06-01', '01/010', 'PENYULUH', 'WNI', 'KAWIN', 'O', '2016-06-01', 11, '2016-06-30');

-- --------------------------------------------------------

--
-- Table structure for table `gapoktan`
--

CREATE TABLE `gapoktan` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `daerah_id` int(11) NOT NULL,
  `ketua_gapoktan` int(11) DEFAULT NULL,
  `bumdes_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gapoktan`
--

INSERT INTO `gapoktan` (`id`, `nama`, `alamat`, `daerah_id`, `ketua_gapoktan`, `bumdes_id`) VALUES
(1, 'Sejahtera', 'Paingan', 11, 130, 3);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `desc` varchar(45) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `harga` decimal(9,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `category_id`, `desc`, `active`, `harga`) VALUES
(1, 'Urea', 1, 'Pupuk Urea', 1, 1800.00),
(2, 'NPK', 1, 'Pupuk NPK', 1, 2000.00),
(3, 'SP-36', 1, 'Pupuk SP-36', 1, 1200.00),
(4, 'Insektisida', 3, 'Insektisida', 1, 31550.00),
(5, 'ZA', 1, 'Pupuk ZA', 1, 1100.00),
(6, 'Padi', 2, 'Benih', 1, 1000.00),
(7, 'Herbisida', 3, 'Herbisida', 1, 30000.00),
(8, 'Jagung', 2, 'Jagung', 1, 700.00),
(9, 'Kakao', 2, 'Kakao', 1, 900.00),
(12, 'Organik', 1, 'Organik', 1, 500.00);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_daerah`
--

CREATE TABLE `jenis_daerah` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_daerah`
--

INSERT INTO `jenis_daerah` (`id`, `nama`) VALUES
(1, 'Kelurahan'),
(2, 'Kecamatan'),
(3, 'Kabupaten'),
(4, 'Provinsi');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_lahan`
--

CREATE TABLE `jenis_lahan` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jenis_lahan`
--

INSERT INTO `jenis_lahan` (`id`, `nama`) VALUES
(1, 'garap'),
(2, 'milik');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_tanah`
--

CREATE TABLE `jenis_tanah` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_tanah`
--

INSERT INTO `jenis_tanah` (`id`, `nama`) VALUES
(1, 'gembur');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_tanaman`
--

CREATE TABLE `jenis_tanaman` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_tanaman`
--

INSERT INTO `jenis_tanaman` (`id`, `nama`) VALUES
(1, 'Padi'),
(2, 'Jagung'),
(3, 'Kakao');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_user`
--

CREATE TABLE `jenis_user` (
  `id` int(11) NOT NULL,
  `jenis` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_user`
--

INSERT INTO `jenis_user` (`id`, `jenis`) VALUES
(1, 'Admin'),
(2, 'BPS'),
(3, 'Pegawai Bumdes');

-- --------------------------------------------------------

--
-- Table structure for table `kepala_daerah`
--

CREATE TABLE `kepala_daerah` (
  `id` int(11) NOT NULL,
  `nomor_induk` varchar(255) NOT NULL,
  `nama` int(11) NOT NULL,
  `jabatan` int(11) NOT NULL,
  `alamat` int(11) NOT NULL,
  `daerah_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `komoditas`
--

CREATE TABLE `komoditas` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `keterangan` int(11) DEFAULT NULL,
  `subsektor_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komoditas`
--

INSERT INTO `komoditas` (`id`, `nama`, `keterangan`, `subsektor_id`) VALUES
(1, 'Padi', 1, 1),
(2, 'Jagung', 2, 1),
(3, 'Kakao', NULL, 1),
(4, 'Buah Apel', NULL, 2),
(5, 'Ayam Kampung', NULL, 4),
(6, 'Kebun Sayur', NULL, 3),
(7, 'Ikan Mas', NULL, 5);

-- --------------------------------------------------------

--
-- Table structure for table `lahan`
--

CREATE TABLE `lahan` (
  `id` int(11) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `daerah_id` int(11) NOT NULL,
  `luas` decimal(8,2) NOT NULL,
  `jenis_tanaman_id` int(50) NOT NULL,
  `petani_id` int(11) NOT NULL,
  `jenis_tanah_id` int(11) NOT NULL,
  `komoditas_id` int(11) NOT NULL DEFAULT '0',
  `jenis_lahan_id` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lahan`
--

INSERT INTO `lahan` (`id`, `alamat`, `daerah_id`, `luas`, `jenis_tanaman_id`, `petani_id`, `jenis_tanah_id`, `komoditas_id`, `jenis_lahan_id`) VALUES
(24, 'Paingan', 11, 0.30, 1, 56, 0, 0, 1),
(26, 'Dusun Sukomulya Purwosuman', 11, 10.00, 1, 131, 0, 0, 2),
(27, 'MOJOBAYI', 2, 100.00, 2, 74, 1, 0, 1),
(31, 'jl Paingan', 6, 10.00, 1, 100, 0, 0, 2),
(32, 'Paiman', 11, 0.20, 1, 132, 0, 0, 1),
(33, 'PAINGAN', 11, 1.00, 1, 57, 0, 0, 2),
(34, 'Paingan', 11, 0.80, 2, 58, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` varchar(150) NOT NULL,
  `date` datetime NOT NULL,
  `poktan_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `user_id`, `action`, `date`, `poktan_id`) VALUES
(1, 8, 'ApprovalRequest', '2016-05-12 16:41:53', 4),
(2, 8, 'ApprovalRequest', '2016-05-12 16:47:33', 4),
(3, 8, 'ApprovalRequest', '2016-05-12 16:48:53', 4),
(4, 8, 'ApprovalRequest', '2016-05-12 16:53:51', 4),
(6, 8, 'ApprovalRequest', '2016-05-13 03:58:27', 4),
(7, 8, 'ApprovalRequest', '2016-05-13 03:59:16', 4),
(8, 8, 'ApprovalRequest', '2016-05-13 04:14:42', 4),
(9, 8, 'ApprovalRequest', '2016-05-13 09:15:15', 4),
(17, 12, 'ApprovalRequest', '2016-05-20 09:00:18', 6),
(19, 12, 'ApprovalRequest', '2016-05-20 09:03:36', 6),
(21, 12, 'ApprovalRequest', '2016-05-20 09:23:40', 6),
(22, 8, 'ApprovalRequest', '2016-06-07 06:03:27', 6),
(23, 8, 'ApprovalRequest', '2016-06-07 06:06:31', 6),
(24, 8, 'ApprovalRequest', '2016-06-07 06:07:27', 6),
(25, 8, 'ApprovalRequest', '2016-06-07 06:07:42', 6),
(26, 8, 'ApprovalRequest', '2016-06-07 06:08:56', 6),
(27, 14, 'Approved', '2016-06-07 06:52:42', 6),
(28, 8, 'ApprovalRequest', '2016-06-07 06:57:26', 6),
(29, 14, 'Approved', '2016-06-07 06:58:20', 6),
(30, 14, 'Approved', '2016-06-07 07:00:31', 6),
(31, 8, 'ApprovalRequest', '2016-06-08 06:41:12', 4);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `flag` decimal(3,0) DEFAULT NULL,
  `date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `send_to_mail` decimal(8,2) DEFAULT NULL,
  `notes` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `title`, `url`, `flag`, `date`, `user_id`, `send_to_mail`, `notes`) VALUES
(9, 'Pembatalan Persetujuan Poktan', '/poktan/view/4', 0, '2016-05-18 03:53:25', 8, 0.00, 'oeu'),
(10, 'Pembatalan Persetujuan Poktan', '/poktan/view/4', 0, '2016-05-18 03:53:43', 8, 0.00, 'oeu'),
(16, 'Pembatalan Persetujuan Poktan', '/poktan/view/6', 0, '2016-05-20 09:01:38', 12, 0.00, 'kurang luas lahannya'),
(19, 'Poktan Disetujui', '/poktan/view/6', 0, '2016-05-20 09:24:03', 12, 0.00, NULL),
(20, 'Pengajuan Persetujuan Poktan', '/poktan/view/6', 1, '2016-06-07 06:08:56', 14, 0.00, NULL),
(21, 'Poktan Disetujui', '/poktan/view/6', 0, '2016-06-07 06:52:42', 3, 0.00, NULL),
(22, 'Pengajuan Persetujuan Poktan', '/poktan/view/6', 1, '2016-06-07 06:57:26', 14, 0.00, NULL),
(24, 'Poktan Disetujui', '/poktan/view/6', 0, '2016-06-07 07:00:31', 8, 0.00, NULL),
(25, 'Pengajuan Persetujuan Poktan', '/poktan/view/4', 1, '2016-06-08 06:41:12', 14, 0.00, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `penyuluh`
--

CREATE TABLE `penyuluh` (
  `id` int(11) NOT NULL,
  `no_ektp` varchar(20) NOT NULL,
  `photo` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penyuluh`
--

INSERT INTO `penyuluh` (`id`, `no_ektp`, `photo`) VALUES
(2, '3302444433332222', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `petani`
--

CREATE TABLE `petani` (
  `id` int(11) NOT NULL,
  `no_tlp` varchar(255) NOT NULL,
  `no_ektp` varchar(20) NOT NULL,
  `poktan_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petani`
--

INSERT INTO `petani` (`id`, `no_tlp`, `no_ektp`, `poktan_id`) VALUES
(56, '81399887763', '3302444433330003', 4),
(57, '81399887764', '3302444433330004', 4),
(58, '81399887765', '3302444433330005', 4),
(59, '81399887766', '3302444433330006', 4),
(60, '81399887767', '3302444433330007', 4),
(61, '81399887768', '3302444433330008', 4),
(62, '81399887769', '3302444433330009', 4),
(63, '81399887770', '3302444433330010', 4),
(64, '81399887771', '3302444433330011', 4),
(65, '81399887772', '3302444433330012', 4),
(66, '81399887773', '3302444433330013', 4),
(67, '81399887774', '3302444433330014', 4),
(68, '81399887775', '3302444433330015', 4),
(69, '81399887776', '3302444433330016', 4),
(70, '81399887777', '3302444433330017', 4),
(71, '81399887778', '3302444433330018', 4),
(72, '81399887779', '3302444433330019', 4),
(73, '81399887780', '3302444433330020', 4),
(74, '81399887781', '3302444433330021', 4),
(75, '81399887782', '3302444433330022', 7),
(76, '81399887783', '3302444433330023', 7),
(77, '81399887784', '3302444433330024', 7),
(78, '81399887785', '3302444433330025', 7),
(79, '81399887786', '3302444433330026', 7),
(80, '81399887787', '3302444433330027', 7),
(81, '81399887788', '3302444433330028', 7),
(82, '81399887789', '3302444433330029', 7),
(83, '81399887790', '3302444433330030', 7),
(84, '81399887791', '3302444433330031', 7),
(85, '81399887792', '3302444433330032', 7),
(86, '81399887793', '3302444433330033', 7),
(87, '81399887794', '3302444433330034', 7),
(88, '81399887795', '3302444433330035', 7),
(89, '81399887796', '3302444433330036', 7),
(90, '81399887797', '3302444433330037', 7),
(91, '81399887798', '3302444433330038', 7),
(92, '81399887799', '3302444433330039', 7),
(93, '81399887100', '3302444433330040', 7),
(94, '81399887101', '3302444433330041', 7),
(95, '81399887102', '3302444433330042', 7),
(96, '81399887103', '3302444433330043', 7),
(97, '81399887104', '3302444433330044', 7),
(98, '81399887105', '3302444433330045', 7),
(99, '81399887106', '3302444433330046', 7),
(100, '81399887107', '3302444433330047', 6),
(101, '81399887108', '3302444433330048', 6),
(102, '81399887109', '3302444433330049', 6),
(103, '81399887110', '3302444433330050', 6),
(104, '81399887111', '3302444433330051', 6),
(105, '81399887112', '3302444433330052', 6),
(106, '81399887113', '3302444433330053', 6),
(107, '81399887114', '3302444433330054', 6),
(108, '81399887115', '3302444433330055', 6),
(109, '81399887116', '3302444433330056', 6),
(110, '81399887117', '3302444433330057', 6),
(111, '81399887118', '3302444433330058', 6),
(112, '81399887119', '3302444433330059', 6),
(113, '81399887120', '3302444433330060', 6),
(114, '81399887121', '3302444433330061', 6),
(115, '81399887122', '3302444433330062', 6),
(116, '81399887123', '3302444433330063', 6),
(117, '81399887124', '3302444433330064', 6),
(118, '81399887125', '3302444433330065', 6),
(119, '81399887126', '3302444433330066', 6),
(120, '81399887127', '3302444433330067', 6),
(121, '81399887128', '3302444433330068', 6),
(122, '81399887129', '3302444433330069', 6),
(123, '81399887130', '3302444433330070', 6),
(124, '81399887131', '3302444433330071', 6),
(125, '81399887132', '3302444433330072', 6),
(126, '81399887133', '3302444433330073', 6),
(128, '81399887135', '3302444433330075', 6),
(129, '81399887136', '3302444433330076', 6),
(130, '81399887137', '3302444433330077', 6),
(131, '081399887761', '3302444433330001', 4),
(132, '082124217369', '3302444433330074', 4);

-- --------------------------------------------------------

--
-- Table structure for table `phinxlog`
--

CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phinxlog`
--

INSERT INTO `phinxlog` (`version`, `migration_name`, `start_time`, `end_time`) VALUES
(20160518095505, NULL, '2016-05-20 11:54:37', '2016-05-18 03:01:14'),
(20160518095844, NULL, '2016-05-20 11:54:24', '2016-05-18 03:01:14'),
(20160520112246, NULL, '2016-05-20 04:54:50', '2016-05-20 04:54:50'),
(20160523081437, NULL, '2016-05-23 02:33:22', '2016-05-23 02:33:23'),
(20160523092541, NULL, '2016-05-23 02:36:44', '2016-05-23 02:36:45'),
(20160523124844, NULL, '2016-05-23 06:00:45', '2016-05-23 06:00:46'),
(20160525021531, 'AlterPrices', '2016-05-25 19:14:41', '2016-05-25 19:14:41'),
(20160529144154, 'CreateTransaksiRekapRdkk', '2016-05-29 07:57:44', '2016-05-29 07:57:44'),
(20160530041007, 'AlterRdkkMasaTanam', '2016-05-29 21:21:11', '2016-05-29 21:21:11'),
(20160603090129, 'AlterGapoktan', '2016-06-03 02:03:09', '2016-06-03 02:03:09'),
(20160603092326, 'AlterGapoktanAddRegionsField', '2016-06-03 02:29:54', '2016-06-03 02:29:55'),
(20160606045642, 'AlterUsersBumdesID', '2016-06-05 21:58:17', '2016-06-05 21:58:17'),
(20160606065731, 'DeleteEktpContent', '2016-06-06 00:06:42', '2016-06-06 00:06:42'),
(20160606070730, 'DeleteUsersAttempt1', '2016-06-06 00:08:01', '2016-06-06 00:08:01'),
(20160606072219, 'CreateDataDaerah', '2016-06-06 00:49:42', '2016-06-06 00:49:42'),
(20160607103529, 'CreateJenisLahan', '2016-06-07 03:45:32', '2016-06-07 03:45:32'),
(20160608052653, 'AlterJenisTanah', '2016-06-07 22:28:40', '2016-06-07 22:28:41'),
(20160608104036, NULL, '2016-06-08 03:47:08', '2016-06-08 03:47:08'),
(20160609031800, NULL, '2016-06-08 20:22:56', '2016-06-08 20:22:57'),
(20160609035654, NULL, '2016-06-08 21:06:17', '2016-06-08 21:06:17'),
(20160609050016, NULL, '2016-06-08 22:09:53', '2016-06-08 22:09:53'),
(20160610082151, NULL, '2016-06-10 02:02:17', '2016-06-10 02:02:18'),
(20160610090640, NULL, '2016-06-10 02:07:18', '2016-06-10 02:07:18'),
(20160611085326, NULL, '2016-06-11 01:54:30', '2016-06-11 01:54:30');

-- --------------------------------------------------------

--
-- Table structure for table `poktan`
--

CREATE TABLE `poktan` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `daerah_id` int(11) NOT NULL,
  `gapoktan_id` int(11) NOT NULL,
  `lat` float NOT NULL,
  `lon` float NOT NULL,
  `aprroved` int(11) DEFAULT NULL,
  `ketua_poktan` int(11) DEFAULT NULL,
  `penyuluh_id` int(11) NOT NULL,
  `approval_request` int(11) NOT NULL,
  `bumdes_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poktan`
--

INSERT INTO `poktan` (`id`, `nama`, `alamat`, `daerah_id`, `gapoktan_id`, `lat`, `lon`, `aprroved`, `ketua_poktan`, `penyuluh_id`, `approval_request`, `bumdes_id`) VALUES
(4, 'Tani Rahayu', 'Paingan', 11, 1, 106.676, 1098, 0, 56, 2, 1, 3),
(6, 'NGUDI REJEKI', 'Mojobayi', 11, 1, 106.676, 1098, 1, 57, 2, 0, 3),
(7, 'KRIDA TANI', 'JENGGRIK', 11, 1, 106.676, 1098, NULL, 75, 2, 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `rdkk`
--

CREATE TABLE `rdkk` (
  `id` int(11) NOT NULL,
  `petani_id` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `masa_tanam` int(11) NOT NULL,
  `lahan_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rdkk`
--

INSERT INTO `rdkk` (`id`, `petani_id`, `tanggal`, `masa_tanam`, `lahan_id`) VALUES
(10, 56, '2016-06-08', 1, 0),
(13, 57, '2016-06-09', 1, 0),
(14, 58, '2016-06-09', 1, 24),
(15, 58, '2016-06-09', 2, 26),
(16, 58, '2016-06-09', 3, 27),
(17, 57, '2016-06-09', 2, 0),
(18, 56, '2016-06-09', 2, 0),
(19, 56, '2016-06-09', 3, 0),
(20, 58, '2016-06-11', 2, 24);

-- --------------------------------------------------------

--
-- Table structure for table `rekap_log`
--

CREATE TABLE `rekap_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` varchar(150) NOT NULL,
  `date` datetime NOT NULL,
  `rekap_rdkk_id` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rekap_rdkk`
--

CREATE TABLE `rekap_rdkk` (
  `id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `approved_date` date NOT NULL,
  `daerah_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subsektor`
--

CREATE TABLE `subsektor` (
  `id` int(11) NOT NULL,
  `nama` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subsektor`
--

INSERT INTO `subsektor` (`id`, `nama`) VALUES
(1, 'Tanaman Pangan'),
(2, 'Holtikultura'),
(3, 'Perkebunan'),
(4, 'Peternak'),
(5, 'Petambak');

-- --------------------------------------------------------

--
-- Table structure for table `tr_rdkk`
--

CREATE TABLE `tr_rdkk` (
  `id` int(11) NOT NULL,
  `rdkk_id` int(11) NOT NULL,
  `lahan_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `masa_tanam` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_rdkk`
--

INSERT INTO `tr_rdkk` (`id`, `rdkk_id`, `lahan_id`, `item_id`, `jumlah`, `masa_tanam`) VALUES
(1, 10, 24, 1, 101, NULL),
(2, 10, 24, 2, 10, NULL),
(3, 10, 24, 3, 100, NULL),
(4, 10, 24, 4, 100, NULL),
(5, 10, 24, 5, 100, NULL),
(6, 10, 24, 6, 100, NULL),
(7, 10, 24, 7, 100, NULL),
(8, 10, 24, 8, 100, NULL),
(9, 10, 24, 9, 100, NULL),
(10, 10, 24, 12, 100, NULL),
(11, 13, 33, 1, 300, NULL),
(12, 13, 33, 2, 100, NULL),
(13, 13, 33, 3, 200, NULL),
(14, 13, 33, 4, 100, NULL),
(15, 13, 33, 5, 100, NULL),
(16, 13, 33, 6, 7, NULL),
(17, 13, 33, 7, 0, NULL),
(18, 13, 33, 8, 0, NULL),
(19, 13, 33, 9, 0, NULL),
(20, 13, 33, 12, 0, NULL),
(21, 14, 34, 5, 200, NULL),
(22, 14, 34, 1, 0, NULL),
(23, 14, 34, 12, 600, NULL),
(24, 14, 34, 2, 0, NULL),
(25, 14, 34, 3, 0, NULL),
(26, 14, 34, 9, 0, NULL),
(27, 14, 34, 6, 0, NULL),
(28, 14, 34, 8, 3, NULL),
(29, 14, 34, 4, 30, NULL),
(30, 14, 34, 7, 0, NULL),
(31, 15, 34, 5, 10, NULL),
(32, 15, 34, 1, 20, NULL),
(33, 15, 34, 12, 30, NULL),
(34, 15, 34, 2, 40, NULL),
(35, 15, 34, 3, 50, NULL),
(36, 15, 34, 9, 60, NULL),
(37, 15, 34, 6, 70, NULL),
(38, 15, 34, 8, 80, NULL),
(39, 15, 34, 4, 90, NULL),
(40, 15, 34, 7, 100, NULL),
(41, 16, 34, 5, 5, NULL),
(42, 16, 34, 1, 5, NULL),
(43, 16, 34, 12, 5, NULL),
(44, 16, 34, 2, 5, NULL),
(45, 16, 34, 3, 5, NULL),
(46, 16, 34, 9, 5, NULL),
(47, 16, 34, 6, 5, NULL),
(48, 16, 34, 8, 5, NULL),
(49, 16, 34, 4, 5, NULL),
(50, 16, 34, 7, 0, NULL),
(51, 17, 33, 5, 3, NULL),
(52, 17, 33, 1, 3, NULL),
(53, 17, 33, 12, 3, NULL),
(54, 17, 33, 2, 3, NULL),
(55, 17, 33, 3, 3, NULL),
(56, 17, 33, 9, 3, NULL),
(57, 17, 33, 6, 3, NULL),
(58, 17, 33, 8, 3, NULL),
(59, 17, 33, 4, 3, NULL),
(60, 17, 33, 7, 3, NULL),
(61, 18, 24, 5, 3, NULL),
(62, 18, 24, 1, 4, NULL),
(63, 18, 24, 12, 4, NULL),
(64, 18, 24, 2, 4, NULL),
(65, 18, 24, 3, 4, NULL),
(66, 18, 24, 9, 4, NULL),
(67, 18, 24, 6, 4, NULL),
(68, 18, 24, 8, 4, NULL),
(69, 18, 24, 4, 4, NULL),
(70, 18, 24, 7, 4, NULL),
(71, 19, 24, 5, 0, NULL),
(72, 19, 24, 1, 250, NULL),
(73, 19, 24, 12, 0, NULL),
(74, 19, 24, 2, 0, NULL),
(75, 19, 24, 3, 0, NULL),
(76, 19, 24, 9, 0, NULL),
(77, 19, 24, 6, 6, NULL),
(78, 19, 24, 8, 0, NULL),
(79, 19, 24, 4, 40, NULL),
(80, 19, 24, 7, 0, NULL),
(81, 20, 34, 1, 4, NULL),
(82, 20, 34, 2, 4, NULL),
(83, 20, 34, 3, 4, NULL),
(84, 20, 34, 5, 4, NULL),
(85, 20, 34, 12, 4, NULL),
(86, 20, 34, 6, 4, NULL),
(87, 20, 34, 8, 4, NULL),
(88, 20, 34, 9, 4, NULL),
(89, 20, 34, 4, 4, NULL),
(90, 20, 34, 7, 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tr_rdkk_copy`
--

CREATE TABLE `tr_rdkk_copy` (
  `id` int(11) NOT NULL,
  `rdkk_id` int(11) NOT NULL,
  `lahan_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `masa_tanam` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_rdkk_copy`
--

INSERT INTO `tr_rdkk_copy` (`id`, `rdkk_id`, `lahan_id`, `item_id`, `jumlah`, `masa_tanam`) VALUES
(15, 8, 22, 2, 1, 1),
(16, 8, 22, 1, 2, 2),
(17, 8, 22, 4, 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tr_rekaprdkk`
--

CREATE TABLE `tr_rekaprdkk` (
  `id` int(11) NOT NULL,
  `rdkk_id` int(11) NOT NULL,
  `rekap_rdkk_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `jenis_user_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `petani_id` int(11) DEFAULT NULL,
  `penyuluh_id` int(11) DEFAULT NULL,
  `kepala_daerah_id` int(11) DEFAULT NULL,
  `bumdes_id` int(11) DEFAULT NULL,
  `daerah_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_date`, `jenis_user_id`, `active`, `petani_id`, `penyuluh_id`, `kepala_daerah_id`, `bumdes_id`, `daerah_id`) VALUES
(1, 'admin', '$2y$10$0rZzJoL3jaP5ygqo91ZlROGoiiyyQptQtTr4gScLWRWa3bfQe.oCS', '2016-05-04 02:35:00', 1, 1, 0, NULL, NULL, NULL, 0),
(2, 'User1', '$2y$10$QuKErBly3cFT9sLTpYzbCOJJLPoiTXnCJ6jWS4gzIF7P3vEF.rsEa', '2016-05-04 03:24:00', 2, 1, 0, NULL, NULL, NULL, 0),
(3, 'ucup', '$2y$10$nEw/sizM7HmYUKYu8TwYqOCd6DcEnrQrcjGJEAl9fHb53qsYz.MX.', '2016-05-04 07:24:00', 1, 1, 0, NULL, NULL, NULL, 0),
(5, 'mimin', '$2y$10$XLouBkXDfD3DvTQMv6ehG.BuICzZws4KMsvRLvyyopwxICN42Gpta', '2016-05-04 11:53:00', 1, 0, 0, NULL, NULL, NULL, 0),
(6, 'dennis', '$2y$10$nHTVGYtYUJlRjZtJbh1KXejlVznjTKm4LK3EfNdtG.D2d387wyvt6', '2016-05-04 11:53:00', 1, 0, 0, NULL, NULL, NULL, 0),
(8, 'joko', '$2y$10$ZkovoXgaQgPszpAOHEa.nOcrDaJC/gYY9SucOuv0dr/LuAVYkwASq', '2016-05-12 10:28:00', 3, 1, 45, NULL, NULL, 3, 0),
(12, 'petani', '$2y$10$0zWCx.NYQmXUdrwBBwXGT.4cqIu5CDwCUxbNkh68xWdHOpY2e0DPO', '2016-05-20 04:44:00', 3, 1, 53, NULL, NULL, 0, 0),
(14, 'penyuluh', '$2y$10$b9epi9zXITwvqX5YAuC7muIuArvW.WQRjDHbpr4ZDxWZGFiNhnLKC', '2016-06-07 05:32:00', 2, 1, NULL, 2, NULL, NULL, 8);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aprroves`
--
ALTER TABLE `aprroves`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_aprroves_rekap_rdkk1_idx` (`user_id`),
  ADD KEY `rekap_rdkk_id` (`rekap_rdkk_id`);

--
-- Indexes for table `bps`
--
ALTER TABLE `bps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bumdes`
--
ALTER TABLE `bumdes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXFK_bumdes_poktan` (`daerah_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daerah`
--
ALTER TABLE `daerah`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXFK_daerah_daerah` (`parent_id`),
  ADD KEY `IXFK_daerah_jenis_daerah` (`jenis_daerah_id`);

--
-- Indexes for table `ektp`
--
ALTER TABLE `ektp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXFK_ektp_daerah` (`daerah_id`);

--
-- Indexes for table `gapoktan`
--
ALTER TABLE `gapoktan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXFK_gapoktan_daerah` (`daerah_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_items_category_idx` (`category_id`);

--
-- Indexes for table `jenis_daerah`
--
ALTER TABLE `jenis_daerah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_lahan`
--
ALTER TABLE `jenis_lahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_tanah`
--
ALTER TABLE `jenis_tanah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_tanaman`
--
ALTER TABLE `jenis_tanaman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_user`
--
ALTER TABLE `jenis_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kepala_daerah`
--
ALTER TABLE `kepala_daerah`
  ADD PRIMARY KEY (`id`),
  ADD KEY `daerah_id` (`daerah_id`);

--
-- Indexes for table `komoditas`
--
ALTER TABLE `komoditas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lahan`
--
ALTER TABLE `lahan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXFK_lahan_petani` (`petani_id`),
  ADD KEY `IXFK_lahan_daerah` (`daerah_id`),
  ADD KEY `jenis_tanaman_id` (`jenis_tanaman_id`),
  ADD KEY `jenis_tanah_id` (`jenis_tanah_id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXFK_log_rdkk` (`poktan_id`),
  ADD KEY `IXFK_log_user` (`user_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXFK_notifications_user` (`user_id`);

--
-- Indexes for table `penyuluh`
--
ALTER TABLE `penyuluh`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXFK_penyuluh_ektp` (`no_ektp`),
  ADD KEY `no_ektp` (`no_ektp`);

--
-- Indexes for table `petani`
--
ALTER TABLE `petani`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXFK_petani_ektp` (`no_ektp`),
  ADD KEY `poktan_id` (`poktan_id`);

--
-- Indexes for table `phinxlog`
--
ALTER TABLE `phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `poktan`
--
ALTER TABLE `poktan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXFK_poktan_gapoktan` (`gapoktan_id`),
  ADD KEY `IXFK_poktan_daerah` (`daerah_id`),
  ADD KEY `ketua_poktan` (`ketua_poktan`),
  ADD KEY `penyuluh_id` (`penyuluh_id`);

--
-- Indexes for table `rdkk`
--
ALTER TABLE `rdkk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rekap_log`
--
ALTER TABLE `rekap_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXFK_log_rdkk` (`rekap_rdkk_id`),
  ADD KEY `IXFK_log_user` (`user_id`);

--
-- Indexes for table `rekap_rdkk`
--
ALTER TABLE `rekap_rdkk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `daerah_id` (`daerah_id`);

--
-- Indexes for table `subsektor`
--
ALTER TABLE `subsektor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_rdkk`
--
ALTER TABLE `tr_rdkk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXid_RDKK` (`rdkk_id`),
  ADD KEY `fk_tr_rdkk_items1_idx` (`item_id`),
  ADD KEY `fk_tr_rdkk_lahan1_idx` (`lahan_id`);

--
-- Indexes for table `tr_rdkk_copy`
--
ALTER TABLE `tr_rdkk_copy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXid_RDKK` (`rdkk_id`),
  ADD KEY `fk_tr_rdkk_items1_idx` (`item_id`),
  ADD KEY `fk_tr_rdkk_lahan1_idx` (`lahan_id`);

--
-- Indexes for table `tr_rekaprdkk`
--
ALTER TABLE `tr_rekaprdkk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXid_jenis_user` (`jenis_user_id`),
  ADD KEY `petani_id` (`petani_id`),
  ADD KEY `petani_id_2` (`petani_id`),
  ADD KEY `kepala_daerah_id` (`kepala_daerah_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aprroves`
--
ALTER TABLE `aprroves`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bps`
--
ALTER TABLE `bps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bumdes`
--
ALTER TABLE `bumdes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `daerah`
--
ALTER TABLE `daerah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `gapoktan`
--
ALTER TABLE `gapoktan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `jenis_daerah`
--
ALTER TABLE `jenis_daerah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `jenis_lahan`
--
ALTER TABLE `jenis_lahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `jenis_tanah`
--
ALTER TABLE `jenis_tanah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `jenis_tanaman`
--
ALTER TABLE `jenis_tanaman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `jenis_user`
--
ALTER TABLE `jenis_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kepala_daerah`
--
ALTER TABLE `kepala_daerah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `komoditas`
--
ALTER TABLE `komoditas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `lahan`
--
ALTER TABLE `lahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `penyuluh`
--
ALTER TABLE `penyuluh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `petani`
--
ALTER TABLE `petani`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;
--
-- AUTO_INCREMENT for table `poktan`
--
ALTER TABLE `poktan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `rdkk`
--
ALTER TABLE `rdkk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `rekap_log`
--
ALTER TABLE `rekap_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rekap_rdkk`
--
ALTER TABLE `rekap_rdkk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subsektor`
--
ALTER TABLE `subsektor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tr_rdkk`
--
ALTER TABLE `tr_rdkk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT for table `tr_rdkk_copy`
--
ALTER TABLE `tr_rdkk_copy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tr_rekaprdkk`
--
ALTER TABLE `tr_rekaprdkk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `aprroves`
--
ALTER TABLE `aprroves`
  ADD CONSTRAINT `aprroves_ibfk_1` FOREIGN KEY (`rekap_rdkk_id`) REFERENCES `rekap_rdkk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_aprroves_rekap_rdkk1` FOREIGN KEY (`user_id`) REFERENCES `rekap_rdkk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bumdes`
--
ALTER TABLE `bumdes`
  ADD CONSTRAINT `FK_bumdes_daerah` FOREIGN KEY (`daerah_id`) REFERENCES `daerah` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `daerah`
--
ALTER TABLE `daerah`
  ADD CONSTRAINT `FK_daerah_jenis_daerah` FOREIGN KEY (`jenis_daerah_id`) REFERENCES `jenis_daerah` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ektp`
--
ALTER TABLE `ektp`
  ADD CONSTRAINT `FK_ektp_daerah` FOREIGN KEY (`daerah_id`) REFERENCES `daerah` (`id`);

--
-- Constraints for table `gapoktan`
--
ALTER TABLE `gapoktan`
  ADD CONSTRAINT `FK_gapoktan_daerah` FOREIGN KEY (`daerah_id`) REFERENCES `daerah` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `fk_items_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kepala_daerah`
--
ALTER TABLE `kepala_daerah`
  ADD CONSTRAINT `kepala_daerah_ibfk_1` FOREIGN KEY (`daerah_id`) REFERENCES `daerah` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lahan`
--
ALTER TABLE `lahan`
  ADD CONSTRAINT `fk_lahan_daerah1` FOREIGN KEY (`daerah_id`) REFERENCES `daerah` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lahan_jenis_tanaman1` FOREIGN KEY (`jenis_tanaman_id`) REFERENCES `jenis_tanaman` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lahan_petani1` FOREIGN KEY (`petani_id`) REFERENCES `petani` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `FK_log_poktan02` FOREIGN KEY (`poktan_id`) REFERENCES `poktan` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_log_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `FK_notifications_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `penyuluh`
--
ALTER TABLE `penyuluh`
  ADD CONSTRAINT `penyuluh_ibfk_1` FOREIGN KEY (`no_ektp`) REFERENCES `ektp` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `petani`
--
ALTER TABLE `petani`
  ADD CONSTRAINT `petani_ibfk_1` FOREIGN KEY (`poktan_id`) REFERENCES `poktan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `petani_ibfk_2` FOREIGN KEY (`no_ektp`) REFERENCES `ektp` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rekap_log`
--
ALTER TABLE `rekap_log`
  ADD CONSTRAINT `rekap_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rekap_log_ibfk_2` FOREIGN KEY (`rekap_rdkk_id`) REFERENCES `rekap_rdkk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rekap_rdkk`
--
ALTER TABLE `rekap_rdkk`
  ADD CONSTRAINT `rekap_rdkk_ibfk_1` FOREIGN KEY (`daerah_id`) REFERENCES `daerah` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tr_rdkk`
--
ALTER TABLE `tr_rdkk`
  ADD CONSTRAINT `fk_tr_rdkk_items1` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tr_rdkk_lahan1` FOREIGN KEY (`lahan_id`) REFERENCES `lahan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `id_RDKK` FOREIGN KEY (`rdkk_id`) REFERENCES `rdkk` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `jenis_user_id_ibfk_1` FOREIGN KEY (`jenis_user_id`) REFERENCES `jenis_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
