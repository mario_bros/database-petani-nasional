-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 20, 2016 at 05:15 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `petani_v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `aprroves`
--

CREATE TABLE IF NOT EXISTS `aprroves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `note` text,
  `rekap_rdkk_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_aprroves_rekap_rdkk1_idx` (`user_id`),
  KEY `rekap_rdkk_id` (`rekap_rdkk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE IF NOT EXISTS `assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_lahan` int(11) NOT NULL,
  `nama_asset` varchar(255) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_status` (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `bps`
--

CREATE TABLE IF NOT EXISTS `bps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_ktp` varchar(20) NOT NULL,
  `no_tlp` varchar(255) DEFAULT NULL,
  `daerah_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `bps`
--

INSERT INTO `bps` (`id`, `no_ktp`, `no_tlp`, `daerah_id`) VALUES
(1, '3302444433330003', '08121139010', 9);

-- --------------------------------------------------------

--
-- Table structure for table `bumdes`
--

CREATE TABLE IF NOT EXISTS `bumdes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `lat` float NOT NULL,
  `lon` float NOT NULL,
  `daerah_id` int(11) NOT NULL,
  `bps_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IXFK_bumdes_poktan` (`daerah_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `bumdes`
--

INSERT INTO `bumdes` (`id`, `nama`, `alamat`, `lat`, `lon`, `daerah_id`, `bps_id`) VALUES
(3, 'Sukses Mandiri', 'Sidoharjo, Sragen, Jawa Tengah', 100, 100, 11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Pupuk'),
(2, 'Benih'),
(3, 'Pestisida');

-- --------------------------------------------------------

--
-- Table structure for table `daerah`
--

CREATE TABLE IF NOT EXISTS `daerah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `lat` float NOT NULL,
  `lon` float NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `jenis_daerah_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IXFK_daerah_daerah` (`parent_id`),
  KEY `IXFK_daerah_jenis_daerah` (`jenis_daerah_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=113 ;

--
-- Dumping data for table `daerah`
--

INSERT INTO `daerah` (`id`, `nama`, `lat`, `lon`, `note`, `parent_id`, `jenis_daerah_id`, `lft`, `rght`) VALUES
(2, 'Purworejo', -7.71375, 110.009, '', 0, 3, NULL, NULL),
(3, 'Lampung', 106.676, 234235, 'test', 0, 4, NULL, NULL),
(4, 'Lampung selatan', 106.676, 10234200, '', 3, 3, NULL, NULL),
(6, 'Kelurahan lampung selatan', 106.676, 103242000, '', 5, 2, NULL, NULL),
(8, 'JAWA TENGAH', -7.13683, 110.145, NULL, 0, 4, NULL, NULL),
(9, 'SRAGEN', -7.42894, 111.011, NULL, 8, 3, NULL, NULL),
(10, 'SIDOHARJO', -7.42354, 110.968, NULL, 9, 2, NULL, NULL),
(11, 'PURWOSUMAN', -7.44996, 110.952, NULL, 10, 1, NULL, NULL),
(12, 'Daerah Istimewa Yogyakarta', -7.81145, 110.372, NULL, 0, 4, NULL, NULL),
(13, 'Sleman', -7.71514, 110.335, NULL, 12, 3, NULL, NULL),
(14, 'Gamping', -7.79412, 110.323, '', 13, 2, NULL, NULL),
(15, 'Godean', -7.77429, 110.302, '', 13, 2, NULL, NULL),
(16, 'Moyudan', -7.77702, 110.252, '', 13, 2, NULL, NULL),
(17, 'Minggir', -7.73122, 110.248, '', 13, 2, NULL, NULL),
(18, 'Seyegan', -7.73825, 110.298, '', 13, 2, NULL, NULL),
(19, 'Mlati', -7.73323, 110.338, '', 13, 2, NULL, NULL),
(20, 'Depok', -7.76221, 110.394, '', 13, 2, NULL, NULL),
(21, 'Berbah', -7.80455, 110.444, '', 13, 2, NULL, NULL),
(22, 'Prambanan', -7.79723, 110.498, '', 13, 2, NULL, NULL),
(23, 'Kalasan', -7.75792, 110.469, '', 13, 2, NULL, NULL),
(24, 'Ngemplak', -7.70846, 110.449, '', 13, 2, NULL, NULL),
(25, 'Ngalik', -7.71574, 110.406, '', 13, 2, NULL, NULL),
(26, 'Sleman', -7.69036, 110.354, '', 13, 2, NULL, NULL),
(27, 'Tempel', -7.66708, 110.316, '', 13, 2, NULL, NULL),
(28, 'Turi', -7.65043, 110.369, '', 13, 2, NULL, NULL),
(29, 'Pakem', -7.65993, 110.408, '', 13, 2, NULL, NULL),
(30, 'Cangkringan', -7.66436, 110.457, '', 13, 2, NULL, NULL),
(31, 'Balecatur', -7.71514, 110.335, '', 14, 1, NULL, NULL),
(32, 'Ambarketawang', -7.71514, 110.335, '', 14, 1, NULL, NULL),
(33, 'Banyuraden', -7.71514, 110.335, '', 14, 1, NULL, NULL),
(34, 'Nogotirto', -7.71514, 110.335, '', 14, 1, NULL, NULL),
(35, 'Trihanggo', -7.71514, 110.335, '', 14, 1, NULL, NULL),
(36, 'Sidorejo', -7.71514, 110.335, '', 15, 1, NULL, NULL),
(37, 'Sidoluhur', -7.71514, 110.335, '', 15, 1, NULL, NULL),
(38, 'Sidomulyo', -7.71514, 110.335, '', 15, 1, NULL, NULL),
(39, 'Sidoagung', -7.71514, 110.335, '', 15, 1, NULL, NULL),
(40, 'Sidokarto', -7.71514, 110.335, '', 15, 1, NULL, NULL),
(41, 'Sidoarum', -7.71514, 110.335, '', 15, 1, NULL, NULL),
(42, 'Sidomoyo', -7.71514, 110.335, '', 15, 1, NULL, NULL),
(43, 'Sumberahayu', -7.71514, 110.335, '', 16, 1, NULL, NULL),
(44, 'Sumbersari', -771514, 110335, '', 16, 1, NULL, NULL),
(45, 'Sumberagung', -771514, 110335, '', 16, 1, NULL, NULL),
(46, 'Sumberarum', -771514, 110335, '', 16, 1, NULL, NULL),
(47, 'Sendangarum', -771514, 110335, '', 17, 1, NULL, NULL),
(48, 'Sendangmulyo', -771514, 110335, '', 17, 1, NULL, NULL),
(49, 'Sendangagung', -771514, 110335, '', 17, 1, NULL, NULL),
(50, 'Sendangsari', -771514, 110335, '', 17, 1, NULL, NULL),
(51, 'Sendangrejo', -771514, 110335, '', 17, 1, NULL, NULL),
(52, 'Margoluwih', -771514, 110335, '', 18, 1, NULL, NULL),
(53, 'Margodadi', -771514, 110335, '', 18, 1, NULL, NULL),
(54, 'Margomulyo', -771514, 110335, '', 18, 1, NULL, NULL),
(55, 'Margokaton', -771514, 110335, '', 18, 1, NULL, NULL),
(56, 'Margoagung', -771514, 110335, '', 18, 1, NULL, NULL),
(57, 'Sinduadi', -771514, 110335, '', 19, 1, NULL, NULL),
(58, 'Sendangadi', -771514, 110335, '', 19, 1, NULL, NULL),
(59, 'Tlogoadi', -771514, 110335, '', 19, 1, NULL, NULL),
(60, 'Tirtoadi', -771514, 110335, '', 19, 1, NULL, NULL),
(61, 'Sumberadi', -771514, 110335, '', 19, 1, NULL, NULL),
(62, 'Caturtunggal', -771514, 110335, '', 20, 1, NULL, NULL),
(63, 'Maguwuharjo', -771514, 110335, '', 20, 1, NULL, NULL),
(64, 'Condongcatur', -771514, 110335, '', 20, 1, NULL, NULL),
(65, 'Sumberharjo', -771514, 110335, '', 22, 1, NULL, NULL),
(66, 'Wukirharjo', -771514, 110335, '', 22, 1, NULL, NULL),
(67, 'Gayamharjo', -771514, 110335, '', 22, 1, NULL, NULL),
(68, 'Sambirejo', -771514, 110335, '', 22, 1, NULL, NULL),
(69, 'Madurejo', -771514, 110335, '', 22, 1, NULL, NULL),
(70, 'Bokoharjo', -771514, 110335, '', 22, 1, NULL, NULL),
(71, 'Purwomartani', -771514, 110335, '', 23, 1, NULL, NULL),
(72, 'Tirtomartani', -771514, 110335, '', 23, 1, NULL, NULL),
(73, 'Tamanmartani', -771514, 110335, '', 23, 1, NULL, NULL),
(74, 'Selomartani', -771514, 110335, '', 23, 1, NULL, NULL),
(75, 'Sindumartani', -771514, 110335, '', 24, 1, NULL, NULL),
(76, 'Bimomartani', -771514, 110335, '', 24, 1, NULL, NULL),
(77, 'Widodomartani', -771514, 110335, '', 24, 1, NULL, NULL),
(78, 'Wedomartani', -771514, 110335, '', 24, 1, NULL, NULL),
(79, 'Umbulmartani', -771514, 110335, '', 24, 1, NULL, NULL),
(80, 'Sariharjo', -771514, 110335, '', 25, 1, NULL, NULL),
(81, 'Minomartani', -771514, 110335, '', 25, 1, NULL, NULL),
(82, 'Sinduharjo', -771514, 110335, '', 25, 1, NULL, NULL),
(83, 'Sukoharjo', -771514, 110335, '', 25, 1, NULL, NULL),
(84, 'Sardonoharjo', -771514, 110335, '', 25, 1, NULL, NULL),
(85, 'Donoharjo', -771514, 110335, '', 25, 1, NULL, NULL),
(86, 'Caturharjo', -771514, 110335, '', 26, 1, NULL, NULL),
(87, 'Triharjo', -771514, 110335, '', 26, 1, NULL, NULL),
(88, 'Tridadi', -771514, 110335, '', 26, 1, NULL, NULL),
(89, 'Pandowoharjo', -771514, 110335, '', 26, 1, NULL, NULL),
(90, 'Trimulyo', -771514, 110335, '', 26, 1, NULL, NULL),
(91, 'Banyurejo', -771514, 110335, '', 27, 1, NULL, NULL),
(92, 'Tambakrejo', -771514, 110335, '', 27, 1, NULL, NULL),
(93, 'Sumberrejo', -771514, 110335, '', 27, 1, NULL, NULL),
(94, 'Pondokrejo', -771514, 110335, '', 27, 1, NULL, NULL),
(95, 'Mororejo', -771514, 110335, '', 27, 1, NULL, NULL),
(96, 'Margorejo', -771514, 110335, '', 27, 1, NULL, NULL),
(97, 'Lumbungrejo', -771514, 110335, '', 27, 1, NULL, NULL),
(98, 'Merdikorejo', -771514, 110335, '', 27, 1, NULL, NULL),
(99, 'Bangunkerto', -771514, 110335, '', 28, 1, NULL, NULL),
(100, 'Donokerto', -771514, 110335, '', 28, 1, NULL, NULL),
(101, 'Girikerto', -771514, 110335, '', 28, 1, NULL, NULL),
(102, 'Wonokerto', -771514, 110335, '', 28, 1, NULL, NULL),
(103, 'Purwobinangun', -771514, 110335, '', 29, 1, NULL, NULL),
(104, 'Candibinangun', -771514, 110335, '', 29, 1, NULL, NULL),
(105, 'Harjobinangun', -771514, 110335, '', 29, 1, NULL, NULL),
(106, 'Pakembinangun', -771514, 110335, '', 29, 1, NULL, NULL),
(107, 'Hargobinangun', -771514, 110335, '', 29, 1, NULL, NULL),
(108, 'Argomulyo', -771514, 110335, '', 30, 1, NULL, NULL),
(109, 'Wukisari', -771514, 110335, '', 30, 1, NULL, NULL),
(110, 'Glagaharjo', -771514, 110335, '', 30, 1, NULL, NULL),
(111, 'Kepulharjo', -771514, 110335, '', 30, 1, NULL, NULL),
(112, 'Umbulharjo', -771514, 110335, '', 30, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_lahan`
--

CREATE TABLE IF NOT EXISTS `data_lahan` (
  `petani_id` bigint(20) NOT NULL DEFAULT '0',
  `alamat` varchar(11) NOT NULL,
  `luas` float NOT NULL,
  `subsektor_id` bit(1) NOT NULL,
  `komoditas_id` int(11) NOT NULL,
  `jenis_lahan_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_lahan`
--

INSERT INTO `data_lahan` (`petani_id`, `alamat`, `luas`, `subsektor_id`, `komoditas_id`, `jenis_lahan_id`) VALUES
(131, 'PAINGAN', 1, b'1', 1, 2),
(134, 'PAINGAN', 0.5, b'1', 2, 2),
(56, 'PAINGAN', 0.4, b'1', 2, 2),
(57, 'PAINGAN', 1, b'1', 1, 2),
(58, 'PAINGAN', 0.7, b'1', 1, 2),
(59, 'PAINGAN', 0.85, b'1', 1, 2),
(60, 'PAINGAN', 0.5, b'1', 1, 2),
(61, 'PAINGAN', 0.4, b'1', 1, 2),
(62, 'PAINGAN', 0.75, b'1', 1, 2),
(63, 'PAINGAN', 0.7, b'1', 1, 2),
(64, 'PAINGAN', 0.8, b'1', 1, 2),
(65, 'PAINGAN', 0.5, b'1', 1, 2),
(66, 'PAINGAN', 0.65, b'1', 3, 1),
(67, 'PAINGAN', 0.6, b'1', 3, 1),
(68, 'PAINGAN', 0.7, b'1', 2, 1),
(69, 'PAINGAN', 1.1, b'1', 2, 1),
(70, 'PAINGAN', 0.5, b'1', 1, 1),
(71, 'PAINGAN', 0.4, b'1', 1, 2),
(72, 'PAINGAN', 0.6, b'1', 1, 2),
(73, 'PAINGAN', 0.7, b'1', 1, 2),
(74, 'PAINGAN', 0.8, b'1', 1, 2),
(75, 'JENGGRIK', 1.5, b'1', 1, 2),
(76, 'JENGGRIK', 0.9, b'1', 1, 2),
(77, 'JENGGRIK', 0.85, b'1', 1, 2),
(78, 'JENGGRIK', 0.45, b'1', 1, 2),
(79, 'JENGGRIK', 0.75, b'1', 1, 2),
(80, 'JENGGRIK', 0.3, b'1', 1, 1),
(81, 'JENGGRIK', 1.1, b'1', 1, 1),
(82, 'JENGGRIK', 0.85, b'1', 1, 1),
(83, 'JENGGRIK', 0.45, b'1', 1, 1),
(84, 'JENGGRIK', 0.5, b'1', 1, 1),
(85, 'JENGGRIK', 0.65, b'1', 2, 1),
(86, 'JENGGRIK', 0.9, b'1', 2, 1),
(87, 'JENGGRIK', 0.85, b'1', 2, 2),
(88, 'JENGGRIK', 0.45, b'1', 2, 2),
(89, 'JENGGRIK', 0.4, b'1', 2, 2),
(90, 'JENGGRIK', 0.65, b'1', 1, 2),
(91, 'JENGGRIK', 1, b'1', 1, 2),
(92, 'JENGGRIK', 0.85, b'1', 1, 2),
(93, 'JENGGRIK', 0.45, b'1', 3, 2),
(94, 'JENGGRIK', 0.6, b'1', 3, 2),
(95, 'JENGGRIK', 0.65, b'1', 3, 2),
(96, 'JENGGRIK', 0.9, b'1', 1, 2),
(97, 'JENGGRIK', 0.85, b'1', 1, 2),
(98, 'JENGGRIK', 0.45, b'1', 1, 2),
(99, 'JENGGRIK', 0.75, b'1', 1, 2),
(100, 'MOJOBAYI', 1.75, b'1', 1, 2),
(101, 'MOJOBAYI', 0.8, b'1', 1, 2),
(102, 'MOJOBAYI', 0.9, b'1', 1, 2),
(103, 'MOJOBAYI', 0.4, b'1', 1, 2),
(104, 'MOJOBAYI', 0.5, b'1', 1, 2),
(105, 'MOJOBAYI', 0.7, b'1', 2, 2),
(106, 'MOJOBAYI', 0.65, b'1', 2, 2),
(107, 'MOJOBAYI', 0.55, b'1', 2, 2),
(108, 'MOJOBAYI', 0.8, b'1', 2, 2),
(109, 'MOJOBAYI', 0.65, b'1', 2, 2),
(110, 'MOJOBAYI', 0.4, b'1', 2, 2),
(111, 'MOJOBAYI', 1, b'1', 2, 2),
(112, 'MOJOBAYI', 0.7, b'1', 1, 2),
(113, 'MOJOBAYI', 0.65, b'1', 1, 2),
(114, 'MOJOBAYI', 0.55, b'1', 1, 2),
(115, 'MOJOBAYI', 0.8, b'1', 1, 1),
(116, 'MOJOBAYI', 0.9, b'1', 1, 1),
(117, 'MOJOBAYI', 0.4, b'1', 1, 1),
(118, 'MOJOBAYI', 0.65, b'1', 1, 1),
(119, 'MOJOBAYI', 0.7, b'1', 1, 1),
(120, 'MOJOBAYI', 0.65, b'1', 1, 1),
(121, 'MOJOBAYI', 0.35, b'1', 3, 1),
(122, 'MOJOBAYI', 0.8, b'1', 3, 1),
(123, 'MOJOBAYI', 0.9, b'1', 3, 1),
(124, 'MOJOBAYI', 0.65, b'1', 3, 1),
(125, 'MOJOBAYI', 1, b'1', 3, 1),
(126, 'MOJOBAYI', 0.7, b'1', 3, 2),
(132, 'MOJOBAYI', 0.65, b'1', 1, 2),
(128, 'MOJOBAYI', 0.55, b'1', 1, 2),
(129, 'MOJOBAYI', 0.8, b'1', 1, 2),
(130, 'MOJOBAYI', 2.5, b'1', 1, 2),
(0, 'DAWANGAN', 0.5, b'1', 1, 2),
(0, 'DAWANGAN', 0.7, b'1', 1, 2),
(0, 'DAWANGAN', 0.65, b'1', 1, 2),
(0, 'DAWANGAN', 0.55, b'1', 1, 2),
(0, 'DAWANGAN', 0.5, b'1', 1, 2),
(0, 'DAWANGAN', 0.7, b'1', 2, 2),
(0, 'DAWANGAN', 0.65, b'1', 2, 2),
(0, 'DAWANGAN', 0.55, b'1', 2, 2),
(0, 'BANJAR', 0.5, b'1', 2, 2),
(0, 'BANJAR', 0.7, b'1', 2, 2),
(0, 'BANJAR', 0.65, b'1', 2, 2),
(0, 'BANJAR', 0.55, b'1', 1, 2),
(0, 'BANJAR', 0.5, b'1', 1, 2),
(0, 'BANJAR', 0.7, b'1', 1, 2),
(0, 'BANJAR', 0.65, b'1', 1, 2),
(0, 'BANJAR', 0.55, b'1', 1, 2),
(0, 'JONGGRANGAN', 0.5, b'1', 1, 2),
(0, 'JONGGRANGAN', 0.7, b'1', 1, 2),
(0, 'JONGGRANGAN', 0.65, b'1', 3, 2),
(0, 'JONGGRANGAN', 0.55, b'1', 3, 2),
(0, 'JONGGRANGAN', 0.5, b'1', 1, 2),
(0, 'JONGGRANGAN', 0.7, b'1', 1, 2),
(0, 'JONGGRANGAN', 0.65, b'1', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ektp`
--

CREATE TABLE IF NOT EXISTS `ektp` (
  `id` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `rt_rw` varchar(50) NOT NULL,
  `pekerjaan` varchar(150) NOT NULL,
  `kewarganegaraan` varchar(150) NOT NULL,
  `status_perkawinan` varchar(150) NOT NULL,
  `gol_darah` varchar(150) NOT NULL,
  `tanggal_terbit` date NOT NULL,
  `daerah_id` int(11) NOT NULL,
  `tanggal_berakhir` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IXFK_ektp_daerah` (`daerah_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ektp`
--

INSERT INTO `ektp` (`id`, `nama`, `alamat`, `tanggal_lahir`, `rt_rw`, `pekerjaan`, `kewarganegaraan`, `status_perkawinan`, `gol_darah`, `tanggal_terbit`, `daerah_id`, `tanggal_berakhir`) VALUES
('3302444433330001', 'NGATNO BINTORO', 'PAINGAN', '1950-01-01', '01/010', 'Petani', 'WNI', 'Kawin', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330002', 'SUWARTO', 'PAINGAN', '1960-01-02', '01/010', 'Petani', 'WNI', 'Kawin', 'A', '2016-01-12', 10, '2016-12-12'),
('3302444433330003', 'JOKO LAMUR', 'PAINGAN', '1970-01-03', '01/010', 'Petani', 'WNI', 'Kawin', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330004', 'MULYONO', 'PAINGAN', '1980-01-04', '01/010', 'Petani', 'WNI', 'Kawin', 'O', '2016-01-12', 10, '2016-12-12'),
('3302444433330005', 'SANGAJI', 'PAINGAN', '1951-01-05', '01/010', 'Petani', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330006', 'SULAMIN', 'PAINGAN', '1961-01-06', '01/010', 'Petani', 'WNI', 'Kawin', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330007', 'NGATIMIN', 'PAINGAN', '1971-01-07', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330008', 'PONIMAN', 'PAINGAN', '1981-01-08', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330009', 'SUTARJO', 'PAINGAN', '1952-01-09', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330010', 'SUTARMIN', 'PAINGAN', '1962-01-10', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330011', 'SUTARMAN', 'PAINGAN', '1972-01-11', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330012', 'NGATIJO', 'PAINGAN', '1982-01-12', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330013', 'BAMBANG', 'PAINGAN', '1953-01-13', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330014', 'NUGROHO', 'PAINGAN', '1963-01-14', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330015', 'TONI ADI', 'PAINGAN', '1973-01-15', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330016', 'BUNALI', 'PAINGAN', '1983-01-16', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330017', 'PAIMIN', 'PAINGAN', '1954-01-17', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'B', '2016-01-12', 11, '2016-12-12'),
('3302444433330018', 'MARYONO', 'PAINGAN', '1964-01-18', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330019', 'BADRUN', 'PAINGAN', '1974-01-19', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330020', 'MARKUN', 'PAINGAN', '1984-01-20', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'BELUM KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330021', 'AHMADI', 'PAINGAN', '1955-01-21', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'AB', '2016-01-12', 11, '2016-12-12'),
('3302444433330022', 'KARNO', 'JENGGRIK', '1965-01-22', '01/010', 'Petani', 'WNI', 'KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330023', 'MARMAN', 'JENGGRIK', '1975-01-23', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330024', 'SUGIARTO', 'JENGGRIK', '1985-01-24', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330025', 'SARJONO', 'JENGGRIK', '1956-01-25', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330026', 'MARJO', 'JENGGRIK', '1966-01-26', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330027', 'NGADI', 'JENGGRIK', '1976-01-27', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330028', 'EKO RAHARJO', 'JENGGRIK', '1986-01-28', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'BELUM KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330029', 'JATI SUKMONO', 'JENGGRIK', '1957-01-29', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330030', 'BAGIO', 'JENGGRIK', '1967-01-30', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330031', 'AMIN SUYITNO', 'JENGGRIK', '1977-01-31', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'B', '2016-01-12', 11, '2016-12-12'),
('3302444433330032', 'SALMAN', 'JENGGRIK', '1987-02-01', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'B', '2016-01-12', 11, '2016-12-12'),
('3302444433330033', 'EDI PITOYO', 'JENGGRIK', '1958-02-02', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'AB', '2016-01-12', 11, '2016-12-12'),
('3302444433330034', 'MADIKIN', 'JENGGRIK', '1968-02-03', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'AB', '2016-01-12', 11, '2016-12-12'),
('3302444433330035', 'AMAN MARYANTO', 'JENGGRIK', '1978-02-04', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330036', 'BASUKI', 'JENGGRIK', '1988-02-05', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330037', 'LATIF', 'JENGGRIK', '1959-02-06', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330038', 'MASKUR', 'JENGGRIK', '1969-02-07', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330039', 'HERI CAHYONO', 'JENGGRIK', '1979-02-08', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330040', 'BAGUS SUTOPO', 'JENGGRIK', '1989-02-09', '01/010', 'WIRASWASTA', 'WNI', 'BELUM KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330041', 'BAHTIAR', 'JENGGRIK', '1951-02-10', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330042', 'LILIK', 'JENGGRIK', '1961-02-11', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330043', 'AGUS TEDI', 'JENGGRIK', '1971-02-12', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330044', 'BRAHMANTO', 'JENGGRIK', '2081-02-13', '01/010', 'WIRASWASTA', 'WNI', 'BELUM KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330045', 'MASHURI', 'JENGGRIK', '1952-02-14', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330046', 'MARKJUKI', 'JENGGRIK', '1962-02-15', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330047', 'SUTARTO', 'MOJOBAYI', '1972-02-16', '01/010', 'Petani', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330048', 'MARSUDI', 'MOJOBAYI', '1982-02-17', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'B', '2016-01-12', 11, '2016-12-12'),
('3302444433330049', 'MARYANTO', 'MOJOBAYI', '1953-02-18', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'B', '2016-01-12', 11, '2016-12-12'),
('3302444433330050', 'RUDI SALAM', 'MOJOBAYI', '1963-02-19', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'B', '2016-01-12', 11, '2016-12-12'),
('3302444433330051', 'SUDIRO', 'MOJOBAYI', '1973-02-20', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'AB', '2016-01-12', 11, '2016-12-12'),
('3302444433330052', 'BASMAN', 'MOJOBAYI', '1983-02-21', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'AB', '2016-01-12', 11, '2016-12-12'),
('3302444433330053', 'SUMARYONO', 'MOJOBAYI', '1954-02-22', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330054', 'PARTONO', 'MOJOBAYI', '1964-02-23', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330055', 'HASAN', 'MOJOBAYI', '1974-02-24', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330056', 'ROBERT?', 'MOJOBAYI', '1984-02-25', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330057', 'BUDI PRAPTONO', 'MOJOBAYI', '1955-02-26', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330058', 'PUGUH', 'MOJOBAYI', '1965-02-27', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330059', 'WARSITO', 'MOJOBAYI', '1975-02-28', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330060', 'SUHARTO', 'MOJOBAYI', '1985-03-01', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330061', 'SUKARNO', 'MOJOBAYI', '1956-03-02', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330062', 'GUNAWAN', 'MOJOBAYI', '1966-03-03', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330063', 'DENNA LISA SILALAHI', 'MOJOBAYI', '1976-03-04', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330064', 'EMSURIANI', 'MOJOBAYI', '1977-03-05', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330065', 'Y E L F I  SH', 'MOJOBAYI', '1957-03-06', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330066', 'RIRI INDRIANI SH,SPN', 'MOJOBAYI', '1967-03-07', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330067', 'YUYUN', 'MOJOBAYI', '1977-03-08', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330068', 'MILSON JONI', 'MOJOBAYI', '1987-03-09', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'BELUM KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330069', 'WELLY BATHESTA', 'MOJOBAYI', '1958-03-10', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330070', 'WIWID FERYANTO RAHARDIAN', 'MOJOBAYI', '1968-03-11', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330071', 'HAERMAN', 'MOJOBAYI', '1978-03-12', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'B', '2016-01-12', 11, '2016-12-12'),
('3302444433330072', 'MARGONO', 'MOJOBAYI', '1988-03-13', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'B', '2016-01-12', 11, '2016-12-12'),
('3302444433330073', 'AKADINA', 'MOJOBAYI', '1959-03-14', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330074', 'STEFAN', 'MOJOBAYI', '1969-03-15', '01/010', 'Petani', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330075', 'MAHMUD', 'MOJOBAYI', '1979-03-16', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330076', 'OKTOVERI', 'MOJOBAYI', '1989-03-17', '01/010', 'WIRASWASTA', 'WNI', 'BELUM KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330077', 'H. SUMARNO', 'MOJOBAYI', '1951-03-18', '01/010', 'Petani', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330078', 'SAT LANTAS 2', 'DAWANGAN', '1961-03-19', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330079', 'UMMI AZIZAH', 'DAWANGAN', '1971-03-20', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330080', 'AISYAH MEI SAPUTRI', 'DAWANGAN', '1981-03-21', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330081', 'RUDI SAKYAKIRTI', 'DAWANGAN', '1952-03-22', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330082', 'HERYANTO', 'DAWANGAN', '1962-03-23', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330083', 'WISNU PUTRA ALMUTTAQIN', 'DAWANGAN', '1972-03-24', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330084', 'FU TSHOI PHIONG', 'DAWANGAN', '1982-03-25', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330085', 'ROHMAN', 'DAWANGAN', '1953-03-26', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330086', 'ACHSAN SAJRI', 'BANJAR', '1963-03-27', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'B', '2016-01-12', 11, '2016-12-12'),
('3302444433330087', 'SHOLIKUL HUDA', 'BANJAR', '1973-03-28', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'B', '2016-01-12', 11, '2016-12-12'),
('3302444433330088', 'POI TJAN', 'BANJAR', '1983-03-29', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'AB', '2016-01-12', 11, '2016-12-12'),
('3302444433330089', 'JOHNI TAN', 'BANJAR', '1954-03-30', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'AB', '2016-01-12', 11, '2016-12-12'),
('3302444433330090', 'ANDI FAISAL', 'BANJAR', '1964-03-31', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'AB', '2016-01-12', 11, '2016-12-12'),
('3302444433330091', 'AGUS LEONARD PRASETYO', 'BANJAR', '1974-04-01', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330092', 'YULIATI', 'BANJAR', '1984-04-02', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330093', 'KAHARUDDIN', 'BANJAR', '1955-04-03', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330094', 'SURIYATI', 'JONGGRANGAN', '1965-04-04', '01/010', 'BURUH TANI/PERKEBUNAN', 'WNI', 'KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330095', 'IBNU RUSHD', 'JONGGRANGAN', '1975-04-05', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'AB', '2016-01-12', 11, '2016-12-12'),
('3302444433330096', 'JIMMY WIJAYA', 'JONGGRANGAN', '1985-04-06', '01/010', 'WIRASWASTA', 'WNI', 'BELUM KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433330097', 'MULYADI', 'JONGGRANGAN', '1956-04-07', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330098', 'R ISKANDAR', 'JONGGRANGAN', '1966-04-08', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330099', 'IRIANTO RUSMAN', 'JONGGRANGAN', '1976-04-09', '01/010', 'WIRASWASTA', 'WNI', 'KAWIN', 'A', '2016-01-12', 11, '2016-12-12'),
('3302444433330100', 'ANTI DINILLAH', 'JONGGRANGAN', '1986-04-10', '01/010', 'WIRASWASTA', 'WNI', 'BELUM KAWIN', 'O', '2016-01-12', 11, '2016-12-12'),
('3302444433332222', 'BUDI RAHARJO', 'MOJOBAYI', '2016-06-01', '01/010', 'PENYULUH', 'WNI', 'KAWIN', 'O', '2016-06-01', 11, '2016-06-30');

-- --------------------------------------------------------

--
-- Table structure for table `gapoktan`
--

CREATE TABLE IF NOT EXISTS `gapoktan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `daerah_id` int(11) NOT NULL,
  `ketua_gapoktan` int(11) DEFAULT NULL,
  `bumdes_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IXFK_gapoktan_daerah` (`daerah_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `gapoktan`
--

INSERT INTO `gapoktan` (`id`, `nama`, `alamat`, `daerah_id`, `ketua_gapoktan`, `bumdes_id`) VALUES
(1, 'Sejahtera', 'Paingan', 11, 130, 3);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `desc` varchar(45) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `harga` decimal(9,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `fk_items_category_idx` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `category_id`, `desc`, `active`, `harga`) VALUES
(1, 'Urea', 1, 'Pupuk Urea', 1, '4000.00'),
(2, 'NPK', 1, 'Pupuk NPK', 1, '5000.00'),
(3, 'SP-36', 1, 'Pupuk SP-36', 1, '3000.00'),
(4, 'Insektisida', 3, 'Insektisida', 1, '60.00'),
(5, 'ZA', 1, 'Pupuk ZA', 1, '3000.00'),
(6, 'Padi', 2, 'Benih', 1, '2500.00'),
(7, 'Herbisida', 3, 'Herbisida', 1, '70.00'),
(8, 'Jagung', 2, 'Jagung', 1, '2295.00'),
(9, 'Kedelai', 2, 'Kakao', 1, '900.00'),
(12, 'Organik', 1, 'Organik', 1, '1000.00');

-- --------------------------------------------------------

--
-- Table structure for table `items_komoditas`
--

CREATE TABLE IF NOT EXISTS `items_komoditas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `komoditas_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `items_komoditas`
--

INSERT INTO `items_komoditas` (`id`, `item_id`, `komoditas_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 2, 2),
(4, 2, 3),
(5, 3, 1),
(6, 3, 2),
(7, 3, 3),
(8, 4, 1),
(9, 4, 2),
(10, 4, 3),
(11, 5, 1),
(12, 5, 2),
(13, 5, 3),
(14, 6, 1),
(15, 7, 1),
(16, 7, 2),
(17, 7, 3),
(18, 9, 2);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_daerah`
--

CREATE TABLE IF NOT EXISTS `jenis_daerah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `jenis_daerah`
--

INSERT INTO `jenis_daerah` (`id`, `nama`) VALUES
(1, 'Kelurahan'),
(2, 'Kecamatan'),
(3, 'Kabupaten'),
(4, 'Provinsi');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_lahan`
--

CREATE TABLE IF NOT EXISTS `jenis_lahan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `jenis_lahan`
--

INSERT INTO `jenis_lahan` (`id`, `nama`) VALUES
(1, 'Mengelola milik sendiri'),
(2, 'Mengelola dengan bagi hasil'),
(3, 'Mengelola dengan menerima upah');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_petani`
--

CREATE TABLE IF NOT EXISTS `jenis_petani` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis_petani` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `jenis_petani`
--

INSERT INTO `jenis_petani` (`id`, `nama_jenis_petani`, `status`) VALUES
(1, 'Mengelola milik sendiri', 'Pemilik lahan'),
(2, 'Mengelola dengan bagi hasil', 'Bagi hasil'),
(3, 'Mengelola dengan menerima upah', 'Pekerja upah');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_tanah`
--

CREATE TABLE IF NOT EXISTS `jenis_tanah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jenis_tanah`
--

INSERT INTO `jenis_tanah` (`id`, `nama`) VALUES
(1, 'gembur');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_tanaman`
--

CREATE TABLE IF NOT EXISTS `jenis_tanaman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `jenis_tanaman`
--

INSERT INTO `jenis_tanaman` (`id`, `nama`) VALUES
(1, 'Padi'),
(2, 'Jagung'),
(3, 'Kakao');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_user`
--

CREATE TABLE IF NOT EXISTS `jenis_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `jenis_user`
--

INSERT INTO `jenis_user` (`id`, `jenis`) VALUES
(1, 'Admin'),
(2, 'BPS'),
(3, 'Pegawai Bumdes');

-- --------------------------------------------------------

--
-- Table structure for table `kepala_daerah`
--

CREATE TABLE IF NOT EXISTS `kepala_daerah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor_induk` varchar(255) NOT NULL,
  `nama` int(11) NOT NULL,
  `jabatan` int(11) NOT NULL,
  `alamat` int(11) NOT NULL,
  `daerah_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `daerah_id` (`daerah_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `komoditas`
--

CREATE TABLE IF NOT EXISTS `komoditas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `keterangan` int(11) DEFAULT NULL,
  `subsektor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `komoditas`
--

INSERT INTO `komoditas` (`id`, `nama`, `keterangan`, `subsektor_id`) VALUES
(1, 'Padi Sawah', 1, 1),
(2, 'Padi Ladang', 2, 1),
(3, 'Jagung', NULL, 9),
(4, 'Kedelai', NULL, 9),
(5, 'Kacang Tanah', NULL, 9),
(6, 'Kacang hijau', NULL, 9),
(7, 'Ubi Kayu', NULL, 9),
(8, 'Ubi Jalar', NULL, 9),
(9, 'Sawi', NULL, 2),
(10, 'Kacang Panjang', NULL, 2),
(11, 'Cabai Merah', NULL, 2),
(12, 'Petai', NULL, 2),
(13, 'Melinjo', NULL, 2),
(14, 'Kangkung', NULL, 2),
(15, 'Bayam', NULL, 2),
(16, 'Mangga', NULL, 2),
(17, 'Nangka/Cempedak', NULL, 2),
(18, 'Pisang', NULL, 2),
(19, 'Rambutan', NULL, 2),
(20, 'Salak Pondoh', NULL, 2),
(21, 'Melon', NULL, 2),
(22, 'Semangka', NULL, 2),
(23, 'Pepaya', NULL, 2),
(24, 'Tembakau Rakyat', NULL, 3),
(25, 'Kelapa', NULL, 3),
(26, 'Tebu', NULL, 3),
(27, 'Mendong', NULL, 3),
(28, 'Jahe', NULL, 3),
(29, 'Tanaman Hias', NULL, 3),
(30, 'Jati', NULL, 8),
(31, 'Mahoni', NULL, 8),
(32, 'Bambu', NULL, 8),
(33, 'S. Keling', NULL, 8),
(34, 'Sengon', NULL, 8);

-- --------------------------------------------------------

--
-- Table structure for table `komoditas_petani`
--

CREATE TABLE IF NOT EXISTS `komoditas_petani` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_petani` int(11) NOT NULL,
  `id_komoditas` int(11) NOT NULL,
  `id_jenis_petani` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_petani` (`id_petani`,`id_komoditas`),
  KEY `id_komoditas` (`id_komoditas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lahan`
--

CREATE TABLE IF NOT EXISTS `lahan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alamat` varchar(255) NOT NULL,
  `daerah_id` int(11) DEFAULT NULL,
  `luas` float(8,2) NOT NULL,
  `jenis_tanaman_id` int(50) DEFAULT NULL,
  `petani_id` int(11) NOT NULL,
  `jenis_tanah_id` int(11) DEFAULT NULL,
  `jenis_lahan_id` int(11) DEFAULT '0',
  `komoditas_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IXFK_lahan_petani` (`petani_id`),
  KEY `IXFK_lahan_daerah` (`daerah_id`),
  KEY `jenis_tanaman_id` (`jenis_tanaman_id`),
  KEY `jenis_tanah_id` (`jenis_tanah_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=124 ;

--
-- Dumping data for table `lahan`
--

INSERT INTO `lahan` (`id`, `alamat`, `daerah_id`, `luas`, `jenis_tanaman_id`, `petani_id`, `jenis_tanah_id`, `jenis_lahan_id`, `komoditas_id`) VALUES
(1, 'PAINGAN', 11, 1.00, 1, 131, 0, 2, 1),
(2, 'PAINGAN', 11, 0.40, 2, 56, 0, 2, 2),
(3, 'PAINGAN', 11, 1.00, 1, 57, 0, 2, 1),
(4, 'PAINGAN', 11, 0.70, 1, 58, 0, 2, 1),
(5, 'PAINGAN', 11, 0.85, 1, 59, 0, 2, 1),
(6, 'PAINGAN', 11, 0.50, 1, 60, 0, 2, 1),
(7, 'PAINGAN', 11, 0.40, 1, 61, 0, 2, 1),
(8, 'PAINGAN', 11, 0.75, 1, 62, 0, 2, 1),
(9, 'PAINGAN', 11, 0.70, 1, 63, 0, 2, 1),
(10, 'PAINGAN', 11, 0.80, 1, 64, 0, 2, 1),
(11, 'PAINGAN', 11, 0.50, 1, 65, 0, 2, 1),
(12, 'PAINGAN', 11, 0.65, 3, 66, 0, 1, 3),
(13, 'PAINGAN', 11, 0.60, 3, 67, 0, 1, 3),
(14, 'PAINGAN', 11, 0.70, 2, 68, 0, 1, 2),
(15, 'PAINGAN', 11, 1.10, 2, 69, 0, 1, 2),
(16, 'PAINGAN', 11, 0.50, 1, 70, 0, 1, 1),
(17, 'PAINGAN', 11, 0.40, 1, 71, 0, 2, 1),
(18, 'PAINGAN', 11, 0.60, 1, 72, 0, 2, 1),
(19, 'PAINGAN', 11, 0.70, 1, 73, 0, 2, 1),
(20, 'PAINGAN', 11, 0.80, 1, 74, 0, 2, 1),
(21, 'JENGGRIK', 11, 1.50, 1, 75, 0, 2, 1),
(22, 'JENGGRIK', 11, 0.90, 1, 76, 0, 2, 1),
(23, 'JENGGRIK', 11, 0.85, 1, 77, 0, 2, 1),
(24, 'JENGGRIK', 11, 0.45, 1, 78, 0, 2, 1),
(25, 'JENGGRIK', 11, 0.75, 1, 79, 0, 2, 1),
(26, 'JENGGRIK', 11, 0.30, 1, 80, 0, 1, 1),
(27, 'JENGGRIK', 11, 1.10, 1, 81, 0, 1, 1),
(28, 'JENGGRIK', 11, 0.85, 1, 82, 0, 1, 1),
(29, 'JENGGRIK', 11, 0.45, 1, 83, 0, 1, 1),
(30, 'JENGGRIK', 11, 0.50, 1, 84, 0, 1, 1),
(31, 'JENGGRIK', 11, 0.65, 2, 85, 0, 1, 2),
(32, 'JENGGRIK', 11, 0.90, 2, 86, 0, 1, 2),
(33, 'JENGGRIK', 11, 0.85, 2, 87, 0, 2, 2),
(34, 'JENGGRIK', 11, 0.45, 2, 88, 0, 2, 2),
(35, 'JENGGRIK', 11, 0.40, 2, 89, 0, 2, 2),
(36, 'JENGGRIK', 11, 0.65, 1, 90, 0, 2, 1),
(37, 'JENGGRIK', 11, 1.00, 1, 91, 0, 2, 1),
(38, 'JENGGRIK', 11, 0.85, 1, 92, 0, 2, 1),
(39, 'JENGGRIK', 11, 0.45, 3, 93, 0, 2, 3),
(40, 'JENGGRIK', 11, 0.60, 3, 94, 0, 2, 3),
(41, 'JENGGRIK', 11, 0.65, 3, 95, 0, 2, 3),
(42, 'JENGGRIK', 11, 0.90, 1, 96, 0, 2, 1),
(43, 'JENGGRIK', 11, 0.85, 1, 97, 0, 2, 1),
(44, 'JENGGRIK', 11, 0.45, 1, 98, 0, 2, 1),
(45, 'JENGGRIK', 11, 0.75, 1, 99, 0, 2, 1),
(46, 'MOJOBAYI', 11, 1.75, 1, 100, 0, 2, 1),
(47, 'MOJOBAYI', 11, 0.80, 1, 101, 0, 2, 1),
(48, 'MOJOBAYI', 11, 0.90, 1, 102, 0, 2, 1),
(49, 'MOJOBAYI', 11, 0.40, 1, 103, 0, 2, 1),
(50, 'MOJOBAYI', 11, 0.50, 1, 104, 0, 2, 1),
(51, 'MOJOBAYI', 11, 0.70, 2, 105, 0, 2, 2),
(52, 'MOJOBAYI', 11, 0.65, 2, 106, 0, 2, 2),
(53, 'MOJOBAYI', 11, 0.55, 2, 107, 0, 2, 2),
(54, 'MOJOBAYI', 11, 0.80, 2, 108, 0, 2, 2),
(55, 'MOJOBAYI', 11, 0.65, 2, 109, 0, 2, 2),
(56, 'MOJOBAYI', 11, 0.40, 2, 110, 0, 2, 2),
(57, 'MOJOBAYI', 11, 1.00, 2, 111, 0, 2, 2),
(58, 'MOJOBAYI', 11, 0.70, 1, 112, 0, 2, 1),
(59, 'MOJOBAYI', 11, 0.65, 1, 113, 0, 2, 1),
(60, 'MOJOBAYI', 11, 0.55, 1, 114, 0, 2, 1),
(61, 'MOJOBAYI', 11, 0.80, 1, 115, 0, 1, 1),
(62, 'MOJOBAYI', 11, 0.90, 1, 116, 0, 1, 1),
(63, 'MOJOBAYI', 11, 0.40, 1, 117, 0, 1, 1),
(64, 'MOJOBAYI', 11, 0.65, 1, 118, 0, 1, 1),
(65, 'MOJOBAYI', 11, 0.70, 1, 119, 0, 1, 1),
(66, 'MOJOBAYI', 11, 0.65, 1, 120, 0, 1, 1),
(67, 'MOJOBAYI', 11, 0.35, 3, 121, 0, 1, 3),
(68, 'MOJOBAYI', 11, 0.80, 3, 122, 0, 1, 3),
(69, 'MOJOBAYI', 11, 0.90, 3, 123, 0, 1, 3),
(70, 'MOJOBAYI', 11, 0.65, 3, 124, 0, 1, 3),
(71, 'MOJOBAYI', 11, 1.00, 3, 125, 0, 1, 3),
(72, 'MOJOBAYI', 11, 0.70, 3, 126, 0, 2, 3),
(73, 'MOJOBAYI', 11, 0.65, 1, 132, 0, 2, 1),
(74, 'MOJOBAYI', 11, 0.55, 1, 128, 0, 2, 1),
(75, 'MOJOBAYI', 11, 0.80, 1, 129, 0, 2, 1),
(76, 'MOJOBAYI', 11, 2.50, 1, 130, 0, 2, 1),
(81, 'PAINGAN', 11, 0.85, 0, 59, 0, 2, 1),
(82, 'PAINGAN', 11, 0.85, 0, 59, 0, 0, 2),
(83, 'PAINGAN', 11, 1.00, 0, 57, 0, 2, 1),
(85, 'PAINGAN', 11, 0.85, 0, 59, 0, 2, 5),
(86, 'PAINGAN', 11, 1.00, NULL, 57, NULL, 2, 1),
(95, 'test', 11, 10.00, 1, 143, NULL, 1, 1),
(96, 'fgdfgdfg', 11, 10.00, 1, 144, NULL, 1, 1),
(123, 'dasdasads', 11, 10.00, 1, 171, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `lahan_backup`
--

CREATE TABLE IF NOT EXISTS `lahan_backup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alamat` varchar(255) NOT NULL,
  `daerah_id` int(11) DEFAULT NULL,
  `luas` decimal(8,2) NOT NULL,
  `jenis_tanaman_id` int(50) NOT NULL,
  `petani_id` int(11) NOT NULL,
  `jenis_tanah_id` int(11) NOT NULL,
  `jenis_lahan_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IXFK_lahan_petani` (`petani_id`),
  KEY `IXFK_lahan_daerah` (`daerah_id`),
  KEY `jenis_tanaman_id` (`jenis_tanaman_id`),
  KEY `jenis_tanah_id` (`jenis_tanah_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `lahan_backup`
--

INSERT INTO `lahan_backup` (`id`, `alamat`, `daerah_id`, `luas`, `jenis_tanaman_id`, `petani_id`, `jenis_tanah_id`, `jenis_lahan_id`) VALUES
(24, 'Paingan', 11, '0.30', 1, 56, 2, 2),
(26, 'Dusun Sukomulya Purwosuman', 11, '10.00', 1, 131, 0, 2),
(27, 'MOJOBAYI', 2, '100.00', 2, 74, 1, 1),
(31, 'jl Paingan', 11, '10.00', 1, 100, 2, 2),
(32, 'Paiman', 11, '0.20', 1, 132, 0, 1),
(33, 'PAINGAN', 10, '1.00', 1, 57, 1, 1),
(34, 'Paingan', 10, '0.80', 2, 58, 2, 1),
(35, 'Jl. Purwosuman no 84', 11, '10.00', 1, 133, 0, 2),
(36, 'Jl. Sido Mulyo no 90', 11, '10.00', 1, 75, 0, 2),
(37, 'jl Purwo suman indah', 11, '10.00', 1, 130, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `action` varchar(150) NOT NULL,
  `date` datetime NOT NULL,
  `poktan_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IXFK_log_rdkk` (`poktan_id`),
  KEY `IXFK_log_user` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `user_id`, `action`, `date`, `poktan_id`) VALUES
(1, 8, 'ApprovalRequest', '2016-05-12 16:41:53', 4),
(2, 8, 'ApprovalRequest', '2016-05-12 16:47:33', 4),
(3, 8, 'ApprovalRequest', '2016-05-12 16:48:53', 4),
(4, 8, 'ApprovalRequest', '2016-05-12 16:53:51', 4),
(6, 8, 'ApprovalRequest', '2016-05-13 03:58:27', 4),
(7, 8, 'ApprovalRequest', '2016-05-13 03:59:16', 4),
(8, 8, 'ApprovalRequest', '2016-05-13 04:14:42', 4),
(9, 8, 'ApprovalRequest', '2016-05-13 09:15:15', 4),
(17, 12, 'ApprovalRequest', '2016-05-20 09:00:18', 6),
(19, 12, 'ApprovalRequest', '2016-05-20 09:03:36', 6),
(21, 12, 'ApprovalRequest', '2016-05-20 09:23:40', 6),
(22, 8, 'ApprovalRequest', '2016-06-07 06:03:27', 6),
(23, 8, 'ApprovalRequest', '2016-06-07 06:06:31', 6),
(24, 8, 'ApprovalRequest', '2016-06-07 06:07:27', 6),
(25, 8, 'ApprovalRequest', '2016-06-07 06:07:42', 6),
(26, 8, 'ApprovalRequest', '2016-06-07 06:08:56', 6),
(27, 14, 'Approved', '2016-06-07 06:52:42', 6),
(28, 8, 'ApprovalRequest', '2016-06-07 06:57:26', 6),
(29, 14, 'Approved', '2016-06-07 06:58:20', 6),
(30, 14, 'Approved', '2016-06-07 07:00:31', 6),
(31, 8, 'ApprovalRequest', '2016-06-08 06:41:12', 4);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `flag` decimal(3,0) DEFAULT NULL,
  `date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `send_to_mail` decimal(8,2) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`),
  KEY `IXFK_notifications_user` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53 ;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `title`, `url`, `flag`, `date`, `user_id`, `send_to_mail`, `notes`) VALUES
(9, 'Pembatalan Persetujuan Poktan', '/poktan/view/4', '0', '2016-05-18 03:53:25', 8, '0.00', 'oeu'),
(10, 'Pembatalan Persetujuan Poktan', '/poktan/view/4', '0', '2016-05-18 03:53:43', 8, '0.00', 'oeu'),
(16, 'Pembatalan Persetujuan Poktan', '/poktan/view/6', '0', '2016-05-20 09:01:38', 12, '0.00', 'kurang luas lahannya'),
(19, 'Poktan Disetujui', '/poktan/view/6', '0', '2016-05-20 09:24:03', 12, '0.00', NULL),
(20, 'Pengajuan Persetujuan Poktan', '/poktan/view/6', '1', '2016-06-07 06:08:56', 14, '0.00', NULL),
(21, 'Poktan Disetujui', '/poktan/view/6', '0', '2016-06-07 06:52:42', 3, '0.00', NULL),
(22, 'Pengajuan Persetujuan Poktan', '/poktan/view/6', '1', '2016-06-07 06:57:26', 14, '0.00', NULL),
(24, 'Poktan Disetujui', '/poktan/view/6', '1', '2016-06-07 07:00:31', 8, '0.00', NULL),
(25, 'Pengajuan Persetujuan Poktan', '/poktan/view/4', '1', '2016-06-08 06:41:12', 14, '0.00', NULL),
(26, 'Pengajuan data petani baru', '/petani/view/143', '0', '2016-07-12 05:46:27', 8, '0.00', 'Pengajuan baru'),
(27, 'Pengajuan data petani baru', '/petani/view/144', '0', '2016-07-12 05:49:48', 8, '0.00', 'Pengajuan baru'),
(48, 'Pengajuan data petani baru', '/petani/view/171', '0', '2016-07-12 07:31:08', 8, '0.00', 'Pengajuan baru'),
(49, 'Pengajuan data petani baru', '/petani/view/56', '0', '2016-07-12 09:32:20', 8, '0.00', 'Pengajuan baru'),
(50, 'Pengajuan data petani baru', '/petani/view/57', '0', '2016-07-12 09:43:24', 8, '0.00', 'Pengajuan baru'),
(51, 'Pengajuan data petani baru', '/petani/view/58', '0', '2016-07-12 09:55:01', 8, '0.00', 'Pengajuan baru'),
(52, 'Pengajuan data petani baru', '/petani/view/59', '0', '2016-07-12 09:56:55', 8, '0.00', 'Pengajuan baru');

-- --------------------------------------------------------

--
-- Table structure for table `penyuluh`
--

CREATE TABLE IF NOT EXISTS `penyuluh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_ektp` varchar(20) NOT NULL,
  `photo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IXFK_penyuluh_ektp` (`no_ektp`),
  KEY `no_ektp` (`no_ektp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `penyuluh`
--

INSERT INTO `penyuluh` (`id`, `no_ektp`, `photo`) VALUES
(2, '3302444433332222', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `petani`
--

CREATE TABLE IF NOT EXISTS `petani` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_tlp` varchar(255) NOT NULL,
  `no_ektp` varchar(20) NOT NULL,
  `poktan_id` int(11) DEFAULT NULL,
  `bumdes_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `id_jenis_petani` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IXFK_petani_ektp` (`no_ektp`),
  KEY `poktan_id` (`poktan_id`),
  KEY `id_jenis_petani` (`id_jenis_petani`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=172 ;

--
-- Dumping data for table `petani`
--

INSERT INTO `petani` (`id`, `no_tlp`, `no_ektp`, `poktan_id`, `bumdes_id`, `status`, `id_jenis_petani`) VALUES
(56, '081399887763', '3302444433330003', 4, 3, 1, 0),
(57, '081399887764', '3302444433330004', 4, 3, 1, 0),
(58, '81399887765', '3302444433330005', 4, 3, 1, 0),
(59, '81399887766', '3302444433330006', 4, 3, 1, 0),
(60, '81399887767', '3302444433330007', 4, 3, 0, 0),
(61, '81399887768', '3302444433330008', 4, 3, 0, 0),
(62, '81399887769', '3302444433330009', 4, 3, 0, 0),
(63, '81399887770', '3302444433330010', 4, 3, 0, 0),
(64, '81399887771', '3302444433330011', 4, 3, 0, 0),
(65, '81399887772', '3302444433330012', 4, 3, 0, 0),
(66, '81399887773', '3302444433330013', 4, 3, 0, 0),
(67, '81399887774', '3302444433330014', 4, 3, 0, 0),
(68, '81399887775', '3302444433330015', 4, 3, 0, 0),
(69, '81399887776', '3302444433330016', 4, 3, 0, 0),
(70, '81399887777', '3302444433330017', 4, 3, 0, 0),
(71, '81399887778', '3302444433330018', 4, 3, 0, 0),
(72, '81399887779', '3302444433330019', 4, 3, 0, 0),
(73, '81399887780', '3302444433330020', 4, 3, 0, 0),
(74, '81399887781', '3302444433330021', 4, 3, 0, 0),
(75, '81399887782', '3302444433330022', 7, 3, 0, 0),
(76, '81399887783', '3302444433330023', 7, 3, 0, 0),
(77, '81399887784', '3302444433330024', 7, 3, 0, 0),
(78, '81399887785', '3302444433330025', 7, 3, 0, 0),
(79, '81399887786', '3302444433330026', 7, 3, 0, 0),
(80, '81399887787', '3302444433330027', 7, 3, 0, 0),
(81, '81399887788', '3302444433330028', 7, 3, 0, 0),
(82, '81399887789', '3302444433330029', 7, 3, 0, 0),
(83, '81399887790', '3302444433330030', 7, 3, 0, 0),
(84, '81399887791', '3302444433330031', 7, 3, 0, 0),
(85, '81399887792', '3302444433330032', 7, 3, 0, 0),
(86, '81399887793', '3302444433330033', 7, 3, 0, 0),
(87, '81399887794', '3302444433330034', 7, 3, 0, 0),
(88, '81399887795', '3302444433330035', 7, 3, 0, 0),
(89, '81399887796', '3302444433330036', 7, 3, 0, 0),
(90, '81399887797', '3302444433330037', 7, 3, 0, 0),
(91, '81399887798', '3302444433330038', 7, 3, 0, 0),
(92, '81399887799', '3302444433330039', 7, 3, 0, 0),
(93, '81399887100', '3302444433330040', 7, 3, 0, 0),
(94, '81399887101', '3302444433330041', 7, 3, 0, 0),
(95, '81399887102', '3302444433330042', 7, 3, 0, 0),
(96, '81399887103', '3302444433330043', 7, 3, 0, 0),
(97, '81399887104', '3302444433330044', 7, 3, 0, 0),
(98, '81399887105', '3302444433330045', 7, 3, 0, 0),
(99, '81399887106', '3302444433330046', 7, 3, 0, 0),
(100, '81399887107', '3302444433330047', 6, 3, 0, 0),
(101, '81399887108', '3302444433330048', 6, 3, 0, 0),
(102, '81399887109', '3302444433330049', 6, 3, 0, 0),
(103, '81399887110', '3302444433330050', 6, 3, 0, 0),
(104, '81399887111', '3302444433330051', 6, 3, 0, 0),
(105, '81399887112', '3302444433330052', 6, 3, 0, 0),
(106, '81399887113', '3302444433330053', 6, 3, 0, 0),
(107, '81399887114', '3302444433330054', 6, 3, 0, 0),
(108, '81399887115', '3302444433330055', 6, 3, 0, 0),
(109, '81399887116', '3302444433330056', 6, 3, 0, 0),
(110, '81399887117', '3302444433330057', 6, 3, 0, 0),
(111, '81399887118', '3302444433330058', 6, 3, 0, 0),
(112, '81399887119', '3302444433330059', 6, 3, 0, 0),
(113, '81399887120', '3302444433330060', 6, 3, 0, 0),
(114, '81399887121', '3302444433330061', 6, 3, 0, 0),
(115, '81399887122', '3302444433330062', 6, 3, 0, 0),
(116, '81399887123', '3302444433330063', 6, 3, 0, 0),
(117, '81399887124', '3302444433330064', 6, 3, 0, 0),
(118, '81399887125', '3302444433330065', 6, 3, 0, 0),
(119, '81399887126', '3302444433330066', 6, 3, 0, 0),
(120, '81399887127', '3302444433330067', 6, 3, 0, 0),
(121, '81399887128', '3302444433330068', 6, 3, 0, 0),
(122, '81399887129', '3302444433330069', 6, 3, 0, 0),
(123, '81399887130', '3302444433330070', 6, 3, 0, 0),
(124, '81399887131', '3302444433330071', 6, 3, 0, 0),
(125, '81399887132', '3302444433330072', 6, 3, 0, 0),
(126, '81399887133', '3302444433330073', 6, 3, 0, 0),
(128, '81399887135', '3302444433330075', 6, 3, 0, 0),
(129, '81399887136', '3302444433330076', 6, 3, 0, 0),
(130, '81399887137', '3302444433330077', 6, 3, 0, 0),
(131, '081399887761', '3302444433330001', 4, 3, 0, 0),
(132, '082124217369', '3302444433330074', 4, 3, 0, 0),
(133, '08121145098', '3302444433330004', 6, 3, 0, 0),
(134, '09887654432', '3302444433330002', 4, 3, 0, 0),
(143, '08121145098', '3302444433330004', NULL, 3, 0, 0),
(144, '08121139010', '3302444433330004', NULL, 3, 0, 0),
(171, '08121145098', '3302444433330002', NULL, 3, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `petani_log`
--

CREATE TABLE IF NOT EXISTS `petani_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `petani_id` int(11) NOT NULL,
  `action` varchar(150) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IXFK_log_rdkk` (`petani_id`),
  KEY `IXFK_log_user` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `petani_log`
--

INSERT INTO `petani_log` (`id`, `user_id`, `petani_id`, `action`, `date`) VALUES
(1, 8, 171, 'pengajuan data baru', '2016-07-12 07:31:08'),
(2, 8, 56, '', '2016-07-12 09:32:20'),
(3, 8, 57, '', '2016-07-12 09:43:24'),
(4, 8, 58, '', '2016-07-12 09:55:01'),
(5, 8, 59, '', '2016-07-12 09:56:55');

-- --------------------------------------------------------

--
-- Table structure for table `phinxlog`
--

CREATE TABLE IF NOT EXISTS `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phinxlog`
--

INSERT INTO `phinxlog` (`version`, `migration_name`, `start_time`, `end_time`) VALUES
(20160518095505, NULL, '2016-05-20 11:54:37', '2016-05-18 03:01:14'),
(20160518095844, NULL, '2016-05-20 11:54:24', '2016-05-18 03:01:14'),
(20160520112246, NULL, '2016-05-20 04:54:50', '2016-05-20 04:54:50'),
(20160523081437, NULL, '2016-05-23 02:33:22', '2016-05-23 02:33:23'),
(20160523092541, NULL, '2016-05-23 02:36:44', '2016-05-23 02:36:45'),
(20160523124844, NULL, '2016-05-23 06:00:45', '2016-05-23 06:00:46'),
(20160525021531, 'AlterPrices', '2016-05-25 19:14:41', '2016-05-25 19:14:41'),
(20160529144154, 'CreateTransaksiRekapRdkk', '2016-05-29 07:57:44', '2016-05-29 07:57:44'),
(20160530041007, 'AlterRdkkMasaTanam', '2016-05-29 21:21:11', '2016-05-29 21:21:11'),
(20160603090129, 'AlterGapoktan', '2016-06-03 02:03:09', '2016-06-03 02:03:09'),
(20160603092326, 'AlterGapoktanAddRegionsField', '2016-06-03 02:29:54', '2016-06-03 02:29:55'),
(20160606045642, 'AlterUsersBumdesID', '2016-06-05 21:58:17', '2016-06-05 21:58:17'),
(20160606065731, 'DeleteEktpContent', '2016-06-06 00:06:42', '2016-06-06 00:06:42'),
(20160606070730, 'DeleteUsersAttempt1', '2016-06-06 00:08:01', '2016-06-06 00:08:01'),
(20160606072219, 'CreateDataDaerah', '2016-06-06 00:49:42', '2016-06-06 00:49:42'),
(20160607103529, 'CreateJenisLahan', '2016-06-07 03:45:32', '2016-06-07 03:45:32'),
(20160608052653, 'AlterJenisTanah', '2016-06-07 22:28:40', '2016-06-07 22:28:41'),
(20160608104036, NULL, '2016-06-08 03:47:08', '2016-06-08 03:47:08'),
(20160609031800, NULL, '2016-06-08 20:22:56', '2016-06-08 20:22:57'),
(20160609035654, NULL, '2016-06-08 21:06:17', '2016-06-08 21:06:17'),
(20160609050016, NULL, '2016-06-08 22:09:53', '2016-06-08 22:09:53'),
(20160610082151, NULL, '2016-06-10 02:02:17', '2016-06-10 02:02:18'),
(20160610090640, NULL, '2016-06-10 02:07:18', '2016-06-10 02:07:18'),
(20160611085326, NULL, '2016-06-11 01:54:30', '2016-06-11 01:54:30'),
(20160613032746, NULL, '2016-06-12 21:06:45', '2016-06-12 21:06:49'),
(20160622045049, 'CreateItemsKomoditas', '2016-06-23 01:19:04', '2016-06-23 01:19:06');

-- --------------------------------------------------------

--
-- Table structure for table `poktan`
--

CREATE TABLE IF NOT EXISTS `poktan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `daerah_id` int(11) NOT NULL,
  `gapoktan_id` int(11) NOT NULL,
  `lat` float NOT NULL,
  `lon` float NOT NULL,
  `aprroved` int(11) DEFAULT NULL,
  `ketua_poktan` int(11) DEFAULT NULL,
  `penyuluh_id` int(11) NOT NULL,
  `approval_request` int(11) NOT NULL,
  `bumdes_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IXFK_poktan_gapoktan` (`gapoktan_id`),
  KEY `IXFK_poktan_daerah` (`daerah_id`),
  KEY `ketua_poktan` (`ketua_poktan`),
  KEY `penyuluh_id` (`penyuluh_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `poktan`
--

INSERT INTO `poktan` (`id`, `nama`, `alamat`, `daerah_id`, `gapoktan_id`, `lat`, `lon`, `aprroved`, `ketua_poktan`, `penyuluh_id`, `approval_request`, `bumdes_id`) VALUES
(4, 'Tani Rahayu', 'Paingan', 11, 1, 106.676, 1098, 0, 131, 2, 1, 3),
(6, 'NGUDI REJEKI', 'Mojobayi', 11, 1, 106.676, 1098, 1, 100, 2, 0, 3),
(7, 'KRIDA TANI', 'JENGGRIK', 11, 1, 106.676, 1098, NULL, 75, 2, 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `rdkk`
--

CREATE TABLE IF NOT EXISTS `rdkk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `petani_id` int(11) NOT NULL,
  `id_subsektor` int(11) DEFAULT NULL,
  `id_komoditas` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `masa_tanam` int(11) NOT NULL,
  `lahan_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IXFK_RDKK_Subsektor` (`id_subsektor`),
  KEY `id_komoditas` (`id_komoditas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `rdkk`
--

INSERT INTO `rdkk` (`id`, `petani_id`, `id_subsektor`, `id_komoditas`, `tanggal`, `masa_tanam`, `lahan_id`) VALUES
(13, 57, 1, 1, '2016-06-09', 1, 33),
(14, 58, 1, 2, '2016-06-09', 1, 34),
(15, 58, 1, 1, '2016-06-09', 2, 34),
(16, 58, 2, 4, '2016-06-09', 3, 34),
(17, 57, 1, 1, '2016-06-09', 2, 33);

-- --------------------------------------------------------

--
-- Table structure for table `rekap_log`
--

CREATE TABLE IF NOT EXISTS `rekap_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `action` varchar(150) NOT NULL,
  `date` datetime NOT NULL,
  `rekap_rdkk_id` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IXFK_log_rdkk` (`rekap_rdkk_id`),
  KEY `IXFK_log_user` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `rekap_log`
--

INSERT INTO `rekap_log` (`id`, `user_id`, `action`, `date`, `rekap_rdkk_id`, `comment`) VALUES
(1, 8, 'Created', '2016-06-13 06:16:20', 2, ''),
(2, 8, 'Created', '2016-06-13 06:20:09', 3, ''),
(3, 8, 'Created', '2016-06-13 06:29:27', 4, ''),
(4, 8, 'Created', '2016-06-13 06:30:30', 5, '');

-- --------------------------------------------------------

--
-- Table structure for table `rekap_rdkk`
--

CREATE TABLE IF NOT EXISTS `rekap_rdkk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `approved_date` date NOT NULL,
  `daerah_id` int(11) NOT NULL,
  `poktan_id` int(11) NOT NULL,
  `masa_tanam` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `daerah_id` (`daerah_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `rekap_rdkk`
--

INSERT INTO `rekap_rdkk` (`id`, `created_date`, `status`, `approved_date`, `daerah_id`, `poktan_id`, `masa_tanam`) VALUES
(2, '2016-06-13 06:16:20', 1, '0000-00-00', 0, 4, 0),
(3, '2016-06-13 06:20:09', 1, '0000-00-00', 0, 4, 1),
(4, '2016-06-13 06:29:27', 1, '0000-00-00', 0, 4, 3),
(5, '2016-06-13 06:30:30', 1, '0000-00-00', 0, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sektor`
--

CREATE TABLE IF NOT EXISTS `sektor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `sektor`
--

INSERT INTO `sektor` (`id`, `nama`, `description`) VALUES
(1, 'PERTANIAN', ''),
(2, 'PERTAMBANGAN', ''),
(3, 'PARIWISATA', ''),
(4, 'PERDAGANGAN', ''),
(5, 'INDUSTRI & MANUFACTURE', '');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subsektor`
--

CREATE TABLE IF NOT EXISTS `subsektor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(2000) DEFAULT NULL,
  `sektor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `subsektor`
--

INSERT INTO `subsektor` (`id`, `nama`, `sektor_id`) VALUES
(1, 'Tanaman Padi', 1),
(2, 'Holtikultura', 1),
(3, 'Perkebunan', 1),
(8, 'Kehutanan', 1),
(9, 'Palawija', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tr_rdkk`
--

CREATE TABLE IF NOT EXISTS `tr_rdkk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rdkk_id` int(11) NOT NULL,
  `lahan_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `masa_tanam` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IXid_RDKK` (`rdkk_id`),
  KEY `fk_tr_rdkk_items1_idx` (`item_id`),
  KEY `fk_tr_rdkk_lahan1_idx` (`lahan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=91 ;

--
-- Dumping data for table `tr_rdkk`
--

INSERT INTO `tr_rdkk` (`id`, `rdkk_id`, `lahan_id`, `item_id`, `jumlah`, `masa_tanam`) VALUES
(1, 10, 24, 1, 101, NULL),
(2, 10, 24, 2, 10, NULL),
(3, 10, 24, 3, 100, NULL),
(4, 10, 24, 4, 100, NULL),
(5, 10, 24, 5, 100, NULL),
(6, 10, 24, 6, 100, NULL),
(7, 10, 24, 7, 100, NULL),
(8, 10, 24, 8, 100, NULL),
(9, 10, 24, 9, 100, NULL),
(10, 10, 24, 12, 100, NULL),
(11, 13, 33, 1, 300, NULL),
(12, 13, 33, 2, 100, NULL),
(13, 13, 33, 3, 200, NULL),
(14, 13, 33, 4, 100, NULL),
(15, 13, 33, 5, 100, NULL),
(16, 13, 33, 6, 7, NULL),
(17, 13, 33, 7, 0, NULL),
(18, 13, 33, 8, 0, NULL),
(19, 13, 33, 9, 0, NULL),
(20, 13, 33, 12, 0, NULL),
(21, 14, 34, 5, 200, NULL),
(22, 14, 34, 1, 0, NULL),
(23, 14, 34, 12, 600, NULL),
(24, 14, 34, 2, 0, NULL),
(25, 14, 34, 3, 0, NULL),
(26, 14, 34, 9, 0, NULL),
(27, 14, 34, 6, 0, NULL),
(28, 14, 34, 8, 3, NULL),
(29, 14, 34, 4, 30, NULL),
(30, 14, 34, 7, 0, NULL),
(31, 15, 34, 5, 10, NULL),
(32, 15, 34, 1, 20, NULL),
(33, 15, 34, 12, 30, NULL),
(34, 15, 34, 2, 40, NULL),
(35, 15, 34, 3, 50, NULL),
(36, 15, 34, 9, 60, NULL),
(37, 15, 34, 6, 70, NULL),
(38, 15, 34, 8, 80, NULL),
(39, 15, 34, 4, 90, NULL),
(40, 15, 34, 7, 100, NULL),
(41, 16, 34, 5, 5, NULL),
(42, 16, 34, 1, 5, NULL),
(43, 16, 34, 12, 5, NULL),
(44, 16, 34, 2, 5, NULL),
(45, 16, 34, 3, 5, NULL),
(46, 16, 34, 9, 5, NULL),
(47, 16, 34, 6, 5, NULL),
(48, 16, 34, 8, 5, NULL),
(49, 16, 34, 4, 5, NULL),
(50, 16, 34, 7, 0, NULL),
(51, 17, 33, 5, 3, NULL),
(52, 17, 33, 1, 3, NULL),
(53, 17, 33, 12, 3, NULL),
(54, 17, 33, 2, 3, NULL),
(55, 17, 33, 3, 3, NULL),
(56, 17, 33, 9, 3, NULL),
(57, 17, 33, 6, 3, NULL),
(58, 17, 33, 8, 3, NULL),
(59, 17, 33, 4, 3, NULL),
(60, 17, 33, 7, 3, NULL),
(61, 18, 24, 5, 3, NULL),
(62, 18, 24, 1, 4, NULL),
(63, 18, 24, 12, 4, NULL),
(64, 18, 24, 2, 4, NULL),
(65, 18, 24, 3, 4, NULL),
(66, 18, 24, 9, 4, NULL),
(67, 18, 24, 6, 4, NULL),
(68, 18, 24, 8, 4, NULL),
(69, 18, 24, 4, 4, NULL),
(70, 18, 24, 7, 4, NULL),
(71, 19, 24, 5, 0, NULL),
(72, 19, 24, 1, 250, NULL),
(73, 19, 24, 12, 0, NULL),
(74, 19, 24, 2, 0, NULL),
(75, 19, 24, 3, 0, NULL),
(76, 19, 24, 9, 0, NULL),
(77, 19, 24, 6, 6, NULL),
(78, 19, 24, 8, 0, NULL),
(79, 19, 24, 4, 40, NULL),
(80, 19, 24, 7, 0, NULL),
(81, 21, 24, 1, 101, NULL),
(82, 21, 24, 2, 102, NULL),
(83, 21, 24, 3, 0, NULL),
(84, 21, 24, 5, 0, NULL),
(85, 21, 24, 12, 0, NULL),
(86, 21, 24, 6, 10, NULL),
(87, 21, 24, 8, 20, NULL),
(88, 21, 24, 9, 30, NULL),
(89, 21, 24, 4, 5, NULL),
(90, 21, 24, 7, 20, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tr_rdkk_copy`
--

CREATE TABLE IF NOT EXISTS `tr_rdkk_copy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rdkk_id` int(11) NOT NULL,
  `lahan_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `masa_tanam` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IXid_RDKK` (`rdkk_id`),
  KEY `fk_tr_rdkk_items1_idx` (`item_id`),
  KEY `fk_tr_rdkk_lahan1_idx` (`lahan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `tr_rdkk_copy`
--

INSERT INTO `tr_rdkk_copy` (`id`, `rdkk_id`, `lahan_id`, `item_id`, `jumlah`, `masa_tanam`) VALUES
(15, 8, 22, 2, 1, 1),
(16, 8, 22, 1, 2, 2),
(17, 8, 22, 4, 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tr_rekaprdkk`
--

CREATE TABLE IF NOT EXISTS `tr_rekaprdkk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rdkk_id` int(11) NOT NULL,
  `rekap_rdkk_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(25) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `jenis_user_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `petani_id` int(11) DEFAULT NULL,
  `penyuluh_id` int(11) DEFAULT NULL,
  `kepala_daerah_id` int(11) DEFAULT NULL,
  `bumdes_id` int(11) DEFAULT NULL,
  `daerah_id` int(11) DEFAULT NULL,
  `bps_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IXid_jenis_user` (`jenis_user_id`),
  KEY `petani_id` (`petani_id`),
  KEY `petani_id_2` (`petani_id`),
  KEY `kepala_daerah_id` (`kepala_daerah_id`),
  KEY `bps_id` (`bps_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `created_date`, `jenis_user_id`, `active`, `petani_id`, `penyuluh_id`, `kepala_daerah_id`, `bumdes_id`, `daerah_id`, `bps_id`) VALUES
(1, NULL, 'admin', '$2y$10$0rZzJoL3jaP5ygqo91ZlROGoiiyyQptQtTr4gScLWRWa3bfQe.oCS', '2016-05-04 02:35:00', 1, 1, 0, NULL, NULL, NULL, 0, 0),
(2, NULL, 'User1', '$2y$10$QuKErBly3cFT9sLTpYzbCOJJLPoiTXnCJ6jWS4gzIF7P3vEF.rsEa', '2016-05-04 03:24:00', 2, 1, 0, NULL, NULL, NULL, 0, 0),
(3, NULL, 'ucup', '$2y$10$nEw/sizM7HmYUKYu8TwYqOCd6DcEnrQrcjGJEAl9fHb53qsYz.MX.', '2016-05-04 07:24:00', 1, 1, 0, NULL, NULL, NULL, 0, 0),
(5, NULL, 'mimin', '$2y$10$XLouBkXDfD3DvTQMv6ehG.BuICzZws4KMsvRLvyyopwxICN42Gpta', '2016-05-04 11:53:00', 1, 0, 0, NULL, NULL, NULL, 0, 0),
(6, NULL, 'dennis', '$2y$10$nHTVGYtYUJlRjZtJbh1KXejlVznjTKm4LK3EfNdtG.D2d387wyvt6', '2016-05-04 11:53:00', 1, 0, 0, NULL, NULL, NULL, 0, 0),
(8, 'nico@ilcs.co.id', 'joko', '$2y$10$ZkovoXgaQgPszpAOHEa.nOcrDaJC/gYY9SucOuv0dr/LuAVYkwASq', '2016-05-12 10:28:00', 3, 1, 45, NULL, NULL, 3, 0, 0),
(12, NULL, 'petani', '$2y$10$0zWCx.NYQmXUdrwBBwXGT.4cqIu5CDwCUxbNkh68xWdHOpY2e0DPO', '2016-05-20 04:44:00', 3, 1, 53, NULL, NULL, 0, 0, 0),
(14, NULL, 'penyuluh', '$2y$10$b9epi9zXITwvqX5YAuC7muIuArvW.WQRjDHbpr4ZDxWZGFiNhnLKC', '2016-06-07 05:32:00', 2, 1, NULL, 2, NULL, 3, 8, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assets`
--
ALTER TABLE `assets`
  ADD CONSTRAINT `assets_ibfk_1` FOREIGN KEY (`id_status`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `daerah`
--
ALTER TABLE `daerah`
  ADD CONSTRAINT `FK_daerah_jenis_daerah` FOREIGN KEY (`jenis_daerah_id`) REFERENCES `jenis_daerah` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ektp`
--
ALTER TABLE `ektp`
  ADD CONSTRAINT `FK_ektp_daerah` FOREIGN KEY (`daerah_id`) REFERENCES `daerah` (`id`);

--
-- Constraints for table `gapoktan`
--
ALTER TABLE `gapoktan`
  ADD CONSTRAINT `FK_gapoktan_daerah` FOREIGN KEY (`daerah_id`) REFERENCES `daerah` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `fk_items_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kepala_daerah`
--
ALTER TABLE `kepala_daerah`
  ADD CONSTRAINT `kepala_daerah_ibfk_1` FOREIGN KEY (`daerah_id`) REFERENCES `daerah` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `komoditas_petani`
--
ALTER TABLE `komoditas_petani`
  ADD CONSTRAINT `komoditas_petani_ibfk_1` FOREIGN KEY (`id_petani`) REFERENCES `petani` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `komoditas_petani_ibfk_2` FOREIGN KEY (`id_komoditas`) REFERENCES `komoditas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lahan_backup`
--
ALTER TABLE `lahan_backup`
  ADD CONSTRAINT `fk_lahan_daerah1` FOREIGN KEY (`daerah_id`) REFERENCES `daerah` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lahan_jenis_tanaman1` FOREIGN KEY (`jenis_tanaman_id`) REFERENCES `jenis_tanaman` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_lahan_petani1` FOREIGN KEY (`petani_id`) REFERENCES `petani` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `FK_log_poktan02` FOREIGN KEY (`poktan_id`) REFERENCES `poktan` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_log_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `FK_notifications_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `penyuluh`
--
ALTER TABLE `penyuluh`
  ADD CONSTRAINT `penyuluh_ibfk_1` FOREIGN KEY (`no_ektp`) REFERENCES `ektp` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `petani`
--
ALTER TABLE `petani`
  ADD CONSTRAINT `petani_ibfk_1` FOREIGN KEY (`poktan_id`) REFERENCES `poktan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `petani_ibfk_2` FOREIGN KEY (`no_ektp`) REFERENCES `ektp` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `petani_log`
--
ALTER TABLE `petani_log`
  ADD CONSTRAINT `petani_log_ibfk_1` FOREIGN KEY (`petani_id`) REFERENCES `petani` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rdkk`
--
ALTER TABLE `rdkk`
  ADD CONSTRAINT `FK_rdkk_subsektor02` FOREIGN KEY (`id_subsektor`) REFERENCES `subsektor` (`id`),
  ADD CONSTRAINT `rdkk_ibfk_1` FOREIGN KEY (`id_komoditas`) REFERENCES `komoditas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rekap_log`
--
ALTER TABLE `rekap_log`
  ADD CONSTRAINT `rekap_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rekap_log_ibfk_2` FOREIGN KEY (`rekap_rdkk_id`) REFERENCES `rekap_rdkk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
