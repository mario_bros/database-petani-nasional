<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RekapRdkk Entity.
 *
 * @property int $id
 * @property \Cake\I18n\Time $created_date
 * @property int $status
 * @property \Cake\I18n\Time $approved_date
 * @property int $poktan_id
 * @property \App\Model\Entity\Daerah $daerah
 * @property \App\Model\Entity\Aprrove[] $aprroves
 * @property \App\Model\Entity\RekapLog[] $rekap_log
 * @property \App\Model\Entity\TrRekaprdkk[] $tr_rekaprdkk
 */
class RekapRdkk extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
