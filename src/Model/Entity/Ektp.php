<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Ektp Entity.
 *
 * @property int $id
 * @property string $nama
 * @property string $alamat
 * @property \Cake\I18n\Time $tanggal_lahir
 * @property string $rt_rw
 * @property string $desa_kelurahan
 * @property string $kecamatan
 * @property string $kabupaten_kota
 * @property string $pekerjaan
 * @property string $kewarganegaraan
 * @property string $status_perkawinan
 * @property string $gol_darah
 * @property \Cake\I18n\Time $tanggal_terbit
 * @property int $daerah_id
 * @property \App\Model\Entity\Daerah $daerah
 * @property \Cake\I18n\Time $tanggal_berakhir
 * @property \App\Model\Entity\Petani $petani
 */
class Ektp extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'no_ktp' => false,
    ];
}
