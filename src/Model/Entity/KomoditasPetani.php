<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * KomoditasPetani Entity.
 *
 * @property int $id
 * @property int $id_petani
 * @property int $id_komoditas
 * @property int $id_jenis_petani
 */
class KomoditasPetani extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
