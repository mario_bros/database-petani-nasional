<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DaerahStatus Entity.
 *
 * @property int $id
 * @property int $status
 * @property int $daerah_id
 * @property \App\Model\Entity\Daerah $daerah
 * @property \Cake\I18n\Time $approval_date
 */
class DaerahStatus extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
