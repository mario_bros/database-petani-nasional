<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Lahan Entity.
 *
 * @property int $id
 * @property string $alamat
 * @property int $daerah_id
 * @property float $luas
 * @property int $jenis_tanaman_id
 * @property int $petani_id
 * @property \App\Model\Entity\Daerah $daerah
 */
class Lahan extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
