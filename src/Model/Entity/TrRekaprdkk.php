<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TrRekaprdkk Entity.
 *
 * @property int $id
 * @property int $rdkk_id
 * @property \App\Model\Entity\Rdkk $rdkk
 * @property int $rdkk_rekap_id
 * @property \App\Model\Entity\RdkkRekap $rdkk_rekap
 */
class TrRekaprdkk extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
