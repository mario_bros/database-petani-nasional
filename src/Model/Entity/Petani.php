<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use JeremyHarris\LazyLoad\ORM\LazyLoadEntityTrait;

/**
 * Petani Entity.
 *
 * @property int $id
 * @property string $no_tlp
 * @property string $photo
 * @property int $no_ektp
 * @property int $poktan_id
 * @property \App\Model\Entity\Poktan $poktan
 * @property \App\Model\Entity\Lahan[] $lahan
 * @property \App\Model\Entity\User $user
 */
class Petani extends Entity
{
    use LazyLoadEntityTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _getRdkkMasaTanam()
    {
        $tbl = TableRegistry::get('Rdkk');
        $res = $tbl->find()->where(['petani_id' => $this->id])->combine('masa_tanam', 'masa_tanam')->toArray();
                
        return $res;
    }
}
