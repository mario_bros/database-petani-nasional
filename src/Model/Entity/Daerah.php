<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use JeremyHarris\LazyLoad\ORM\LazyLoadEntityTrait;

/**
 * Daerah Entity.
 *
 * @property int $id
 * @property string $nama
 * @property float $lat
 * @property float $lon
 * @property string $note
 * @property int $parent_id
 * @property \App\Model\Entity\Daerah $parent_daerah
 * @property int $jenis_daerah_id
 * @property \App\Model\Entity\JenisDaerah $jenis_daerah
 * @property \App\Model\Entity\Bumde[] $bumdes
 * @property \App\Model\Entity\Daerah[] $child_daerah
 * @property \App\Model\Entity\Ektp[] $ektp
 * @property \App\Model\Entity\Gapoktan[] $gapoktan
 * @property \App\Model\Entity\Lahan[] $lahan
 * @property \App\Model\Entity\Poktan[] $poktan
 * @property \App\Model\Entity\User[] $user
 */
class Daerah extends Entity
{

    use LazyLoadEntityTrait;
    
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
    
    protected function _getParent()
    {
        $parent = TableRegistry::get('Daerah');
        return $parent->find()->where(['parent_id' => $this->parent_id])->first();
    }
}
