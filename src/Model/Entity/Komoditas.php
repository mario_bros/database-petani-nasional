<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Poktan Entity.
 *
 * @property int $id
 * @property string $nama
 * @property string $alamat
 * @property int $daerah_id
 * @property \App\Model\Entity\Daerah $daerah
 * @property int $gapoktan_id
 * @property \App\Model\Entity\Gapoktan $gapoktan
 * @property float $lat
 * @property float $lon
 * @property float $aprroved
 * @property int $ketua_poktan
 * @property int $penyuluh_id
 * @property \App\Model\Entity\Penyuluh $penyuluh
 * @property \App\Model\Entity\Petani[] $petani
 * @property \App\Model\Entity\Log[] $log
 */
class Komoditas extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'items' => true,
    ];
}
