<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RekapLog Entity.
 *
 * @property int $id
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property string $action
 * @property \Cake\I18n\Time $date
 * @property int $rekap_rdkk_id
 * @property \App\Model\Entity\RekapRdkk $rekap_rdkk
 * @property string $comment
 */
class RekapLog extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
