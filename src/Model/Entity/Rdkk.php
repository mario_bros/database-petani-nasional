<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Rdkk Entity.
 *
 * @property int $id
 * @property int $petani_id
 * @property \App\Model\Entity\Petani $petani
 * @property \Cake\I18n\Time $tanggal
 * @property int $id_subsektor
 * @property \App\Model\Entity\Subsektor $subsektor
 * @property int $id_komoditas
 * @property \App\Model\Entity\Komodita $komodita
 * @property \App\Model\Entity\TrRdkk[] $tr_rdkk
 */
class Rdkk extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
