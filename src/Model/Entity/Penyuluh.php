<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Penyuluh Entity.
 *
 * @property int $id
 * @property string $no_ektp
 * @property \App\Model\Entity\Ektp $ektp
 * @property string $photo
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Poktan[] $poktan
 */
class Penyuluh extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
