<?php
namespace App\Model\Table;

use App\Model\Entity\Ektp;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ektp Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Daerah
 */
class EktpTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('ektp');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Daerah', [
            'foreignKey' => 'daerah_id',
            'joinType' => 'INNER'
        ]);

        $this->hasOne('Petani', [
            'foreignKey' => 'no_ektp',
            'joinType' => 'INNER'
        ]);

        $this->hasOne('Penyuluh', [
            'foreignKey' => 'no_ektp',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nama', 'create')
            ->notEmpty('nama');

        $validator
            ->requirePresence('alamat', 'create')
            ->notEmpty('alamat');

        $validator
            ->date('tanggal_lahir')
            ->requirePresence('tanggal_lahir', 'create')
            ->notEmpty('tanggal_lahir');

        $validator
            ->requirePresence('rt_rw', 'create')
            ->notEmpty('rt_rw');

        $validator
            ->requirePresence('pekerjaan', 'create')
            ->notEmpty('pekerjaan');

        $validator
            ->requirePresence('kewarganegaraan', 'create')
            ->notEmpty('kewarganegaraan');

        $validator
            ->requirePresence('status_perkawinan', 'create')
            ->notEmpty('status_perkawinan');

        $validator
            ->requirePresence('jenis_kelamin', 'create')
            ->notEmpty('jenis_kelamin');


        $validator
            ->requirePresence('gol_darah', 'create')
            ->notEmpty('gol_darah');

        $validator
            ->date('tanggal_terbit')
            ->requirePresence('tanggal_terbit', 'create')
            ->notEmpty('tanggal_terbit');

        $validator
            //->date('tanggal_berakhir')
            ->requirePresence('tanggal_berakhir', 'create')
            ->notEmpty('tanggal_berakhir');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['daerah_id'], 'Daerah'));
        return $rules;
    }
}
