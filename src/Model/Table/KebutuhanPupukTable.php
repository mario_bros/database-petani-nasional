<?php
namespace App\Model\Table;

use App\Model\Entity\KebutuhanPupuk;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * KebutuhanPupuk Model
 *
 */
class KebutuhanPupukTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('kebutuhan_pupuk');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('id_rdkk')
            ->requirePresence('id_rdkk', 'create')
            ->notEmpty('id_rdkk');

        $validator
            ->decimal('mt')
            ->requirePresence('mt', 'create')
            ->notEmpty('mt');

        $validator
            ->decimal('jumlah')
            ->allowEmpty('jumlah');

        $validator
            ->decimal('bulan')
            ->allowEmpty('bulan');

        $validator
            ->allowEmpty('tahun');

        $validator
            ->integer('id_pupuk')
            ->allowEmpty('id_pupuk');

        return $validator;
    }
}
