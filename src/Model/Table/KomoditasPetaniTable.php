<?php
namespace App\Model\Table;

use App\Model\Entity\KomoditasPetani;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * KomoditasPetani Model
 *
 */
class KomoditasPetaniTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);


        $this->belongsTo('Komoditas', [
            'foreignKey' => 'id_komoditas',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('JenisPetani', [
            'foreignKey' => 'id_jenis_petani',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Petani', [
            'foreignKey' => 'id_petani',
            'joinType' => 'LEFT'
        ]);

        $this->hasOne('Lahan', [
            'foreignKey' => 'komoditas_petani_id',
            'joinType' => 'LEFT'
        ]);

        $this->table('komoditas_petani');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('id_petani')
            ->allowEmpty('id_petani', 'create');


        $validator
            ->integer('id_komoditas')
            ->requirePresence('id_komoditas', 'create')
            ->notEmpty('id_komoditas');

        $validator
            ->integer('id_jenis_petani')
            ->requirePresence('id_jenis_petani', 'create')
            ->notEmpty('id_jenis_petani');

        return $validator;
    }
}
