<?php
namespace App\Model\Table;

use App\Model\Entity\RekapRdkk;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RekapRdkk Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Poktan
 * @property \Cake\ORM\Association\HasMany $Aprroves
 * @property \Cake\ORM\Association\HasMany $RekapLog
 * @property \Cake\ORM\Association\HasMany $TrRekaprdkk
 */
class RekapRdkkTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('rekap_rdkk');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Poktan', [
            'foreignKey' => 'poktan_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Aprroves', [
            'foreignKey' => 'rekap_rdkk_id'
        ]);
        $this->hasMany('RekapLog', [
            'foreignKey' => 'rekap_rdkk_id'
        ]);
        $this->hasMany('TrRekaprdkk', [
            'foreignKey' => 'rekap_rdkk_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('created_date')
            ->requirePresence('created_date', 'create')
            ->notEmpty('created_date');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        /*$validator
            ->date('approved_date')
            ->requirePresence('approved_date', 'create')
            ->notEmpty('approved_date');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['poktan_id'], 'Poktan'));
        return $rules;
    }
}
