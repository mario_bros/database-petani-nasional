<?php
namespace App\Model\Table;

use App\Model\Entity\Daerah;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Daerah Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ParentDaerah
 * @property \Cake\ORM\Association\BelongsTo $JenisDaerah
 * @property \Cake\ORM\Association\HasMany $Bumdes
 * @property \Cake\ORM\Association\HasMany $ChildDaerah
 * @property \Cake\ORM\Association\HasMany $Ektp
 * @property \Cake\ORM\Association\HasMany $Gapoktan
 * @property \Cake\ORM\Association\HasMany $Lahan
 * @property \Cake\ORM\Association\HasMany $Poktan
 * @property \Cake\ORM\Association\HasMany $Users
 */
class DaerahTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('daerah');
        $this->displayField('nama');
        $this->primaryKey('id');
        //$this->addBehavior('Tree');

        $this->belongsTo('ParentDaerah', [
            'className' => 'Daerah',
            'foreignKey' => 'parent_id'
        ]);

        $this->belongsTo('GrandParentDaerah', [
            'className' => 'Daerah',
            'foreignKey' => 'parent_id'
        ]);

        $this->belongsTo('ThirdLevelParentDaerah', [
            'className' => 'Daerah',
            'foreignKey' => 'parent_id'
        ]);

        $this->belongsTo('JenisDaerah', [
            'foreignKey' => 'jenis_daerah_id'
        ]);
        $this->hasMany('Bumdes', [
            'foreignKey' => 'daerah_id'
        ]);

        $this->hasMany('DaerahStatus', [
            'foreignKey' => 'daerah_id',
            'joinType'=>'LEFT'
        ]);

        $this->hasMany('ChildDaerah', [
            'className' => 'Daerah',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('Ektp', [
            'foreignKey' => 'daerah_id'
        ]);
        $this->hasMany('Gapoktan', [
            'foreignKey' => 'daerah_id',
            'joinType' => 'LEFT'
        ]);
        $this->hasMany('Lahan', [
            'foreignKey' => 'daerah_id'
        ]);
        $this->hasMany('Poktan', [
            'foreignKey' => 'daerah_id'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'daerah_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nama', 'create')
            ->notEmpty('nama');

        $validator
            ->numeric('lat')
            ->requirePresence('lat', 'create')
            ->notEmpty('lat');

        $validator
            ->numeric('lon')
            ->requirePresence('lon', 'create')
            ->notEmpty('lon');

        $validator
            ->allowEmpty('note');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentDaerah'));
        $rules->add($rules->existsIn(['jenis_daerah_id'], 'JenisDaerah'));
        return $rules;
    }
}
