<?php
namespace App\Model\Table;

use App\Model\Entity\TrRdkk;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TrRdkk Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Rdkks
 * @property \Cake\ORM\Association\BelongsTo $Lahans
 * @property \Cake\ORM\Association\BelongsTo $Items
 */
class TrRdkkTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tr_rdkk');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Rdkk', [
            'foreignKey' => 'rdkk_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Lahan', [
            'foreignKey' => 'lahan_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Items', [
            'foreignKey' => 'item_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('jumlah')
            ->requirePresence('jumlah', 'create')
            ->notEmpty('jumlah');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['rdkk_id'], 'Rdkk'));
        $rules->add($rules->existsIn(['lahan_id'], 'Lahan'));
        $rules->add($rules->existsIn(['item_id'], 'Items'));
        return $rules;
    }
}
