<?php
namespace App\Model\Table;

use App\Model\Entity\JenisTanah;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * JenisTanah Model
 *
 * @property \Cake\ORM\Association\HasMany $Lahan
 */
class JenisTanahTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('jenis_tanah');
        $this->displayField('nama');
        $this->primaryKey('id');

        $this->hasMany('Lahan', [
            'foreignKey' => 'jenis_tanah_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('nama')
            ->requirePresence('nama', 'create')
            ->notEmpty('nama');

        return $validator;
    }
}
