<?php
namespace App\Model\Table;

use App\Model\Entity\JenisUser;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * JenisUser Model
 *
 * @property \Cake\ORM\Association\HasMany $Users
 */
class JenisUserTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('jenis_user');
        $this->displayField('jenis');
        $this->primaryKey('id');

        $this->hasMany('Users', [
            'foreignKey' => 'jenis_user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('jenis', 'create')
            ->notEmpty('jenis');

        return $validator;
    }
}
