<?php
namespace App\Model\Table;

use App\Model\Entity\Petani;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;

/**
 * Petani Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Poktan
 * @property \Cake\ORM\Association\HasMany $Lahan
 * @property \Cake\ORM\Association\HasMany $Users
 */
class PetaniTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('petani');
        $this->displayField('nama');
        $this->primaryKey('id');

        $this->belongsTo('Poktan', [
            'foreignKey' => 'poktan_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Bumdes', [
            'foreignKey' => 'bumdes_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Ektp', [
            //'propertyName' => 'e_ktp',
            'foreignKey' => 'no_ektp',
            'joinType' => 'LEFT'
        ]);
        /*$this->hasMany('Lahan', [
            'foreignKey' => 'petani_id',
            'joinType'=>'INNER'
        ]);*/

        /*$this->hasMany('Rdkk', [
            'foreignKey' => 'petani_id'
        ]);*/

        $this->hasOne('Users', [
            'foreignKey' => 'petani_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('Rdkk', [
            'foreignKey' => 'petani_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('KomoditasPetani', [
            'foreignKey' => 'id_petani',
            'joinType' => 'LEFT'
        ]);

        $this->hasOne('KetuaPoktan', [
            'foreignKey' => 'ketua_poktan',
            'joinType' => 'LEFT',
            'className'=>'Poktan'
        ]);

    }

    public function listPetani(Query $query, array $options = [])
    {
        //debug($options); exit(' bye');
        $result = $query->select(['Petani.id', 'ektp.nama'])
                    ->innerJoin([ 'ektp' ], ['Petani.no_ektp = ektp.id'])
                    ->order(['ektp.nama' => 'ASC'])->combine('id', 'ektp.nama')->toArray();
        return $result;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        /*$validator
            ->requirePresence('no_tlp', 'create')
            ->notEmpty('no_tlp');*/

        /*$validator
            ->requirePresence('daerah_id', 'create')
            ->notEmpty('daerah_id');*/

        /*$validator
            ->requirePresence('photo', 'create')
            ->notEmpty('photo');*/

        $validator
            ->integer('no_ektp')
            ->requirePresence('no_ektp', 'create')
            ->notEmpty('no_ektp')
            ->add('no_ektp','unique',[
              'rule'=>'validateUnique',
              'provider'=>'table',
              'message'=>'no ktp anda sudah terdaftar, mohon periksa kembali'
            ]);

        return $validator;
    }

    public function afterSave($created, $options = array()) {

       if ($created) {
           $event = new Event('Model.Petani.created', $this, $options);
           $this->eventManager()->dispatch($event);
       }else{

         $event = new Event('Model.Petani.updated', $this, $options);
         $this->eventManager()->dispatch($event);
       }
   }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['poktan_id'], 'Poktan'));
        return $rules;
    }
}
