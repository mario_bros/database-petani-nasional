<?php
namespace App\Model\Table;

use App\Model\Entity\Asset;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Assets Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Lahans
 * @property \Cake\ORM\Association\BelongsTo $Status
 */
class AssetsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('assets');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Lahan', [
            'foreignKey' => 'lahan_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Status', [
            'foreignKey' => 'status_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nama_asset', 'create')
            ->notEmpty('nama_asset');

        $validator
            ->integer('jumlah')
            ->requirePresence('jumlah', 'create')
            ->notEmpty('jumlah');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        //$rules->add($rules->existsIn(['lahan_id'], 'Lahans'));
        $rules->add($rules->existsIn(['status_id'], 'Status'));
        return $rules;
    }
}
