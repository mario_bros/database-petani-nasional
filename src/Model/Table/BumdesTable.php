<?php
namespace App\Model\Table;

use App\Model\Entity\Bumde;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Bumdes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Daerah
 */
class BumdesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('bumdes');
        $this->displayField('nama');
        $this->primaryKey('id');

        $this->belongsTo('Daerah', [
            'foreignKey' => 'daerah_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('Petani', [
            'foreignKey' => 'bumdes_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Bps', [
            'foreignKey' => 'bps_id',
            'joinType' => 'INNER'
        ]);

        $this->hasOne('Users', [
            'foreignKey' => 'bumdes_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('Poktan', [
            'foreignKey' => 'bumdes_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('Gapoktan', [
            'foreignKey' => 'bumdes_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nama', 'create')
            ->notEmpty('nama');

        $validator
            ->requirePresence('alamat', 'create')
            ->notEmpty('alamat');

        $validator
            ->numeric('lat')
            ->requirePresence('lat', 'create')
            ->notEmpty('lat');

        $validator
            ->numeric('lon')
            ->requirePresence('lon', 'create')
            ->notEmpty('lon');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['daerah_id'], 'Daerah'));
        return $rules;
    }
}
