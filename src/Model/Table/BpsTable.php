<?php
namespace App\Model\Table;

use App\Model\Entity\Bp;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Bps Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Daerah
 */
class BpsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('bps');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Daerah', [
            'foreignKey' => 'daerah_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Ektp', [
            'foreignKey' => 'no_ktp',
            'joinType' => 'INNER'
        ]);

        $this->hasOne('Users', [
            'foreignKey' => 'bps_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('Bumdes', [
            'foreignKey' => 'bps_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Daerah', [
            'foreignKey' => 'daerah_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('no_ktp', 'create')
            ->notEmpty('no_ktp');

        $validator
            ->requirePresence('no_tlp', 'create')
            ->notEmpty('no_tlp');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['daerah_id'], 'Daerah'));
        return $rules;
    }
}
