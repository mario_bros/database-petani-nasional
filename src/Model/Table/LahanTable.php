<?php
namespace App\Model\Table;

use App\Model\Entity\Lahan;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Lahan Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Daerah
 * @property \Cake\ORM\Association\BelongsTo $JenisTanaman
 * @property \Cake\ORM\Association\BelongsTo $JenisLahan
 */
class LahanTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('lahan');
        $this->displayField('alamat');
        $this->primaryKey('id');

        $this->belongsTo('Daerah', [
            'foreignKey' => 'daerah_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('JenisTanaman', [
            'foreignKey' => 'jenis_tanaman_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('JenisLahan', [
            'foreignKey' => 'jenis_lahan_id',
            'joinType' => 'LEFT'
        ]);

        /*$this->belongsTo('Petani', [
            'foreignKey' => 'petani_id',
            'joinType' => 'INNER'
        ]);*/

        $this->belongsTo('Komoditas', [
            'foreignKey' => 'komoditas_id',
            'joinType' => 'LEFT',
            'propertyName' => 'komoditas'

        ]);

        $this->belongsTo('KomoditasPetani', [
            'foreignKey' => 'komoditas_petani_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('Rdkk', [
            'foreignKey' => 'lahan_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('Assets', [
            'foreignKey' => 'lahan_id',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('alamat', 'create')
            ->notEmpty('alamat');

        $validator
            ->requirePresence('jenis_lahan_id', 'create')
            ->notEmpty('jenis_lahan_id');

        $validator
            ->requirePresence('komoditas_id', 'create')
            ->notEmpty('komoditas_id');

        $validator
            ->requirePresence('daerah_id', 'create')
            ->notEmpty('daerah_id');

        $validator
            ->decimal('luas')
            ->requirePresence('luas', 'create')
            ->notEmpty('luas');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['daerah_id'], 'Daerah'));
        //$rules->add($rules->existsIn(['jenis_tanaman_id'], 'JenisTanaman'));
        //$rules->add($rules->existsIn(['petani_id'], 'Petani'));
        return $rules;
    }
}
