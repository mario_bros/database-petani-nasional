<?php
namespace App\Model\Table;

use App\Model\Entity\Gapoktan;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Gapoktan Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Daerah
 * @property \Cake\ORM\Association\HasMany $Poktan
 */
class GapoktanTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('gapoktan');
        $this->displayField('nama');
        $this->primaryKey('id');

        $this->belongsTo('Daerah', [
            'foreignKey' => 'daerah_id',
            'joinType' => 'LEFT'
        ]);
        $this->hasMany('Poktan', [
            'foreignKey' => 'gapoktan_id'
        ]);
        $this->belongsTo('Petani', [
            'foreignKey' => 'ketua_gapoktan',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Bumdes', [
            'foreignKey' => 'bumdes_id',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nama', 'create')
            ->notEmpty('nama');

        $validator
            ->requirePresence('alamat', 'create')
            ->notEmpty('alamat');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['daerah_id'], 'Daerah'));
        return $rules;
    }
}
