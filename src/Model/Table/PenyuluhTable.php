<?php
namespace App\Model\Table;

use App\Model\Entity\Penyuluh;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Penyuluh Model
 *
 * @property \Cake\ORM\Association\HasMany $Poktan
 */
class PenyuluhTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('penyuluh');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Poktan', [
            'foreignKey' => 'penyuluh_id'
        ]);

        $this->belongsTo('Ektp', [
            'foreignKey' => 'no_ektp',
            'joinType' => 'LEFT'
        ]);

        $this->hasOne('Users',[
           'foregnKey'=>'user_id',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('no_ektp', 'create')
            ->notEmpty('no_ektp');

        $validator
            ->allowEmpty('photo');

        return $validator;
    }
}
