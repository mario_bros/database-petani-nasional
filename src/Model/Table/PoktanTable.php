<?php
namespace App\Model\Table;

use App\Model\Entity\Poktan;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Poktan Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Daerah
 * @property \Cake\ORM\Association\BelongsTo $Gapoktan
 * @property \Cake\ORM\Association\BelongsTo $Penyuluhs
 * @property \Cake\ORM\Association\HasMany $Log
 * @property \Cake\ORM\Association\HasMany $Petani
 */
class PoktanTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('poktan');
        $this->displayField('nama');
        $this->primaryKey('id');

        $this->belongsTo('Daerah', [
            'foreignKey' => 'daerah_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Gapoktan', [
            'foreignKey' => 'gapoktan_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Penyuluh', [
            'foreignKey' => 'penyuluh_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('Log', [
            'foreignKey' => 'poktan_id'
        ]);
        $this->hasMany('Petani', [
            'foreignKey' => 'poktan_id',
            'propertyName'=>'petani',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('RekapRdkk', [
            'foreignKey' => 'poktan_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('KetuaPoktan', [
            'foreignKey' => 'ketua_poktan',
            'propertyName'=>'ketuaPoktan',
            'className'=>'Petani',
            'joinType' => 'LEFT'

        ]);

        $this->belongsTo('Bumdes', [
            'foreignKey' => 'bumdes_id',
            'joinType' => 'LEFT'

        ]);


    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nama', 'create')
            ->notEmpty('nama');

        $validator
            ->requirePresence('alamat', 'create')
            ->notEmpty('alamat');

        $validator
            ->numeric('lat')
            ->requirePresence('lat', 'create')
            ->notEmpty('lat');

        $validator
            ->numeric('lon')
            ->requirePresence('lon', 'create')
            ->notEmpty('lon');

        $validator
            ->decimal('aprroved')
            ->allowEmpty('aprroved');

        /*$validator
            ->integer('ketua_poktan')
            ->allowEmpty('ketua_poktan');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        //$rules->add($rules->existsIn(['daerah_id'], 'Daerah'));
        //$rules->add($rules->existsIn(['gapoktan_id'], 'Gapoktan'));
        //$rules->add($rules->existsIn(['penyuluh_id'], 'Penyuluh'));
        //$rules->add($rules->existsIn(['petani_id'], 'Petani'));
        return $rules;
    }
}
