<?php
namespace App\Model\Table;

use App\Model\Entity\JenisLahan;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * JenisLahan Model
 *
 */
class JenisLahanTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('jenis_lahan');
        $this->displayField('nama');
        $this->primaryKey('id');

        $this->belongsTo('Subsektor', [
            'foreignKey' => 'subsektor_id'
        ]);

        $this->hasMany('Lahan', [
            'foreignKey' => 'jenis_lahan_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nama', 'create')
            ->notEmpty('nama');

        return $validator;
    }
}
