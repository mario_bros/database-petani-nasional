<?php
namespace App\Model\Table;

use App\Model\Entity\Komoditas;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Komoditas Model
 *
 */
class KomoditasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('komoditas');
        $this->displayField('nama');
        $this->primaryKey('id');

        $this->belongsTo('Subsektor', [
            'foreignKey' => 'subsektor_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsToMany('Items', [
            'propertyName' => "Items",
            'joinTable' => 'items_komoditas',
            'dependent' => true,
            //'strategy' => "subquery",
            'targetForeignKey' => 'item_id',
            'foreignKey' => 'komoditas_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nama', 'create')
            ->notEmpty('nama');

        $validator
            ->integer('keterangan')
            ->allowEmpty('keterangan');

        return $validator;
    }
}
