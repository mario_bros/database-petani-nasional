<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $JenisUser
 * @property \Cake\ORM\Association\BelongsTo $Daerah
 * @property \Cake\ORM\Association\BelongsTo $Petanis
 * @property \Cake\ORM\Association\BelongsTo $Penyuluhs
 * @property \Cake\ORM\Association\HasMany $Log
 * @property \Cake\ORM\Association\HasMany $Notifications
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('username');
        $this->primaryKey('id');

        $this->belongsTo('JenisUser', [
            'foreignKey' => 'jenis_user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Bumdes', [
            'foreignKey' => 'bumdes_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Daerah', [
            'foreignKey' => 'daerah_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Petani', [
            'foreignKey' => 'petani_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Bps', [
            'foreignKey' => 'bps_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Penyuluh', [
            'foreignKey' => 'penyuluh_id',
            'joinType' => 'LEFT'
        ]);
        $this->hasMany('Log', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT'
        ]);
        $this->hasMany('Notifications', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('username', 'create')
            ->notEmpty('username')
            ->add('username','unique',[
              'rule'=>'validateUnique',
              'provider'=>'table',
              'message'=>'Username anda sudah terdaftar, mohon gunakan Username lain'
            ]);

        $validator
            ->requirePresence('email','create')
            ->notEmpty('email')
            ->add('email','validFormat',[
              'rule'=>'email',
              'message'=>'periksa kembali format email anda (email@yourdomain.com)'
            ])
            ->add('email','unique',[
              'rule'=>'validateUnique',
              'provider'=>'table',
              'message'=>'Email anda sudah terdaftar, mohon gunakan email lain'
            ]);

        $validator
          ->requirePresence('bumdes_id','create')
          ->notEmpty('bumdes_id');

        $validator
            ->requirePresence('nama', 'create')
            ->notEmpty('nama');

        $validator
            ->requirePresence('alamat', 'create')
            ->notEmpty('alamat');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->dateTime('created_date')
            ->requirePresence('created_date', 'create')
            ->notEmpty('created_date');

        /*$validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmpty('active');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->existsIn(['jenis_user_id'], 'JenisUser'));
        $rules->add($rules->existsIn(['daerah_id'], 'Daerah'));
       // $rules->add($rules->existsIn(['petani_id'], 'Petani'));
       // $rules->add($rules->existsIn(['penyuluh_id'], 'Penyuluh'));
        return $rules;
    }
}
