<?php
namespace App\Model\Table;

use App\Model\Entity\JenisPetani;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * JenisPetani Model
 *
 */
class JenisPetaniTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('jenis_petani');
        $this->displayField('nama_jenis_petani');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nama_jenis_petani', 'create')
            ->notEmpty('nama_jenis_petani');

        $validator
            ->allowEmpty('status');

        return $validator;
    }
}
