<?php
namespace App\Model\Table;

use App\Model\Entity\Rdkk;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Rdkk Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Petani
 * @property \Cake\ORM\Association\HasMany $TrRdkk
 */
class RdkkTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('rdkk');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Petani', [
            'foreignKey' => 'petani_id',
            'joinType' => 'INNER'
        ]);

		      $this->belongsTo('Komoditas', [
            'foreignKey' => 'id_komoditas',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Subsektor', [
            'foreignKey' => 'id_subsektor',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Lahan', [
            'foreignKey' => 'lahan_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('TrRdkk', [
            'foreignKey' => 'rdkk_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('RekapRdkk', [
            'foreignKey' => 'rdkk_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('tanggal')
            ->allowEmpty('tanggal');

        $validator
            ->integer('id_subsektor')
            ->allowEmpty('id_subsektor');

        $validator
            ->integer('id_komoditas')
            ->allowEmpty('id_komoditas');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['petani_id'], 'Petani'));
        return $rules;
    }
}
