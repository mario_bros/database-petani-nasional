<?php

namespace App\Event;

use Cake\Log\Log;
use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\View\Helper\SessionHelper;

class PetaniListener implements EventListenerInterface {

    public function implementedEvents() {
        return array(
            'Model.Petani.created' => 'notifyReviewer',
            'Model.Petani.updated' => 'notifyUser',
        );
    }

    public function sendMail($toMail,$subject,$body){
        $email = new Email('gmail');
      $sendmail =  @$email->from(['info@datapetani.com' => 'administrator'])
                          ->to($toMail)
                          ->subject($subject)
                          ->send($body);
        if(!empty($sendmail))
          return 1;
        else
          return 0;
    }

    //write to log table
    public function writeLog($data){
      //get log table
      $logTable = TableRegistry::get('PetaniLog');
      $log = $logTable->newEntity();
      $logData = [
        'user_id'=>$_SESSION['Auth']['User']['id'],
        'petani_id'=>$data->data['id'],
        'action'=>(!empty($data->data['alasan']))?$data->data['alasan']:"",
        'date'=>date('Y-m-d H:i:s')
      ];

      $log = $logTable->patchEntity($log, $logData, ['validate' => false]);
      if($logTable->save($log))
        return 1;
      else
        return 0;


    }

    public function notifyReviewer($event,  $entity, $options) {
         //notify reviewer when petani data was created
         $NotificationsTable = TableRegistry::get('Notifications');
         $notifications = $NotificationsTable->newEntity();
         $UsersTable = TableRegistry::get('Users');
         //get reviewer data

         $reviewer = $UsersTable->find('all')
                                 ->where([
                                   'bumdes_id'=>$event->data['bumdes_id']
                                 ])->first();

         //set notifications data
         $dataNotif = [
           'title'=>'Pengajuan data petani baru',
           'url'=>'/petani/view/'.$event->data['id'],
           'flag'=>0,
           'date'=>date('Y-m-d H:i:s'),
           'user_id'=>$reviewer['id'],
           'send_to_mail'=>0,
           'notes'=>'Pengajuan baru'
         ];
         $notifications = $NotificationsTable->patchEntity($notifications, $dataNotif, ['validate' => false]);
         $this->writeLog($event);
         //save Datasource
         //if($NotificationsTable->save($notifications)){
           //write log
           //$this->writeLog($event);
           //send mail
           //$this->sendMail($reviewer->email,'Data Petani Baru','Mohon untuk review data petani baru di');

         //}else{
           //Log::write(
           //'error',
           //'Error on saving notif ID for petani id: ' . $event->data['id']);
         //}

    }

    public function notifyUser($event,$entity, $options) {
      //notify reviewer when petani data was created
      echo "test"; exit;
      $NotificationsTable = TableRegistry::get('Notifications');
      $notifications = $NotificationsTable->newEntity();
      $UsersTable = TableRegistry::get('Users');


      //set notifications data
      $dataNotif = [
        'title'=>'Data Petani '.$event->data['status_label'],
        'url'=>'/petani/view/'.$event->data['id'],
        'flag'=>0,
        'date'=>date('Y-m-d H:i:s'),
        'user_id'=>$_SESSION['Auth']['User']['id'],
        'send_to_mail'=>0,
        'notes'=>$event->alasan
      ];
      $notifications = $NotificationsTable->patchEntity($notifications, $dataNotif, ['validate' => true]);

      //save Datasource
      if($NotificationsTable->save($notifications)){

        //write log
        $this->writeLog($event);
        //send mail
        //$this->sendMail($reviewer->email,'Data Petani '.$event->status_label,'Mohon untuk review data petani baru di ');

      }else{
      debug($notifications);
    }
  }
}
