<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Pupuk'), ['action' => 'edit', $pupuk->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Pupuk'), ['action' => 'delete', $pupuk->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pupuk->id)]) ?> </li>
<li><?= $this->Html->link(__('List Pupuk'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Pupuk'), ['action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
<li><?= $this->Html->link(__('Edit Pupuk'), ['action' => 'edit', $pupuk->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Pupuk'), ['action' => 'delete', $pupuk->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pupuk->id)]) ?> </li>
<li><?= $this->Html->link(__('List Pupuk'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Pupuk'), ['action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($pupuk->id) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Nama') ?></td>
            <td><?= h($pupuk->nama) ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($pupuk->id) ?></td>
        </tr>
    </table>
</div>

