<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $pupuk->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $pupuk->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Pupuk'), ['action' => 'index']) ?></li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $pupuk->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $pupuk->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Pupuk'), ['action' => 'index']) ?></li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($pupuk); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Pupuk']) ?></legend>
    <?php
    echo $this->Form->input('nama');
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
