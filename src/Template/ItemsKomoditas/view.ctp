<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Items Komodita'), ['action' => 'edit', $itemsKomodita->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Items Komodita'), ['action' => 'delete', $itemsKomodita->id], ['confirm' => __('Are you sure you want to delete # {0}?', $itemsKomodita->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Items Komoditas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Items Komodita'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Komoditas'), ['controller' => 'Komoditas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Komodita'), ['controller' => 'Komoditas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="itemsKomoditas view large-9 medium-8 columns content">
    <h3><?= h($itemsKomodita->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Item') ?></th>
            <td><?= $itemsKomodita->has('item') ? $this->Html->link($itemsKomodita->item->name, ['controller' => 'Items', 'action' => 'view', $itemsKomodita->item->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Komodita') ?></th>
            <td><?= $itemsKomodita->has('komodita') ? $this->Html->link($itemsKomodita->komodita->nama, ['controller' => 'Komoditas', 'action' => 'view', $itemsKomodita->komodita->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($itemsKomodita->id) ?></td>
        </tr>
    </table>
</div>
