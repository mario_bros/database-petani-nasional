<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('New User'), ['action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List JenisUser'), ['controller' => 'JenisUser', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Jenis User'), ['controller' => ' JenisUser', 'action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => ' Daerah', 'action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List Petani'), ['controller' => 'Petani', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Petani'), ['controller' => ' Petani', 'action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List Log'), ['controller' => 'Log', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Log'), ['controller' => ' Log', 'action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List Notifications'), ['controller' => 'Notifications', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Notification'), ['controller' => ' Notifications', 'action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List Penyuluh'), ['controller' => 'Penyuluh', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Penyuluh'), ['controller' => ' Penyuluh', 'action' => 'add']); ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-pills">' . $this->fetch('tb_actions') . '</ul>'); ?>
<!-- <div class="row">
  <div class="col-md-12">
    <a href="<?php echo $this->request->webroot ?>users/add" class="btn btn-success">Add User</a>  </div>
</div> -->

<div class="row">
    <div class="col-md-6 searchForm">
        <?php echo $this->Form->create($users); ?>
        <div class="col-md-4">
          <?php echo $this->Form->input('username',['required'=>0]); ?>
        </div>
        <div class="col-md-4">
          <?php echo $this->Form->input('email',['required'=>0]); ?>
        </div>

        <div class="col-md-4">
          <?php echo $this->Form->input('nama',['required'=>0]); ?>
        </div>

        <input type="Submit" value="cari" class="btn btn-primary col-md-4" />
        <?php echo $this->Form->end(); ?>
    </div>
</div>

<div class="table-responsive">
<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('no'); ?></th>
            <th><?= $this->Paginator->sort('username'); ?></th>

            <th><?= $this->Paginator->sort('created_date'); ?></th>
            <th><?= $this->Paginator->sort('jenis_user_id'); ?></th>
            <th><?= $this->Paginator->sort('active'); ?></th>
            <th><?= $this->Paginator->sort('daerah_id',['label'=>'Desa']); ?></th>
            <th class="actions"><?= __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 0; foreach ($users as $user): $no++; ?>
        <tr>
            <td><?= $no ?></td>
            <td><?= h($user->username) ?></td>

            <td><?= h($user->created_date) ?></td>
            <td>
                <?= $user->has('jenis_user') ? $this->Html->link($user->jenis_user->jenis, ['controller' => 'JenisUser', 'action' => 'view', $user->jenis_user->id]) : '' ?>
            </td>
            <td><?= h($user->active) ?></td>
            <td>
                <?= $user->has('bumde') ? $this->Html->link($user->bumde->daerah->nama, ['controller' => 'Daerah', 'action' => 'view', $user->bumde->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link('', ['action' => 'view', $user->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                <?= $this->Html->link('', ['action' => 'edit', $user->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
