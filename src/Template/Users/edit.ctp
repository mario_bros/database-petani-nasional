<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $user->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Jenis User'), ['controller' => 'JenisUser', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Jenis User'), ['controller' => 'JenisUser', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Petani'), ['controller' => 'Petani', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Petani'), ['controller' => 'Petani', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Log'), ['controller' => 'Log', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Log'), ['controller' => 'Log', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Notifications'), ['controller' => 'Notifications', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Notification'), ['controller' => 'Notifications', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Penyuluh'), ['controller' => 'Penyuluh', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Penyuluh'), ['controller' => 'Penyuluh', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $user->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Jenis User'), ['controller' => 'JenisUser', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Jenis User'), ['controller' => 'JenisUser', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Petani'), ['controller' => 'Petani', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Petani'), ['controller' => 'Petani', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Log'), ['controller' => 'Log', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Log'), ['controller' => 'Log', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Notifications'), ['controller' => 'Notifications', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Notification'), ['controller' => 'Notifications', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Penyuluh'), ['controller' => 'Penyuluh', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Penyuluh'), ['controller' => 'Penyuluh', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($user); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['User']) ?></legend>
    <?php
    echo $this->Form->input('username');
    echo $this->Form->input('password');
    echo $this->Form->input('nama');
    echo $this->Form->input('email');
    echo $this->Form->input('alamat',['type'=>'textarea']);   
    //echo $this->Form->input('created_date');
    if($this->request->session()->read('Auth.Users.jenis_user_id') == 1 ){
        echo $this->Form->input('jenis_user_id', ['options' => $jenisUser]);
        echo $this->Form->input('active');
    }
    echo $this->Form->input('bumdes_id', ['options' => $bumdes,'class'=>'select2','label'=>'Desa']);
    //echo $this->Form->input('bps_id', ['options' => $bps,'empty'=>'please select one']);
    //echo $this->Form->input('petani_id', ['options' => $petanis,'empty'=>'please select one']);
    //echo $this->Form->input('penyuluh_id', ['options' => $penyuluhs,'empty'=>'please select one']);
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
