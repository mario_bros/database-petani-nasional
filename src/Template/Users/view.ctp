<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
<li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Jenis User'), ['controller' => 'JenisUser', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Jenis User'), ['controller' => 'JenisUser', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Petani'), ['controller' => 'Petani', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Petani'), ['controller' => 'Petani', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Log'), ['controller' => 'Log', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Log'), ['controller' => 'Log', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Notifications'), ['controller' => 'Notifications', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Notification'), ['controller' => 'Notifications', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Penyuluh'), ['controller' => 'Penyuluh', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Penyuluh'), ['controller' => 'Penyuluh', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
<li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
<li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Jenis User'), ['controller' => 'JenisUser', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Jenis User'), ['controller' => 'JenisUser', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Petani'), ['controller' => 'Petani', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Petani'), ['controller' => 'Petani', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Log'), ['controller' => 'Log', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Log'), ['controller' => 'Log', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Notifications'), ['controller' => 'Notifications', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Notification'), ['controller' => 'Notifications', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Penyuluh'), ['controller' => 'Penyuluh', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Penyuluh'), ['controller' => 'Penyuluh', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($user->username) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Username') ?></td>
            <td><?= h($user->username) ?></td>
        </tr>
        <tr>
            <td><?= __('Password') ?></td>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <td><?= __('Jenis User') ?></td>
            <td><?= $user->has('jenis_user') ? $this->Html->link($user->jenis_user->jenis, ['controller' => 'JenisUser', 'action' => 'view', $user->jenis_user->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Daerah') ?></td>
            <td><?= $user->has('daerah') ? $this->Html->link($user->daerah->nama, ['controller' => 'Daerah', 'action' => 'view', $user->daerah->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Petani') ?></td>
            <td><?= $user->has('petani') ? $this->Html->link($user->petani->id, ['controller' => 'Petani', 'action' => 'view', $user->petani->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <td><?= __('Created Date') ?></td>
            <td><?= h($user->created_date) ?></td>
        </tr>
        <tr>
            <td><?= __('Active') ?></td>
            <td><?= $user->active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related Log') ?></h3>
    </div>
    <?php if (!empty($user->log)): ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Action') ?></th>
                <th><?= __('Date') ?></th>
                <th><?= __('Poktan Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($user->log as $log): ?>
                <tr>
                    <td><?= h($log->id) ?></td>
                    <td><?= h($log->user_id) ?></td>
                    <td><?= h($log->action) ?></td>
                    <td><?= h($log->date) ?></td>
                    <td><?= h($log->poktan_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link('', ['controller' => 'Log', 'action' => 'view', $log->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                        <?= $this->Html->link('', ['controller' => 'Log', 'action' => 'edit', $log->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                        <?= $this->Form->postLink('', ['controller' => 'Log', 'action' => 'delete', $log->id], ['confirm' => __('Are you sure you want to delete # {0}?', $log->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p class="panel-body">no related Log</p>
    <?php endif; ?>
</div>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related Notifications') ?></h3>
    </div>
    <?php if (!empty($user->notifications)): ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Url') ?></th>
                <th><?= __('Flag') ?></th>
                <th><?= __('Date') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Send To Mail') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($user->notifications as $notifications): ?>
                <tr>
                    <td><?= h($notifications->id) ?></td>
                    <td><?= h($notifications->title) ?></td>
                    <td><?= h($notifications->url) ?></td>
                    <td><?= h($notifications->flag) ?></td>
                    <td><?= h($notifications->date) ?></td>
                    <td><?= h($notifications->user_id) ?></td>
                    <td><?= h($notifications->send_to_mail) ?></td>
                    <td class="actions">
                        <?= $this->Html->link('', ['controller' => 'Notifications', 'action' => 'view', $notifications->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                        <?= $this->Html->link('', ['controller' => 'Notifications', 'action' => 'edit', $notifications->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                        <?= $this->Form->postLink('', ['controller' => 'Notifications', 'action' => 'delete', $notifications->id], ['confirm' => __('Are you sure you want to delete # {0}?', $notifications->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p class="panel-body">no related Notifications</p>
    <?php endif; ?>
</div>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related Penyuluh') ?></h3>
    </div>
    <?php if (!empty($user->penyuluh)): ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('No Ektp') ?></th>
                <th><?= __('Photo') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($user->penyuluh as $penyuluh): ?>
                <tr>
                    <td><?= h($penyuluh->id) ?></td>
                    <td><?= h($penyuluh->no_ektp) ?></td>
                    <td><?= h($penyuluh->photo) ?></td>
                    <td class="actions">
                        <?= $this->Html->link('', ['controller' => 'Penyuluh', 'action' => 'view', $penyuluh->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                        <?= $this->Html->link('', ['controller' => 'Penyuluh', 'action' => 'edit', $penyuluh->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                        <?= $this->Form->postLink('', ['controller' => 'Penyuluh', 'action' => 'delete', $penyuluh->id], ['confirm' => __('Are you sure you want to delete # {0}?', $penyuluh->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p class="panel-body">no related Penyuluh</p>
    <?php endif; ?>
</div>
