<?php $this->extend('../Layout/TwitterBootstrap/signin'); ?>


<h2 class="normal">Login ke Logistik Tani</h2>
          <p>Data tunggal petani, pusat informasi data agraria nasional<br></p>
          <p class="p-b-20">Login menggunakan username dan password yang telah diberikan</p>
		  </div>
		<div class="tiles grey p-t-20 p-b-20 text-black">
			<?= $this->Form->create(null,['class'=>'form-signin animated fadeIn','id'=>'frm_login']) ?>
                    <div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">
                      <div class="col-md-6 col-sm-6 ">

                        <?= $this->Form->input('username',['id'=>'login_username','class'=>'form-control','placeholder'=>'username']) ?>
                      </div>
                      <div class="col-md-6 col-sm-6">

                        <?= $this->Form->input('password',['id'=>'login_pass','class'=>'form-control','placeholder'=>'password']) ?>
                      </div>
                    </div>
                    <div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">
                    <div class="btn-group">
	                    <button type="submit" class="btn btn-primary btn-cons" id="login_toggle">Login</button>
                      <?= $this->Html->link(__('Daftar User Baru'),['controller'=>'users','action'=>'add'],['class'=>'btn btn-success']) ?>
                      <?= $this->Html->link(__('Download User Guide'),"/documentation/user_guide.pdf",['class'=>'btn btn-default']) ?>
                    </div>
	        		</div>

				<div class="row p-t-10 m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">
				  <div class="control-group  col-md-10">

				  </div>
				  </div>
				  <?= $this->Form->end() ?>

		</div>
