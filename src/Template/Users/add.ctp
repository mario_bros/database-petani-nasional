<?php
if($this->request->session()->read('Auth.User.jenis_user_id') == 1)
  $this->extend('../Layout/TwitterBootstrap/dashboard');
else
  $this->extend('../Layout/TwitterBootstrap/signin');

if(empty($this->request->session()->read('Auth.User.jenis_user_id'))){
?>

<ul class="nav nav-pills">

    <li><?= $this->Html->link(__('Kembali ke halaman login'),['controller'=>'Pages','action'=>'display']) ?></li>
</ul>

<?php } ?>

<?= $this->Form->create($user); ?>
<fieldset>
    <legend><?= __('Pendaftaran {0}', ['User']) ?></legend>
    <?php
    echo $this->Form->input('username',['label'=>'Username']);
    echo $this->Form->input('email',['label'=>'Email','maxlength'=>255]);
    echo $this->Form->input('nama',['label'=>'Nama Lengkap']);
    echo $this->Form->input('alamat',['label'=>'Alamat Lengkap','type'=>'textarea']);
    echo $this->Form->input('password');
    //echo $this->Form->hidden('last_login',['value'=>date('Y-m-d H:i:s')]);
    echo $this->Form->hidden('created_date',['value'=>date('Y-m-d H:i:s')]);
    if($this->request->session()->read('Auth.User.jenis_user_id')==1 &&
       !empty($this->request->session()->read('Auth.User.jenis_user_id')
       )
       ){

      echo $this->Form->input('jenis_user_id', ['options' => $jenisUser]);
    } else {
      echo $this->Form->hidden('jenis_user_id', ['value' => 3]);
    }
    echo $this->Form->hidden('active');
    echo $this->Form->input('bumdes_id', ['options' => $bumdes,'class'=>'select2','label'=>'Desa','empty'=>'Pilih Desa']);
    echo $this->Form->hidden('bps_id', ['options' => $bps]);
    echo $this->Form->hidden('petani_id', ['options' => $petanis,'empty'=>'please select one']);
    echo $this->Form->hidden('penyuluh_id', ['label'=>'Pegawai BPS','options' => $penyuluhs,'empty'=>'please select one']);
    ?>
</fieldset>
<?= $this->Form->button(__("Save"),['class'=>'btn btn-primary col-md-12']); ?>
<?= $this->Form->end() ?>
