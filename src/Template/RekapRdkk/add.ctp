<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('List Rekap Rdkk'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Aprroves'), ['controller' => 'Aprroves', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Aprrove'), ['controller' => 'Aprroves', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Rekap Log'), ['controller' => 'RekapLog', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Rekap Log'), ['controller' => 'RekapLog', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?= $this->Html->link(__('List Rekap Rdkk'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Aprroves'), ['controller' => 'Aprroves', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Aprrove'), ['controller' => 'Aprroves', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Rekap Log'), ['controller' => 'RekapLog', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Rekap Log'), ['controller' => 'RekapLog', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($rekapRdkk); ?>
<fieldset>
    <legend><?= __('Add {0}', ['Rekap Rdkk']) ?></legend>
    <?php
    echo $this->Form->input('created_date');
    echo $this->Form->input('status');
    echo $this->Form->input('approved_date');
    echo $this->Form->input('daerah_id', ['options' => $daerah]);
    ?>
</fieldset>
<?= $this->Form->button(__("Add")); ?>
<?= $this->Form->end() ?>
