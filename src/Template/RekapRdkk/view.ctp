<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Rekap Rdkk'), ['action' => 'edit', $rekapRdkk->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Rekap Rdkk'), ['action' => 'delete', $rekapRdkk->id], ['confirm' => __('Are you sure you want to delete # {0}?', $rekapRdkk->id)]) ?> </li>
<li><?= $this->Html->link(__('List Rekap Rdkk'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Rekap Rdkk'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Aprroves'), ['controller' => 'Aprroves', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Aprrove'), ['controller' => 'Aprroves', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Rekap Log'), ['controller' => 'RekapLog', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Rekap Log'), ['controller' => 'RekapLog', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
<li><?= $this->Html->link(__('Edit Rekap Rdkk'), ['action' => 'edit', $rekapRdkk->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Rekap Rdkk'), ['action' => 'delete', $rekapRdkk->id], ['confirm' => __('Are you sure you want to delete # {0}?', $rekapRdkk->id)]) ?> </li>
<li><?= $this->Html->link(__('List Rekap Rdkk'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Rekap Rdkk'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Aprroves'), ['controller' => 'Aprroves', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Aprrove'), ['controller' => 'Aprroves', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Rekap Log'), ['controller' => 'RekapLog', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Rekap Log'), ['controller' => 'RekapLog', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($rekapRdkk->id) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Daerah') ?></td>
            <td><?= $rekapRdkk->has('daerah') ? $this->Html->link($rekapRdkk->daerah->nama, ['controller' => 'Daerah', 'action' => 'view', $rekapRdkk->daerah->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($rekapRdkk->id) ?></td>
        </tr>
        <tr>
            <td><?= __('Status') ?></td>
            <td><?= $this->Number->format($rekapRdkk->status) ?></td>
        </tr>
        <tr>
            <td><?= __('Created Date') ?></td>
            <td><?= h($rekapRdkk->created_date) ?></td>
        </tr>
        <tr>
            <td><?= __('Approved Date') ?></td>
            <td><?= h($rekapRdkk->approved_date) ?></td>
        </tr>
    </table>
</div>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related Aprroves') ?></h3>
    </div>
    <?php if (!empty($rekapRdkk->aprroves)): ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Date') ?></th>
                <th><?= __('Note') ?></th>
                <th><?= __('Rekap Rdkk Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($rekapRdkk->aprroves as $aprroves): ?>
                <tr>
                    <td><?= h($aprroves->id) ?></td>
                    <td><?= h($aprroves->user_id) ?></td>
                    <td><?= h($aprroves->date) ?></td>
                    <td><?= h($aprroves->note) ?></td>
                    <td><?= h($aprroves->rekap_rdkk_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link('', ['controller' => 'Aprroves', 'action' => 'view', $aprroves->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                        <?= $this->Html->link('', ['controller' => 'Aprroves', 'action' => 'edit', $aprroves->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                        <?= $this->Form->postLink('', ['controller' => 'Aprroves', 'action' => 'delete', $aprroves->id], ['confirm' => __('Are you sure you want to delete # {0}?', $aprroves->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p class="panel-body">no related Aprroves</p>
    <?php endif; ?>
</div>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related RekapLog') ?></h3>
    </div>
    <?php if (!empty($rekapRdkk->rekap_log)): ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Action') ?></th>
                <th><?= __('Date') ?></th>
                <th><?= __('Rekap Rdkk Id') ?></th>
                <th><?= __('Comment') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($rekapRdkk->rekap_log as $rekapLog): ?>
                <tr>
                    <td><?= h($rekapLog->id) ?></td>
                    <td><?= h($rekapLog->user_id) ?></td>
                    <td><?= h($rekapLog->action) ?></td>
                    <td><?= h($rekapLog->date) ?></td>
                    <td><?= h($rekapLog->rekap_rdkk_id) ?></td>
                    <td><?= h($rekapLog->comment) ?></td>
                    <td class="actions">
                        <?= $this->Html->link('', ['controller' => 'RekapLog', 'action' => 'view', $rekapLog->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                        <?= $this->Html->link('', ['controller' => 'RekapLog', 'action' => 'edit', $rekapLog->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                        <?= $this->Form->postLink('', ['controller' => 'RekapLog', 'action' => 'delete', $rekapLog->id], ['confirm' => __('Are you sure you want to delete # {0}?', $rekapLog->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p class="panel-body">no related RekapLog</p>
    <?php endif; ?>
</div>
