<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('New Rekap Rdkk'), ['action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => ' Daerah', 'action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List Aprroves'), ['controller' => 'Aprroves', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Aprrove'), ['controller' => ' Aprroves', 'action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List RekapLog'), ['controller' => 'RekapLog', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Rekap Log'), ['controller' => ' RekapLog', 'action' => 'add']); ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>

<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id'); ?></th>
            <th><?= $this->Paginator->sort('created_date'); ?></th>
            <th><?= $this->Paginator->sort('status'); ?></th>
            <th><?= $this->Paginator->sort('approved_date'); ?></th>
            <th><?= $this->Paginator->sort('daerah_id'); ?></th>
            <th class="actions"><?= __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($rekapRdkk as $rekapRdkk): ?>
        <tr>
            <td><?= $this->Number->format($rekapRdkk->id) ?></td>
            <td><?= h($rekapRdkk->created_date) ?></td>
            <td><?= $this->Number->format($rekapRdkk->status) ?></td>
            <td><?= h($rekapRdkk->approved_date) ?></td>
            <td>
                <?= $rekapRdkk->has('daerah') ? $this->Html->link($rekapRdkk->daerah->nama, ['controller' => 'Daerah', 'action' => 'view', $rekapRdkk->daerah->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link('', ['action' => 'view', $rekapRdkk->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                <?= $this->Html->link('', ['action' => 'edit', $rekapRdkk->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $rekapRdkk->id], ['confirm' => __('Are you sure you want to delete # {0}?', $rekapRdkk->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>