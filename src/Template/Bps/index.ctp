<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('New Bp'), ['action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => ' Daerah', 'action' => 'add']); ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>

<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id'); ?></th>
            <th><?= $this->Paginator->sort('no_ektp'); ?></th>
            <th><?= $this->Paginator->sort('no_telp'); ?></th>
            <th><?= $this->Paginator->sort('daerah_id'); ?></th>
            <th class="actions"><?= __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($bps as $bp): ?>
        <tr>
            <td><?= $this->Number->format($bp->id) ?></td>
            <td><?= h($bp->no_ktp) ?></td>
            <td><?= h($bp->no_tlp) ?></td>
            <td>
                <?= $bp->has('daerah') ? $this->Html->link($bp->daerah->nama, ['controller' => 'Daerah', 'action' => 'view', $bp->daerah->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link('', ['action' => 'view', $bp->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                <?= $this->Html->link('', ['action' => 'edit', $bp->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $bp->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bp->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>