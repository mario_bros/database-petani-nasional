<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Bp'), ['action' => 'edit', $bp->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Bp'), ['action' => 'delete', $bp->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bp->id)]) ?> </li>
<li><?= $this->Html->link(__('List Bps'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Bp'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
<li><?= $this->Html->link(__('Edit Bp'), ['action' => 'edit', $bp->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Bp'), ['action' => 'delete', $bp->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bp->id)]) ?> </li>
<li><?= $this->Html->link(__('List Bps'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Bp'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($bp->id) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('No Ktp') ?></td>
            <td><?= h($bp->no_ktp) ?></td>
        </tr>
        <tr>
            <td><?= __('No Tlp') ?></td>
            <td><?= h($bp->no_tlp) ?></td>
        </tr>
        <tr>
            <td><?= __('Daerah') ?></td>
            <td><?= $bp->has('daerah') ? $this->Html->link($bp->daerah->nama, ['controller' => 'Daerah', 'action' => 'view', $bp->daerah->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($bp->id) ?></td>
        </tr>
    </table>
</div>

