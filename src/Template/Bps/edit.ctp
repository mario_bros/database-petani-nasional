<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $bp->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $bp->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Bps'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $bp->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $bp->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Bps'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($bp); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Bp']) ?></legend>
    <?php
    echo $this->Form->input('no_ktp');
    echo $this->Form->input('no_tlp');
    echo $this->Form->input('daerah_id', ['options' => $daerah]);
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
