<?php
    $this->extend('../Layout/TwitterBootstrap/dashboard');
?>
<?= $this->Html->link(__('Kembali ke list RKP'), ['action' => 'listrkppoktan', $dataPoktan->id],['class'=>'btn btn-default']) ?>
<div class="row">
    <div class="col-md-12 text-center">
    <h3>Rencana Kebutuhan Petani (RKP) <span class="label label-<?php echo $class ?>"><?php echo $status ?></span></h3>
    <!--<h5>Pupuk Bersubsidi</h5>-->
    </div>
</div>

<?php if($status == 'Ditolak'){ ?>
    <div class="row">
        <div class="col-md-12 alert alert-danger"><strong>Alasan Penolakan :</strong><?php
                $lastLog = end($dataPoktan->rekap_rdkk[0]->rekap_log);
                echo $lastLog['comment'];
            ?>
        </div>
    </div>
<?php } ?>

<div class="row">
    <div class="col-md-6 pull-left">
        <table>
            <tr>
                <td>Tanggal</td>
                <td>:</td>
                <td><?= date('d-m-Y') ?></td>
            </tr>
            <tr>
                <td>Bumdes</td>
                <td>:</td>
                <td><?= strtoupper( $dataPoktan->bumde->nama ) ?></td>
            </tr>
            <tr>
                <td>Gapoktan</td>
                <td>:</td>
                <td><?= strtoupper($dataPoktan->gapoktan->nama) ?></td>
            </tr>
            <tr>
                <td>Poktan</td>
                <td>:</td>
                <td><?= strtoupper($dataPoktan->nama) ?></td>
            </tr>
            <tr>
                <td>Masa Tanam</td>
                <td>:</td>
                <td><?= $masaTanam ?></td>
            </tr>
        </table>
    </div>

    <div class="col-md-6 pull-right">
        <table>
            <tr>
                <td>Provinsi</td>
                <td>:</td>
                <td><?= h(strtoupper($dataPoktan->daerah->parent_daerah->grand_parent_daerah->third_level_parent_daerah->nama)) ?></td>
            </tr>
            <tr>
                <td>Kota</td>
                <td>:</td>
                <td><?= h(strtoupper($dataPoktan->daerah->parent_daerah->grand_parent_daerah->nama)) ?></td>
            </tr>
            <tr>
                <td>Kecamatan</td>
                <td>:</td>
                <td><?= h(strtoupper($dataPoktan->daerah->parent_daerah->nama)) ?></td>
            </tr>
            <tr>
                <td>Desa</td>
                <td>:</td>
                <td><?= strtoupper($dataPoktan->daerah->nama) ?></td>
            </tr>
        </table>
    </div>
</div>


<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?> </button>

<div class="table-responsive">
    
    <table class="table">
        <thead>
            <tr>
                <th rowspan="3" style="vertical-align: middle;text-align: center; border: 1px solid;"><?= __('No') ?></th>
                <th rowspan="3" style="vertical-align: middle;text-align: center; border: 1px solid;"><?= __('Nama Petani') ?></th>
                <th rowspan="3" style="vertical-align: middle;text-align: center; border: 1px solid;"><?= __('Lokasi Lahan') ?></th>
                <th rowspan="3" style="vertical-align: middle;text-align: center; border: 1px solid;"><?= __('Milik / Garap') ?></th>
                <th rowspan="3" style="vertical-align: middle;text-align: center; border: 1px solid;"><?= __('Luas Tanam (Ha)') ?></th>
                <?php
                foreach($dataCategory as $category):
                ?>
                    <th colspan="<?= count($category->items) * 2 ?>" style="text-align: center; border: 1px solid;" >
                        <?php 
                        if ($category->name == "Pestisida") 
                            $metrik = "mL";
                        else
                            $metrik = "Kg";
                        echo $category->name . " (" . $metrik . ") ";
                        ?>
                    </th>
                <?php 
                endforeach; 
                ?>
            </tr>
            <tr>
                <?php foreach($dataItems as $item) : ?>
                    <th colspan="2" style="text-align: center; border: 1px solid;"><?= $item['name'] ?></th>
                <?php endforeach; ?>
            </tr>
            <tr>
                <?php
                foreach ($aliasItemCollections as $identifier) :

                    $totalItemSemuaPoktan[$identifier] = 0;
                    if ( strpos($identifier, "harga_") === false ) :
                        if ($identifier === "Insektisida" || $identifier === "Herbisida") {
                            $fieldPerMasaTanam = "(mL)";
                        }else{$fieldPerMasaTanam = "(Kg)";}                        
                    ?>
                        <th style="text-align: center; border: 1px solid;"><?= $fieldPerMasaTanam ?></th>
                    <?php 
                    else :
                        
                        $fieldPerTotalMasaTanam = "(Rp)";
                    ?>
                        <th style="text-align: center; border: 1px solid;"><?= $fieldPerTotalMasaTanam; ?></th>
                    <?php 
                    endif; 
                    ?>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <?php 
            $no = $totalLahan = 0;
            foreach ( $hasilRekap as $rekap ) : 
                $no++;
            ?>
            <tr> <!-- Baris untuk jumlah -->
                <td>
                    <?= $no ?>
                </td>
                <td>
                    <?= $rekap['nama_petani'] ?>
                </td>
                <td>
                    <?php
                    echo strtoupper( $rekap['alamat_lahan'] );
                    ?>
                </td>
                <td>
                    <?php echo strtoupper( $rekap['nama_jenis_lahan'] ); ?>
                </td>
                <td>
                    <?php 
                    $totalLahan += $rekap['luas_lahan'];
                    echo $rekap['luas_lahan'];  
                    ?>
                </td>
                
                <?php 
                //debug($aliasItemCollections); exit;
                //debug($totalItemSemuaPoktan); exit;
                //debug($rekap); exit;
                
                foreach($aliasItemCollections as $aliasItem) : 
                //debug( $aliasItemCollections ); exit;
                ?>
                    <td>
                        <?php
                        $totalItemSemuaPoktan[$aliasItem] += $rekap[$aliasItem];
                        echo number_format($rekap[$aliasItem]);
                        ?>
                    </td>
                <?php endforeach; ?>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr> <!-- Baris untuk jumlah -->
                <td colspan="4" style="text-align: center">
                    <?= "Total" ?>
                </td>
                <td  >
                    <?= $totalLahan ?>
                </td>
                <?php foreach ($aliasItemCollections as $aliasItem) : ?>
                <td>
                    <?= number_format($totalItemSemuaPoktan[$aliasItem]) ?>
                </td>
                <?php endforeach; ?>
            </tr>
        </tfoot>
    </table>
</div>


<div class="row">
    <?php
    if($this->request->session()->read('Auth.User.jenis_user_id')==3 && $dataPoktan->rekap_rdkk[0]->status==0) :
        echo $this->Html->link(__("Ajukan Persetujuan"), ['controller' => 'poktan', 'action' => 'processReview', $dataPoktan->id, 1, $dataPoktan->rekap_rdkk[0]->id], ['class' => 'btn btn-warning']);
    elseif($this->request->session()->read('Auth.User.jenis_user_id')==2 ) :?>
        <div class="btn-group" role="group" aria-label="...">
            <a href="<?= $this->Url->build([
                'controller'=>'Poktan',
                'action'=>'processReview',
                $dataPoktan->id,2,$dataPoktan->rekap_rdkk[0]->id
            ]); ?>" class="btn btn-success">Setujui</a>
            <a class="btn btn-danger" data-toggle="modal"  data-target="#rejectModal">Tolak</a>

        </div>
    <?php endif ?>
</div>
<?php $this->start('modalBox'); ?>
<div id="rejectModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <?= $this->Form->create(null,[
                    'name'=>'postPoktan',
                    'url'=>[
                    'controller'=>'poktan',
                    'action'=>'processReview',$dataPoktan->id,3,$dataPoktan->rekap_rdkk[0]->id
                ]
            ]); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mohon Sebutkan Alasan Penolakan</h4>
            </div>
            <div class="modal-body">
                <!--<p>&nbsp;</p>-->
                <?= $this->Form->input('alasan', ['type' => "textarea"]);?>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <?= $this->Form->button(__("Submit"), ['class'=>'btn btn-info', 'name' => "Disapproved"]); ?>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <?= $this->Form->end(); ?>
        </div>

    </div>
</div>
<?php $this->end(); ?>
<?php $this->assign('modal',$this->fetch('modalBox')); ?>
