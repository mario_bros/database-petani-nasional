<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
?>
<h2>RKP Kopdes <STRONG><?= $this->request->session()->read('Bumdes.nama') ?></STRONG></h2>

<div class="row">
    <div class="col-md-12 pull-right hide" id="secondNav">
        <ul class="nav nav-pills">
            <?php
            foreach ( $masaTanamArr as $masaTanam ) :

                $status = " ";
                foreach ( $dataPoktan->rekap_rdkk as $rekapRdkk ) :

                    if ( $rekapRdkk->masa_tanam == $masaTanam ) :

                        switch ( $rekapRdkk->status ) :
                            case 1:
                                $status = "Pengajuan";
                                break;
                            case 2:
                                $status = "Disetujui";
                                break;
                            case 3:
                                $status = "Ditolak";
                                break;

                            default:
                                $status = " ";
                                break;
                        endswitch;

                    endif;

                endforeach;
            ?>
                <li><?= $this->Html->link(__('Rekap RKP Masa Tanam ' . $masaTanam . ' ' . $status), ['action' => 'rekappoktan', $dataPoktan->id, $masaTanam]); ?></li>
            <?php
            endforeach
            ?>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-md-6 searchForm">
        <?php echo $this->Form->create($rdkk); ?>
        <?php
            echo $this->Form->input('NIK');
            echo $this->Form->input('nama');
          //  echo $this->Form->input('daerah',['template'=>'horizontal']);
        ?>
        <input type="Submit" value="search" class="btn btn-primary" />
        <?php echo $this->Form->end(); ?>
    </div>
</div>


<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>
<div class="table-responsive">
<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('no') ?></th>
            <th><?= $this->Paginator->sort('nama_petani') ?></th>
            <th><?= $this->Paginator->sort('masa_tanam') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 0;
        //debug( $rdkk ); exit;
        //$uniqRdkkByPetanis = $this->Custom->arrayUniqueByValue( $rdkk, 'petani_id' ); //debug( $uniqRdkkByPetanis ); exit;
        foreach ($rdkk as $item):
            $no++;
            //debug($item); exit;
        ?>
            <tr>
                <td><?= $no ?></td>
                <td><?= h($item->ektp->nama) ?></td>
                <td>
                  <?php if(!empty($item->rdkk)){ ?>
                  <div class="btn-group"> <a class="btn btn-success dropdown-toggle btn-demo-space" data-toggle="dropdown" href="#"> List RKP <span class="caret"></span> </a>
                    <ul class="dropdown-menu">

                    <?php
                       $countMasaTanam = 0;
                       foreach($item->rdkk as $val) :
                           $countMasaTanam++;
                           ?>
                                  <li> <a  href="<?= $this->Url->build(['action'=>'view',$val->id]) ?>"><?= 'Lahan : '.$val->lahan['alamat'].' Masa Tanam ' . $val->masa_tanam; ?></a></li>
                           <?php
                       endforeach;
                   ?>

                 </ul>
                  </div>
                  <?php } ?>
                </td>
                <td class="actions">
                    <?php //= ($countMasaTanam < 3) ? $this->Html->link('TAMBAH RKP', ['action' => 'add', $item->id], ['class'=>'btn btn-primary']) : ""; ?>
                    <div class="">
                        <?php //$this->Url->build('/posts', true); $this->requestAction('/rdkk/getAvailableOptions/' . $item->id )?>
                        <?php echo $this->Html->link(__("TAMBAH RKP"),['controller'=>'petani','action'=>'view',$item['id']] ,['class'=>'btn btn-primary']); ?>
                        <?php // $this->Form->button(__("TAMBAH RKP"), ['class'=>'btn btn-primary load-remote', 'type' => "button", 'data-toggle' => "modal", 'data-load-remote' => $this->Url->build('/rdkk/getAvailableOptions/' . $item->id), 'data-target' => "#RkpFilteringModal"]); ?>
                        <br/><br/>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>

</table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>

<div id="RkpFilteringModal" class="modal fade" role="dialog" data-backdrop="">
    <div class="modal-dialog" style="margin-top: 10%">

        <!-- Modal content-->
        <div class="modal-content" >
        </div>

    </div>
</div>


<?php $this->Html->scriptStart(['block' => true]); ?>
    $('.load-remote').on('click',function(e) {
        e.preventDefault();
        var $this = $(this);

        //console.log($this.data('load-remote'));

        var remote = $this.data('load-remote');

        if(remote) {

            $("#RkpFilteringModal .modal-content").load(remote);
        }
    });

<?php $this->Html->scriptEnd(); ?>
