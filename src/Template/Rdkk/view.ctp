<?php
    $this->extend('../Layout/TwitterBootstrap/dashboard');
?>
<div class="col-md-12">
    <label class="control-label">Tanggal RKP : <?= h($dataHeader->tanggal->format('d-m-Y')) ?> </label>

</div>

<div class="col-md-4">
    <label class="control-label">Nama Petani : <strong><?= h($dataHeader->petani->ektp['nama']) ?></strong></label>

</div>

<div class="col-md-4">
    <label class="control-label">Alamat : <strong><?= h($dataHeader->petani->ektp['alamat']) ?> </strong></label>


</div>

<!-- <div class="col-md-4">
    <label class="control-label">Nama Poktan : <strong><?= h($dataHeader->petani->poktan['nama']) ?> </strong></label>

</div> -->

<div class="col-md-4">
    <label class="control-label">NIK : <label class="label label-info"><?= h($dataHeader->petani->ektp['id']) ?></label></label>
</div>

<div class="col-md-4">
    <label class="control-label">Subsektor : <?= h($dataHeader->lahan->komoditas->subsektor->nama) ?></label>

</div>

<div class="col-md-4">
    <label class="control-label">Komoditas :  <?= h($dataHeader->lahan->komoditas->nama) ?> </label>

    <br/><br/>
</div>


<div class="col-md-12">
    <label class="control-label">Masa Tanam : <span class="label label-success"><?= h($dataHeader->masa_tanam) ?></span></label>

</div>



<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?> </button>
<div class="col-md-12 table-responsive" style="display: -webkit-inline-box;">
<table class="table table-striped">
    <thead>
        <tr>
            <th rowspan="2" style="text-align: center; border: 1px solid;"><?= __('Luas Lahan (Ha)') ?></th>
            <?php foreach($dataCategory as $categoryName => $count): ?>
                <th colspan="<?= count($count) ?>" style="text-align: center; border: 1px solid;">
                    <?php
                    if ($categoryName == "Pestisida")
                        $metrik = "mL";
                    else
                        $metrik = "Kg";

                    echo $categoryName . " (" . $metrik . ") ";
                    ?>
                </th>
            <?php endforeach ?>
        </tr>
        <tr>
            <?php foreach($dataItem as $itemName => $val):
                if ( strpos($itemName,'Harga') === false ) :
            ?>
                    <th style="text-align: center; border: 1px solid;">
                        <?php echo $itemName ?>
                    </th>
            <?php
                endif;
            endforeach;
            ?>
        </tr>
    </thead>

    <tbody>
        <?php
        //mulai foreach dari detail rdkk
        foreach ($collectionDetails as $row):
            $itemCollections = $row;
            $itemCollections = array_slice($row, 2);
        ?>
            <!-- Baris data trRdkk -->
            <tr>
                <td>
                    <?php echo $row['Lahan_alamat'] ?>
                </td>
                <?php
                $totalKg = 0;
                foreach ($itemCollections as $key => $jumlah):
                    if ( strpos($key,'Harga') === false && $key!='lahan_id' && strpos($key,'_harga') === false ) :
                ?>
                        <td class="text-center">
                            <?php
                            $totalKg += $jumlah;
                            echo $jumlah;
                            ?>
                        </td>
                <?php
                    endif;
                endforeach;
                ?>
            </tr>
        <?php endforeach; ?>

        <tr> <!-- Baris untuk jumlah -->
            <td>
                <?= $this->Form->label(__('Jumlah', ['class'=>'control-label'])) ?>
            </td>

            <?php
            $total=0;
            foreach ($itemCollections as $key => $jumlah):
                if( strpos($key,'Harga_') === false && $key!='lahan_id' ) :
            ?>
                    <td class="text-center">
                        <?php
                        if ( !empty($itemCollections['Harga_'.$key]) ) : //pengecekan utk column total harga
                            //$harga = $jumlah * $itemCollection['Harga_'.$key];
                            //$total = $total + $jumlah * $itemCollection['Harga_'.$key]; //sebelumnya di remark dulu karena jumlahnya beda

                            $harga = $jumlah * 1;
                            echo number_format($harga);
                            $total += $harga;
                        endif;
                        ?>
                    </td>
            <?php
                endif;
            endforeach;
            ?>
        </tr>
    </tbody>
</table>
</div>

<div class="row">
    <div class="col-sm-1">
    <!--<?= $this->Form->button(__("Rekap RDKK"), ['class'=>'btn btn-primary']); ?>-->
        <?= $this->Html->link('Edit', ['action' => 'edit', $dataHeader->petani_id, $dataHeader->id, $dataHeader->masa_tanam], ['class' => 'btn btn-warning']) ?>
        <?php //$this->Html->link('Edit', ['action' => 'edit',$dataHeader->petani_id,$dataHeader->id], ['class' => 'btn btn-warning']) ?>
    </div>
</div>
