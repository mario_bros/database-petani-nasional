<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
?>
    <h2>RKP</h2>
    <div class="col-md-12">
        <label class="control-label">Data RKP Poktan</label>
        <label class="control-label">:</label>

        <?php /*if($this->request->session()->read('Auth.User.jenis_user_id') == 2) : ?>
            <label class="control-label"><?= h($dataPoktan->nama) ?></label>
        <?php elseif($this->request->session()->read('Auth.User.jenis_user_id') == 3) : ?>
            <label class="control-label"><?= h($namaPenyuluh) ?></label>
        <?php endif; */?>

    

        <?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>
        <div class="table-responsive">
            <table class="table table-striped" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th class="hideme"><?= $this->Paginator->sort('no') ?></th>
                        <?php if($this->request->session()->read('Auth.User.jenis_user_id') == 2) : ?>
                                <th><?= $this->Paginator->sort('nama_petani') ?></th>
                        <?php elseif($this->request->session()->read('Auth.User.jenis_user_id') == 3) : ?>
                                <th><?= $this->Paginator->sort('nama_poktan') ?></th>
                        <?php endif; ?>

                        <th class="hideme"><?= $this->Paginator->sort('daerah_id',['label'=>'Desa']) ?></th>

                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>

                    <?php if($this->request->session()->read('Auth.User.jenis_user_id') == 3 || $this->request->session()->read('Auth.User.jenis_user_id') == 2) : ?>

                        <?php $no = 0; foreach ($listPoktan as $poktan): $no++; ?>

                            <tr>
                                <td class="hideme"><?= $no ?></td>
                                <td><?= h(strtoupper($poktan->nama)) ?></td>
                                <td class="hideme"><?= h($poktan->daerah->nama) ?></td>
                                
                                <td class="actions">
                                    <?= $this->Html->link('List RKP', ['action' => 'listrkppoktan', $poktan->id], ['class'=>'btn btn-primary']); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>

                    <?php elseif($this->request->session()->read('Auth.User.jenis_user_id') == 5) : ?>
                        <?php $no = 0; foreach ($listDaerah as $daerah): $no++; ?>
                            <tr>
                                <td class="hideme"><?= $no ?></td>
                                <td><?= h($daerah->nama) ?></td>
                                <td class="hideme"><?= h($daerah->jenis_daerah->nama) ?></td>
                                <td></td>
                                <td class="actions">

                                    <?= $this->Html->link('Rekap Kecamatan', ['action' => 'rekapkecamatan', $daerah->id], ['class'=>'btn btn-primary']); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>

                </tbody>
            </table>
        </div>
    </div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>