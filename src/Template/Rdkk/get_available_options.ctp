<?php
if ( $emptyTrxOpts == false ) :

    if ( $emptyLahanOpts == false ) :

        echo $this->Form->create(null, ['url' => ['controller' => "rdkk", 'action' => "add", $petaniId], 'type' => "get"]);
?>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Mohon Pilih Lahan dan Masa Tanam untuk petani ini</h4>
        </div>
        <div class="modal-body">
            <?= $this->Form->input('lahan_id', ['options' => $optsLahan, 'id' => "lahanOptions", 'empty' => "Pilih Lahan"]);?>
            <?= $this->Form->input('masa_tanam', ['options' => [], 'id' => "masaTanamOptions", 'empty' => "Pilih Masa Tanam"]);?>
        </div>
        <div class="modal-footer">
            <div class="btn-group">
                <?= $this->Form->button(__("Submit"), ['class'=>'btn btn-info']); ?>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <?= $this->Form->end(); ?>

        <script>
            $(document).ready(function () {
                $("#lahanOptions").change(function (){ //change event for select
                    $.ajax({  //ajax call
                        type: "POST", 
                        url: "<?= $this->Url->build('/rdkk/renewMasaTanamOptions/') ?>", //url to be called
                        data: { lahan_id:  $("#lahanOptions").val(), petani_id: <?= $petaniId?>}, //data to be send
                        success: function (data) {
                            $("#masaTanamOptions").html(data);
                        }
                    });
                });

                //alert('hy');
            });
        </script>
<?php
    else :
?>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">PERHATIAN !!!</h4>
        </div>
        <div class="modal-body">
            <p>Semua Lahan dan Masa Tanam untuk <?= $namaPetani ?> sudah ada datanya.</p>
        </div>
        <div class="modal-footer">
            <div class="btn-group">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
<?php
    endif;

else :    
?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">PERHATIAN !!!</h4>
    </div>
    <div class="modal-body">
        <p>Data Lahan untuk <?= $namaPetani ?> belum ada datanya.</p>
    </div>
    <div class="modal-footer">
        <div class="btn-group">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
<?php endif ?>