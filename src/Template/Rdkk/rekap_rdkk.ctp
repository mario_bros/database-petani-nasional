<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
?>
<div class="col-md-4">
	<label class="control-label">Tanggal RDKK</label>
	<label class="control-label">:</label>
	<label class="control-label"><?= h($dataHeader->tanggal) ?></label>
</div>

<div class="col-md-4">
	<label class="control-label">Nama Petani</label>
	<label class="control-label">:</label>
	<label class="control-label"><?= h($dataHeader->petani->ektp['nama']) ?></label>
</div>

<div class="col-md-4">
	<?= $this->Html->link('Edit', ['action' => 'edit',$dataHeader->id], ['class' => 'btn btn-primary']) ?>
</div>

<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?> </button>

<table class="table table-striped">
	<thead>
	<tr>
		<th><?= __('Masa Tanam') ?></th>

		<?php foreach($dataItem as $items): ?>
			<th>
				<?php echo $items->name ?>
			</th>
		<?php endforeach; ?>

		<th><?= __('Total') ?></th>
	</tr>
	</thead>

	<tbody>
	<?php
	//mulai foreach dari detail rdkk
	foreach ($detailRdkk as $row):
	$itemCollection = array_slice($row, 2);
	?>
	<tr> <!-- Baris data trRdkk -->
		<td>
			<?php echo $row['masa_tanam'] ?>
		</td>
		<?php foreach ($itemCollection as $key=>$jumlah): if(strpos($key,'Harga') === false && $key!='masa_tanam') { ?>
			<td>
				<?php
				echo $jumlah;
				echo (!empty($itemCollection['Harga_'.$key]))? '(@Rp.'.$itemCollection['Harga_'.$key].')' : '';
				?>
			</td>

		<?php } endforeach; ?>

		<?php endforeach; ?>
	</tr>

	<tr> <!-- Baris untuk jumlah -->
		<td>
			<?= $this->Form->label(__('Jumlah', ['class'=>'control-label'])) ?>
		</td>

		<?php $total=0; foreach ($itemCollection as $key=> $jumlah): if(strpos($key,'Harga_') === false && $key!='masa_tanam') { ?>
		<td>
			<?php
			if (!empty($itemCollection['Harga_'.$key])) { //pengecekan utk column total harga
				echo $jumlah * $itemCollection['Harga_'.$key];
				$total += $itemCollection['Harga_'.$key];
				//$total = $total + $jumlah * $itemCollection['Harga_'.$key]; //sebelumnya di remark dulu karena jumlahnya beda
			}
			?>
			<?php } endforeach; ?>
			<?php echo $total; ?>
		</td>
	</tr>
	</tbody>
</table>

<div class="row">
	<div class="col-sm-1">
		<?= $this->Form->button(__("Rekap RDKK"), ['class'=>'btn btn-primary']); ?>
	</div>
</div>