<?php
	$this->extend('../Layout/TwitterBootstrap/dashboard'); 
?>
<center>
	<h3>Rencana Kebutuhan Kelompok (RDKK)</h3>
	<h5>Pupuk Bersubsidi</h5>
        <h5>Tingkat Kecamatan</h5>
</center>

<table>
	<tr>
		<td>Desa / Kecamatan</td>
		<td>:</td>
		<td><?= ($headerKecamatan->nama) ?></td>
	</tr>
	<tr>
		<td>Sub Sektor</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td>Komoditas</td>
		<td>:</td>
		<td></td>
	</tr>
</table>

<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?> </button>

<table class="table">
    <thead>
        <tr>
            <th rowspan="3"><?= __('No') ?></th>
            <th rowspan="3"><?= __('Nama Poktan') ?></th>
            <th rowspan="3"><?= __('Luas Tanam (Ha)') ?></th>
            <th colspan="20"><?= __('Kebutuhan Pupuk Bersubsidi (Kg)') ?></th>
        </tr>
        <tr>
            <?php foreach($dataItems as $item) : ?>
                <th colspan="5"><?= $item['name'] ?></th>
            <?php endforeach; ?>
        </tr>

        1. di bawah penyuluh itu petani
        2. pegawai bumdes ngisi masing - masing RKP petani
        3

        <tr>
            <?php foreach ($aliasItemCollections as $identifier) : ?>
                <?php
                
                $totalItemSemuaPoktan[$identifier] = 0;
                if ( strpos($identifier, "_mt") !== false ) : 
                    $strPosition = strpos($identifier, "_mt");
                    $fieldPerMasaTanam = strtoupper( substr( $identifier, $strPosition + 1, 3 ) );
                ?>
                    <th ><?= $fieldPerMasaTanam ?></th>
                <?php 
                elseif ( strpos($identifier, "Total") !== false ) : 
                    $strPosition = strpos($identifier, "Total");
                    $fieldPerTotalMasaTanam = "Jml";
                ?>
                    <th ><?= $fieldPerTotalMasaTanam ?></th>
                <?php 
                endif; 
                ?>
            <?php endforeach;  ?>
        </tr>
    </thead>
    <tbody>
        <?php 
        $no = $totalLahan = 0;
        foreach ( $hasilRekap as $rekap ) : 
            $no++;  
        ?>
        <tr> <!-- Baris untuk jumlah -->
            <td>
                <?= $no ?>
            </td>
            <td>
                <?= $rekap['nama_poktan'] ?>
            </td>
            <td>
                <?php
                $totalLahan += $rekap['luas_lahan'];
                echo $rekap['luas_lahan'];
                ?>
            </td>
            <?php foreach ($aliasItemCollections as $aliasItem) : ?>
                <td>
                    <?php
                    $totalItemSemuaPoktan[$aliasItem] += $rekap[$aliasItem];
                    echo $rekap[$aliasItem]
                    ?>
                </td>
            <?php endforeach; ?>
        </tr>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr> <!-- Baris untuk jumlah -->
            <td colspan="2">
                <?= "Total" ?>
            </td>
            <td>
                <?php 
                echo $totalLahan
                ?>
            </td>
            <?php foreach ($aliasItemCollections as $aliasItem) : ?>
            <td>
                <?= $totalItemSemuaPoktan[$aliasItem] ?>
            </td>
            <?php endforeach; ?>
        </tr>
    </tfoot>
</table>

<div class="row">
	<?php //echo $this->Form->button(__("Perbaharui"), ['class'=>'btn btn-warning']); ?>
	<?php //echo $this->Form->button(__("Verifikasi"), ['class'=>'btn btn-success']); ?>
</div>