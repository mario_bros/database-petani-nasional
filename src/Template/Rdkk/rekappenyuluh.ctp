<?php
	$this->extend('../Layout/TwitterBootstrap/dashboard'); 
?>
<center>
	<h3>Rencana Kebutuhan Petani (RKP)</h3>
	<h5>Pupuk Bersubsidi</h5>
</center>

<table>

	<tr>
		<td>Kelompok Tani</td>
		<td>:</td>
		<td><?= ($headerPenyuluh->nama) ?></td>
	</tr>
	<tr>
		<td>Gapoktan</td>
		<td>:</td>
		<td><?= ($headerPenyuluh->gapoktan->nama) ?></td>
	</tr>
	<tr>
		<td>Desa / Kecamatan</td>
		<td>:</td>
		<td><?= ($headerPenyuluh->daerah->nama) ?></td>
	</tr>
	<tr>
		<td>Sub Sektor</td>
		<td>:</td>
		<td></td>
	</tr>
	<tr>
		<td>Komoditas</td>
		<td>:</td>
		<td></td>
	</tr>
</table>

<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?> </button>

<table class="table">
    <thead>
        <tr>
            <th rowspan="3"><?= __('No') ?></th>
            <th rowspan="3"><?= __('Nama Petani') ?></th>
            <th rowspan="3"><?= __('Luas Tanam (Ha)') ?></th>
            <th colspan="20"><?= __('Kebutuhan Pupuk Bersubsidi (Kg)') ?></th>
        </tr>
        <tr>
            <?php foreach($dataItems as $item) : ?>
                <th colspan="5"><?= $item['name'] ?></th>
            <?php endforeach; ?>
        </tr>
        <tr>
            <th ><?= __('MT I') ?></th>
            <th ><?= __('MT II') ?></th>
            <th ><?= __('MT III') ?></th>
            <th ><?= __('MT IV') ?></th>
            <th ><?= __('Jml') ?></th>
            
            <th ><?= __('MT I') ?></th>
            <th ><?= __('MT II') ?></th>
            <th ><?= __('MT III') ?></th>
            <th ><?= __('MT IV') ?></th>
            <th ><?= __('Jml') ?></th>
            
            <th ><?= __('MT I') ?></th>
            <th ><?= __('MT II') ?></th>
            <th ><?= __('MT III') ?></th>
            <th ><?= __('MT IV') ?></th>
            <th ><?= __('Jml') ?></th>
            
            <th ><?= __('MT I') ?></th>
            <th ><?= __('MT II') ?></th>
            <th ><?= __('MT III') ?></th>
            <th ><?= __('MT IV') ?></th>
            <th ><?= __('Jml') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 0;foreach ( $hasilRekap as $rekap ) : $no++; //debug($rekap['nama_petani'] ); exit(' by'); ?>
        <tr> <!-- Baris untuk jumlah -->
            <td>
                <?= $no ?>
            </td>
            <td>
                <?= $rekap['nama_petani'] ?>
            </td>
            <td>
                <?= $rekap['luas_lahan'] ?>
            </td>
            <?php foreach($aliasItemCollections as $aliasItem) : ?>
                <td>
                    <?= $rekap[$aliasItem] ?>
                </td>
            <?php endforeach; ?>
        </tr>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr> <!-- Baris untuk jumlah -->
            <td colspan="2">
                <?= "Total" ?>
            </td>
            <td>
                <?= "Luas Tanam" ?>
            </td>
            
            <td>
                <?= 19 ?>
            </td>
            <td>
                <?= 945 ?>
            </td>
            <td>
                <?= 995 ?>
            </td>
            <td>
                <?= 945 ?>
            </td>
            <td>
                <?= 2885 ?>
            </td>
            <td>
                <?= 435 ?>
            </td>
            <td>
                <?= 540 ?>
            </td>
            <td>
                <?= 480 ?>
            </td>
            <td>
                <?= 1455 ?>
            </td>
            <td>
                <?= 410 ?>
            </td>
            <td>
                <?= 425 ?>
            </td>
            <td>
                <?= 415 ?>
            </td>
            <td>
                <?= 1250 ?>
            </td>
            <td>
                <?= 490 ?>
            </td>
            <td>
                <?= 500 ?>
            </td>
            <td>
                <?= 460 ?>
            </td>
            <td>
                <?= 490 ?>
            </td>
            <td>
                <?= 500 ?>
            </td>
            <td>
                <?= 460 ?>
            </td>
            <td>
                <?= 1450 ?>
            </td>
        </tr>
    </tfoot>
</table>

<div class="row">
	<?= $this->Form->button(__("Perbaharui"), ['class'=>'btn btn-warning']); ?>
	<?= $this->Form->button(__("Verifikasi"), ['class'=>'btn btn-success']); ?>
</div>