<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');?>


<?= $this->Form->create($rdkk); ?>
<fieldset class="headerData">

    <legend><?= __('Tambah Data RKP Petani') ?></legend>
    <div class="">
        <div class="col-md-4">
            <label class="control-label">
            	Tanggal : <?php echo(date("d-m-Y",time())); ?> <?php echo $this->Form->hidden('tanggal',['value'=>date('Y-m-d')]) ?>
            </label>


        </div>
        <div class="col-md-4">
            <label class="control-label">Nama Petani : <strong><?= h($dataPetani->ektp['nama']) ?></strong></label>
            <?php echo $this->Form->hidden('petani_id',['value'=>$dataPetani->id]) ?>
            <?php echo $this->Form->hidden('id_subsektor',['value'=>$dataPetani->komoditas_petani[0]->komodita->subsektor['id']]) ?>
            <?php echo $this->Form->hidden('id_komoditas',['value'=>$dataPetani->komoditas_petani[0]['id_komoditas']]) ?>
        </div>

        <div class="col-md-4">
            <label class="control-label">Lahan : <?= $alamat_lahan ?></label>

            <?php echo $this->Form->hidden('lahan_id',['value' => $lahan_id]) ?>
        </div>

        <div class="col-md-4">
            <label class="control-label">Komoditas : <?= $dataPetani->komoditas_petani[0]->komodita['nama'] ?></label>


        </div>

        <div class="col-md-4">
            <label class="control-label">Lahan : <?= $alamat_lahan ?></label>

            <?php echo $this->Form->hidden('lahan_id',['value' => $lahan_id]) ?>
        </div>

        <div class="col-md-4">
            <?php echo $this->Form->input('masa_tanam',['required'=>TRUE]) ?>
        </div>
    </div>

</fieldset>


<div class="">
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr class="">
                    <th rowspan="2" style="text-align: center; border: 1px solid;">Lahan</th>
                    <?php foreach($dataCategory as $categoryName => $count): ?>
                        <th colspan="<?= count($count) ?>" style="text-align: center; border: 1px solid;">
                            <?php
                            if ($categoryName == "Pestisida")
                                $metrik = "mL";
                            else
                                $metrik = "Kg";

                            echo $categoryName . " (" . $metrik . ") ";
                            ?>
                        </th>
                    <?php endforeach ?>
                </tr>
                <tr class="">
                    <?php
                    foreach($dataItem as $pupuk):
                    ?>
                        <th><?php echo $pupuk['name'] ?></th>
                    <?php
                    endforeach;
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach($dataPetani->komoditas_petani as $item) :
                    $counter = 0;
                    if(!empty($item->lahan)){
                ?>
                    <tr>
                        <td>
                            <?php echo $item->lahan['alamat'] ?>
                        </td>
                        <?php
                        foreach($dataItem as $pupuk):
                        ?>
                            <td>
                                <?php
                                echo $this->Form->hidden('tr_rdkk.'.$counter.'.lahan_id',['label'=>false,'value'=>$lahan_id]);
                                echo $this->Form->hidden('tr_rdkk.'.$counter.'.item_id',['label'=>false,'value'=>$pupuk['id']]);
                                echo $this->Form->input('tr_rdkk.'.$counter.'.jumlah',['label'=>false, 'value' => 0]);
                                $counter++;
                                ?>
                            </td>
                        <?php
                        endforeach;
                        ?>
                        <td>
                                <!-- total baris kolom total -->
                            <div class="jumlah"></div>
                        </td>
                    </tr>
                    <?php } ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php echo $this->Form->button(__("Tambah RKP"), ['class'=>'btn btn-primary']); ?>
</div>

<div class="col-sm-1">

</div>
</form>
