<?php
	$this->extend('../Layout/TwitterBootstrap/dashboard');
?>
	<div class="col-md-4">
		<label class="control-label">Kelompok Tani</label>
		<label class="control-label">:</label>
		<label class="control-label"><?= h(null) ?></label>
	</div>

	<div class="col-md-4">
		<label class="control-label">Gapoktan</label>
		<label class="control-label">:</label>
		<label class="control-label"><?= h($dataHeader->petani->ektp['nama']) ?></label>
	</div>

    <div class="col-md-4">
        <label class="control-label">Desa / Kecamatan</label>
        <label class="control-label">:</label>
        <label class="control-label"><?= h($dataHeader->petani->ektp['alamat']) ?></label>
    </div>

    <div class="col-md-4">
        <label class="control-label">Sub Sektor</label>
        <label class="control-label">:</label>
        <label class="control-label"><?= h($dataHeader->petani->ektp['alamat']) ?></label>
    </div>

    <div class="col-md-4">
        <label class="control-label">Komoditas</label>
        <label class="control-label">:</label>
        <label class="control-label"><?= h($dataHeader->petani->ektp['id']) ?></label>
    </div>



<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?> </button>

<table class="table table-striped">
	<thead>
		<tr>
			<th><?= __('Lahan') ?></th>
			
			<?php foreach($dataItem as $items): ?>
				<th>
					<?php echo $items->name ?>
				</th>
			<?php endforeach; ?>
			
			<th><?= __('Total') ?></th>
		</tr>
	</thead>

	<tbody>
	<?php
	//mulai foreach dari detail rdkk
	foreach ($detailRdkk as $row):
	$itemCollection = array_slice($row, 2);
	?>
		<tr> <!-- Baris data trRdkk -->
			<td>
				<?php echo $row['Lahan_alamat'] ?>
			</td>
		<?php foreach ($itemCollection as $key=>$jumlah): if(strpos($key,'Harga') === false && $key!='lahan_id') { ?>
			<td>
				<?php
					 echo $jumlah;

					 echo (!empty($itemCollection['Harga_'.$key]))? '(@Rp.'.$itemCollection['Harga_'.$key].')' : '';
				 ?>
			</td>
			
		<?php } endforeach; ?>
			
	<?php endforeach; ?>
		</tr>
	
		<tr> <!-- Baris untuk jumlah -->
			<td>
				<?= $this->Form->label(__('Jumlah', ['class'=>'control-label'])) ?>
			</td>
			
			<?php $total=0; foreach ($itemCollection as $key=> $jumlah): if(strpos($key,'Harga_') === false && $key!='lahan_id') { ?>
			<td>
				<?php
					 if (!empty($itemCollection['Harga_'.$key])) { //pengecekan utk column total harga
						 $harga = $jumlah * $itemCollection['Harga_'.$key];
						 echo "Rp ".number_format($harga);
						 $total = $total + $harga;
						 //$total = $total + $jumlah * $itemCollection['Harga_'.$key]; //sebelumnya di remark dulu karena jumlahnya beda 
					 }
				 ?>
			<?php } endforeach; ?>
			<?php echo 'Rp '.number_format($total); ?>
			</td>
		</tr>
	</tbody>
</table>

<div class="row">
	<div class="col-sm-1">
		<?= $this->Form->button(__("Rekap RDKK"), ['class'=>'btn btn-primary']); ?>

            <?= $this->Html->link('Edit', ['action' => 'edit',$dataHeader->id], ['class' => 'btn btn-warning']) ?>
	</div>
</div>