<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $komoditasPetani->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $komoditasPetani->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Komoditas Petani'), ['action' => 'index']) ?></li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $komoditasPetani->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $komoditasPetani->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Komoditas Petani'), ['action' => 'index']) ?></li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($komoditasPetani); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Komoditas Petani']) ?></legend>
    <?php
    echo $this->Form->input('id_petani');
    echo $this->Form->input('id_komoditas');
    echo $this->Form->input('id_jenis_petani');
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
