<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('New Komoditas Petani'), ['action' => 'add']); ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>

<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id'); ?></th>
            <th><?= $this->Paginator->sort('id_petani'); ?></th>
            <th><?= $this->Paginator->sort('id_komoditas'); ?></th>
            <th><?= $this->Paginator->sort('id_jenis_petani'); ?></th>
            <th class="actions"><?= __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($komoditasPetani as $komoditasPetani): ?>
        <tr>
            <td><?= $this->Number->format($komoditasPetani->id) ?></td>
            <td><?= $this->Number->format($komoditasPetani->id_petani) ?></td>
            <td><?= $this->Number->format($komoditasPetani->id_komoditas) ?></td>
            <td><?= $this->Number->format($komoditasPetani->id_jenis_petani) ?></td>
            <td class="actions">
                <?= $this->Html->link('', ['action' => 'view', $komoditasPetani->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                <?= $this->Html->link('', ['action' => 'edit', $komoditasPetani->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $komoditasPetani->id], ['confirm' => __('Are you sure you want to delete # {0}?', $komoditasPetani->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>