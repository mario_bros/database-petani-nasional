<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Komoditas Petani'), ['action' => 'edit', $komoditasPetani->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Komoditas Petani'), ['action' => 'delete', $komoditasPetani->id], ['confirm' => __('Are you sure you want to delete # {0}?', $komoditasPetani->id)]) ?> </li>
<li><?= $this->Html->link(__('List Komoditas Petani'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Komoditas Petani'), ['action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
<li><?= $this->Html->link(__('Edit Komoditas Petani'), ['action' => 'edit', $komoditasPetani->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Komoditas Petani'), ['action' => 'delete', $komoditasPetani->id], ['confirm' => __('Are you sure you want to delete # {0}?', $komoditasPetani->id)]) ?> </li>
<li><?= $this->Html->link(__('List Komoditas Petani'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Komoditas Petani'), ['action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($komoditasPetani->id) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($komoditasPetani->id) ?></td>
        </tr>
        <tr>
            <td><?= __('Id Petani') ?></td>
            <td><?= $this->Number->format($komoditasPetani->id_petani) ?></td>
        </tr>
        <tr>
            <td><?= __('Id Komoditas') ?></td>
            <td><?= $this->Number->format($komoditasPetani->id_komoditas) ?></td>
        </tr>
        <tr>
            <td><?= __('Id Jenis Petani') ?></td>
            <td><?= $this->Number->format($komoditasPetani->id_jenis_petani) ?></td>
        </tr>
    </table>
</div>

