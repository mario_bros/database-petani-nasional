<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('List Poktan'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Log'), ['controller' => 'Log', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Log'), ['controller' => 'Log', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Petani'), ['controller' => 'Petani', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Petani'), ['controller' => 'Petani', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?= $this->Html->link(__('List Poktan'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Log'), ['controller' => 'Log', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Log'), ['controller' => 'Log', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Petani'), ['controller' => 'Petani', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Petani'), ['controller' => 'Petani', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>

<?= $this->Form->create($poktan); ?>
<fieldset>
    <legend><?= __('Add {0}', ['Poktan']) ?></legend>
    <div class="col-sm-6">
        <?php
        echo $this->Form->input('nama', ['label' => 'Nama Poktan']);
        //{{ Form.input('description', {'type':"textarea", 'label':{'text':"Deskripsi"}, 'class':"form-control", 'placeholder':"Deskripsi"} )|raw }}
        echo $this->Form->input('alamat', ['type' => "textarea"]);
        echo $this->Form->input('daerah_id', ['options' => $daerah, 'empty' => true]);
        echo $this->Form->input('gapoktan_id', ['options' => $gapoktan, 'empty' => true]);
        ?>
    </div>
    <div class="col-sm-6">
        <?php
        echo $this->Form->input('lat');
        echo $this->Form->input('lon');
        //echo $this->Form->input('aprroved');
        echo $this->Form->input('bumdes_id', ['label'=>'Kopdes', 'options' => $bumdes, 'empty' => true]);
        echo $this->Form->input('ketua_poktan', ['options' => $petani, 'empty' => true]);
        echo $this->Form->input('penyuluh_id', ['options' => $penyuluh, 'empty' => true]);
        ?>
    </div>
    <div class="col-sm-12">
        <?= $this->Form->button(__("Tambah"), ['class'=>'btn btn-primary']); ?>
    </div>    
</fieldset>



<?= $this->Form->end() ?>
