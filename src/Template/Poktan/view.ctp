<?php
$roleStr = strtolower(trim($role->jenis));

$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Poktan'), ['action' => 'edit', $poktan->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Poktan'), ['action' => 'delete', $poktan->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poktan->id)]) ?> </li>
<li><?= $this->Html->link(__('List Poktan'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Poktan'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Log'), ['controller' => 'Log', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Log'), ['controller' => 'Log', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Petani'), ['controller' => 'Petani', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Petani'), ['controller' => 'Petani', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Notifikasi'), ['controller' => 'Notifications', 'action' => 'index']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?= $this->Html->link(__('Edit Poktan'), ['action' => 'edit', $poktan->id]) ?> </li>
    <li><?= $this->Form->postLink(__('Delete Poktan'), ['action' => 'delete', $poktan->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poktan->id)]) ?> </li>
    <li><?= $this->Html->link(__('List Poktan'), ['action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Poktan'), ['action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Log'), ['controller' => 'Log', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Log'), ['controller' => 'Log', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Petani'), ['controller' => 'Petani', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Petani'), ['controller' => 'Petani', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Notifikasi'), ['controller' => 'Notifications', 'action' => 'index']) ?> </li>
</ul>
<?php
$this->end();
?>

<?php
if(!empty($notificationList->toARray())){ ?>
    <div class="alert alert-info"><?php
        $notif = $notificationList->toArray();
        $key = @end(array_keys($notif));
        echo $notif[$key]->action;
    ?></div>
<?php } ?>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h(strtoupper($poktan->nama)) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Nama') ?></td>
            <td><?= h(strtoupper($poktan->nama)) ?></td>
        </tr>
        <tr>
            <td><?= __('Alamat') ?></td>
            <td><?= h(strtoupper($poktan->alamat)) ?></td>
        </tr>
        <tr>
            <td><?= __('Desa') ?></td>
            <td><?= h(strtoupper($poktan->daerah->nama)) ?></td>
        </tr>
        <tr>
            <td><?= __('Kecamatan') ?></td>
            <td><?= h(strtoupper($poktan->daerah->parent_daerah->nama)) ?></td>
        </tr>
        <tr>
            <td><?= __('Kabupaten') ?></td>
            <td><?= h(strtoupper($poktan->daerah->parent_daerah->grand_parent_daerah->nama)) ?></td>

        </tr>
        <tr>
            <td><?= __('Provinsi') ?></td>
            <td><?= h($poktan->daerah->parent_daerah->grand_parent_daerah->third_level_parent_daerah->nama) ?></td>
        </tr>
        <tr>
            <td><?= __('Gapoktan') ?></td>
            <td><?= $poktan->has('gapoktan') ? strtoupper($poktan->gapoktan->nama) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('KOPDES') ?></td>
            <td><?= $poktan->has('bumde') ? strtoupper($poktan->bumde['nama']) : 'none' ?></td>
        </tr>
        <tr>
            <td><?= __('Ketua Poktan') ?></td>
            <td><?= $poktan->has('ketuaPoktan') ? strtoupper($poktan->ketuaPoktan['ektp']->nama) : 'none' ?></td>
        </tr>
        <tr>
            <td><?= __('Penyuluh') ?></td>
            <td><?= $poktan->has('penyuluh') ? strtoupper($poktan->penyuluh['ektp']->nama) : 'none' ?></td>
        </tr>
    </table>
</div>

<?= ( $poktan->aprroved == 0 && $poktan->approval_request == 0 ) ?  $this->Html->link( ($this->Form->button(__("Tambah Anggota"), ['class'=>'btn btn-primary'])), ['controller' => 'Petani', 'action' => 'add',$poktan->id], ['escape' => FALSE] ) : '' ?>
<br/><br/>


<?= $this->Html->link('Tambah Data Petani', ['controller' => 'petani', 'action' => 'add', $poktan->id], ['class'=>'btn btn-primary']); ?>
<br/><br/>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= 'Anggota <strong>('.count($poktan->petani).' petani)</strong>' ?></h3>
    </div>
    <?php if (!empty($poktan->petani)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="hideme"><?= __('No') ?></th>

                    <th><?= __('Nama') ?></th>
                    <th class="hideme"><?= __('No Ektp') ?></th>
                    <th class="hideme"><?= __('Alamat') ?></th>
                    <th class="hideme"><?= __('No Tlp') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($poktan->petani as $key=>$petani): ?>
                    <tr>
                        <td class="hideme"><?= h($key+1) ?></td>
                        <td><?= h($petani['ektp']->nama) ?></td>
                        <td class="hideme"><?= h($petani->no_ektp) ?></td>
                        <td class="hideme"><?= h($petani['ektp']->alamat) ?></td>
                        <td class="hideme"><?= h('0'.$petani->no_tlp) ?></td>
                        <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Petani', 'action' => 'view', $petani->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>

                            <?php
                            if ($roleStr != "penyuluh" || $roleStr != "user") :
                                echo $this->Html->link('', ['controller' => 'Petani', 'action' => 'edit', $petani->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']);

                                echo $this->Form->postLink('', ['controller' => 'Petani', 'action' => 'delete', $petani->id], ['confirm' => __('Are you sure you want to delete # {0}?', $petani->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']);
                            endif;
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php else: ?>
        <p class="panel-body">Tidak ada data Petani terkait</p>
    <?php endif; ?>
</div>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Activity Log') ?></h3>
    </div>
    <?php if (!empty($notificationList)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?= __('No') ?></th>
                    <th><?= __('Title') ?></th>
                    <th><?= __('Date') ?></th>
                    <th><?= __('Notes') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
                foreach ($notificationList as $activity):
                    $i++;
                ?>
                    <tr>
                        <td><?= h($i) ?></td>
                        <td><?= h($activity->action) ?></td>
                        <td><?= h($activity->date) ?></td>
                        <td><?= h($activity->notes) ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php else: ?>
        <p class="panel-body">Tidak ada data Log terkait</p>
    <?php endif; ?>
</div>


<?= $this->Form->create($poktan); ?>
<div class="row">
    <div class="col-sm-4 pull-right">
        <?php

        if ($roleStr == "admin" || $roleStr == "bps") :
        ?>
            <?php if ($petani->status == 0 ) : ?>
                <div class="btn-group">
                    <?= $this->Form->button(__("Setujui"), ['class'=>'btn btn-primary', 'name' => "Approved"]); ?>
                    <br/><br/>
                </div>

                <div class="btn-group">
                    <?= $this->Form->button(__("Tolak"), ['class'=>'btn btn-danger', 'type' => "button", 'data-toggle' => "modal", 'data-target' => "#myModal"]); ?>
                    <br/><br/>
                </div>
            <?php endif ?>
        <?php
        endif;
        ?>

        <?php if ($poktan->status == 0 && $roleStr != "penyuluh" && $poktan->approval_request == 0 ) : ?>
            <div class="btn-group">
                <?= $this->Form->button(__("Ajukan Persetujan"), ['class'=>'btn btn-info', 'name' => "ApprovalRequest"]); ?>
                <br/><br/>
            </div>
        <?php endif ?>
    </div>
<!--    <div class="col-sm-1 col-sm-push-1">

        <br/><br/>
    </div>

    <div class="col-sm-1 col-sm-push-7">

        <br/><br/>
    </div>-->
</div>
<?= $this->Form->end() ?>
