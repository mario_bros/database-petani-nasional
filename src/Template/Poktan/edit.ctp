<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $poktan->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $poktan->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Poktan'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Penyuluh'), ['controller' => 'Penyuluh', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Penyuluh'), ['controller' => 'Penyuluh', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Petani'), ['controller' => 'Petani', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Petani'), ['controller' => 'Petani', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Log'), ['controller' => 'Log', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Log'), ['controller' => 'Log', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $poktan->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $poktan->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Poktan'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Penyuluh'), ['controller' => 'Penyuluh', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Penyuluh'), ['controller' => 'Penyuluh', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Petani'), ['controller' => 'Petani', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Petani'), ['controller' => 'Petani', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Log'), ['controller' => 'Log', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Log'), ['controller' => 'Log', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($poktan); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Poktan']) ?></legend>
    <div class="col-md-6">
    <?php
    echo $this->Form->input('nama');
    echo $this->Form->input('alamat');
    echo $this->Form->input('daerah_id', ['options' => $daerah]);
    echo $this->Form->input('gapoktan_id', ['options' => $gapoktan]);

    ?>
        </div>
    <div class="col-md-6">
        <?php
        echo $this->Form->input('lat');
        echo $this->Form->input('lon');
        echo $this->Form->input('bumdes_id', ['label'=>'Kopdes','options' => $bumdes, 'empty' => true]);
        echo $this->Form->input('ketua_poktan', ['options' => $petani,'empty'=>'please select one']);
        echo $this->Form->input('penyuluh_id', ['options' => $penyuluh,'empty'=>'please select one']);

        ?>
    </div>
</fieldset>

<div class="col-sm-1">
    <?= $this->Form->button(__("Simpan"), ['class'=>'btn btn-primary']); ?>
<?= $this->Form->end() ?>
