<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('New Poktan'), ['action' => 'add']); ?></li>
<li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('New Daerah'), ['controller' => ' Daerah', 'action' => 'add']); ?></li>
<li><?= $this->Html->link(__('List Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('New Gapoktan'), ['controller' => ' Gapoktan', 'action' => 'add']); ?></li>
<li><?= $this->Html->link(__('List Log'), ['controller' => 'Log', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('New Log'), ['controller' => ' Log', 'action' => 'add']); ?></li>
<li><?= $this->Html->link(__('List Petani'), ['controller' => 'Petani', 'action' => 'index']); ?></li>
<li><?= $this->Html->link(__('New Petani'), ['controller' => ' Petani', 'action' => 'add']); ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-pills">' . $this->fetch('tb_actions') . '</ul>'); ?>
<h2>Poktan</h2>

<div class="table-responsive">
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th class="hideme"><?= $this->Paginator->sort('no'); ?></th>
                <th><?= $this->Paginator->sort('nama'); ?></th>
                <th class="hideme"><?= $this->Paginator->sort('alamat'); ?></th>
                <th class="hideme"><?= $this->Paginator->sort('daerah_id'); ?></th>
                <th class="hideme"><?= $this->Paginator->sort('gapoktan_id'); ?></th>
                <th class="hideme"><?= $this->Paginator->sort('lat'); ?></th>
                <th class="hideme"><?= $this->Paginator->sort('lon'); ?></th>
                <th class="actions"><?= __('Actions'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 0; foreach ($poktan as $poktan): $no++; ?>
            <tr>
                <td class="hideme"><?= $no ?></td>
                <td><?= h($poktan->nama) ?></td>
                <td class="hideme"><?= h($poktan->alamat) ?></td>
                <td class="hideme">
                    <?= $poktan->has('daerah') ? $this->Html->link($poktan->daerah->nama, ['controller' => 'Daerah', 'action' => 'view', $poktan->daerah->id]) : '' ?>
                </td class="hideme">
                <td class="hideme">
                    <?= $poktan->has('gapoktan') ? $this->Html->link($poktan->gapoktan->nama, ['controller' => 'Gapoktan', 'action' => 'view', $poktan->gapoktan->id]) : '' ?>
                </td class="hideme">
                <td class="hideme"><?= $this->Number->format($poktan->lat) ?></td>
                <td class="hideme"><?= $this->Number->format($poktan->lon) ?></td>
                <td class="actions">
                    <?= $this->Html->link('', ['action' => 'view', $poktan->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                    <?= $this->Html->link('', ['action' => 'edit', $poktan->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                    <?= $this->Form->postLink('', ['action' => 'delete', $poktan->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poktan->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>