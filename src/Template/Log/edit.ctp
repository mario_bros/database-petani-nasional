<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $log->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $log->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Log'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $log->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $log->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Log'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($log); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Log']) ?></legend>
    <?php
    echo $this->Form->input('user_id');
    echo $this->Form->input('action');
    echo $this->Form->input('date');
    echo $this->Form->input('poktan_id', ['options' => $poktan]);
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
