<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Log'), ['action' => 'edit', $log->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Log'), ['action' => 'delete', $log->id], ['confirm' => __('Are you sure you want to delete # {0}?', $log->id)]) ?> </li>
<li><?= $this->Html->link(__('List Log'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Log'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
<li><?= $this->Html->link(__('Edit Log'), ['action' => 'edit', $log->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Log'), ['action' => 'delete', $log->id], ['confirm' => __('Are you sure you want to delete # {0}?', $log->id)]) ?> </li>
<li><?= $this->Html->link(__('List Log'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Log'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($log->id) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Action') ?></td>
            <td><?= h($log->action) ?></td>
        </tr>
        <tr>
            <td><?= __('Poktan') ?></td>
            <td><?= $log->has('poktan') ? $this->Html->link($log->poktan->nama, ['controller' => 'Poktan', 'action' => 'view', $log->poktan->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($log->id) ?></td>
        </tr>
        <tr>
            <td><?= __('User Id') ?></td>
            <td><?= $this->Number->format($log->user_id) ?></td>
        </tr>
        <tr>
            <td><?= __('Date') ?></td>
            <td><?= h($log->date) ?></td>
        </tr>
    </table>
</div>

