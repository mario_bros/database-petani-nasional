<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Daerah'), ['action' => 'edit', $daerah->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Daerah'), ['action' => 'delete', $daerah->id], ['confirm' => __('Are you sure you want to delete # {0}?', $daerah->id)]) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Parent Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Parent Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Jenis Daerah'), ['controller' => 'JenisDaerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Jenis Daerah'), ['controller' => 'JenisDaerah', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Bumdes'), ['controller' => 'Bumdes', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Bumde'), ['controller' => 'Bumdes', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Ektp'), ['controller' => 'Ektp', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Ektp'), ['controller' => 'Ektp', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lahan'), ['controller' => 'Lahan', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
<li><?= $this->Html->link(__('Edit Daerah'), ['action' => 'edit', $daerah->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Daerah'), ['action' => 'delete', $daerah->id], ['confirm' => __('Are you sure you want to delete # {0}?', $daerah->id)]) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Parent Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Parent Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Jenis Daerah'), ['controller' => 'JenisDaerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Jenis Daerah'), ['controller' => 'JenisDaerah', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Bumdes'), ['controller' => 'Bumdes', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Bumde'), ['controller' => 'Bumdes', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Ektp'), ['controller' => 'Ektp', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Ektp'), ['controller' => 'Ektp', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lahan'), ['controller' => 'Lahan', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($daerah->nama) ?></h3>
    </div>
    <div class="table-responsive">
        <table class="table table-striped" cellpadding="0" cellspacing="0">
            <tr>
                <td><?= __('Nama') ?></td>
                <td><?= h($daerah->nama) ?></td>
            </tr>
            <tr>
                <td><?= __('Note') ?></td>
                <td><?= h($daerah->note) ?></td>
            </tr>
            <tr>
                <td><?= __('Parent Daerah') ?></td>
                <td><?= $daerah->has('parent_daerah') ? $this->Html->link($daerah->parent_daerah->nama, ['controller' => 'Daerah', 'action' => 'view', $daerah->parent_daerah->id]) : '' ?></td>
            </tr>
            <tr>
                <td><?= __('Jenis Daerah') ?></td>
                <td><?= $daerah->has('jenis_daerah') ? $this->Html->link($daerah->jenis_daerah->id, ['controller' => 'JenisDaerah', 'action' => 'view', $daerah->jenis_daerah->id]) : '' ?></td>
            </tr>
            <tr>
                <td><?= __('Id') ?></td>
                <td><?= $this->Number->format($daerah->id) ?></td>
            </tr>
            <tr>
                <td><?= __('Lat') ?></td>
                <td><?= $this->Number->format($daerah->lat) ?></td>
            </tr>
            <tr>
                <td><?= __('Lon') ?></td>
                <td><?= $this->Number->format($daerah->lon) ?></td>
            </tr>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related Bumdes') ?></h3>
    </div>
    <?php if (!empty($daerah->bumdes)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('Nama') ?></th>
                    <th><?= __('Alamat') ?></th>
                    <th><?= __('Lat') ?></th>
                    <th><?= __('Lon') ?></th>
                    <th><?= __('Daerah Id') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($daerah->bumdes as $bumdes): ?>
                    <tr>
                        <td><?= h($bumdes->id) ?></td>
                        <td><?= h($bumdes->nama) ?></td>
                        <td><?= h($bumdes->alamat) ?></td>
                        <td><?= h($bumdes->lat) ?></td>
                        <td><?= h($bumdes->lon) ?></td>
                        <td><?= h($bumdes->daerah_id) ?></td>
                        <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Bumdes', 'action' => 'view', $bumdes->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                            <?= $this->Html->link('', ['controller' => 'Bumdes', 'action' => 'edit', $bumdes->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Bumdes', 'action' => 'delete', $bumdes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bumdes->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php else: ?>
        <p class="panel-body">no related Bumdes</p>
    <?php endif; ?>
</div>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related Daerah') ?></h3>
    </div>
    <?php if (!empty($daerah->child_daerah)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('Nama') ?></th>
                    <th><?= __('Lat') ?></th>
                    <th><?= __('Lon') ?></th>
                    <th><?= __('Note') ?></th>
                    <th><?= __('Parent Id') ?></th>
                    <th><?= __('Jenis Daerah Id') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($daerah->child_daerah as $childDaerah): ?>
                    <tr>
                        <td><?= h($childDaerah->id) ?></td>
                        <td><?= h($childDaerah->nama) ?></td>
                        <td><?= h($childDaerah->lat) ?></td>
                        <td><?= h($childDaerah->lon) ?></td>
                        <td><?= h($childDaerah->note) ?></td>
                        <td><?= h($childDaerah->parent_id) ?></td>
                        <td><?= h($childDaerah->jenis_daerah_id) ?></td>
                        <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Daerah', 'action' => 'view', $childDaerah->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                            <?= $this->Html->link('', ['controller' => 'Daerah', 'action' => 'edit', $childDaerah->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Daerah', 'action' => 'delete', $childDaerah->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childDaerah->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php else: ?>
        <p class="panel-body">no related Daerah</p>
    <?php endif; ?>
</div>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related Ektp') ?></h3>
    </div>
    <?php if (!empty($daerah->ektp)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?= __('No Ktp') ?></th>
                    <th><?= __('Nama') ?></th>
                    <th><?= __('Alamat') ?></th>
                    <th><?= __('Tanggal Lahir') ?></th>
                    <th><?= __('Rt Rw') ?></th>
                    <th><?= __('Desa Kelurahan') ?></th>
                    <th><?= __('Kecamatan') ?></th>
                    <th><?= __('Kabupaten Kota') ?></th>
                    <th><?= __('Pekerjaan') ?></th>
                    <th><?= __('Kewarganegaraan') ?></th>
                    <th><?= __('Status Perkawinan') ?></th>
                    <th><?= __('Gol Darah') ?></th>
                    <th><?= __('Tanggal Terbit') ?></th>
                    <th><?= __('Daerah Id') ?></th>
                    <th><?= __('Tanggal Berakhir') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($daerah->ektp as $ektp): ?>
                    <tr>
                        <td><?= h($ektp->no_ktp) ?></td>
                        <td><?= h($ektp->nama) ?></td>
                        <td><?= h($ektp->alamat) ?></td>
                        <td><?= h($ektp->tanggal_lahir) ?></td>
                        <td><?= h($ektp->rt_rw) ?></td>
                        <td><?= h($ektp->desa_kelurahan) ?></td>
                        <td><?= h($ektp->kecamatan) ?></td>
                        <td><?= h($ektp->kabupaten_kota) ?></td>
                        <td><?= h($ektp->pekerjaan) ?></td>
                        <td><?= h($ektp->kewarganegaraan) ?></td>
                        <td><?= h($ektp->status_perkawinan) ?></td>
                        <td><?= h($ektp->gol_darah) ?></td>
                        <td><?= h($ektp->tanggal_terbit) ?></td>
                        <td><?= h($ektp->daerah_id) ?></td>
                        <td><?= h($ektp->tanggal_berakhir) ?></td>
                        <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Ektp', 'action' => 'view', $ektp->no_ktp], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                            <?= $this->Html->link('', ['controller' => 'Ektp', 'action' => 'edit', $ektp->no_ktp], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Ektp', 'action' => 'delete', $ektp->no_ktp], ['confirm' => __('Are you sure you want to delete # {0}?', $ektp->no_ktp), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php else: ?>
        <p class="panel-body">no related Ektp</p>
    <?php endif; ?>
</div>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related Gapoktan') ?></h3>
    </div>
    <?php if (!empty($daerah->gapoktan)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('Nama') ?></th>
                    <th><?= __('Alamat') ?></th>
                    <th><?= __('Daerah Id') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($daerah->gapoktan as $gapoktan): ?>
                    <tr>
                        <td><?= h($gapoktan->id) ?></td>
                        <td><?= h($gapoktan->nama) ?></td>
                        <td><?= h($gapoktan->alamat) ?></td>
                        <td><?= h($gapoktan->daerah_id) ?></td>
                        <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Gapoktan', 'action' => 'view', $gapoktan->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                            <?= $this->Html->link('', ['controller' => 'Gapoktan', 'action' => 'edit', $gapoktan->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Gapoktan', 'action' => 'delete', $gapoktan->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gapoktan->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php else: ?>
        <p class="panel-body">no related Gapoktan</p>
    <?php endif; ?>
</div>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related Lahan') ?></h3>
    </div>
    <?php if (!empty($daerah->lahan)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('Alamat') ?></th>
                    <th><?= __('Daerah Id') ?></th>
                    <th><?= __('Luas') ?></th>
                    <th><?= __('Jenis Tanaman Id') ?></th>
                    <th><?= __('Petani Id') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($daerah->lahan as $lahan): ?>
                    <tr>
                        <td><?= h($lahan->id) ?></td>
                        <td><?= h($lahan->alamat) ?></td>
                        <td><?= h($lahan->daerah_id) ?></td>
                        <td><?= h($lahan->luas) ?></td>
                        <td><?= h($lahan->jenis_tanaman_id) ?></td>
                        <td><?= h($lahan->petani_id) ?></td>
                        <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Lahan', 'action' => 'view', $lahan->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                            <?= $this->Html->link('', ['controller' => 'Lahan', 'action' => 'edit', $lahan->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Lahan', 'action' => 'delete', $lahan->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lahan->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php else: ?>
        <p class="panel-body">no related Lahan</p>
    <?php endif; ?>
</div>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related Poktan') ?></h3>
    </div>
    <?php if (!empty($daerah->poktan)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('Nama') ?></th>
                    <th><?= __('Alamat') ?></th>
                    <th><?= __('Daerah Id') ?></th>
                    <th><?= __('Gapoktan Id') ?></th>
                    <th><?= __('Lat') ?></th>
                    <th><?= __('Lon') ?></th>
                    <th><?= __('Aprroved') ?></th>
                    <th><?= __('Ketua Poktan') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($daerah->poktan as $poktan): ?>
                    <tr>
                        <td><?= h($poktan->id) ?></td>
                        <td><?= h($poktan->nama) ?></td>
                        <td><?= h($poktan->alamat) ?></td>
                        <td><?= h($poktan->daerah_id) ?></td>
                        <td><?= h($poktan->gapoktan_id) ?></td>
                        <td><?= h($poktan->lat) ?></td>
                        <td><?= h($poktan->lon) ?></td>
                        <td><?= h($poktan->aprroved) ?></td>
                        <td><?= h($poktan->ketua_poktan) ?></td>
                        <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Poktan', 'action' => 'view', $poktan->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                            <?= $this->Html->link('', ['controller' => 'Poktan', 'action' => 'edit', $poktan->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Poktan', 'action' => 'delete', $poktan->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poktan->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php else: ?>
        <p class="panel-body">no related Poktan</p>
    <?php endif; ?>
</div>
