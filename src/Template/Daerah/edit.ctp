<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $daerah->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $daerah->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Daerah'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Parent Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Parent Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Jenis Daerah'), ['controller' => 'JenisDaerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Jenis Daerah'), ['controller' => 'JenisDaerah', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Bumdes'), ['controller' => 'Bumdes', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Bumde'), ['controller' => 'Bumdes', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Ektp'), ['controller' => 'Ektp', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Ektp'), ['controller' => 'Ektp', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Lahan'), ['controller' => 'Lahan', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $daerah->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $daerah->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Daerah'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Parent Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Parent Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Jenis Daerah'), ['controller' => 'JenisDaerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Jenis Daerah'), ['controller' => 'JenisDaerah', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Bumdes'), ['controller' => 'Bumdes', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Bumde'), ['controller' => 'Bumdes', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Ektp'), ['controller' => 'Ektp', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Ektp'), ['controller' => 'Ektp', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Lahan'), ['controller' => 'Lahan', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($daerah); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Daerah']) ?></legend>
    <?php
    echo $this->Form->input('nama');
    echo $this->Form->input('lat');
    echo $this->Form->input('lon');
    echo $this->Form->input('note');
    echo $this->Form->input('parent_id', ['options' => $parentDaerah]);
    echo $this->Form->input('jenis_daerah_id', ['options' => $jenisDaerah]);
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
