<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
    
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-pills">' . $this->fetch('tb_actions') . '</ul>'); ?>
<h2>Daerah</h2>
<div class="row">
    <div class="col-md-12 pull-right" id="secondNav">
        <ul class="nav nav-pills">    
            <li><?= $this->Html->link(__('New Daerah'), ['action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('New Parent Daerah'), ['controller' => ' Daerah', 'action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('List JenisDaerah'), ['controller' => 'JenisDaerah', 'action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('New Jenis Daerah'), ['controller' => ' JenisDaerah', 'action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('List Bumdes'), ['controller' => 'Bumdes', 'action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('New Bumde'), ['controller' => ' Bumdes', 'action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('List Ektp'), ['controller' => 'Ektp', 'action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('New Ektp'), ['controller' => ' Ektp', 'action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('List Gapoktan'), ['controller' => 'Gapoktan', 'action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('New Gapoktan'), ['controller' => ' Gapoktan', 'action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('New Lahan'), ['controller' => ' Lahan', 'action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('New Poktan'), ['controller' => ' Poktan', 'action' => 'add']); ?></li>
        </ul>                
    </div>
</div>
<div class="table-responsive">
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('no'); ?></th>
                <th><?= $this->Paginator->sort('nama'); ?></th>
                <th><?= $this->Paginator->sort('lat'); ?></th>
                <th><?= $this->Paginator->sort('lon'); ?></th>
                <th><?= $this->Paginator->sort('note'); ?></th>
                <th><?= $this->Paginator->sort('parent_id'); ?></th>
                <th><?= $this->Paginator->sort('jenis_daerah_id'); ?></th>
                <th class="actions"><?= __('Actions'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 0; foreach ($daerah as $daerah): $no++; ?>
            <tr>
                <td><?= $no ?></td>
                <td><?= h($daerah->nama) ?></td>
                <td><?= $this->Number->format($daerah->lat) ?></td>
                <td><?= $this->Number->format($daerah->lon) ?></td>
                <td><?= h($daerah->note) ?></td>
                <td>
                    <?= $daerah->has('parent_daerah') ? $this->Html->link($daerah->parent_daerah->nama, ['controller' => 'Daerah', 'action' => 'view', $daerah->parent_daerah->id]) : '' ?>
                </td>
                <td>
                    <?= $daerah->has('jenis_daerah') ? $this->Html->link($daerah->jenis_daerah->id, ['controller' => 'JenisDaerah', 'action' => 'view', $daerah->jenis_daerah->id]) : '' ?>
                </td>
                <td class="actions">
                    <?= $this->Html->link('', ['action' => 'view', $daerah->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                    <?= $this->Html->link('', ['action' => 'edit', $daerah->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                    <?= $this->Form->postLink('', ['action' => 'delete', $daerah->id], ['confirm' => __('Are you sure you want to delete # {0}?', $daerah->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>