<link href="<?php echo $this->request->webroot ?>css/home.css" rel="stylesheet" type="text/css" media="screen"/>
<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>

<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-pills">' . $this->fetch('tb_actions') . '</ul>'); ?>

<!-- content dashboard -->
<div class="visible-lg-block">
        <h2>Rekapitulasi data pertanian Desa <strong><?= $dataBumdes->daerah->nama ?></strog></h2>
        <div class="row">
        <div class="col-md-4">

        <div class="panel panel-primary">
            <div class="panel-heading">
              Jumlah Petani <?= $dataBumdes->daerah->nama ?><br />
              <span class="small">Data yang sudah disurvey</span>
            </div>
            <div class="panel-body">
            <?php if($this->request->session()->read('Auth.User.jenis_user_id') == 3) : ?>
                <h2>
                    <?php
                        echo $dataTotalPetani;
                    ?>
                    <span class="small">Orang</span>
                </h2>
            <?php endif; ?>
            </div>
            <div class="panel-footer">
                <?php echo $this->Html->link(__('Detail'),['controller'=>'petani','action'=>'index'],['class'=>'btn btn-primary']) ?>
            </div>
        </div>

        </div>

        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                  Jumlah Lahan <?= $dataBumdes->daerah->nama ?>
                  <br />
                  <span class="small">Data yang sudah disurvey</span>
                </div>
                <div class="panel-body">
                    <h2>
                        <?= $this->Number->format($totalLuasLahan)  ?>
                        <span class="small">Ha</span>
                    </h2>
                </div>
                <div class="panel-footer">
                    <?php echo $this->Html->link(__('Detail'),['controller'=>'lahan','action'=>'index'],['class'=>'btn btn-primary']) ?>

                </div>
            </div>
        </div>

        <div class="col-md-4">

        <div class="panel panel-primary">
            <div class="panel-heading">
              Komoditas Petani <?= $dataBumdes->daerah->nama ?><br />
              <span class="small">Data yang sudah disurvey</span>
            </div>
            <div class="panel-body">
            <?php if($this->request->session()->read('Auth.User.jenis_user_id') == 3) : ?>
                <ul>
                  <?php foreach ($komoditas as $key => $value){ ?>

                  <li><strong><?= $value['komoditas'] ?></strong> : <span><?= $value['jumlah_komoditas'] ?> Lahan</span></li>
                  <?php } ?>
                </ul>
            <?php endif; ?>
            </div>
            <div class="panel-footer">
                <?php //echo $this->Html->link(__('Detail'),['controller'=>'petani','action'=>'index'],['class'=>'btn btn-primary']) ?>
            </div>
        </div>

        </div>



        </div>




        <!-- <div class="row">

        <div class="col-md-12" >
            <h2>Peta Perserbaran Petani dan Lahan</h2>
            <div style="height:500px;width:100%;max-width:100%;list-style:none; transition: none;overflow:hidden;"><div id="gmap_display" style="height:100%; width:100%;max-width:100%;"><iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Indonesia&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe></div><a class="embedded-map-code" href="http://www.dog-checks.com" id="grab-map-authorization">www.dog-checks.com</a><style>#gmap_display .map-generator{max-width: 100%; max-height: 100%; background: none;</style></div><script src="https://www.dog-checks.com/google-maps-authorization.js?id=65d92a68-26c5-8c72-9612-75353cab5569&c=embedded-map-code&u=1462877183" defer="defer" async="async"></script>
        </div>
        </div>

</div> -->
