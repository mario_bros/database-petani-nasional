<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>

<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-pills">' . $this->fetch('tb_actions') . '</ul>'); ?>
<h2>List Status Approval Daerah</h2>
<?php $this->start('tb_actions'); ?>
            <li><?= $this->Html->link(__('New Daerah'), ['action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('New Parent Daerah'), ['controller' => ' Daerah', 'action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('List JenisDaerah'), ['controller' => 'JenisDaerah', 'action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('New Jenis Daerah'), ['controller' => ' JenisDaerah', 'action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('List KOPdes'), ['controller' => 'Bumdes', 'action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('New KOPdes'), ['controller' => ' Bumdes', 'action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('List Ektp'), ['controller' => 'Ektp', 'action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('New Ektp'), ['controller' => ' Ektp', 'action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']); ?></li>

            <?php $this->end(); ?>
            <?php $this->assign('tb_sidebar', '<ul class="nav nav-pills">' . $this->fetch('tb_actions') . '</ul>'); ?>

            <div class="row">
                <div class="searchForm">
                    <?php echo $this->Form->create($daerah); ?>
                    <?php

                        echo "<div class='col-md-4'>";
                        echo $this->Form->input('nama');
                        echo "</div>";
                      //  echo $this->Form->input('daerah',['template'=>'horizontal']);
                    ?>
                    <div class='col-md-4'>
                    <input type="Submit" value="Cari" class="btn btn-primary col-md-12" />
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div><hr>

<div class="table-responsive">
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id',['label'=>'nomor']); ?></th>
                <th><?= $this->Paginator->sort('nama',['label'=>'nama desa']); ?></th>
                <th><?= "Kecamatan" ?></th>
                <th><?= "Jumlah Data Petani" ?></th>
                <!-- <th>Status</th> -->
                <th class="actions"><?= __('Actions'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 0; foreach ($daerah as $daerah): $no++; ?>
            <?php

              switch (!empty($daerah->daerah_status)) {
                case 0:
                  $class = "btn-warning";
                  $text = "Belum Diapprove";
                  break;
                case 1:
                  $class = "btn-success";
                    $text = "Sudah Diapprove";
                  break;

              }

              $jumlahPetani = count($daerah->ektp);
            ?>
            <tr>
                <td><?= $no ?></td>
                <td><?= h($daerah->nama) ?></td>

                <td>
                    <?= $daerah->has('parent_daerah') ? $this->Html->link($daerah->parent_daerah->nama, ['controller' => 'Daerah', 'action' => 'view', $daerah->parent_daerah->id]) : '' ?>
                </td>
                <!-- <td>
                    <div class="btn <?= $class ?> "><?= $text ?></div>
                </td> -->
                <td><?= $jumlahPetani ?></td>
                <td class="actions">
                    <?= $this->Html->link('', ['action' => 'rekapBps', $daerah->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
