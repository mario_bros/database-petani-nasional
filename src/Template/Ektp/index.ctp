<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
    
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-pills">' . $this->fetch('tb_actions') . '</ul>'); ?>
<h2>E-ktp</h2>
<div class="row">
    <div class="col-md-12 pull-right" id="secondNav">
        <ul class="nav nav-pills">    
            <li><?= $this->Html->link(__('New Ektp'), ['action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('New Daerah'), ['controller' => ' Daerah', 'action' => 'add']); ?></li>
        </ul>                
    </div>
</div>
<div class="table-responsive">
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
				<th><?= $this->Paginator->sort('no') ?></th>
                <th><?= $this->Paginator->sort('id','No Ektp'); ?></th>
                <th><?= $this->Paginator->sort('nama'); ?></th>
                <th><?= $this->Paginator->sort('alamat'); ?></th>
                <th><?= $this->Paginator->sort('tanggal_lahir'); ?></th>
                <th><?= $this->Paginator->sort('rt_rw'); ?></th>
                <th><?= $this->Paginator->sort('desa_kelurahan'); ?></th>
                <th><?= $this->Paginator->sort('kecamatan'); ?></th>
                <th class="actions"><?= __('Actions'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 0; foreach ($ektp as $ektp): $no++; ?>
            <tr>
				<td><?= $no ?></td>
                <td><?= h($ektp->id) ?></td>
                <td><?= h($ektp->nama) ?></td>
                <td><?= h($ektp->alamat) ?></td>
                <td><?= h($ektp->tanggal_lahir) ?></td>
                <td><?= h($ektp->rt_rw) ?></td>
                <td><?= h($ektp->desa_kelurahan) ?></td>
                <td><?= h($ektp->kecamatan) ?></td>
                <td class="actions">
                    <?= $this->Html->link('', ['action' => 'view', $ektp->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                    <?= $this->Html->link('', ['action' => 'edit', $ektp->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                    <?= $this->Form->postLink('', ['action' => 'delete', $ektp->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ektp->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>