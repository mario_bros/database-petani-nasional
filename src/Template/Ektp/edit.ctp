<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $ektp->no_ktp],
        ['confirm' => __('Are you sure you want to delete # {0}?', $ektp->no_ktp)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Ektp'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $ektp->no_ktp],
        ['confirm' => __('Are you sure you want to delete # {0}?', $ektp->no_ktp)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Ektp'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($ektp); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Ektp']) ?></legend>
    <?php
    echo $this->Form->input('nama');
    echo $this->Form->input('alamat');
    echo $this->Form->input('tanggal_lahir');
    echo $this->Form->input('rt_rw');
    echo $this->Form->input('desa_kelurahan');
    echo $this->Form->input('kecamatan');
    echo $this->Form->input('kabupaten_kota');
    echo $this->Form->input('pekerjaan');
    echo $this->Form->input('kewarganegaraan');
    echo $this->Form->input('status_perkawinan');
    echo $this->Form->input('gol_darah');
    echo $this->Form->input('tanggal_terbit');
    echo $this->Form->input('daerah_id', ['options' => $daerah]);
    echo $this->Form->input('tanggal_berakhir');
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
