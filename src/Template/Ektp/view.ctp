<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Ektp'), ['action' => 'edit', $ektp->no_ktp]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Ektp'), ['action' => 'delete', $ektp->no_ktp], ['confirm' => __('Are you sure you want to delete # {0}?', $ektp->no_ktp)]) ?> </li>
<li><?= $this->Html->link(__('List Ektp'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Ektp'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
<li><?= $this->Html->link(__('Edit Ektp'), ['action' => 'edit', $ektp->no_ktp]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Ektp'), ['action' => 'delete', $ektp->no_ktp], ['confirm' => __('Are you sure you want to delete # {0}?', $ektp->no_ktp)]) ?> </li>
<li><?= $this->Html->link(__('List Ektp'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Ektp'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title">Data Detil KTP <strong><?= h($ektp->nama) ?></strong></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Nama') ?></td>
            <td><?= h($ektp->nama) ?></td>
        </tr>
        <tr>
            <td><?= __('NIK') ?></td>
            <td><?= $this->Number->format($ektp->id) ?></td>
        </tr>
        <tr>
            <td><?= __('Alamat') ?></td>
            <td><?= h($ektp->alamat) ?></td>
        </tr>
        <tr>
            <td><?= __('RT/RW') ?></td>
            <td><?= h($ektp->rt_rw) ?></td>
        </tr>
        <tr>
            <td><?= __('Desa/Kelurahan') ?></td>
            <td><?= $ektp->has('daerah') ? $this->Html->link($ektp->daerah->nama, ['controller' => 'Daerah', 'action' => 'view', $ektp->daerah->id]) : '' ?></td>
        </tr>
        <?php foreach ($daerah as $key=>$value) { ?>
           
        <tr>
            <td><?= __($key) ?></td>
            <td><?= h($value->nama) ?></td>
        </tr>
        <?php } ?>
        <tr>
            <td><?= __('Pekerjaan') ?></td>
            <td><?= h($ektp->pekerjaan) ?></td>
        </tr>
        <tr>
            <td><?= __('Kewarganegaraan') ?></td>
            <td><?= h($ektp->kewarganegaraan) ?></td>
        </tr>
        <tr>
            <td><?= __('Status Perkawinan') ?></td>
            <td><?= h($ektp->status_perkawinan) ?></td>
        </tr>
        <tr>
            <td><?= __('Gol Darah') ?></td>
            <td><?= h($ektp->gol_darah) ?></td>
        </tr>
        
        
        <tr>
            <td><?= __('Tanggal Lahir') ?></td>
            <td><?= h($ektp->tanggal_lahir) ?></td>
        </tr>
        <tr>
            <td><?= __('Tanggal Terbit') ?></td>
            <td><?= h($ektp->tanggal_terbit) ?></td>
        </tr>
        <tr>
            <td><?= __('Tanggal Berakhir') ?></td>
            <td><?= h($ektp->tanggal_berakhir) ?></td>
        </tr>
    </table>
</div>

