<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Jenis Daerah'), ['action' => 'edit', $jenisDaerah->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Jenis Daerah'), ['action' => 'delete', $jenisDaerah->id], ['confirm' => __('Are you sure you want to delete # {0}?', $jenisDaerah->id)]) ?> </li>
<li><?= $this->Html->link(__('List Jenis Daerah'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Jenis Daerah'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
<li><?= $this->Html->link(__('Edit Jenis Daerah'), ['action' => 'edit', $jenisDaerah->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Jenis Daerah'), ['action' => 'delete', $jenisDaerah->id], ['confirm' => __('Are you sure you want to delete # {0}?', $jenisDaerah->id)]) ?> </li>
<li><?= $this->Html->link(__('List Jenis Daerah'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Jenis Daerah'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($jenisDaerah->id) ?></h3>
    </div>
    <div class="table-responsive">
        <table class="table table-striped" cellpadding="0" cellspacing="0">
            <tr>
                <td><?= __('Nama') ?></td>
                <td><?= h($jenisDaerah->nama) ?></td>
            </tr>
            <tr>
                <td><?= __('Id') ?></td>
                <td><?= $this->Number->format($jenisDaerah->id) ?></td>
            </tr>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related Daerah') ?></h3>
    </div>
    <?php if (!empty($jenisDaerah->daerah)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('Nama') ?></th>
                    <th><?= __('Lat') ?></th>
                    <th><?= __('Lon') ?></th>
                    <th><?= __('Note') ?></th>
                    <th><?= __('Parent Id') ?></th>
                    <th><?= __('Jenis Daerah Id') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($jenisDaerah->daerah as $daerah): ?>
                    <tr>
                        <td><?= h($daerah->id) ?></td>
                        <td><?= h($daerah->nama) ?></td>
                        <td><?= h($daerah->lat) ?></td>
                        <td><?= h($daerah->lon) ?></td>
                        <td><?= h($daerah->note) ?></td>
                        <td><?= h($daerah->parent_id) ?></td>
                        <td><?= h($daerah->jenis_daerah_id) ?></td>
                        <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Daerah', 'action' => 'view', $daerah->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                            <?= $this->Html->link('', ['controller' => 'Daerah', 'action' => 'edit', $daerah->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Daerah', 'action' => 'delete', $daerah->id], ['confirm' => __('Are you sure you want to delete # {0}?', $daerah->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php else: ?>
        <p class="panel-body">no related Daerah</p>
    <?php endif; ?>
</div>
