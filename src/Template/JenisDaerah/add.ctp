<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('List Jenis Daerah'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?= $this->Html->link(__('List Jenis Daerah'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($jenisDaerah); ?>
<fieldset>
    <legend><?= __('Add {0}', ['Jenis Daerah']) ?></legend>
    <?php
    echo $this->Form->input('nama');
    ?>
</fieldset>
<?= $this->Form->button(__("Add")); ?>
<?= $this->Form->end() ?>
