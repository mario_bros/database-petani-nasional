
<?php
  use Cake\Core\Configure;
  $this->Html->script('jquery/bootstrap-datepicker.js', ['block' => true]);


    $notificationCell = $this->cell('Notifications');
     $username = $this->request->session()->read('Auth.User.username');

             $jenis = $this->request->session()->read('Auth.User.jenis_user_id');
                if($jenis == 1)
                    $role = 'Admin';
                if($jenis == 2)
                    $role = 'BPS';
                if($jenis == 3)
                    $role = 'Surveyor';


            ?>
        <?php $this->start('css'); ?>
             <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
            <link href="<?php echo $this->request->webroot ?>assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>
            <link href="<?php echo $this->request->webroot ?>assets/plugins/jquery-slider/css/jquery.sidr.light.css" rel="stylesheet" type="text/css" media="screen"/>
            <!-- BEGIN CORE CSS FRAMEWORK -->
            <link href="<?php echo $this->request->webroot ?>assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo $this->request->webroot ?>css/jquery.auto-complete.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo $this->request->webroot ?>css/jquery-ui.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo $this->request->webroot ?>assets/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo $this->request->webroot ?>assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo $this->request->webroot ?>assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo $this->request->webroot ?>assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen"/>

            <!-- BEGIN CSS TEMPLATE -->
            <link href="<?php echo $this->request->webroot ?>assets/css/style.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo $this->request->webroot ?>assets/css/responsive.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo $this->request->webroot ?>assets/css/custom-icon-set.css" rel="stylesheet" type="text/css"/>
        <?php $this->end(); ?>
<!-- END HEAD -->
<?php
  $this->prepend('tb_body_attrs', ' class="' . implode(' ', [$this->request->controller, $this->request->action]) . '" ');
  $this->start('tb_body_start');
?>
<!-- BEGIN BODY -->
<body <?= $this->fetch('tb_body_attrs') ?>>
<!-- BEGIN HEADER -->
<div class="header navbar navbar-inverse ">
  <!-- BEGIN TOP NAVIGATION BAR -->
  <div class="navbar-inner">
  <div class="header-seperation">
    <ul class="nav pull-left notifcation-center" id="main-menu-toggle-wrapper" style="display:none">
     <li class="dropdown"> <a id="main-menu-toggle" href="#main-menu"  class="" > <div class="iconset top-menu-toggle-white"></div> </a> </li>
    </ul>
      <!-- BEGIN LOGO -->
      <a id="logo" href="<?php echo $this->request->webroot ?>"><h3>Logistik Tani</h3></a>
      <!-- END LOGO -->
      <ul class="nav pull-right notifcation-center">
        <li class="dropdown" id="header_task_bar">
          <a href="<?php echo $this->request->webroot ?>" class="dropdown-toggle active" data-toggle="">
            <div class="iconset top-home"></div>
          </a>
        </li>
        <li class="dropdown" id="icon-logout">
          <a href="<?php echo $this->Url->build(['controller'=>'users','action'=>'logout']) ?>" class="dropdown-toggle active" data-toggle="">
            <div class="fa fa-power-off"></div>
          </a>
        </li>
      </ul>
      </div>
      <!-- END RESPONSIVE MENU TOGGLER -->
      <div class="header-quick-nav" >
      <!-- BEGIN TOP NAVIGATION MENU -->
    <div class="pull-left">
        <ul class="nav quick-section">
          <li class="quicklinks"> <a href="#" class="" id="layout-condensed-toggle" >
            <div class="iconset top-menu-toggle-dark"></div>
            </a> </li>
        </ul>
        <ul class="nav quick-section">
          <li class="quicklinks"> <a href="#" class="" onclick="window.location.reload()"  >
            <div class="iconset top-reload"></div>
            </a> </li>


      </ul>
    </div>
   <!-- END TOP NAVIGATION MENU -->
   <!-- BEGIN CHAT TOGGLER -->
      <div class="pull-right">
    <div class="chat-toggler">
        <a href="#" class="dropdown-toggle" id="my-task-list" data-placement="bottom"  data-content='' data-toggle="dropdown" data-original-title="User Menu">
          <div class="user-details">
            <div class="username">

              <?php echo $username; ?>
            </div>
          </div>
          <div class="iconset top-down-arrow"></div>
        </a>
        <div id="notification-list" style="display:none">
          <div class="btn-group btn-group-vertical">
          <?= $this->Html->link(__('Edit'),['controller'=>'Users','action'=>'edit',$this->request->session()->read('Auth.User.id')],['class'=>'btn btn-warning']) ?></li>
          <?= $this->Html->link(__('logout'),['controller'=>'Users','action'=>'logout'],['class'=>'btn btn-danger']) ?>
        </div>
        </div>
        <div class="profile-pic">
          <img src="<?php echo $this->request->webroot ?>img/Farmer-icon.png"  alt="" data-src="<?php echo $this->request->webroot ?>img/Farmer-icon.png" data-src-retina="<?php echo $this->request->webroot ?>img/Farmer-icon.png" width="35" height="35" />
        </div>
      </div>

      </div>
     <!-- END CHAT TOGGLER -->
      </div>
      <!-- END TOP NAVIGATION MENU -->

  </div>
  <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar" id="main-menu">
  <!-- BEGIN MINI-PROFILE -->
   <div class="page-sidebar-wrapper" id="main-menu-wrapper">
   <div class="user-info-wrapper">
  <div class="profile-wrapper">
    <img src="<?php echo $this->request->webroot ?>img/Farmer-icon.png"  alt="" data-src="<?php echo $this->request->webroot ?>img/Farmer-icon.png" data-src-retina="<?php echo $this->request->webroot ?>img/Farmer-icon.png" width="69" height="69" />
  </div>
    <div class="user-info">

      <div class="greeting">Welcome</div>
      <div class="username"><?php echo $username; ?></div>
      <div class="status">role<a href="#"><div class="status-icon green"></div><?= $role ?></a></div>
    </div>
   </div>
  <!-- END MINI-PROFILE -->

   <!-- BEGIN SIDEBAR MENU -->

    <ul>
      <?php
            //debug($this->request->session()->read()); exit(' bye');
            if ($this->request->session()->read('Auth.User.jenis_user_id')==1) :
            ?>
                <li><?= $this->Html->link(__('<span class="title">Poktan</span> <span class="selected"></span>'), ['controller'=>'poktan','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'poktan','escape'=>false]); ?></li>
                <li><?= $this->Html->link(__('Gapoktan'), ['controller'=>'gapoktan','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'gapoktan']); ?></li>
                <li><?= $this->Html->link(__('Penyuluh'), ['controller'=>'Penyuluh','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'penyuluh']); ?></li>
                <li><?= $this->Html->link(__('Petani'), ['controller'=>'Petani','action' => 'adminIndex'],['class'=>'mdl-navigation__link link-petani','id'=>'petani']); ?></li>
                <li><?= $this->Html->link(__('BPS'), ['controller'=>'Bps','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'petani']); ?></li>
                <li><?= $this->Html->link(__('Ektp'), ['controller'=>'Ektp','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'ektp']); ?></li>
                <li><?= $this->Html->link(__('BumDes'), ['controller'=>'Bumdes','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'bumdes']); ?></li>
                <li><?= $this->Html->link(__('Daerah'), ['controller'=>'Daerah','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'daerah']); ?></li>
                <li><?= $this->Html->link(__('RKP'), ['controller'=>'Rdkk','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'rkp']); ?></li>
                <li><?= $this->Html->link(__('Users'), ['controller'=>'users','action' => 'index'],['class'=>'mdl-navigation__link link-petani']); ?></li>
                <li><?= $this->Html->link(__('Items'), ['controller'=>'items','action' => 'index'],['class'=>'mdl-navigation__link link-petani']); ?></li>
                <li><?= $this->Html->link(__('Subsektor'), ['controller'=>'subsektor','action' => 'index'],['class'=>'mdl-navigation__link link-petani']); ?></li>
                <li><?= $this->Html->link(__('Komoditas'), ['controller'=>'komoditas','action' => 'index'],['class'=>'mdl-navigation__link link-petani']); ?></li>

            <?php elseif ($this->request->session()->read('Auth.User.jenis_user_id')==2) : ?>
                <li><?= $this->Html->link(__('Data Tunggal Petani'), ['controller'=>'poktan','action' => 'index',$this->request->session()->read('Poktan.id')],['class'=>'mdl-navigation__link link-petani','id'=>'poktan']); ?></li>
                <li><?= $this->Html->link(__('Tani Konsumen'), ['controller'=>'Rdkk','action' => 'index',$this->request->session()->read('Rdkk.id')],['class'=>'mdl-navigation__link link-petani','id'=>'rkp']); ?></li>

            <?php elseif($this->request->session()->read('Auth.User.jenis_user_id')==3) : ?>
               <li><?= $this->Html->link(__('<i class="fa fa-male"></i> <span class="title">Data Tunggal Petani</span> <span class="selected"></span>'), ['controller'=>'petani','action' => 'index'],['class'=>'link-petani','id'=>'poktan','escape'=>false]); ?></li>
                <li><?= $this->Html->link(__('<i class="fa fa-calendar-o"></i> <span class="title">Tani Konsumen</span> <span class="selected"></span>'), ['controller'=>'Rdkk','action' => 'listrkppoktan',6],['class'=>'mdl-navigation__link link-petani','id'=>'rkp','escape'=>false]); ?></li>
                <li><?= $this->Html->link(__('<i class="fa fa-bar-chart-o"></i> <span class="title">Transaksi Kopdes</span> <span class="selected"></span>'), "#" ,['class'=>'mdl-navigation__link link-petani','id'=>'rkp','escape'=>false]); ?></li>
                <li><?= $this->Html->link(__('<i class="fa fa-retweet"></i> <span class="title">Tani Produsen</span> <span class="selected"></span>'), "#",['class'=>'mdl-navigation__link link-petani','id'=>'rkp','escape'=>false]); ?></li>
                 <li><?= $this->Html->link(__('<i class="fa fa-map-marker"></i> <span class="title">Telusur</span> <span class="selected"></span>'),  "#" ,['class'=>'mdl-navigation__link link-petani','id'=>'rkp','escape'=>false]); ?></li>
                <li><?= $this->Html->link(__('<i class="fa fa-money"></i> <span class="title">Laporan</span> <span class="selected"></span>'),  "#" ,['class'=>'mdl-navigation__link link-petani','id'=>'rkp','escape'=>false]); ?></li>


            <?php else : ?>
                <li><?= $this->Html->link(__('Poktan Saya'), ['controller'=>'poktan','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'poktan']); ?></li>
                <li><?= $this->Html->link(__('RKP Saya'), ['controller'=>'Rdkk','action' => 'index',$this->request->session()->read('Rdkk.id')],['class'=>'mdl-navigation__link link-petani','id'=>'rkp']); ?></li>

            <?php endif; ?>
                <li><?= $this->Html->link(__('<i class="fa fa-power-off"></i> <span class="title">Logout</span> <span class="selected"></span>'), ['controller'=>'Users','action' => 'logout'],['class'=>'mdl-navigation__link link-petani','id'=>'logout','escape'=>false]); ?></li>

    </ul>
  <div class="side-bar-widgets">


  </div>
  <div class="clearfix"></div>
    <!-- END SIDEBAR MENU -->
  </div>
  </div>
  <a href="#" class="scrollup">Scroll</a>
   <div class="footer-widget">
  <div class="progress transparent progress-small no-radius no-margin">
    <div data-percentage="79%" class="progress-bar progress-bar-success animate-progress-bar" ></div>
  </div>
  <div class="pull-right">
    <div class="details-status">

  </div>
  <a href="<?php echo $this->Url->build(['controller'=>'users','action'=>'logout']) ?>"><i class="fa fa-power-off"></i></a></div>
  </div>
  <!-- END SIDEBAR -->
  <!-- BEGIN PAGE CONTAINER-->
  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div id="portlet-config" class="modal hide">
      <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button"></button>
        <h3>Widget Settings</h3>
      </div>
      <div class="modal-body"> Widget settings form goes here </div>
    </div>
    <div class="clearfix"></div>
    <div class="content">

      <!-- <h3><?php echo strtoupper($this->request->action) ?></h3>    -->

      <!-- start content here -->
        <?php
        $this->end(); ?>
        <div class="content-wrapper">
            <?php   if ($this->request->session()->read('Auth.User.jenis_user_id')==1)
                        echo $this->fetch('tb_sidebar');
            ?>
        <?php echo $this->fetch('content'); ?>
        </div>
        <?php
        $this->start('tb_body_end');

          //content


        ?>
      <!-- end content here -->

    </div>
    </div>
  </div>
 </div>
<!-- END CONTAINER -->
<!-- BEGIN CHAT -->
<div id="sidr" class="chat-window-wrapper">
  <div id="main-chat-wrapper" >
  <div class="chat-window-wrapper fadeIn" id="chat-users" >
    <div class="chat-header">
    <div class="pull-left">
      <input type="text" placeholder="search">
    </div>
      <div class="pull-right">
        <a href="#" class="" ><div class="iconset top-settings-dark "></div> </a>
      </div>
    </div>
    <div class="side-widget">
       <div class="side-widget-title">group chats</div>
        <div class="side-widget-content">
       <div id="groups-list">
        <ul class="groups" >
          <li><a href="#"><div class="status-icon green"></div>Office work</a></li>
          <li><a href="#"><div class="status-icon green"></div>Personal vibes</a></li>
        </ul>
      </div>
      </div>
    </div>
    <div class="side-widget fadeIn">
       <div class="side-widget-title">favourites</div>
       <div id="favourites-list">
        <div class="side-widget-content" >
        <div class="user-details-wrapper active" data-chat-status="online" data-chat-user-pic="<?php echo $this->request->webroot ?>assets/img/profiles/d.jpg" data-chat-user-pic-retina="<?php echo $this->request->webroot ?>assets/img/profiles/d2x.jpg" data-user-name="Jane Smith">
          <div class="user-profile">
            <img src="<?php echo $this->request->webroot ?>assets/img/profiles/d.jpg"  alt="" data-src="<?php echo $this->request->webroot ?>assets/img/profiles/d.jpg" data-src-retina="<?php echo $this->request->webroot ?>assets/img/profiles/d2x.jpg" width="35" height="35">
          </div>
          <div class="user-details">
            <div class="user-name">
            Jane Smith
            </div>
            <div class="user-more">
            Hello you there?
            </div>
          </div>
          <div class="user-details-status-wrapper">
            <span class="badge badge-important">3</span>
          </div>
          <div class="user-details-count-wrapper">
            <div class="status-icon green"></div>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="user-details-wrapper" data-chat-status="busy" data-chat-user-pic="<?php echo $this->request->webroot ?>assets/img/profiles/d.jpg" data-chat-user-pic-retina="<?php echo $this->request->webroot ?>assets/img/profiles/d2x.jpg" data-user-name="David Nester">
          <div class="user-profile">
            <img src="<?php echo $this->request->webroot ?>assets/img/profiles/c.jpg"  alt="" data-src="<?php echo $this->request->webroot ?>assets/img/profiles/c.jpg" data-src-retina="<?php echo $this->request->webroot ?>assets/img/profiles/c2x.jpg" width="35" height="35">
          </div>
          <div class="user-details">
            <div class="user-name">
            David Nester
            </div>
            <div class="user-more">
            Busy, Do not disturb
            </div>
          </div>
          <div class="user-details-status-wrapper">
            <div class="clearfix"></div>
          </div>
          <div class="user-details-count-wrapper">
            <div class="status-icon red"></div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      </div>
    </div>
    <div class="side-widget">
       <div class="side-widget-title">more friends</div>
       <div class="side-widget-content" id="friends-list">
        <div class="user-details-wrapper" data-chat-status="online" data-chat-user-pic="<?php echo $this->request->webroot ?>assets/img/profiles/d.jpg" data-chat-user-pic-retina="<?php echo $this->request->webroot ?>assets/img/profiles/d2x.jpg" data-user-name="Jane Smith">
          <div class="user-profile">
            <img src="<?php echo $this->request->webroot ?>assets/img/profiles/d.jpg"  alt="" data-src="<?php echo $this->request->webroot ?>assets/img/profiles/d.jpg" data-src-retina="<?php echo $this->request->webroot ?>assets/img/profiles/d2x.jpg" width="35" height="35">
          </div>
          <div class="user-details">
            <div class="user-name">
            Jane Smith
            </div>
            <div class="user-more">
            Hello you there?
            </div>
          </div>
          <div class="user-details-status-wrapper">

          </div>
          <div class="user-details-count-wrapper">
            <div class="status-icon green"></div>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="user-details-wrapper" data-chat-status="busy" data-chat-user-pic="<?php echo $this->request->webroot ?>assets/img/profiles/d.jpg" data-chat-user-pic-retina="<?php echo $this->request->webroot ?>assets/img/profiles/d2x.jpg" data-user-name="David Nester">
          <div class="user-profile">
            <img src="<?php echo $this->request->webroot ?>assets/img/profiles/h.jpg"  alt="" data-src="<?php echo $this->request->webroot ?>assets/img/profiles/h.jpg" data-src-retina="<?php echo $this->request->webroot ?>assets/img/profiles/h2x.jpg" width="35" height="35">
          </div>
          <div class="user-details">
            <div class="user-name">
            David Nester
            </div>
            <div class="user-more">
            Busy, Do not disturb
            </div>
          </div>
          <div class="user-details-status-wrapper">
            <div class="clearfix"></div>
          </div>
          <div class="user-details-count-wrapper">
            <div class="status-icon red"></div>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="user-details-wrapper" data-chat-status="online" data-chat-user-pic="<?php echo $this->request->webroot ?>assets/img/profiles/d.jpg" data-chat-user-pic-retina="<?php echo $this->request->webroot ?>assets/img/profiles/d2x.jpg" data-user-name="Jane Smith">
          <div class="user-profile">
            <img src="<?php echo $this->request->webroot ?>assets/img/profiles/c.jpg"  alt="" data-src="<?php echo $this->request->webroot ?>assets/img/profiles/c.jpg" data-src-retina="<?php echo $this->request->webroot ?>assets/img/profiles/c2x.jpg" width="35" height="35">
          </div>
          <div class="user-details">
            <div class="user-name">
            Jane Smith
            </div>
            <div class="user-more">
            Hello you there?
            </div>
          </div>
          <div class="user-details-status-wrapper">

          </div>
          <div class="user-details-count-wrapper">
            <div class="status-icon green"></div>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="user-details-wrapper" data-chat-status="busy" data-chat-user-pic="<?php echo $this->request->webroot ?>assets/img/profiles/d.jpg" data-chat-user-pic-retina="<?php echo $this->request->webroot ?>assets/img/profiles/d2x.jpg" data-user-name="David Nester">
          <div class="user-profile">
            <img src="<?php echo $this->request->webroot ?>assets/img/profiles/h.jpg"  alt="" data-src="<?php echo $this->request->webroot ?>assets/img/profiles/h.jpg" data-src-retina="<?php echo $this->request->webroot ?>assets/img/profiles/h2x.jpg" width="35" height="35">
          </div>
          <div class="user-details">
            <div class="user-name">
            David Nester
            </div>
            <div class="user-more">
            Busy, Do not disturb
            </div>
          </div>
          <div class="user-details-status-wrapper">
            <div class="clearfix"></div>
          </div>
          <div class="user-details-count-wrapper">
            <div class="status-icon red"></div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>

  <div class="chat-window-wrapper fadeIn" id="messages-wrapper" style="display:none">
  <div class="chat-header">
    <div class="pull-left">
      <input type="text" placeholder="search">
    </div>
      <div class="pull-right">
        <a href="#" class="" ><div class="iconset top-settings-dark "></div> </a>
      </div>
    </div>
  <div class="clearfix"></div>
  <div class="chat-messages-header">
  <div class="status online"></div><span class="semi-bold">Jane Smith(Typing..)</span>
  <a href="#" class="chat-back"><i class="icon-custom-cross"></i></a>
  </div>
  <div class="chat-messages">
    <div class="sent_time">Yesterday 11:25pm</div>
    <div class="user-details-wrapper " >
      <div class="user-profile">
        <img src="<?php echo $this->request->webroot ?>assets/img/profiles/d.jpg"  alt="" data-src="<?php echo $this->request->webroot ?>assets/img/profiles/d.jpg" data-src-retina="<?php echo $this->request->webroot ?>assets/img/profiles/d2x.jpg" width="35" height="35">
      </div>
      <div class="user-details">
        <div class="bubble">
          Hello, You there?
         </div>
      </div>
      <div class="clearfix"></div>
       <div class="sent_time off">Yesterday 11:25pm</div>
    </div>
    <div class="user-details-wrapper ">
      <div class="user-profile">
        <img src="<?php echo $this->request->webroot ?>assets/img/profiles/d.jpg"  alt="" data-src="<?php echo $this->request->webroot ?>assets/img/profiles/d.jpg" data-src-retina="<?php echo $this->request->webroot ?>assets/img/profiles/d2x.jpg" width="35" height="35">
      </div>
      <div class="user-details">
        <div class="bubble">
          How was the meeting?
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="sent_time off">Yesterday 11:25pm</div>
    </div>
    <div class="user-details-wrapper ">
      <div class="user-profile">
        <img src="<?php echo $this->request->webroot ?>assets/img/profiles/d.jpg"  alt="" data-src="<?php echo $this->request->webroot ?>assets/img/profiles/d.jpg" data-src-retina="<?php echo $this->request->webroot ?>assets/img/profiles/d2x.jpg" width="35" height="35">
      </div>
      <div class="user-details">
        <div class="bubble">
          Let me know when you free
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="sent_time off">Yesterday 11:25pm</div>
    </div>
    <div class="sent_time ">Today 11:25pm</div>
    <div class="user-details-wrapper pull-right">
      <div class="user-details">
        <div class="bubble sender">
          Let me know when you free
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="sent_time off">Sent On Tue, 2:45pm</div>
    </div>
  </div>
  </div>
  <div class="chat-input-wrapper" style="display:none">
    <textarea id="chat-message-input" rows="1" placeholder="Type your message"></textarea>
  </div>
  <div class="clearfix"></div>
  </div>
</div>
<!-- END CHAT -->
<!-- END CONTAINER -->
<?= $this->fetch('modal'); ?>
<?php  if(!empty($petani)){ ?>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <?= $this->Form->create($petani); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Mohon Sebutkan Alasan Penolakan</h4>
                </div>
                <div class="modal-body">
                    <!--<p>&nbsp;</p>-->
                    <?= $this->Form->input('alasan', ['type' => "textarea"]);?>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <?= $this->Form->button(__("Submit"), ['class'=>'btn btn-info', 'name' => "Disapproved"]); ?>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <?= $this->Form->end(); ?>
            </div>

        </div>
    </div>
<?php
  }

?>

<div id="template">
  <ul class="nav nav-tabs tabSubSektorTemplate" >
    <li class=""><a href="#tab1hellowWorld">Hello World</a></li>

  </ul>

  <!-- general FORM -->
  <div class="tab-pane generalForm" id="tab1hellowWorld" >
    <div class="row column-seperation lahanForm">

        <div class='col-md-12'>Jenis usaha terdiri atas : </div><hr>

        <div class="add_lahan" class="col-md-12">
            <div class="lahan_content_form">
                <fieldset class="">
                    <div class="dataSubsektor">
                    <?php
                        // echo $this->Form->input('lahan.0.sektor_id',['empty'=>'Pilih sektor', 'options' => ['Pertanian'], 'class' => "",'template'=>['horizontal']]);
                        // echo $this->Form->input('lahan.0.subsektor_id',['empty'=>'Pilih Jenis Kegiatan Usaha', 'options' => $optsSubSektor, 'class' => "lahan-subsektor_id", 'label'=>'Jenis Kegiatan Usaha Pertanian']);
                        // echo $this->Form->input('lahan.0.komoditas_id',['empty'=>'Pilih Komoditas', 'options' => [], 'class' => "lahan-komoditas_id"]);
                    ?>
                  </div>
                </fieldset>

                <fieldset class="col-md-12">
                    <?php
                        echo "<div class='col-md-3'>";
                        echo $this->Form->hidden('komoditas_petani.0.subsektor_id',['class'=>'subsektor_id_lahan','value'=>0]);
                        echo $this->Form->hidden('komoditas_petani.0.id_komoditas',['class'=>'komoditasPetani']);
                        echo $this->Form->hidden('komoditas_petani.0.lahan.jenis_tanaman_id',['value'=>1,'class'=>'JenisTanaman']);
                        echo $this->Form->input('komoditas_petani.0.id_jenis_petani',['type'=>'select','empty'=>'please select one','options'=>$jenis_petani, 'class' => "lahan-jenis_tanaman_id", 'label' => "Status Pengelolaan"]);
                        echo "</div>";
                        ?>
                        <div class="dataLahanDetil">
                            <?php
                            echo "<div class='col-md-3 jenisLahan'>";
                            echo $this->Form->input('komoditas_petani.0.lahan.jenis_lahan_id',['label'=>'Tipe Lahan', 'class' => "lahan-id_jenis_lahan", 'options' => $jenis_lahan,'empty'=>'Pilih Salah Satu','required'=>FALSE]);
                            echo "</div>";
                            echo "<div class='col-md-3 luasLahan'>";
                            echo $this->Form->input('komoditas_petani.0.lahan.luas', ['class' => "lahan-luas", 'type'=>'number','step'=>"0.01",'label' => 'Luas Lahan (Ha)','required'=>0]);
                            echo "</div>";

                            echo "<div class='col-md-3'>";
                            echo $this->Form->input('komoditas_petani.0.lahan.daerah_id',['label'=>'Nama Desa Lahan', 'class' => "lahan-daerah_id", 'options' => $daerah,'empty'=>'Pilih Desa']);
                            echo "</div>";
                            //field peternakan

                            echo "<div class='peternakan'><div class='col-md-6'>";
                            echo $this->Form->hidden('komoditas_petani.0.lahan.assets.1.nama_asset',['value'=>'Ternak Betina','class'=>'namaAsset']);
                            echo $this->Form->input('komoditas_petani.0.lahan.assets.1.jumlah',['label'=>'Jumlah Ternak Betina','class'=>'jumlahAsset']);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->hidden('komoditas_petani.0.lahan.assets.2.nama_asset',['value'=>'Ternak Jantan','class'=>'namaAsset2']);
                            echo $this->Form->input('komoditas_petani.0.lahan.assets.2.jumlah',['label'=>'Jumlah Ternak Jantan','class'=>'jumlahAsset2']);
                            echo "</div></div>";
                            //end of peternakan
                            echo "<div class='col-md-12'>";
                            echo $this->Form->input('komoditas_petani.0.lahan.alamat',['type'=>'textarea', 'class' => "lahan-alamat", 'label' => 'Alamat Lahan','required'=>0]);
                            echo "</div>";
                            ?>
                        </div>
                </fieldset>
            </div>
        </div>
        <fieldset class="col-md-12 actionButton" data-id="0">
            <legend><a href="#" class="addAllLahan btn btn-success">Add Lahan</a>  <a href="#" class="delLahan btn btn-warning" >Remove Lahan</a></legend>
        </fieldset>
    </div>
  </div>
  <!-- end of general form -->
</div>

<!-- BEGIN CORE JS FRAMEWORK-->
<script src="<?php echo $this->request->webroot ?>assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/breakpoints.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<!-- END CORE JS FRAMEWORK -->
<!-- END CORE JS FRAMEWORK -->
<script src="<?php echo $this->request->webroot ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/jquery-slider/jquery.sidr.min.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo $this->request->webroot ?>assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/jquery-block-ui/jqueryblockui.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/jquery-inputmask/jquery.inputmask.min.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/jquery-autonumeric/autoNumeric.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/ios-switch/ios7-switch.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/boostrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="<?php echo $this->request->webroot ?>assets/js/form_elements.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/js/tabs_accordian.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>
<!-- BEGIN CORE TEMPLATE JS -->
<script src="<?php echo $this->request->webroot ?>assets/js/form_elements.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/js/form_validations.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/js/core.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/js/chat.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/js/demo.js" type="text/javascript"></script>
<!-- END CORE TEMPLATE JS -->

</body>
<?php $this->end(); ?>

<?php

  if (!$this->fetch('tb_flash')) {
    $this->start('tb_flash');
    if (isset($this->Flash))
        echo $this->Flash->render();
    $this->end();
}
?>
