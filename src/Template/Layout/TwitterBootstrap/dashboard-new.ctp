<?php
/* @var $this \Cake\View\View */
use Cake\Core\Configure;
$this->Html->script('jquery/bootstrap-datepicker.js', ['block' => true]);
$this->Html->css('BootstrapUI.dashboard', ['block' => true]);
$this->Html->css('bootstrap/dashboard', ['block' => true]);
$this->prepend('tb_body_attrs', ' class="' . implode(' ', [$this->request->controller, $this->request->action]) . '" ');
$this->start('tb_body_start');

$notificationCell = $this->cell('Notifications');
?>

    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="<?php echo $this->request->webroot ?>css/material.min.css">
    <link rel="stylesheet" href="<?php echo $this->request->webroot ?>css/styles.css">
    <link rel="stylesheet" href="<?php echo $this->request->webroot ?>css/petani.css"> 
    <script src="https://code.getmdl.io/1.1.3/material.min.js"></script>-->
    <link href="<?php echo $this->request->webroot ?>pages/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $this->request->webroot ?>pages/assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $this->request->webroot ?>pages/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $this->request->webroot ?>pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo $this->request->webroot ?>pages/assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo $this->request->webroot ?>pages/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo $this->request->webroot ?>pages/assets/plugins/nvd3/nv.d3.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo $this->request->webroot ?>pages/assets/plugins/mapplic/css/mapplic.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $this->request->webroot ?>pages/assets/plugins/rickshaw/rickshaw.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $this->request->webroot ?>pages/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo $this->request->webroot ?>pages/assets/plugins/jquery-metrojs/MetroJs.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo $this->request->webroot ?>pages/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="<?php echo $this->request->webroot ?>pages/pages/css/pages.css" rel="stylesheet" type="text/css" />
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->request->webroot ?>pages/assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->request->webroot ?>pages/assets/plugins/classie/classie.js"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/nvd3/lib/d3.v3.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/nvd3/nv.d3.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/nvd3/src/utils.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/nvd3/src/tooltip.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/nvd3/src/interactiveLayer.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/nvd3/src/models/axis.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/nvd3/src/models/line.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/nvd3/src/models/lineWithFocusChart.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/mapplic/js/hammer.js"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/mapplic/js/jquery.mousewheel.js"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/mapplic/js/mapplic.js"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/rickshaw/rickshaw.min.js"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/jquery-metrojs/MetroJs.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/jquery-sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/skycons/skycons.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="<?php echo $this->request->webroot ?>pages/pages/js/pages.min.js"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="<?php echo $this->request->webroot ?>pages/assets/js/dashboard.js" type="text/javascript"></script>
    <script src="<?php echo $this->request->webroot ?>pages/assets/js/scripts.js" type="text/javascript"></script>

  <body <?= $this->fetch('tb_body_attrs') ?>class="fixed-header dashboard">
    <div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
      <div class="android-header mdl-layout__header mdl-layout__header--waterfall">
        <div class="mdl-layout__header-row">
          <span class="android-title mdl-layout-title title-petani">
            DATA TUNGGAL PETANI
          </span>

          <!-- Add spacer, to align navigation to the right in desktop -->
          <div class="android-header-spacer mdl-layout-spacer"></div>
          <!-- low -->
          <div class="android-search-box mdl-textfield mdl-js-textfield mdl-textfield--expandable mdl-textfield--floating-label mdl-textfield--align-right mdl-textfield--full-width">
            <!-- <label class="mdl-button mdl-js-button mdl-button--icon" for="search-field">
              <i class="material-icons">search</i>
            </label> -->
            <div class="mdl-textfield__expandable-holder">
              <input class="mdl-textfield__input" type="text" id="search-field">
            </div>
            <!-- end low -->
          </div>
          <!-- Navigation -->
          <div class="android-navigation-container">
            <nav class="android-navigation mdl-navigation">
              <?php echo $this->Html->link(
                'Home',
                '/pages/home',
                ['class' => 'mdl-navigation__link mdl-typography--text-uppercase'])
                ?>
              <?php echo $notificationCell ?>
              <?php echo $this->Html->link(
                'Logout',
                '/users/logout',
                ['class' => 'mdl-navigation__link mdl-typography--text-uppercase']) ?>
            </nav>
          </div>
          <span class="android-mobile-title mdl-layout-title title-petani">
            <div id="logoPutih">m-Petani</div>
          </span>

          <button class="android-more-button mdl-button mdl-js-button mdl-button--icon mdl-js-ripple-effect" id="more-button">
            <i class="material-icons">more_vert</i>
          </button>
          <ul class="mdl-menu mdl-js-menu mdl-menu--bottom-right mdl-js-ripple-effect" for="more-button">
            <li class="mdl-menu__item"><?php echo $this->Html->link('Home','/pages/home',['class' => 'mdl-navigation__link mdl-typography--text-uppercase'])
                ?></li>
            <li class="mdl-menu__item"><?php echo $notificationCell ?></li>
            <li class="mdl-menu__item"><?php echo $this->Html->link('Logout','/users/logout',['class' => 'mdl-navigation__link mdl-typography--text-uppercase']) ?></li>
          </ul>
        </div>
      </div>

      <div class="android-drawer mdl-layout__drawer back-petani">
        <span class="mdl-layout-title text-petani">
          <!-- <img src="images/person.png" class="profile-petani"><br/> -->
            Selamat Datang
            <?php 
            $username = $this->request->session()->read('Auth.User.username');
            echo $username;
            ?>
            -
            Anda adalah
        <b class="admin-petani">
            <?php
                $jenis = $this->request->session()->read('Auth.User.jenis_user_id');
                if($jenis == 1)
                    $role = 'Admin';
                if($jenis == 2)
                    $role = 'BPS';
                if($jenis == 3)
                    $role = 'Pegawai KOPDes';
                echo $role;
            ?>
        </b>
        </span>
        <nav class="mdl-navigation">
          <ul class="menu_side">
            <?php
            //debug($this->request->session()->read()); exit(' bye');
            if ($this->request->session()->read('Auth.User.jenis_user_id')==1) :
            ?>
                <li><?= $this->Html->link(__('Poktan'), ['controller'=>'poktan','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'poktan']); ?></li>
                <li><?= $this->Html->link(__('Gapoktan'), ['controller'=>'gapoktan','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'gapoktan']); ?></li>
                <li><?= $this->Html->link(__('Penyuluh'), ['controller'=>'Penyuluh','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'penyuluh']); ?></li>
                <li><?= $this->Html->link(__('Petani'), ['controller'=>'Petani','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'petani']); ?></li>
                <li><?= $this->Html->link(__('BPS'), ['controller'=>'Bps','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'petani']); ?></li>
                <li><?= $this->Html->link(__('Ektp'), ['controller'=>'Ektp','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'ektp']); ?></li>
                <li><?= $this->Html->link(__('KOPDes'), ['controller'=>'Bumdes','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'bumdes']); ?></li>
                <li><?= $this->Html->link(__('Daerah'), ['controller'=>'Daerah','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'daerah']); ?></li>
                <li><?= $this->Html->link(__('RKP'), ['controller'=>'Rdkk','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'rkp']); ?></li>
                <li><?= $this->Html->link(__('Users'), ['controller'=>'users','action' => 'index'],['class'=>'mdl-navigation__link link-petani']); ?></li>
                <li><?= $this->Html->link(__('Items'), ['controller'=>'items','action' => 'index'],['class'=>'mdl-navigation__link link-petani']); ?></li>
                <li><?= $this->Html->link(__('Subsektor'), ['controller'=>'subsektor','action' => 'index'],['class'=>'mdl-navigation__link link-petani']); ?></li>
                <li><?= $this->Html->link(__('Komoditas'), ['controller'=>'komoditas','action' => 'index'],['class'=>'mdl-navigation__link link-petani']); ?></li>

            <?php elseif ($this->request->session()->read('Auth.User.jenis_user_id')==2) : ?>
                <li><?= $this->Html->link(__('Data Tunggal Petani'), ['controller'=>'petani','action' => 'index',$this->request->session()->read('Poktan.id')],['class'=>'mdl-navigation__link link-petani','id'=>'poktan']); ?></li>
                <li><?= $this->Html->link(__('Rencana Kebutuhan Petani'), ['controller'=>'Rdkk','action' => 'index',$this->request->session()->read('Rdkk.id')],['class'=>'mdl-navigation__link link-petani','id'=>'rkp']); ?></li>

            <?php elseif($this->request->session()->read('Auth.User.jenis_user_id')==3) : ?>
                <li><?= $this->Html->link(__('Data Tunggal Petani'), ['controller'=>'petani','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'gapoktan']); ?></li>
                <li><?= $this->Html->link(__('Rencana Kebutuhan Petani'), ['controller'=>'Rdkk','action' => 'index',$this->request->session()->read('Rdkk.id')],['class'=>'mdl-navigation__link link-petani','id'=>'rkp']); ?></li>
                <li><?= $this->Html->link(__('Transaksi Saprodi'), "#",['class'=>'mdl-navigation__link link-petani','id'=>'rkp']); ?></li>
                <li><?= $this->Html->link(__('Transaksi Hasil Panen'), "#",['class'=>'mdl-navigation__link link-petani','id'=>'rkp']); ?></li>
                <li><?= $this->Html->link(__('Pengajuan KUR'), "#",['class'=>'mdl-navigation__link link-petani','id'=>'rkp']); ?></li>
                <li><?= $this->Html->link(__('Pengajuan KKP'), "#",['class'=>'mdl-navigation__link link-petani','id'=>'rkp']); ?></li>

            <?php else : ?>
                <li><?= $this->Html->link(__('Poktan Saya'), ['controller'=>'poktan','action' => 'index'],['class'=>'mdl-navigation__link link-petani','id'=>'poktan']); ?></li>
                <li><?= $this->Html->link(__('RKP Saya'), ['controller'=>'Rdkk','action' => 'index',$this->request->session()->read('Rdkk.id')],['class'=>'mdl-navigation__link link-petani','id'=>'rkp']); ?></li>

            <?php endif; ?>
            <div class="android-drawer-separator"></div>
          </ul>
        </nav>
      </div>

      <div class="android-content mdl-layout__content">
          <?php  if ($this->request->session()->read('Auth.User.jenis_user_id')==1) { ?>

              <div class="row">
                  <div class="col-md-12 pull-right" id="secondNav">
                      <?php echo $this->fetch('tb_sidebar') ?>
                  </div>
              </div>
              <?php } ?>
        <?php
        /**
         * Default `flash` block.
         */
        if (!$this->fetch('tb_flash')) {
            $this->start('tb_flash');
            if (isset($this->Flash))
                echo $this->Flash->render();
            $this->end();
        }
        $this->end();
        $this->start('tb_body_end');
        ?>

        <!-- <footer class="android-footer mdl-mega-footer">
          <div class="mdl-mega-footer--bottom-section">
            <a class="android-link android-link-menu mdl-typography--font-light" id="version-dropdown">
              Versions
              <i class="material-icons">abc</i>
            </a>
            <ul class="mdl-menu mdl-js-menu mdl-menu--top-left mdl-js-ripple-effect" for="version-dropdown">
              <li class="mdl-menu__item">abc</li>
              <li class="mdl-menu__item">abc</li>
              <li class="mdl-menu__item">abc</li>
              <li class="mdl-menu__item">abc</li>
            </ul>
            <a class="android-link mdl-typography--font-light" href="">Blog</a>
            <a class="android-link mdl-typography--font-light" href="">Privacy Policy</a>
          </div>
        </footer> -->
        <?php
        echo '</body>';
        $this->end();
        $this->append('content', '</div></div></div>');
        echo $this->fetch('content');
        ?>
          <?= $this->fetch('modal'); ?>
<?php  if(!empty($poktan)){ ?>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <?= $this->Form->create($poktan); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Mohon Sebutkan Alasan Penolakan</h4>
                </div>
                <div class="modal-body">
                    <!--<p>&nbsp;</p>-->
                    <?= $this->Form->input('alasan', ['type' => "textarea"]);?>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <?= $this->Form->button(__("Submit"), ['class'=>'btn btn-info', 'name' => "Disapproved"]); ?>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <?= $this->Form->end(); ?>
            </div>

        </div>
    </div>
<?php } ?>
