<?php

		$this->prepend('tb_body_attrs', ' class="' . implode(' ', [$this->request->controller, $this->request->action]) . '" ');
		$this->start('tb_body_start');
?>
<!-- BEGIN CORE CSS FRAMEWORK -->
<link href="<?php echo $this->request->webroot ?>assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?php echo $this->request->webroot ?>assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $this->request->webroot ?>assets/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $this->request->webroot ?>assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $this->request->webroot ?>assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $this->request->webroot ?>assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen"/>
<!-- END CORE CSS FRAMEWORK -->
<!-- BEGIN CSS TEMPLATE -->
<link href="<?php echo $this->request->webroot ?>assets/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $this->request->webroot ?>assets/css/responsive.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $this->request->webroot ?>assets/css/magic_space.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $this->request->webroot ?>assets/css/custom-icon-set.css" rel="stylesheet" type="text/css"/>
<style>

@media screen and (max-width: 1000px) {
    body {
    	background: url(../img/Gambar-Pemandangan.jpg) no-repeat center center fixed !important;
        /*background: url(../../img/Gambar-Pemandangan-Sawah-yang-Indah-13s.jpg) no-repeat;*/
    }
}
body {
  background: url(../images/Gambar-Pemandangan-Sawah-yang-Indah-14.jpg) no-repeat center center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
</style>
<!-- END CSS TEMPLATE -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="error-body no-top lazy"  data-original="<?php echo $this->request->webroot ?>img/Gambar-Pemandangan-Sawah-yang-Indah-14.jpg" >
<div class="container">
  <div class="row login-container animated fadeInUp">
        <div class="col-md-7 col-md-offset-2 tiles white no-padding">
		 <div class="p-t-30 p-l-40 p-b-20 xs-p-t-10 xs-p-l-10 xs-p-b-10">
		 <?php $this->end(); ?>
		 <!-- content -->
          <?php echo $this->fetch("content"); ?>
		<!-- end of content -->

		<?php  $this->start('tb_body_end'); ?>
		</div>
      </div>
  </div>
</div>
<!-- END CONTAINER -->
<!-- BEGIN CORE JS FRAMEWORK-->
<script src="<?php echo $this->request->webroot ?>assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/jquery-lazyload/jquery.lazyload.min.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/js/login_v2.js" type="text/javascript"></script>
<script src="<?php echo $this->request->webroot ?>assets/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>
<!-- BEGIN CORE TEMPLATE JS -->
<!-- END CORE TEMPLATE JS -->
<script>
	$(document).ready(function(){
		$('.select2').select2();
	});
</script>
</body>



</html>
<?php
		 $this->end();

		if (!$this->fetch('tb_flash')) {
	    $this->start('tb_flash');
	    if (isset($this->Flash))
	        echo $this->Flash->render();
	    $this->end();
	    $this->start('tb_footer');
	    $this->end();
}
?>
