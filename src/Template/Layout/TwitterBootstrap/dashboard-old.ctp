<?php
/* @var $this \Cake\View\View */
use Cake\Core\Configure;
$this->Html->script('jquery/bootstrap-datepicker.js', ['block' => true]);
$this->Html->css('BootstrapUI.dashboard', ['block' => true]);
$this->Html->css('bootstrap/dashboard', ['block' => true]);
$this->prepend('tb_body_attrs', ' class="' . implode(' ', [$this->request->controller, $this->request->action]) . '" ');
$this->start('tb_body_start');

$notificationCell = $this->cell('Notifications');
?>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="<?php echo $this->request->webroot ?>css/material.min.css">
<link rel="stylesheet" href="<?php echo $this->request->webroot ?>css/styles.css">
<link rel="stylesheet" href="<?php echo $this->request->webroot ?>css/petani.css">
<script src="https://code.getmdl.io/1.1.3/material.min.js"></script>

<body <?= $this->fetch('tb_body_attrs') ?>>
<div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <div class="android-header mdl-layout__header mdl-layout__header--waterfall">
        <div class="mdl-layout__header-row">
          <span class="android-title mdl-layout-title title-petani">
            APLIKASI DATABASE PETANI
          </span>

            <!-- Add spacer, to align navigation to the right in desktop -->
            <div class="android-header-spacer mdl-layout-spacer"></div>
            <!-- low -->
            <div class="android-search-box mdl-textfield mdl-js-textfield mdl-textfield--expandable mdl-textfield--floating-label mdl-textfield--align-right mdl-textfield--full-width">
                <label class="mdl-button mdl-js-button mdl-button--icon" for="search-field">
                    <i class="material-icons">search</i>
                </label>
                <div class="mdl-textfield__expandable-holder">
                    <input class="mdl-textfield__input" type="text" id="search-field">
                </div>
                <!-- end low -->
            </div>
            <!-- Navigation -->
            <div class="android-navigation-container">
                <nav class="android-navigation mdl-navigation">
                    <?php echo $this->Html->link(
                        'Home',
                        '/pages/home',
                        ['class' => 'mdl-navigation__link mdl-typography--text-uppercase'])
                    ?>
                    <?php echo $notificationCell ?>
                    <?php echo $this->Html->link(
                        'Logout',
                        '/users/logout',
                        ['class' => 'mdl-navigation__link mdl-typography--text-uppercase']) ?>
                </nav>
            </div>
          <span class="android-mobile-title mdl-layout-title title-petani">
            PETANI
          </span>

            <button class="android-more-button mdl-button mdl-js-button mdl-button--icon mdl-js-ripple-effect" id="more-button">
                <i class="material-icons">more_vert</i>
            </button>
            <ul class="mdl-menu mdl-js-menu mdl-menu--bottom-right mdl-js-ripple-effect" for="more-button">
                <li class="mdl-menu__item"><?php echo $this->Html->link('Home','/pages/home',['class' => 'mdl-navigation__link mdl-typography--text-uppercase', 'target' => '_blank'])
                    ?></li>
                <li class="mdl-menu__item"><?php echo $notificationCell ?></li>
                <li class="mdl-menu__item"><?php echo $this->Html->link('Logout','/users/logout',['class' => 'mdl-navigation__link mdl-typography--text-uppercase']) ?></li>
            </ul>
        </div>
    </div>

    <div class="android-drawer mdl-layout__drawer back-petani">
        <span class="mdl-layout-title text-petani">
          <!-- <img src="images/person.png" class="profile-petani"><br/> -->
          Selamat Datang
            <?php $username = $this->request->session()->read('Auth.User.username');
            echo $username;?>
            -
		  Anda adalah
        <b class="admin-petani">
            <?php
            //debug( $this->request->session()->read('Auth.User.jenis_user_id') ); exit;
            $jenis = $this->request->session()->read('Auth.User.jenis_user_id');
            if($jenis == 1)
                $role = 'Admin';
            if($jenis == 3)
                $role = 'Pegawai Bumdes';
            if($jenis == 4)
                $role = 'Pegawai BPS';
            if($jenis == 5)
                $role = 'Camat';
            echo $role;
            ?>
        </b>
        </span>
        <nav class="mdl-navigation">
            <?php if($this->request->session()->read('Auth.User.jenis_user_id')==3){ ?>
                <?= $this->Html->link(__('Poktan Saya'), ['controller'=>'poktan','action' => 'index'],['class'=>'mdl-navigation__link link-petani']); ?>
                <?= $this->Html->link(__('RKP Saya'), ['controller'=>'Rdkk','action' => 'index',$this->request->session()->read('Rdkk.id')],['class'=>'mdl-navigation__link link-petani']); ?>
            <?php }elseif($this->request->session()->read('Auth.User.jenis_user_id')==1) { ?>
                <?= $this->Html->link(__('Poktan'), ['controller'=>'poktan','action' => 'index'],['class'=>'mdl-navigation__link link-petani']); ?>
                <?= $this->Html->link(__('Penyuluh'), ['controller'=>'Penyuluh','action' => 'index'],['class'=>'mdl-navigation__link link-petani']); ?>
                <?= $this->Html->link(__('Petani'), ['controller'=>'Petani','action' => 'index'],['class'=>'mdl-navigation__link link-petani']); ?>
                <?= $this->Html->link(__('Ektp'), ['controller'=>'Ektp','action' => 'index'],['class'=>'mdl-navigation__link link-petani']); ?>
                <?= $this->Html->link(__('Bumdes'), ['controller'=>'Bumdes','action' => 'index'],['class'=>'mdl-navigation__link link-petani']); ?>
                <?= $this->Html->link(__('Daerah'), ['controller'=>'Daerah','action' => 'index'],['class'=>'mdl-navigation__link link-petani']); ?>
                <?= $this->Html->link(__('RKP'), ['controller'=>'Rdkk','action' => 'index'],['class'=>'mdl-navigation__link link-petani']); ?>
            <?php }else { ?>
                <?= $this->Html->link(__('Poktan Saya'), ['controller'=>'poktan','action' => 'index'],['class'=>'mdl-navigation__link link-petani']); ?>
                <?= $this->Html->link(__('RKP Saya'), ['controller'=>'Rdkk','action' => 'index',$this->request->session()->read('Rdkk.id')],['class'=>'mdl-navigation__link link-petani']); ?>
            <?php } ?>
            <div class="android-drawer-separator"></div>
        </nav>
    </div>

    <div class="android-content mdl-layout__content">
        <div class="row">
            <div class="col-md-12 pull-right" id="secondNav">
                <ul class="nav nav-pills">
                    <?php ?>
                </ul>
            </div>
        </div>
        <?php
        /**
         * Default `flash` block.
         */

        if (!$this->fetch('tb_flash')) {
            $this->start('tb_flash');
            if (isset($this->Flash))
                echo $this->Flash->render();
            $this->end();
        }
        $this->end();
        $this->start('tb_body_end');
        ?>
        <!-- <footer class="android-footer mdl-mega-footer">
          <div class="mdl-mega-footer--bottom-section">
            <a class="android-link android-link-menu mdl-typography--font-light" id="version-dropdown">
              Versions
              <i class="material-icons">abc</i>
            </a>
            <ul class="mdl-menu mdl-js-menu mdl-menu--top-left mdl-js-ripple-effect" for="version-dropdown">
              <li class="mdl-menu__item">abc</li>
              <li class="mdl-menu__item">abc</li>
              <li class="mdl-menu__item">abc</li>
              <li class="mdl-menu__item">abc</li>
            </ul>
            <a class="android-link mdl-typography--font-light" href="">Blog</a>
            <a class="android-link mdl-typography--font-light" href="">Privacy Policy</a>
          </div>
        </footer> -->
        <?php

        echo '</body>';
        $this->end();

        $this->append('content', '</div></div></div>');
        echo $this->fetch('content');

        ?>

