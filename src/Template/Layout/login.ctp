<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>.:: Portal - Bulir ::.</title>

    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo $this->request->webroot ?>bootstrap/css/bootstrap.min.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $this->request->webroot ?>dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo $this->request->webroot ?>css/custom.css">
    <link rel="stylesheet" href="<?php echo $this->request->webroot ?>css/skins/_all-skins.min.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>
<body class="hold-transition bodylogin">

    <div class="login-box">

        <div class="login-logo">
            <a href="#"><img src="<?php echo $this->request->webroot ?>image/logo.png"><br/>
                <b>Portal</b>BULIR</a>
        </div>
        <!-- /.login-logo -->

        <?= $this->Flash->render() ?>
    
        <?= $this->fetch('content') ?>

    </div>
    <!-- jQuery 2.1.4 -->
    <script src="<?php echo $this->request->webroot ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo $this->request->webroot ?>js/jquery-ui.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo $this->request->webroot ?>bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
