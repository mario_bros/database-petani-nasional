<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

//$cakeDescription = __d('cake_dev', 'ILCS Domain Manager');
?>
<!DOCTYPE html>
<html>
<head>
	<!-- BEGIN PLUGIN CSS -->
	<link href="<?php echo $this->webroot; ?>plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="<?php echo $this->webroot; ?>plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="<?php echo $this->webroot; ?>plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $this->webroot; ?>plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->webroot; ?>plugins/jquery-ricksaw-chart/css/rickshaw.css" type="text/css" media="screen" >
	<link rel="stylesheet" href="<?php echo $this->webroot; ?>plugins/jquery-morris-chart/css/morris.css" type="text/css" media="screen">
	<link href="<?php echo $this->webroot; ?>plugins/jquery-slider/css/jquery.sidr.light.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="<?php echo $this->webroot; ?>plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="<?php echo $this->webroot; ?>plugins/jquery-jvectormap/css/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="<?php echo $this->webroot; ?>plugins/boostrap-checkbox/css/bootstrap-checkbox.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PLUGIN CSS -->
	<!-- BEGIN CORE CSS FRAMEWsORK -->
	<link href="<?php echo $this->webroot; ?>plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $this->webroot; ?>plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $this->webroot; ?>plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $this->webroot; ?>css/animate.min.css" rel="stylesheet" type="text/css"/>
	<!-- END CORE CSS FRAMEWORK -->
	<!-- BEGIN CSS TEMPLATE -->
	<link href="<?php echo $this->webroot; ?>css/style.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $this->webroot; ?>css/responsive.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $this->webroot; ?>css/custom-icon-set.css" rel="stylesheet" type="text/css"/>
	<!-- END CSS TEMPLATE -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="<?php echo $this->webroot; ?>js/bootstrap.min.js"></script>
    <script src="<?php echo $this->webroot; ?>js/docs.min.js"></script>
    <!-- BEGIN CORE JS FRAMEWORK--> 
	<script src="<?php echo $this->webroot; ?>plugins/jquery-1.8.3.min.js" type="text/javascript"></script> 
	<script src="<?php echo $this->webroot; ?>plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script> 
	<script src="<?php echo $this->webroot; ?>plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script> 
	<script src="<?php echo $this->webroot; ?>plugins/breakpoints.js" type="text/javascript"></script> 
	<script src="<?php echo $this->webroot; ?>plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script> 
	<!-- END CORE JS FRAMEWORK --> 
	<!-- BEGIN PAGE LEVEL JS --> 
	<script src="<?php echo $this->webroot; ?>plugins/pace/pace.min.js" type="text/javascript"></script>  
	<script src="<?php echo $this->webroot; ?>plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
	<script src="<?php echo $this->webroot; ?>plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script> 
	<script src="<?php echo $this->webroot; ?>plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>  
	<script src="<?php echo $this->webroot; ?>plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
	<script src="<?php echo $this->webroot; ?>plugins/jquery-block-ui/jqueryblockui.js" type="text/javascript"></script> 
	<script src="<?php echo $this->webroot; ?>plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->webroot; ?>plugins/jquery-ricksaw-chart/js/raphael-min.js"></script> 
	<script src="<?php echo $this->webroot; ?>plugins/jquery-ricksaw-chart/js/d3.v2.js"></script>
	<script src="<?php echo $this->webroot; ?>plugins/jquery-ricksaw-chart/js/rickshaw.min.js"></script>
	<script src="<?php echo $this->webroot; ?>plugins/jquery-morris-chart/js/morris.min.js"></script>
	<script src="<?php echo $this->webroot; ?>plugins/jquery-easy-pie-chart/js/jquery.easypiechart.min.js"></script>
	<script src="<?php echo $this->webroot; ?>plugins/jquery-slider/jquery.sidr.min.js" type="text/javascript"></script> 	
	<script src="<?php echo $this->webroot; ?>plugins/jquery-jvectormap/js/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script> 	
	<script src="<?php echo $this->webroot; ?>plugins/jquery-jvectormap/js/jquery-jvectormap-us-lcc-en.js" type="text/javascript"></script> 	
	<script src="<?php echo $this->webroot; ?>plugins/jquery-sparkline/jquery-sparkline.js"></script>
	<script src="<?php echo $this->webroot; ?>plugins/jquery-flot/jquery.flot.min.js"></script>
	<script src="<?php echo $this->webroot; ?>plugins/jquery-flot/jquery.flot.animator.min.js"></script>
	<script src="<?php echo $this->webroot; ?>plugins/skycons/skycons.js"></script>
	<!-- END PAGE LEVEL PLUGINS   --> 	
	<!-- PAGE JS --> 	
	<script src="<?php echo $this->webroot; ?>js/dashboard.js" type="text/javascript"></script>
	<!-- BEGIN CORE TEMPLATE JS --> 
	<script src="<?php echo $this->webroot; ?>js/core.js" type="text/javascript"></script> 
	<script src="<?php echo $this->webroot; ?>js/chat.js" type="text/javascript"></script> 
	<script src="<?php echo $this->webroot; ?>js/demo.js" type="text/javascript"></script> 
	<!-- END CORE TEMPLATE JS -->
    <!--<link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/cover.css" rel="stylesheet">-->
    
    <title>IPC Signage</title>


	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		//echo $this->Html->meta('icon');

		//echo $this->Html->css('bootstrap');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<!-- BEGIN BODY -->
<body class="">
<?php //if ($this->Session->read('Auth.User')): ?>
<div class="container">
  <div class="row login-container column-seperation">  
        <div class="col-md-5 col-md-offset-1" style="text-align: center">
          <img src="/ipc-signage/img/logo_ipc_white.png" class="logo" alt="" data-src="/ipc-signage/img/logo_ipc_white.png" data-src-retina="/ipc-signage/img/logo_ipc_white.png" width="auto" height="auto">
        </div>
        <div class="col-md-5 "> <br>
		<?php echo $this->Session->flash('auth'); ?>
		<?php echo $this->Form->create('User'); ?>
		 <div class="row">
		 <div class="form-group col-md-10">
            <label class="form-label">Username</label>
            <div class="controls">
				<div class="input-with-icon  right">                                       
					<i class=""></i>
					<input name="data[User][username]" type="text" id="UserUsername" required="required" class="form-control">                          
				</div>
            </div>
          </div>
          </div>
		  <div class="row">
          <div class="form-group col-md-10">
            <label class="form-label">Password</label>
            <span class="help"></span>
            <div class="controls">
				<div class="input-with-icon  right">                                       
					<i class=""></i>
					<input name="data[User][password]" type="password" id="UserPassword" required="required" class="form-control">                          
				</div>
            </div>
          </div>
          </div>
		  <div class="row">
          </div>
          <div class="row">
            <div class="col-md-10">
              <input type="submit" value="Login" class="btn btn-primary btn-cons pull-right">
            </div>
          </div>
		  </form>
        </div>
     
    
  </div>
</div>
        
	<?php //echo $this->element('sql_dump'); ?>
</body>
</html>
