<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Item'), ['action' => 'edit', $item->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Item'), ['action' => 'delete', $item->id], ['confirm' => __('Are you sure you want to delete # {0}?', $item->id)]) ?> </li>
<li><?= $this->Html->link(__('List Items'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Item'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Category'), ['controller' => 'Category', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Category'), ['controller' => 'Category', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Prices'), ['controller' => 'Prices', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Price'), ['controller' => 'Prices', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Tr Rdkk'), ['controller' => 'TrRdkk', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Tr Rdkk'), ['controller' => 'TrRdkk', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
<li><?= $this->Html->link(__('Edit Item'), ['action' => 'edit', $item->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Item'), ['action' => 'delete', $item->id], ['confirm' => __('Are you sure you want to delete # {0}?', $item->id)]) ?> </li>
<li><?= $this->Html->link(__('List Items'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Item'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Category'), ['controller' => 'Category', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Category'), ['controller' => 'Category', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Prices'), ['controller' => 'Prices', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Price'), ['controller' => 'Prices', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Tr Rdkk'), ['controller' => 'TrRdkk', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Tr Rdkk'), ['controller' => 'TrRdkk', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($item->name) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Name') ?></td>
            <td><?= h($item->name) ?></td>
        </tr>
        <tr>
            <td><?= __('Category') ?></td>
            <td><?= $item->has('category') ? $this->Html->link($item->category->name, ['controller' => 'Category', 'action' => 'view', $item->category->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Desc') ?></td>
            <td><?= h($item->desc) ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($item->id) ?></td>
        </tr>
        <tr>
            <td><?= __('Active') ?></td>
            <td><?= $item->active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related Prices') ?></h3>
    </div>
    <?php if (!empty($item->prices)): ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Item Id') ?></th>
                <th><?= __('Harga') ?></th>
                <th><?= __('Tanggal') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($item->prices as $prices): ?>
                <tr>
                    <td><?= h($prices->id) ?></td>
                    <td><?= h($prices->item_id) ?></td>
                    <td><?= h($prices->harga) ?></td>
                    <td><?= h($prices->tanggal) ?></td>
                    <td class="actions">
                        <?= $this->Html->link('', ['controller' => 'Prices', 'action' => 'view', $prices->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                        <?= $this->Html->link('', ['controller' => 'Prices', 'action' => 'edit', $prices->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                        <?= $this->Form->postLink('', ['controller' => 'Prices', 'action' => 'delete', $prices->id], ['confirm' => __('Are you sure you want to delete # {0}?', $prices->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p class="panel-body">no related Prices</p>
    <?php endif; ?>
</div>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related TrRdkk') ?></h3>
    </div>
    <?php if (!empty($item->tr_rdkk)): ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Rdkk Id') ?></th>
                <th><?= __('Lahan Id') ?></th>
                <th><?= __('Item Id') ?></th>
                <th><?= __('Jumlah') ?></th>
                <th><?= __('Masa Tanam') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($item->tr_rdkk as $trRdkk): ?>
                <tr>
                    <td><?= h($trRdkk->id) ?></td>
                    <td><?= h($trRdkk->rdkk_id) ?></td>
                    <td><?= h($trRdkk->lahan_id) ?></td>
                    <td><?= h($trRdkk->item_id) ?></td>
                    <td><?= h($trRdkk->jumlah) ?></td>
                    <td><?= h($trRdkk->masa_tanam) ?></td>
                    <td class="actions">
                        <?= $this->Html->link('', ['controller' => 'TrRdkk', 'action' => 'view', $trRdkk->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                        <?= $this->Html->link('', ['controller' => 'TrRdkk', 'action' => 'edit', $trRdkk->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                        <?= $this->Form->postLink('', ['controller' => 'TrRdkk', 'action' => 'delete', $trRdkk->id], ['confirm' => __('Are you sure you want to delete # {0}?', $trRdkk->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p class="panel-body">no related TrRdkk</p>
    <?php endif; ?>
</div>
