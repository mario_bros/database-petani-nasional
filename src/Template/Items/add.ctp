<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('List Items'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Category'), ['controller' => 'Category', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Category'), ['controller' => 'Category', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Prices'), ['controller' => 'Prices', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Price'), ['controller' => 'Prices', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Tr Rdkk'), ['controller' => 'TrRdkk', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Tr Rdkk'), ['controller' => 'TrRdkk', 'action' => 'add']) ?> </li>

<?php
$this->end();
$this->assign('tb_sidebar', '<ul class="nav nav-pills">' . $this->fetch('tb_actions') . '</ul>'); ?>

<?= $this->Form->create($item); ?>
<fieldset>
    <legend><?= __('Add {0}', ['Item']) ?></legend>
    <?php
    echo $this->Form->input('name');
    echo $this->Form->input('category_id', ['options' => $category]);
    echo $this->Form->input('desc');
    echo $this->Form->input('active');
    echo $this->Form->input('harga');
    echo $this->Form->input('Komoditas._ids', ['options' => $optsKomoditas]);
    ?>
</fieldset>
<?= $this->Form->button(__("Add")); ?>
<?= $this->Form->end() ?>
