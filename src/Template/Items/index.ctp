<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('New Item'), ['action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List Category'), ['controller' => 'Category', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Category'), ['controller' => ' Category', 'action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List Prices'), ['controller' => 'Prices', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Price'), ['controller' => ' Prices', 'action' => 'add']); ?></li>
    
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-pills">' . $this->fetch('tb_actions') . '</ul>'); ?>
<div class="row">
  <div class="col-md-12">
    <a href="<?php echo $this->request->webroot ?>items/add" class="btn btn-success">Add Item</a>  </div>
</div>
<div class="table-responsive">
<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id'); ?></th>
            <th><?= $this->Paginator->sort('name'); ?></th>
            <th><?= $this->Paginator->sort('category_id'); ?></th>
            <th><?= $this->Paginator->sort('desc'); ?></th>
            <th><?= $this->Paginator->sort('harga'); ?></th>
            <th class="actions"><?= __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item): ?>
        <tr>
            <td><?= $this->Number->format($item->id) ?></td>
            <td><?= h($item->name) ?></td>
            <td>
                <?= $item->has('category') ? $this->Html->link($item->category->name, ['controller' => 'Category', 'action' => 'view', $item->category->id]) : '' ?>
            </td>
            <td><?= h($item->desc) ?></td>
            <td><?= h($item->harga) ?></td>
            <td class="actions">
                <?= $this->Html->link('', ['action' => 'view', $item->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                <?= $this->Html->link('', ['action' => 'edit', $item->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $item->id], ['confirm' => __('Are you sure you want to delete # {0}?', $item->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
