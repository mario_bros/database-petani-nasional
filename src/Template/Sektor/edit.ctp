<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $sektor->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $sektor->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Sektor'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Subsektor'), ['controller' => 'Subsektor', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Subsektor'), ['controller' => 'Subsektor', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $sektor->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $sektor->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Sektor'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Subsektor'), ['controller' => 'Subsektor', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Subsektor'), ['controller' => 'Subsektor', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($sektor); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Sektor']) ?></legend>
    <?php
    echo $this->Form->input('nama');
    echo $this->Form->input('description');
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
