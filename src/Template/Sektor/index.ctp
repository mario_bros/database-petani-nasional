<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('New Sektor'), ['action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List Subsektor'), ['controller' => 'Subsektor', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Subsektor'), ['controller' => ' Subsektor', 'action' => 'add']); ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>

<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id'); ?></th>
            <th><?= $this->Paginator->sort('nama'); ?></th>
            <th><?= $this->Paginator->sort('description'); ?></th>
            <th class="actions"><?= __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($sektor as $sektor): ?>
        <tr>
            <td><?= $this->Number->format($sektor->id) ?></td>
            <td><?= h($sektor->nama) ?></td>
            <td><?= h($sektor->description) ?></td>
            <td class="actions">
                <?= $this->Html->link('', ['action' => 'view', $sektor->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                <?= $this->Html->link('', ['action' => 'edit', $sektor->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $sektor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sektor->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>