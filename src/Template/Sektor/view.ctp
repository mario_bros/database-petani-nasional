<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Sektor'), ['action' => 'edit', $sektor->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Sektor'), ['action' => 'delete', $sektor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sektor->id)]) ?> </li>
<li><?= $this->Html->link(__('List Sektor'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Sektor'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Subsektor'), ['controller' => 'Subsektor', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Subsektor'), ['controller' => 'Subsektor', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
<li><?= $this->Html->link(__('Edit Sektor'), ['action' => 'edit', $sektor->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Sektor'), ['action' => 'delete', $sektor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sektor->id)]) ?> </li>
<li><?= $this->Html->link(__('List Sektor'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Sektor'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Subsektor'), ['controller' => 'Subsektor', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Subsektor'), ['controller' => 'Subsektor', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($sektor->id) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Nama') ?></td>
            <td><?= h($sektor->nama) ?></td>
        </tr>
        <tr>
            <td><?= __('Description') ?></td>
            <td><?= h($sektor->description) ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($sektor->id) ?></td>
        </tr>
    </table>
</div>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related Subsektor') ?></h3>
    </div>
    <?php if (!empty($sektor->subsektor)): ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nama') ?></th>
                <th><?= __('Sektor Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($sektor->subsektor as $subsektor): ?>
                <tr>
                    <td><?= h($subsektor->id) ?></td>
                    <td><?= h($subsektor->nama) ?></td>
                    <td><?= h($subsektor->sektor_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link('', ['controller' => 'Subsektor', 'action' => 'view', $subsektor->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                        <?= $this->Html->link('', ['controller' => 'Subsektor', 'action' => 'edit', $subsektor->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                        <?= $this->Form->postLink('', ['controller' => 'Subsektor', 'action' => 'delete', $subsektor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $subsektor->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p class="panel-body">no related Subsektor</p>
    <?php endif; ?>
</div>
