<link href="<?php echo $this->request->webroot ?>css/home.css" rel="stylesheet" type="text/css" media="screen"/>
<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>

<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-pills">' . $this->fetch('tb_actions') . '</ul>'); ?>

<!-- content dashboard -->
<div class="visible-lg-block">
        
        <div class="row">

        <div class="col-md-12" >
            <h2>Peta Sleman</h2>
            <div style="height:500px;width:100%;max-width:100%;list-style:none; transition: none;overflow:hidden;"><div id="gmap_display" style="height:100%; width:100%;max-width:100%;">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15814.768586110511!2d110.32664821463703!3d-7.716143497491987!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a58ab4742e6c5%3A0xaec17281f611d2a9!2sSleman%2C+Sumberadi%2C+Mlati%2C+Sleman+Regency%2C+Special+Region+of+Yogyakarta!5e0!3m2!1sen!2sid!4v1470032326899" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div><a class="embedded-map-code" href="http://www.dog-checks.com" id="grab-map-authorization">www.dog-checks.com</a><style>#gmap_display .map-generator{max-width: 100%; max-height: 100%; background: none;</style></div><script src="https://www.dog-checks.com/google-maps-authorization.js?id=65d92a68-26c5-8c72-9612-75353cab5569&c=embedded-map-code&u=1462877183" defer="defer" async="async"></script>
        </div>
        </div>

</div>

<!-- layout for mobile phone -->
<div class="hidden-lg" id="phone">
    <?php
        $userRole = $this->request->session()->read('Auth.User.jenis_user_id');
        //jika user role admin
    if($userRole == 1){
        $url = $this->Url->build([
            'controller'=>'petani',
            'action'=>'index'
        ]);
    }elseif($userRole==2){
        //jika user role adalah BPS
        $url = $this->Url->build([
            'controller'=>'petani',
            'action'=>'index'
        ]);
    }else {
        //jka user role adalah KOPdes
        $url = $this->Url->build([
            'controller'=>'petani',
            'action'=>'index'
        ]);
    }
    ?>
    <div class="row">

        <div class="col-xs-12">
            <h4 id="logoPutih"></h4>
        </div>

        <div class="col-xs-6">
            <a href="<?php echo $url; ?>" class="mobileBtn dbPetani">
                <h4>Data Tunggal Petani</h4>

            </a>
        </div>

        <div class="col-xs-6">
            <a href="#" class="mobileBtn rkpOnline">
                <h4>Tani Produsen</h4>

            </a>
        </div>
    </div>
    <div class="row">

        <div class="col-xs-6">
            <a href="<?php echo $this->Url->build([
                'controller'=>'rdkk',
                'action'=>'listrkppoktan',6
            ]); ?>" class="mobileBtn saprodi">
                <h4>Tani Konsumen</h4>



            </a>
        </div>

        <div class="col-xs-6">
            <a href="#" class="mobileBtn kur">
                <h4>Transaksi Kopdes</h4>

            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <a href="#" class="mobileBtn kkp">
                <h4>Telusur</h4>

            </a>
        </div>

        <div class="col-xs-6">
            <a href="#" class="mobileBtn hasilTani">
                <h4>Laporan</h4>

            </a>
        </div>
    </div>
    </div>
</div>
<!-- end of mobile phone -->
