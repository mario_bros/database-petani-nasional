<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('New Gapoktan'), ['action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => ' Daerah', 'action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Poktan'), ['controller' => ' Poktan', 'action' => 'add']); ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-pills">' . $this->fetch('tb_actions') . '</ul>'); ?>

<h5>LIST GAPOKTAN</h5>
<div class="table-responsive">
<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id'); ?></th>
            <th><?= $this->Paginator->sort('nama'); ?></th>
            <th><?= $this->Paginator->sort('alamat'); ?></th>
            <th><?= $this->Paginator->sort('daerah_id',['label'=>'Desa']); ?></th>
            <th class="actions"><?= __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($gapoktan as $gapoktan): ?>
        <tr>
            <td><?= $this->Number->format($gapoktan->id) ?></td>
            <td><?= h($gapoktan->nama) ?></td>
            <td><?= h($gapoktan->alamat) ?></td>
            <td>
                <?= $gapoktan->has('daerah') ? $this->Html->link($gapoktan->daerah->nama, ['controller' => 'Daerah', 'action' => 'view', $gapoktan->daerah->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link('', ['action' => 'view', $gapoktan->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                <?= $this->Html->link('', ['action' => 'edit', $gapoktan->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $gapoktan->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gapoktan->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>