<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Gapoktan'), ['action' => 'edit', $gapoktan->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Gapoktan'), ['action' => 'delete', $gapoktan->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gapoktan->id)]) ?> </li>
<li><?= $this->Html->link(__('List Gapoktan'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Gapoktan'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
<li><?= $this->Html->link(__('Edit Gapoktan'), ['action' => 'edit', $gapoktan->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Gapoktan'), ['action' => 'delete', $gapoktan->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gapoktan->id)]) ?> </li>
<li><?= $this->Html->link(__('List Gapoktan'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Gapoktan'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= "GAPOKTAN " . h($gapoktan->nama) ?></h3>
    </div>
    <div class="table-responsive">
        <table class="table table-striped" cellpadding="0" cellspacing="0">
            <tr>
                <td><?= __('Nama Ketua') ?></td>
                <td><?= ( $gapoktan->has('petani') ) ? h(strtoupper($gapoktan->petani->ektp->nama)) : ""?></td>
            </tr>
            <tr>
                <td><?= __('Nama Penyuluh') ?></td>
                <td><?= ( $gapoktan->has('poktan') ) ? h(strtoupper($gapoktan->poktan[0]->penyuluh->ektp->nama)) : ""?></td>
            </tr>
            <tr>
                <td><?= __('Alamat') ?></td>
                <td><?= h(strtoupper($gapoktan->alamat)) ?></td>
            </tr>
            <tr>
                <td><?= __('Desa') ?></td>
                <td><?= h(strtoupper($gapoktan->daerah->nama)) ?></td>
            </tr>
            <tr>
                <td><?= __('Kecamatan') ?></td>
                <td><?= h(strtoupper($gapoktan->daerah->parent_daerah->nama)) ?></td>
            </tr>
            <tr>
                <td><?= __('Kabupaten') ?></td>
                <td><?= h(strtoupper($gapoktan->daerah->parent_daerah->grand_parent_daerah->nama)) ?></td>
                
            </tr>
            <tr>
                <td><?= __('Provinsi') ?></td>
                <td><?= h(strtoupper($provinsi->nama)) ?></td>
            </tr>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related Poktan') ?></h3>
    </div>
    <?php if (!empty($gapoktan->poktan)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th><?= __('No') ?></th>
                        <th><?= __('Nama') ?></th>
                        <th><?= __('Ketua Poktan') ?></th>
                        <th><?= __('Alamat') ?></th>
                        <th><?= __('Desa') ?></th>
                        <th><?= __('Kecamatan') ?></th>
                        <th><?= __('Kabupaten') ?></th>
                        <th><?= __('Provinsi') ?></th>
<!--                        <th><?php // __('Lat') ?></th>
                        <th><?php // __('Lon') ?></th>-->
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $no=0; 
                    foreach ($gapoktan->poktan as $poktan): 
                        $no++;
                        //debug($poktan->daerah->parent_daerah->grand_parent_daerah); exit;
                    ?>
                        <tr>
                            <td><?= $no ?></td>
                            <td><?= h(strtoupper($poktan->nama)) ?></td>
                            <td><?= h(strtoupper($poktan->ketuaPoktan->ektp->nama)) ?></td>
                            <td><?= h(strtoupper($poktan->alamat)) ?></td>
                            <td><?= h(strtoupper($poktan->daerah->nama)) ?></td>
                            <td><?= h(strtoupper($poktan->daerah->parent_daerah->nama)) ?></td>
                            <td><?= h(strtoupper($poktan->daerah->parent_daerah->grand_parent_daerah->parent->nama)) ?></td>
                            <td><?= h(strtoupper($poktan->daerah->parent_daerah->grand_parent_daerah->third_level_parent_daerah->nama)) ?></td>
                            
                            <td class="actions">
                                <?= $this->Html->link('', ['controller' => 'Poktan', 'action' => 'view', $poktan->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                                <?= $this->Html->link('', ['controller' => 'Poktan', 'action' => 'edit', $poktan->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                                <?= $this->Form->postLink('', ['controller' => 'Poktan', 'action' => 'delete', $poktan->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poktan->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php else: ?>
        <p class="panel-body">no related Poktan</p>
    <?php endif; ?>
</div>
