<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $gapoktan->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $gapoktan->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Gapoktan'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $gapoktan->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $gapoktan->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Gapoktan'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($gapoktan); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Gapoktan']) ?></legend>
    <?php
    echo $this->Form->input('nama');
    echo $this->Form->input('alamat');
    echo $this->Form->input('daerah_id', ['options' => $daerah,'empty'=>'select one']);
    echo $this->Form->input('ketua_gapoktan', ['options' => $petani,'empty'=>'select one']);
    echo $this->Form->input('bps_id', ['label'=>'pegawai BPS','options' => $bps,'empty'=>'select one']);
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
