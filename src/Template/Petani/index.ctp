<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>

<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-pills">' . $this->fetch('tb_actions') . '</ul>'); ?>

<?php if(!empty($this->request->session()->read('Bumdes.nama'))){ ?>
  <h2>Bumdes <strong><br><?php echo $this->request->session()->read('Bumdes.nama'); ?></strong></h2>
<?php } ?>
<div class="row">
    <div class="searchForm">
        <?php echo $this->Form->create($petani); ?>
        <?php
            echo "<div class='col-md-4'>";
            echo $this->Form->input('NIK',['template'=>'horizontal']);
            echo "</div>";
            echo "<div class='col-md-4'>";
            echo $this->Form->input('nama',['template'=>'horizontal']);
            echo "</div>";
          //  echo $this->Form->input('daerah',['template'=>'horizontal']);
        ?>
        <div class='col-md-4'>
        <input type="Submit" value="Cari" class="btn btn-primary col-md-12" />
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div><hr>
<div class="row">
  <div class="col-md-12">
    <?= $this->Html->link(__('Tambah Petani'), ['action' => 'add'],['class'=>'btn btn-success tambahPetani']); ?>
  </div>
</div><br>
  <?php if(empty($error)) { ?>
<div class="table-responsive">

    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th class="hideme"><?= $this->Paginator->sort('no'); ?></th>
				<th><?= $this->Paginator->sort('nama'); ?></th>
                <th class="hideme"><?= $this->Paginator->sort('no_tlp'); ?></th>
                  <th class="hideme"><?= $this->Paginator->sort('no_ektp',['label'=>'NIK']); ?></th>

                <th class="actions"><?= __('Actions'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 0; foreach ($petani as $petani): $no++; ?>
              <?php
                switch($petani->status){
                  case 0 : $status = "diajukan";
                           $class ="label-info";
                           break;
                  case 1 : $status = "disetujui";
                           $class ="label-success";
                           break;
                  case 2 : $status = "ditolak";
                           $class ="label-danger";
                           break;
                }
              ?>
            <tr>
                <td class="hideme"><?= $no ?></td>
				<td><?= $petani->has('ektp') ? $this->Html->link($petani->ektp->nama, ['controller' => 'Petani', 'action' => 'view', $petani->id]) : '' ?></td>
                <td class="hideme"><?= h($petani->no_tlp) ?></td>
                <td class="hideme"><?= h($petani->no_ektp) ?></td>
               <!-- <td> <label class="label <?= $class ?>"><?= $status ?></label> </td> -->
                <td class="actions">
                    <?= $this->Html->link('', ['action' => 'view', $petani->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                    <?= $this->Html->link('', ['action' => 'edit', $petani->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                    <?php //$this->Form->postLink('', ['action' => 'delete', $petani->id], ['confirm' => __('Are you sure you want to delete # {0}?', $petani->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
<?php }else { ?>
  <h3>Data Tidak Ditemukan</h3>
<?php } ?>
