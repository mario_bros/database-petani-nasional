<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$roleStr = strtolower(trim($role->jenis));

$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Petani'), ['action' => 'edit', $petani->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Petani'), ['action' => 'delete', $petani->id], ['confirm' => __('Are you sure you want to delete # {0}?', $petani->id)]) ?> </li>
<li><?= $this->Html->link(__('List Petani'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Petani'), ['action' => 'add'],['class'=>'']) ?> </li>

<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?= $this->Html->link(__('Edit Petani'), ['action' => 'edit', $petani->id]) ?> </li>
    <li><?= $this->Form->postLink(__('Delete Petani'), ['action' => 'delete', $petani->id], ['confirm' => __('Are you sure you want to delete # {0}?', $petani->id)]) ?> </li>
    <li><?= $this->Html->link(__('List Petani'), ['action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Petani'), ['action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Lahan'), ['controller' => 'Lahan', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>

<?php
  switch($petani->status){
    case 0 : $status = "diajukan";
             $class ="label-info";
             break;
    case 1 : $status = "disetujui";
             $class ="label-success";
             break;
    case 2 : $status = "ditolak";
             $class ="label-danger";
             break;
  }
?>
<div class="col-md-12">
  <?= $this->Html->link(__('Edit'),['action'=>'edit',$petani['id']],['class'=>'btn btn-warning pull-right']) ?>
</div>
<h2>Data Petani <br><strong><?= $petani->ektp->nama ?></strong></h2>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading ">
        <h3 class="panel-title">Data Diri<!-- <?= $petani->has('ektp') ? $this->Html->link($petani->ektp->nama, ['controller' => 'Poktan', 'action' => 'view', $petani->ektp->id]) : '' ?> --></h3>

    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">

        <tr>
            <td><?= __('NIK ') ?></td>
            <td><?= $petani->has('ektp') ? $this->Html->link($petani->ektp->id, ['controller' => 'Ektp', 'action' => 'view', $petani->ektp->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('No Hp') ?></td>
            <td><?= h($petani->no_tlp) ?></td>
        </tr>
        <tr>
            <td><?= __('Desa') ?></td>
            <td><?= h($petani->ektp->daerah['nama']) ?></td>
        </tr>
        <tr>
            <td><?= __('Bumdes') ?></td>
            <td><?= $petani->has('bumde') ? $this->Html->link($petani->bumde->nama, ['controller' => 'Bumdes', 'action' => 'view', $petani->bumde->id]) : '' ?></td>
        </tr>


    </table>
</div>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Lahan pertanian') ?></h3>
    </div>
    <div class="table-responsive">
    <?php if (!empty($petani->komoditas_petani)): ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th><?= __('Komoditas') ?></th>
                <th><?= __('Jenis Tani') ?></th>
                <!-- start data lahan -->
                <th><?= __('Alamat') ?></th>
                <th><?= __('Desa') ?></th>
                <th><?= __('Luas') ?></th>
                <th><?= __('Jenis Lahan') ?></th>
                <?php
                  if(!empty($petani->komoditas_petani[0]->lahan->assets)) {
                  ?>
                  <td>Ternak Jantan</td>
                  <td>Ternak Betina</td>
                <?php } ?>
                  <?php //if(!empty($petani->komoditas_petani)) { ?>
                <th class="actions"><?= __('View Rkp') ?></th>
                <th class="actions"><?= __('Tambah Rkp') ?></th>
                <?php //} ?>
                <!-- end data lahan -->
            </tr>
            </thead>
            <tbody>
            <?php foreach ($petani->komoditas_petani as $komoditas_petani): ?>
                <tr>

                    <td><?= h($komoditas_petani->komodita['nama']) ?></td>
                    <td><?= h($komoditas_petani->jenis_petani->nama_jenis_petani) ?></td>
                    <!-- start data lahan -->
                      <?php if(!empty($komoditas_petani->lahan)) { ?>
                        <td><?= h($komoditas_petani->lahan['alamat'])  ?> </td>
                        <td><?= h($komoditas_petani->lahan->daerah['nama'])  ?> </td>

                        <td><?= h($komoditas_petani->lahan['luas'])  ?> Ha</td>
                        <td><?= h($komoditas_petani->lahan->jenis_lahan['nama'])  ?></td>
                        <?php
                          if(!empty($komoditas_petani->lahan['assets'])) {
                            foreach ($komoditas_petani->lahan['assets'] as $key => $value) {
                          ?>
                          <td><?= $value['jumlah']  ?></td>
                        <?php }} ?>
                    <td class="actions">


                      <div class="btn-group">

                          <?php
                          foreach ($komoditas_petani->lahan->rdkk as $value) {

                            echo $this->Html->link(__($value['masa_tanam']),[
                              'controller'=>'rdkk',
                              'action'=>'view',$value['id']],
                              ['class'=>'btn btn-default']);
                            }


                          ?>


                      </div>

                    </td>
                  <td>
                      <?php
                     //debug($komoditas_petani->komodita->subsektor['rkp_aktiv']); exit;
                      if($komoditas_petani->komodita->subsektor['rkp_aktiv'] == 1){
                     echo $this->Html->link(__('Add RKP'),['controller'=>'rdkk','action'=>'add',$komoditas_petani['id_petani'],'?'=>[
                          'lahan_id'=>$komoditas_petani->lahan['id']
                        ]],['class'=>'btn btn-primary']);  } ?>

                  </td>
                      <?php  } ?>
                    <!-- end data lahan -->
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php else: ?>
        <p class="panel-body">Tidak ada komoditas</p>
    <?php endif; ?>
</div>

<?= $this->Form->create($petani); ?>
<div class="row" style="display:none">
    <div class="col-sm-4 pull-right">
        <?php

        if ($roleStr == "pegawai bumdes" || $roleStr == "bps") :
        ?>
            <?php if ($petani->status == 0 ) : ?>
                <div class="btn-group">
                    <?= $this->Form->button(__("Setujui"), ['class'=>'btn btn-primary', 'name' => "Approved"]); ?>
                    <br/><br/>
                </div>

                <div class="btn-group">
                    <?= $this->Form->button(__("Tolak"), ['class'=>'btn btn-danger', 'type' => "button", 'data-toggle' => "modal", 'data-target' => "#myModal"]); ?>
                    <br/><br/>
                </div>
            <?php endif ?>
        <?php
        endif;
        ?>


    </div>
<!--    <div class="col-sm-1 col-sm-push-1">

        <br/><br/>
    </div>

    <div class="col-sm-1 col-sm-push-7">

        <br/><br/>
    </div>-->
</div>
<?= $this->Form->end() ?>
