<?php
$this->layout('layout');
$this->Html->script('autocomplete.js', ['block' => true]);
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('List Petani'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Lahan'), ['controller' => 'Lahan', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?= $this->Html->link(__('List Petani'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Lahan'), ['controller' => 'Lahan', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($petani,['id'=>'addLahanForm']) ?>
<fieldset class="col-md-12">
    <div class='col-md-2'><h4>Provinsi</h4></div><div class='col-md-10'><h4 class="semi-bold"><?= $headerDaerah['Provinsi']['nama'] ?></h4></div>
    <div class='col-md-2'><h4>Kabupaten</h4></div><div class='col-md-10'><h4 class="semi-bold"><?= $headerDaerah['Kabupaten']['nama'] ?></h4></div>
    <div class='col-md-2'><h4>Kecamatan</h4></div><div class='col-md-10'><h4 class="semi-bold"><?= $headerDaerah['Kecamatan']['nama'] ?></h4></div>
    <div class='col-md-2'><h4>Desa/Kelurahan</h4></div><div class='col-md-10'><h4 class="semi-bold"><?php echo $this->request->session()->read('Bumdes.daerah.nama'); ?></h4></div>
    <div class='col-md-2'><h4>Nama Koperasi</h4></div><div class='col-md-10'><h4 class="semi-bold"><?php echo $this->request->session()->read('Bumdes.nama'); ?></h4></div>
</fieldset><p>&nbsp;</p>
<fieldset class="col-md-12">
    <div class="grid solid blue wide">
        <div class="grid-title">
            <h3>I. Data Individu Petani</h3>
        </div>
    </div>
    <div class="col-md-12">
        <?php
        echo "<div class='col-md-12'>";
        echo $this->Form->hidden('id');
        echo $this->Form->input('no_ektp', array('label'=>'NIK','id'=>'no_ektp','type'=>'number','autocomplete'=>'on','class'=>'ui-autocomplete-input'));
        echo "</div>";
        echo "<div class='col-md-6'>";
        echo $this->Form->input('ektp.nama');
        echo "</div>";
        echo "<div class='col-md-6'>";
        echo $this->Form->input('ektp.jenis_kelamin',['options'=>['L'=>'Laki - Laki','P'=>'Perempuan'],'empty'=>'Pilih Jenis Kelamin']);
        echo "</div>";
        echo "<div class='col-md-12'>";
        echo $this->Form->input('ektp.alamat');
        echo "</div>";
        echo "<div class='col-md-8'>";
        echo $this->Form->input('ektp.tanggal_lahir',array('id'=>'lahir','type'=>'text'));
        echo "</div>";
        echo "<div class='col-md-4'>";
        $lahirTahun = date('Y',strtotime($petani['tanggal_lahir']));
        if (is_null($petani['tanggal_lahir'])) {
            $umur = null;
        } else {
            $umur = date('Y') - $lahirTahun;
        }
        echo $this->Form->input(
          'umur',array('id'=>'umur','type'=>'text','value'=>$umur
        ));
        echo "</div>";
        echo "<div class='col-md-12'>";
        echo $this->Form->hidden('ektp.daerah_id',
        ['label'=>'Desa saya', 'value' => $this->request->session()->read('Bumdes.daerah.id')]);
        echo $this->Form->input('namaDesa',
        ['label'=>'Desa saya', 'value' => $this->request->session()->read('Bumdes.daerah.nama'),'readonly'=>'readonly']);
        echo "</div>";
        echo "<div class='col-md-12'>";
       // echo $this->Form->hidden('ektp.desa_kelurahan',array('label'=>'desa/kelurahan'));
        echo $this->Form->input('ektp.rt_rw',['label'=>'RT/RW']);
        echo "</div>";
        echo "<div class='col-md-12'>";
        echo $this->Form->input('ektp.gol_darah',array('label'=>'Golongan Darah',
        'options'=>[
            'A'=>'A','B'=>'B',"AB"=>"AB",'O'=>'O','Tidak Tahu'=>'Tidak Tahu'
        ],'empty'=>'Pilih Golongan Darah','required'=>FALSE));
        echo "</div>";
        echo "<div class='col-md-12'>";
        echo $this->Form->input('ektp.status_perkawinan',array('label'=>'Status Perkawinan','options'=>[
            'KAWIN'=>'Kawin','BELUM KAWIN'=>'Belum Kawin'
        ],'empty'=>'Pilih Status Perkawinan'));
        echo "</div>";
        echo "<div class='col-md-12'>";
        // echo $this->Form->input('ektp.kecamatan',array('label'=>'kecamatan'));
        // echo $this->Form->input('ektp.kabupaten_kota',array('label'=>'kabupaten/kota'));
        echo $this->Form->input('ektp.pekerjaan',array('value'=>'Petani','readonly'=>'readonly'));
        echo "</div>";
        echo "<div class='col-md-12'>";
        echo $this->Form->input('ektp.kewarganegaraan',array('value'=>'WNI','readonly'=>'readonly'));
        echo "</div>";
        echo "<div class='col-md-12'>";
        echo $this->Form->input('ektp.tanggal_terbit',array('label'=>'Tanggal Terbit','type'=>'text','id'=>'ektp-tgl-terbit'));
        echo "</div>";
        echo "<div class='col-md-8'>";
        echo $this->Form->input('ektp.tanggal_berakhir',array('label'=>'Tanggal Berakhir','type'=>'text','id'=>'ektp-tgl-berakhir'));
        echo "</div>";
        echo "<div class='col-md-4'>";
        echo $this->Form->input('ektp.seumur_hidup',['options'=>[1=>'ya','0'=>'tidak'],'empty'=>'berlaku seumur hidup ?']);
        echo "</div>";
        echo "<div class='col-md-12'>";
        echo $this->Form->input('no_tlp',['label'=>'Nomor HP','type'=>'number']);
        echo "</div>";
        ?>
    </div>
</fieldset>
<fieldset class="col-md-12">
    <div class="grid solid blue wide">
        <div class="grid-title">
            <h3>II. Keanggotaan Koperasi</h3>
        </div>
    </div>
    <?php
    // echo "<div class='col-md-4'>";
    // echo $this->Form->input('photo',['type'=>'file']);
    // echo "</div>";
    echo $this->Form->hidden('bumdes_id',['label'=>'Bumdes', 'options'=>$bumdes,'default'=>$this->request->session()->read('Bumdes.id')]);
    echo "<div class='col-md-12'>";
    echo $this->Form->input('anggota_kop',array('label'=>'Status Keanggotaan','class' => 'check-success','options'=>[1=>'Ya',0=>'tidak'],'empty'=>'Pilih Status Keanggotaan Koprasi'));
    echo "</div>";
    ?>
</fieldset>
<fieldset class="col-md-12">
    <div class="grid solid blue wide">
        <div class="grid-title">
            <h3>III. Jenis Kegiatan Usaha Petani</h3>
        </div>
    </div>
    <div class='col-md-12'>Jenis usaha pertanian terdiri atas : </div>
    <div class='col-md-12'>
      <?php
        $count=0; foreach ($optsSubSektor as $key => $value) {
        $count++;
        $checked = '';

        if(!empty($subsektors) && in_array($key,$subsektors)){
          $checked = " checked='checked' ";
        }
      ?>

          <div class="row-fluid">
            <div class="checkbox check-primary">
              <input id="checkbox<?= $key ?>" type="checkbox" name="subsektor[]" <?= $checked; ?> class="subSectorSelect" value="<?= $key ?>">
              <label for="checkbox<?= $key ?>"><?= $count ?>. <span class="subSektorLabel"><?= $value ?><span></label>
            </div>
          </div>
          <?php } ?>
    </div>
</fieldset>

<fieldset class="col-md-12">
    <div class="grid solid blue wide">
        <div class="grid-title">
            <h3>IV. Penguasaan/Pengusahaan Kegiatan Usaha Pertanian</h3>
        </div>
    </div>

    <ul class="nav nav-tabs tabSubSektor" >
      <?php
        foreach ($dataKomoditas as $key => $value) {

          if($key == key($dataKomoditas))
            $active = "active";
          else
            $active = "";
      ?>
        <li class="tab<?= $key ?> <?= $active; ?>">
          <a href="#tab<?= $key ?> "><?= $value[0]['nama_subsektor']; ?></a>
        </li>
      <?php } ?>

    </ul>

    <div class="tab-content" data-size="<?= $jumlahData ?>" id="add_lahan">
      <?php
      $count = 0;
        foreach ($dataKomoditas as $key => $value) {

          if($key == key($dataKomoditas))
            $active = "active";
          else
            $active = "";

          $labelLuasLahan = 'Luas Lahan (Ha)';
          $labelJenisLahan = "Tipe Lahan";
          if($key==10 || $key==12){
            $labelJenisLahan = "Jenis Usaha";
          }elseif ($key==11) {
            $labelJenisLahan = "Jenis Kapal";
            $labelLuasLahan = "Berat Kapal (GT)";
          }
      ?>


      <div class="tab-pane generalForm tab<?= $key?> <?= $active; ?>" id="tab<?= $key  ?>" >

        <?php foreach ($value as $index => $item){ ?>
        <div class="row column-seperation lahanForm">

          <div class="add_lahan" class="col-md-12">
                <div class="lahan_content_form">


                    <fieldset class="col-md-12">
                      <legend><?= $item['nama_subsektor']; ?></legend>
                        <?php

                            if(empty($item->komodita['subsektor_id'])){
                              $komoditaId = $item['subsektor_id'];
                            }else{
                              $komoditaId = $item->komodita['subsektor_id'];
                            }

                            echo "<div class='col-md-3'>";
                            echo $this->Form->input('komoditas_petani.'.$count.'.id_komoditas',['options'=>$optsKomoditas[$komoditaId],'empty'=>'Pilih Salah Satu','label'=>'Komoditas']);
                            echo "</div>";
                            echo "<div class='col-md-3'>";
                            echo $this->Form->hidden('komoditas_petani.'.$count.'.lahan.komoditas_id',['class'=>'komoditasPetani']);
                            echo $this->Form->hidden('komoditas_petani.'.$count.'.id');
                            echo $this->Form->hidden('komoditas_petani.'.$count.'.lahan.id');
                            echo $this->Form->hidden('komoditas_petani.0.subsektor_id',['class'=>'subsektor_id_lahan']);
                            echo $this->Form->hidden('komoditas_petani.'.$count.'.lahan.jenis_tanaman_id',['value'=>1,'class'=>'JenisTanaman']);
                            echo $this->Form->input('komoditas_petani.'.$count.'.id_jenis_petani',['type'=>'select','empty'=>'Pilih Status Pengelolaan','options'=>$jenis_petani, 'class' => "lahan-jenis_tanaman_id", 'label' => "Status Pengelolaan"]);
                            echo "</div>";
                            $hide = '';
                            if($item['id_jenis_petani']==3)
                              $hide ="style='display:none;'";

                            ?>
                            <div class="dataLahanDetil" <?= $hide; ?> >
                                <?php

                                echo "<div class='col-md-3 jenisLahan'>";
                                echo $this->Form->input('komoditas_petani.'.$count.'.lahan.jenis_lahan_id',['label'=>$labelJenisLahan, 'class' => "lahan-id_jenis_lahan", 'options' => $optsJenisLahan[$komoditaId],'required'=>FALSE]);
                                echo "</div>";
                                echo "<div class='col-md-3 luasLahan'>";
                                echo $this->Form->input('komoditas_petani.'.$count.'.lahan.luas', ['class' => "lahan-luas", 'type'=>'number','step'=>"0.01",'label' => $labelLuasLahan,'required'=>0]);
                                echo "</div>";

                                echo "<div class='col-md-3'>";
                                echo $this->Form->input('komoditas_petani.'.$count.'.lahan.daerah_id',['label'=>'Nama Desa Lahan', 'class' => "lahan-daerah_id select2", 'options' => $daerah,'required'=>FALSE,'empty'=>'Pilih Desa']);
                                echo "</div>";

                                if($key==10){
                                  echo "<div class='peternakan'><div class='col-md-6'>";
                                  echo $this->Form->hidden('komoditas_petani.'.$count.'.lahan.assets.0.id');
                                  echo $this->Form->hidden('komoditas_petani.'.$count.'.lahan.assets.0.nama_asset',['value'=>'Ternak Betina','class'=>'namaAsset','required'=>FALSE]);
                                  echo $this->Form->input('komoditas_petani.'.$count.'.lahan.assets.0.jumlah',['label'=>'Jumlah Ternak Betina','class'=>'jumlahAsset','required'=>FALSE]);
                                  echo "</div>";
                                  echo "<div class='col-md-6'>";
                                  echo $this->Form->hidden('komoditas_petani.'.$count.'.lahan.assets.1.id');
                                  echo $this->Form->hidden('komoditas_petani.'.$count.'.lahan.assets.1.nama_asset',['value'=>'Ternak Jantan','class'=>'namaAsset2']);
                                  echo $this->Form->input('komoditas_petani.'.$count.'.lahan.assets.1.jumlah',['label'=>'Jumlah Ternak Jantan','class'=>'jumlahAsset2','required'=>FALSE]);;
                                  echo "</div></div>";
                                }

                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('komoditas_petani.'.$count.'.lahan.alamat',['type'=>'textarea', 'class' => "lahan-alamat", 'label' => 'Alamat Lahan','required'=>0]);
                                echo "</div>";
                                ?>
                            </div>
                    </fieldset>
                </div>
            </div>
              <fieldset class="col-md-12 actionButton" data-id="<?= $key ?>">
                <legend><a href="#" class="addAllLahan btn btn-success">Add Lahan</a>  <a href="#" class="delLahan btn btn-warning" >Remove Lahan</a></legend>
            </fieldset>
        </div>

        <?php  $count++; } ?>
      </div>

<?php } ?>

</div>

</fieldset>

<div class="submitButton col-md-12 row">
    <input type="submit" class="btn btn-primary col-md-12 center" value="submit data" id="submitData" />
<!-- </div> -->



<!-- </div> -->
<?= $this->Form->end(); ?>






<?php $this->Html->scriptStart(['block' => true]); ?>
    $(".lahan-subsektor_id").change(function (){

        var $komoditasEL = $(this).parent().next();
        console.log($komoditasEL);

        $.ajax({
            type: "POST",
            url: "<?= $this->Url->build('/komoditas/getKomoditas/') ?>",
            data: { subsektor_id:  $(this).val() },
            success: function (data) {

                //console.log( $(this));
                //console.log( data );

                $komoditasEL.parent().append(data);
                $komoditasEL.remove();
            }
        });
    });
<?php $this->Html->scriptEnd(); ?>
