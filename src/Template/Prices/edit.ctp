<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $price->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $price->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Prices'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $price->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $price->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Prices'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($price); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Price']) ?></legend>
    <?php
    echo $this->Form->input('item_id', ['options' => $items]);
    echo $this->Form->input('harga');
    echo $this->Form->input('tanggal');
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
