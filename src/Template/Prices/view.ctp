<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Price'), ['action' => 'edit', $price->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Price'), ['action' => 'delete', $price->id], ['confirm' => __('Are you sure you want to delete # {0}?', $price->id)]) ?> </li>
<li><?= $this->Html->link(__('List Prices'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Price'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
<li><?= $this->Html->link(__('Edit Price'), ['action' => 'edit', $price->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Price'), ['action' => 'delete', $price->id], ['confirm' => __('Are you sure you want to delete # {0}?', $price->id)]) ?> </li>
<li><?= $this->Html->link(__('List Prices'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Price'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($price->id) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Item') ?></td>
            <td><?= $price->has('item') ? $this->Html->link($price->item->name, ['controller' => 'Items', 'action' => 'view', $price->item->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($price->id) ?></td>
        </tr>
        <tr>
            <td><?= __('Harga') ?></td>
            <td><?= $this->Number->format($price->harga) ?></td>
        </tr>
        <tr>
            <td><?= __('Tanggal') ?></td>
            <td><?= h($price->tanggal) ?></td>
        </tr>
    </table>
</div>

