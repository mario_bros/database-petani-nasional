<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Bumde'), ['action' => 'edit', $bumde->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Bumde'), ['action' => 'delete', $bumde->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bumde->id)]) ?> </li>
<li><?= $this->Html->link(__('List Bumdes'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Bumde'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
<li><?= $this->Html->link(__('Edit Bumde'), ['action' => 'edit', $bumde->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Bumde'), ['action' => 'delete', $bumde->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bumde->id)]) ?> </li>
<li><?= $this->Html->link(__('List Bumdes'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Bumde'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title">Data Kopdes</h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Nama') ?></td>
            <td><?= h($bumde->nama) ?></td>
        </tr>
        <tr>
            <td><?= __('Alamat') ?></td>
            <td><?= h($bumde->alamat) ?></td>
        </tr>
        <tr>
            <td><?= __('Daerah') ?></td>
            <td><?= $bumde->has('daerah') ? $this->Html->link($bumde->daerah->nama, ['controller' => 'Daerah', 'action' => 'view', $bumde->daerah->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($bumde->id) ?></td>
        </tr>
        <tr>
            <td><?= __('Lat') ?></td>
            <td><?= $this->Number->format($bumde->lat) ?></td>
        </tr>
        <tr>
            <td><?= __('Lon') ?></td>
            <td><?= $this->Number->format($bumde->lon) ?></td>
        </tr>
    </table>
</div>

