<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $bumde->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $bumde->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Bumdes'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $bumde->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $bumde->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Bumdes'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($bumde); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Bumde']) ?></legend>
    <?php
    echo $this->Form->input('nama');
    echo $this->Form->input('alamat');
    echo $this->Form->input('lat');
    echo $this->Form->input('lon');
    echo $this->Form->input('daerah_id', ['options' => $daerah]);
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
