<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
    
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-pills">' . $this->fetch('tb_actions') . '</ul>'); ?>
<h2>Bumdes</h2>
<div class="row">
    <div class="col-md-12 pull-right" id="secondNav">
        <ul class="nav nav-pills">    
            <!-- <li><?= $this->Html->link(__('New Bumdes'), ['action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('New Daerah'), ['controller' => ' Daerah', 'action' => 'add']); ?></li> -->
        </ul>                
    </div>
</div>
<div class="row">
  <div class="col-md-12">
    <a href="<?php echo $this->request->webroot ?>bumdes/add" class="btn btn-success">Add Bumdes</a>  
  </div>
</div>
<div class="table-responsive">
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('no'); ?></th>
                <th><?= $this->Paginator->sort('nama'); ?></th>
                <th><?= $this->Paginator->sort('alamat'); ?></th>
                <th><?= $this->Paginator->sort('lat'); ?></th>
                <th><?= $this->Paginator->sort('lon'); ?></th>
                <th><?= $this->Paginator->sort('daerah_id'); ?></th>
                <th class="actions"><?= __('Actions'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 0; foreach ($bumdes as $bumde): $no++; ?>
            <tr>
                <td><?= $no ?></td>
                <td><?= h($bumde->nama) ?></td>
                <td><?= h($bumde->alamat) ?></td>
                <td><?= $this->Number->format($bumde->lat) ?></td>
                <td><?= $this->Number->format($bumde->lon) ?></td>
                <td>
                    <?= $bumde->has('daerah') ? $this->Html->link($bumde->daerah->nama, ['controller' => 'Daerah', 'action' => 'view', $bumde->daerah->id]) : '' ?>
                </td>
                <td class="actions">
                    <?= $this->Html->link('', ['action' => 'view', $bumde->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                    <?= $this->Html->link('', ['action' => 'edit', $bumde->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                    <?= $this->Form->postLink('', ['action' => 'delete', $bumde->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bumde->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>