<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $jenisUser->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $jenisUser->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Jenis User'), ['action' => 'index']) ?></li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $jenisUser->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $jenisUser->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Jenis User'), ['action' => 'index']) ?></li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($jenisUser); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Jenis User']) ?></legend>
    <?php
    echo $this->Form->input('jenis');
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
