<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Jenis User'), ['action' => 'edit', $jenisUser->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Jenis User'), ['action' => 'delete', $jenisUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $jenisUser->id)]) ?> </li>
<li><?= $this->Html->link(__('List Jenis User'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Jenis User'), ['action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
<li><?= $this->Html->link(__('Edit Jenis User'), ['action' => 'edit', $jenisUser->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Jenis User'), ['action' => 'delete', $jenisUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $jenisUser->id)]) ?> </li>
<li><?= $this->Html->link(__('List Jenis User'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Jenis User'), ['action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($jenisUser->jenis) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Jenis') ?></td>
            <td><?= h($jenisUser->jenis) ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($jenisUser->id) ?></td>
        </tr>
    </table>
</div>

