<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $komoditas->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $komoditas->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Komoditas'), ['action' => 'index']) ?></li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $komoditas->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $komoditas->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Komoditas'), ['action' => 'index']) ?></li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($komoditas); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Komoditas']) ?></legend>
    <?php
    echo $this->Form->input('nama');
    echo $this->Form->input('subsektor_id', ['options' => $subsektor]);
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
