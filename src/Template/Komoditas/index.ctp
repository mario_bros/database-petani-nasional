<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?><h2>Komoditas</h2>
    <li><?= $this->Html->link(__('Tambah Komoditas'), ['action' => 'add']); ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-pills">' . $this->fetch('tb_actions') . '</ul>'); ?>
<div class="row">
  <div class="col-md-12">
    <a href="<?php echo $this->request->webroot ?>komoditas/add" class="btn btn-success">Add Komoditas</a>  </div>
</div>
<div class="table-responsive">
<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id'); ?></th>
            <th><?= $this->Paginator->sort('nama'); ?></th>
            <th><?= $this->Paginator->sort('subsektor'); ?></th>
            <th class="actions"><?= __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $no= 0;
        foreach ($komoditas as $item): 
            $no++;
        ?>
        <tr>
            <td><?= $no ?></td>
            <td><?= h($item->nama) ?></td>
            <td><?= ( $item->subsektor !== null) ? h($item->subsektor->nama) : ""?></td>
            <td class="actions">
                <?= $this->Html->link('', ['action' => 'view', $item->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                <?= $this->Html->link('', ['action' => 'edit', $item->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $item->id], ['confirm' => __('Are you sure you want to delete # {0}?', $item->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>