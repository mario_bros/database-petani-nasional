<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Komodita'), ['action' => 'edit', $komodita->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Komodita'), ['action' => 'delete', $komodita->id], ['confirm' => __('Are you sure you want to delete # {0}?', $komodita->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Komoditas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Komodita'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="komoditas view large-9 medium-8 columns content">
    <h3><?= h($komodita->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nama') ?></th>
            <td><?= h($komodita->nama) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($komodita->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Keterangan') ?></th>
            <td><?= $this->Number->format($komodita->keterangan) ?></td>
        </tr>
    </table>
</div>
