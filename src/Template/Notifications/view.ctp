<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('Edit Notification'), ['action' => 'edit', $notification->id]) ?> </li>
    <li><?= $this->Form->postLink(__('Delete Notification'), ['action' => 'delete', $notification->id], ['confirm' => __('Are you sure you want to delete # {0}?', $notification->id)]) ?> </li>
    <li><?= $this->Html->link(__('List Notifications'), ['action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Notification'), ['action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
    <ul class="nav nav-pills">
        <li><?= $this->Html->link(__('Edit Notification'), ['action' => 'edit', $notification->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Notification'), ['action' => 'delete', $notification->id], ['confirm' => __('Are you sure you want to delete # {0}?', $notification->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Notifications'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Notification'), ['action' => 'add']) ?> </li>
    </ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($notification->title) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Title') ?></td>
            <td><?= h($notification->title) ?></td>
        </tr>
        <tr>
            <td><?= __('Url') ?></td>
            <td><a href="<?= $notification->url ?>"><?= h($notification->url) ?></a></td>
        </tr>
        <tr>
            <td><?= __('Flag') ?></td>
            <td><?= $this->Number->format($notification->flag) ?></td>
        </tr>
        <tr>
            <td><?= __('User Id') ?></td>
            <td><?= $this->Number->format($notification->user_id) ?></td>
        </tr>
        <tr>
            <td><?= __('Send To Mail') ?></td>
            <td><?= $this->Number->format($notification->send_to_mail) ?></td>
        </tr>
        <tr>
            <td><?= __('Date') ?></td>
            <td><?= h($notification->date) ?></td>
        </tr>
    </table>
</div>