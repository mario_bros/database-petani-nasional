<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $notification->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $notification->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Notifications'), ['action' => 'index']) ?></li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $notification->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $notification->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Notifications'), ['action' => 'index']) ?></li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($notification); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Notification']) ?></legend>
    <?php
    echo $this->Form->input('title');
    echo $this->Form->input('url');
    echo $this->Form->input('flag');
    echo $this->Form->input('date');
    echo $this->Form->input('user_id');
    echo $this->Form->input('send_to_mail');
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
