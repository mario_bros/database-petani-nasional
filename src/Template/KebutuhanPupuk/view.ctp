<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Kebutuhan Pupuk'), ['action' => 'edit', $kebutuhanPupuk->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Kebutuhan Pupuk'), ['action' => 'delete', $kebutuhanPupuk->id], ['confirm' => __('Are you sure you want to delete # {0}?', $kebutuhanPupuk->id)]) ?> </li>
<li><?= $this->Html->link(__('List Kebutuhan Pupuk'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Kebutuhan Pupuk'), ['action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
<li><?= $this->Html->link(__('Edit Kebutuhan Pupuk'), ['action' => 'edit', $kebutuhanPupuk->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Kebutuhan Pupuk'), ['action' => 'delete', $kebutuhanPupuk->id], ['confirm' => __('Are you sure you want to delete # {0}?', $kebutuhanPupuk->id)]) ?> </li>
<li><?= $this->Html->link(__('List Kebutuhan Pupuk'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Kebutuhan Pupuk'), ['action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($kebutuhanPupuk->id) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Tahun') ?></td>
            <td><?= h($kebutuhanPupuk->tahun) ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($kebutuhanPupuk->id) ?></td>
        </tr>
        <tr>
            <td><?= __('Id Rdkk') ?></td>
            <td><?= $this->Number->format($kebutuhanPupuk->id_rdkk) ?></td>
        </tr>
        <tr>
            <td><?= __('Mt') ?></td>
            <td><?= $this->Number->format($kebutuhanPupuk->mt) ?></td>
        </tr>
        <tr>
            <td><?= __('Jumlah') ?></td>
            <td><?= $this->Number->format($kebutuhanPupuk->jumlah) ?></td>
        </tr>
        <tr>
            <td><?= __('Bulan') ?></td>
            <td><?= $this->Number->format($kebutuhanPupuk->bulan) ?></td>
        </tr>
        <tr>
            <td><?= __('Id Pupuk') ?></td>
            <td><?= $this->Number->format($kebutuhanPupuk->id_pupuk) ?></td>
        </tr>
    </table>
</div>

