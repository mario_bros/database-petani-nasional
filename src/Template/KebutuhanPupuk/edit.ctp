<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $kebutuhanPupuk->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $kebutuhanPupuk->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Kebutuhan Pupuk'), ['action' => 'index']) ?></li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $kebutuhanPupuk->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $kebutuhanPupuk->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Kebutuhan Pupuk'), ['action' => 'index']) ?></li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($kebutuhanPupuk); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Kebutuhan Pupuk']) ?></legend>
    <?php
    echo $this->Form->input('id_rdkk');
    echo $this->Form->input('mt');
    echo $this->Form->input('jumlah');
    echo $this->Form->input('bulan');
    echo $this->Form->input('tahun');
    echo $this->Form->input('id_pupuk');
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
