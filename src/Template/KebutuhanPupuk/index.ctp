<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('New Kebutuhan Pupuk'), ['action' => 'add']); ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-pills">' . $this->fetch('tb_actions') . '</ul>'); ?>

<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id'); ?></th>
            <th><?= $this->Paginator->sort('id_rdkk'); ?></th>
            <th><?= $this->Paginator->sort('mt'); ?></th>
            <th><?= $this->Paginator->sort('jumlah'); ?></th>
            <th><?= $this->Paginator->sort('bulan'); ?></th>
            <th><?= $this->Paginator->sort('tahun'); ?></th>
            <th><?= $this->Paginator->sort('id_pupuk'); ?></th>
            <th class="actions"><?= __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($kebutuhanPupuk as $kebutuhanPupuk): ?>
        <tr>
            <td><?= $this->Number->format($kebutuhanPupuk->id) ?></td>
            <td><?= $this->Number->format($kebutuhanPupuk->id_rdkk) ?></td>
            <td><?= $this->Number->format($kebutuhanPupuk->mt) ?></td>
            <td><?= $this->Number->format($kebutuhanPupuk->jumlah) ?></td>
            <td><?= $this->Number->format($kebutuhanPupuk->bulan) ?></td>
            <td><?= h($kebutuhanPupuk->tahun) ?></td>
            <td><?= $this->Number->format($kebutuhanPupuk->id_pupuk) ?></td>
            <td class="actions">
                <?= $this->Html->link('', ['action' => 'view', $kebutuhanPupuk->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                <?= $this->Html->link('', ['action' => 'edit', $kebutuhanPupuk->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $kebutuhanPupuk->id], ['confirm' => __('Are you sure you want to delete # {0}?', $kebutuhanPupuk->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>