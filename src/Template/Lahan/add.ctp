<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('List Lahan'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?= $this->Html->link(__('List Lahan'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($lahan); ?>
<fieldset>
    <legend><?= __('Add {0}', ['Lahan']) ?></legend>
    <?php
    echo $this->Form->input('alamat');
    echo $this->Form->input('daerah_id', ['options' => $daerah]);
    echo $this->Form->input('luas');
    echo $this->Form->input('jenis_tanaman_id', ['options' => $jenisTanaman]);
    echo $this->Form->input('jenis_tanah_id', ['options' => $jenisTanah]);
    echo $this->Form->input('petani_id', ['options' => $petani]);
    ?>
</fieldset>
<?= $this->Form->button(__("Add")); ?>
<?= $this->Form->end() ?>
