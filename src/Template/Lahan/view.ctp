<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Lahan'), ['action' => 'edit', $lahan->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Lahan'), ['action' => 'delete', $lahan->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lahan->id)]) ?> </li>
<li><?= $this->Html->link(__('List Lahan'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lahan'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
<li><?= $this->Html->link(__('Edit Lahan'), ['action' => 'edit', $lahan->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Lahan'), ['action' => 'delete', $lahan->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lahan->id)]) ?> </li>
<li><?= $this->Html->link(__('List Lahan'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lahan'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<a href="<?= $this->Url->build(['controller'=>'petani','action'=>'view',$lahan->petani_id]) ?>" class="btn btb-default">Kembali Ke petani</a>
<br />
<h2>Data Detil Lahan</h2>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($lahan->alamat) ?></h3>
    </div>

    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Alamat') ?></td>
            <td><?= h($lahan->alamat) ?></td>
        </tr>
        <tr>
            <td><?= __('Daerah') ?></td>
            <td><?= $lahan->has('daerah') ? $this->Html->link($lahan->daerah->nama, ['controller' => 'Daerah', 'action' => 'view', $lahan->daerah->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($lahan->id) ?></td>
        </tr>
        <tr>
            <td><?= __('Daerah Id') ?></td>
            <td><?= $this->Number->format($lahan->daerah_id) ?></td>
        </tr>
        <tr>
            <td><?= __('Luas') ?></td>
            <td><?= $this->Number->format($lahan->luas) ?></td>
        </tr>
        <tr>
            <td><?= __('Jenis Tanaman Id') ?></td>
            <td><?= $this->Number->format($lahan->jenis_tanaman_id) ?></td>
        </tr>
    </table>
</div>
