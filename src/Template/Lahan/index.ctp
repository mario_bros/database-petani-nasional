<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('New Lahan'), ['action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => ' Daerah', 'action' => 'add']); ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-pills">' . $this->fetch('tb_actions') . '</ul>'); ?>

<h5><?= __('List {0}', ['Lahan']) //. " " . $lahan[0]->petani->ektp->nama ?></h5>
<hr>
<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('no'); ?></th>
            <th><?= $this->Paginator->sort('alamat'); ?></th>
            <th><?= $this->Paginator->sort('daerah_id'); ?></th>
            <th><?= $this->Paginator->sort('luas'); ?></th>
            <th><?= $this->Paginator->sort('komoditas_id',['label'=>'Komoditas']); ?></th>
            <th><?= $this->Paginator->sort('jenis_lahan_id',['label'=>'jenis lahan']); ?></th>
            <th><?= $this->Paginator->sort('petani_id'); ?></th>
          <!--  <th class="actions"><?= __('Actions'); ?></th> -->
        </tr>
    </thead>
    <tbody>
        <?php $no = 0; foreach ($lahan as $item): $no++; ?>
        <tr>
            <td><?= $no ?></td>
            <td><?= h($item->alamat) ?></td>
            <td><?= $item->daerah->nama ?></td>
            <td><?= $this->Number->format($item->luas) ?> Ha</td>
            <td> <?= $item->has('komoditas') ? $item->komoditas->nama : '' ?></td>
            <td> <?= $item->has('jenis_lahan') ? $item->jenis_lahan->nama : 'N/A' ?></td>
            <td>
                <?= $item->has('petani') ? $item->petani->ektp->nama : 'N/A' ?>
            </td>
            <!-- <td class="actions">
                <?= $this->Html->link('', ['action' => 'view', $item->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                <?= $this->Html->link('', ['action' => 'edit', $item->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $item->id], ['confirm' => __('Are you sure you want to delete # {0}?', $item->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
            </td> -->
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
