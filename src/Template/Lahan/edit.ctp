<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $lahan->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $lahan->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Lahan'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $lahan->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $lahan->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Lahan'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Daerah'), ['controller' => 'Daerah', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Daerah'), ['controller' => 'Daerah', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($lahan); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Lahan']) ?></legend>
    <?php
    echo $this->Form->input('alamat');
    echo $this->Form->input('daerah_id', ['options' => $dukuh, 'label' => "Desa / Kelurahan"]);
    echo $this->Form->input('luas');
    echo $this->Form->input('jenis_tanaman_id', ['options' => $jenisTanaman, 'label' => "Komoditas"]);
    echo $this->Form->input('jenis_lahan_id', ['options' => $jenisLahan, 'label' => "Jenis lahan"]);
    echo ( $this->request->session()->read('Auth.User.jenis_user_id') == "1") ? $this->Form->input('petani_id', ['options' => $petani]) : "";
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
