<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $subsektor->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $subsektor->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Subsektor'), ['action' => 'index']) ?></li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $subsektor->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $subsektor->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Subsektor'), ['action' => 'index']) ?></li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($subsektor); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Subsektor']) ?></legend>
    <?php
    echo $this->Form->input('nama');
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
