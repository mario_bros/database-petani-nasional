<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Subsektor'), ['action' => 'edit', $subsektor->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Subsektor'), ['action' => 'delete', $subsektor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $subsektor->id)]) ?> </li>
<li><?= $this->Html->link(__('List Subsektor'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Subsektor'), ['action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
<li><?= $this->Html->link(__('Edit Subsektor'), ['action' => 'edit', $subsektor->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Subsektor'), ['action' => 'delete', $subsektor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $subsektor->id)]) ?> </li>
<li><?= $this->Html->link(__('List Subsektor'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Subsektor'), ['action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($subsektor->id) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Nama') ?></td>
            <td><?= h($subsektor->nama) ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($subsektor->id) ?></td>
        </tr>
    </table>
</div>

