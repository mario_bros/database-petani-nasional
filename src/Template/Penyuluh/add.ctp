<?php
//$this->layout('layout');
$this->Html->script('autocompletePenyuluh.js', ['block' => true]);
//$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('List Petani'), ['action' => 'index']) ?></li>
<li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lahan'), ['controller' => 'Lahan', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?= $this->Html->link(__('List Petani'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Lahan'), ['controller' => 'Lahan', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($penyuluh) ?>
<fieldset class="col-md-6">
    <legend><?=__('Data Penyuluh') ?></legend>
    <?php
        echo $this->Form->input('photo',['type'=>'file']);

    ?>
</fieldset>
<fieldset class="col-md-6">
    <legend><?= __('Data EKTP') ?></legend>
    <?php
    echo $this->Form->input('no_ektp', array('id'=>'no_ektp','type'=>'search','autocomplete'=>'off','class'=>'ui-autocomplete-input'));
    echo $this->Form->input('ektp.nama');
    echo $this->Form->input('ektp.alamat');
    echo $this->Form->input('ektp.tanggal_lahir',array('id'=>'ektp-tgl-lahir','type'=>'text'));
    echo $this->Form->input('ektp.daerah_id',['label'=>'desa/kelurahan']);
    // echo $this->Form->input('ektp.desa_kelurahan',array('label'=>'desa/kelurahan'));
    echo $this->Form->input('ektp.rt_rw',['label'=>'RT/RW']);
    echo $this->Form->input('ektp.kecamatan',['label'=>'kecamatan']);
    echo $this->Form->input('ektp.kabupaten_kota',['label'=>'kabupaten/kota']);
    echo $this->Form->input('ektp.pekerjaan',array('value'=>'Petani','readonly'=>'readonly'));
    echo $this->Form->input('ektp.kewarganegaraan',array('value'=>'WNI','readonly'=>'readonly'));
    echo $this->Form->input('ektp.status_perkawinan',array('label'=>'Status Perkawinan','options'=>[
        'Kawin'=>'Kawin','Belum Kawin'=>'Belum Kawin'
    ],'empty'=>'Please Select One'));
    echo $this->Form->input('ektp.gol_darah',array('label'=>'Golongan Darah','options'=>array(
        'A'=>'A','B'=>'B',"AB"=>"AB",'O'=>'O'
    ),'empty'=>'please select one'));
    echo $this->Form->input('ektp.tanggal_terbit',array('label'=>'Tanggal Terbit','type'=>'text','id'=>'ektp-tgl-terbit'));
    echo $this->Form->input('ektp.tanggal_berakhir',array('label'=>'Tanggal Berakhir','type'=>'text','id'=>'ektp-tgl-berakhir'));

    ?>
    <div class="col-md-6">
        
    </div>
</fieldset>
<?= $this->Form->button(__('Submit'), ['class'=>'btn btn-primary']); ?>
<?= $this->Form->end() ?>