    <?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_footer');
?>
    
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-pills">' . $this->fetch('tb_actions') . '</ul>'); ?>


<h2>Penyuluh</h2>
<div class="row">
    <div class="col-md-12 pull-right" id="secondNav">
        <ul class="nav nav-pills">    
            <li><?= $this->Html->link(__('New Penyuluh'), ['action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('New User'), ['controller' => ' Users', 'action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('List Ektp'), ['controller' => 'Ektp', 'action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('New Ektp'), ['controller' => ' Ektp', 'action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('New Poktan'), ['controller' => ' Poktan', 'action' => 'add']); ?></li>
        </ul>                
    </div>
</div>
<div class="table-responsive">
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('no'); ?></th>
                <th><?= $this->Paginator->sort('no_ektp'); ?></th>
                <th><?= $this->Paginator->sort('photo'); ?></th>
                <th class="actions"><?= __('Actions'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 0; foreach ($penyuluh as $penyuluh): $no++; ?>
            <tr>
                <td><?= $no ?></td>
                <td>
                    <?= $penyuluh->has('ektp') ? $this->Html->link($penyuluh->ektp->id, ['controller' => 'Ektp', 'action' => 'view', $penyuluh->ektp->id]) : '' ?>
                </td>
                <td><?= h($penyuluh->photo) ?></td>
                <td class="actions">
                    <?= $this->Html->link('', ['action' => 'view', $penyuluh->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                    <?= $this->Html->link('', ['action' => 'edit', $penyuluh->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                    <?= $this->Form->postLink('', ['action' => 'delete', $penyuluh->id], ['confirm' => __('Are you sure you want to delete # {0}?', $penyuluh->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
