<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Penyuluh'), ['action' => 'edit', $penyuluh->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Penyuluh'), ['action' => 'delete', $penyuluh->id], ['confirm' => __('Are you sure you want to delete # {0}?', $penyuluh->id)]) ?> </li>
<li><?= $this->Html->link(__('List Penyuluh'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Penyuluh'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Ektp'), ['controller' => 'Ektp', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Ektp'), ['controller' => 'Ektp', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
<li><?= $this->Html->link(__('Edit Penyuluh'), ['action' => 'edit', $penyuluh->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Penyuluh'), ['action' => 'delete', $penyuluh->id], ['confirm' => __('Are you sure you want to delete # {0}?', $penyuluh->id)]) ?> </li>
<li><?= $this->Html->link(__('List Penyuluh'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Penyuluh'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Ektp'), ['controller' => 'Ektp', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Ektp'), ['controller' => 'Ektp', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Poktan'), ['controller' => 'Poktan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Poktan'), ['controller' => 'Poktan', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($penyuluh->id) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Ektp') ?></td>
            <td><?= $penyuluh->has('ektp') ? $this->Html->link($penyuluh->ektp->id, ['controller' => 'Ektp', 'action' => 'view', $penyuluh->ektp->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Photo') ?></td>
            <td><?= h($penyuluh->photo) ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($penyuluh->id) ?></td>
        </tr>
    </table>
</div>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related Poktan') ?></h3>
    </div>
    <?php if (!empty($penyuluh->poktan)): ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nama') ?></th>
                <th><?= __('Alamat') ?></th>
                <th><?= __('Daerah Id') ?></th>
                <th><?= __('Gapoktan Id') ?></th>
                <th><?= __('Lat') ?></th>
                <th><?= __('Lon') ?></th>
                <th><?= __('Aprroved') ?></th>
                <th><?= __('Ketua Poktan') ?></th>
                <th><?= __('Penyuluh Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($penyuluh->poktan as $poktan): ?>
                <tr>
                    <td><?= h($poktan->id) ?></td>
                    <td><?= h($poktan->nama) ?></td>
                    <td><?= h($poktan->alamat) ?></td>
                    <td><?= h($poktan->daerah_id) ?></td>
                    <td><?= h($poktan->gapoktan_id) ?></td>
                    <td><?= h($poktan->lat) ?></td>
                    <td><?= h($poktan->lon) ?></td>
                    <td><?= h($poktan->aprroved) ?></td>
                    <td><?= h($poktan->ketua_poktan) ?></td>
                    <td><?= h($poktan->penyuluh_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link('', ['controller' => 'Poktan', 'action' => 'view', $poktan->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                        <?= $this->Html->link('', ['controller' => 'Poktan', 'action' => 'edit', $poktan->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                        <?= $this->Form->postLink('', ['controller' => 'Poktan', 'action' => 'delete', $poktan->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poktan->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p class="panel-body">no related Poktan</p>
    <?php endif; ?>
</div>
