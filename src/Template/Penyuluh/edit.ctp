<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $penyuluh->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $penyuluh->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Penyuluh'), ['action' => 'index']) ?></li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $penyuluh->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $penyuluh->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Penyuluh'), ['action' => 'index']) ?></li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($penyuluh); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Penyuluh']) ?></legend>
    <?php
    echo $this->Form->input('no_ektp');
    echo $this->Form->input('photo');
    echo $this->Form->input('user_id');
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
