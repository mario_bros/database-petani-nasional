<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Tr Rdkk'), ['action' => 'edit', $trRdkk->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Tr Rdkk'), ['action' => 'delete', $trRdkk->id], ['confirm' => __('Are you sure you want to delete # {0}?', $trRdkk->id)]) ?> </li>
<li><?= $this->Html->link(__('List Tr Rdkk'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Tr Rdkk'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Rdkk'), ['controller' => 'Rdkk', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Rdkk'), ['controller' => 'Rdkk', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lahan'), ['controller' => 'Lahan', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
<li><?= $this->Html->link(__('Edit Tr Rdkk'), ['action' => 'edit', $trRdkk->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Tr Rdkk'), ['action' => 'delete', $trRdkk->id], ['confirm' => __('Are you sure you want to delete # {0}?', $trRdkk->id)]) ?> </li>
<li><?= $this->Html->link(__('List Tr Rdkk'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Tr Rdkk'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Rdkk'), ['controller' => 'Rdkk', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Rdkk'), ['controller' => 'Rdkk', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lahan'), ['controller' => 'Lahan', 'action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($trRdkk->id) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Rdkk') ?></td>
            <td><?= $trRdkk->has('rdkk') ? $this->Html->link($trRdkk->rdkk->id, ['controller' => 'Rdkk', 'action' => 'view', $trRdkk->rdkk->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Lahan') ?></td>
            <td><?= $trRdkk->has('lahan') ? $this->Html->link($trRdkk->lahan->alamat, ['controller' => 'Lahan', 'action' => 'view', $trRdkk->lahan->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Item') ?></td>
            <td><?= $trRdkk->has('item') ? $this->Html->link($trRdkk->item->name, ['controller' => 'Items', 'action' => 'view', $trRdkk->item->id]) : '' ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($trRdkk->id) ?></td>
        </tr>
        <tr>
            <td><?= __('Jumlah') ?></td>
            <td><?= $this->Number->format($trRdkk->jumlah) ?></td>
        </tr>
    </table>
</div>

