<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $trRdkk->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $trRdkk->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Tr Rdkk'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Rdkk'), ['controller' => 'Rdkk', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Rdkk'), ['controller' => 'Rdkk', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Lahan'), ['controller' => 'Lahan', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?=
    $this->Form->postLink(
        __('Delete'),
        ['action' => 'delete', $trRdkk->id],
        ['confirm' => __('Are you sure you want to delete # {0}?', $trRdkk->id)]
    )
    ?>
    </li>
    <li><?= $this->Html->link(__('List Tr Rdkk'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Rdkk'), ['controller' => 'Rdkk', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Rdkk'), ['controller' => 'Rdkk', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Lahan'), ['controller' => 'Lahan', 'action' => 'add']) ?> </li>
    <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($trRdkk); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Tr Rdkk']) ?></legend>
    <?php
    echo $this->Form->input('rdkk_id', ['options' => $rdkk]);
    echo $this->Form->input('lahan_id', ['options' => $lahan]);
    echo $this->Form->input('item_id', ['options' => $items]);
    echo $this->Form->input('jumlah');
    ?>
</fieldset>
<?= $this->Form->button(__("Save")); ?>
<?= $this->Form->end() ?>
