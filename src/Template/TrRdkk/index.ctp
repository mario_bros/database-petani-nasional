<?php
/* @var $this \Cake\View\View */
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('New Tr Rdkk'), ['action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List Rdkk'), ['controller' => 'Rdkk', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Rdkk'), ['controller' => ' Rdkk', 'action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Lahan'), ['controller' => ' Lahan', 'action' => 'add']); ?></li>
    <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']); ?></li>
    <li><?= $this->Html->link(__('New Item'), ['controller' => ' Items', 'action' => 'add']); ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav nav-sidebar">' . $this->fetch('tb_actions') . '</ul>'); ?>

<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id'); ?></th>
            <th><?= $this->Paginator->sort('rdkk_id'); ?></th>
            <th><?= $this->Paginator->sort('lahan_id'); ?></th>
            <th><?= $this->Paginator->sort('item_id'); ?></th>
            <th><?= $this->Paginator->sort('jumlah'); ?></th>
            <th class="actions"><?= __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($trRdkk as $trRdkk): ?>
        <tr>
            <td><?= $this->Number->format($trRdkk->id) ?></td>
            <td>
                <?= $trRdkk->has('rdkk') ? $this->Html->link($trRdkk->rdkk->id, ['controller' => 'Rdkk', 'action' => 'view', $trRdkk->rdkk->id]) : '' ?>
            </td>
            <td>
                <?= $trRdkk->has('lahan') ? $this->Html->link($trRdkk->lahan->alamat, ['controller' => 'Lahan', 'action' => 'view', $trRdkk->lahan->id]) : '' ?>
            </td>
            <td>
                <?= $trRdkk->has('item') ? $this->Html->link($trRdkk->item->name, ['controller' => 'Items', 'action' => 'view', $trRdkk->item->id]) : '' ?>
            </td>
            <td><?= $this->Number->format($trRdkk->jumlah) ?></td>
            <td class="actions">
                <?= $this->Html->link('', ['action' => 'view', $trRdkk->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                <?= $this->Html->link('', ['action' => 'edit', $trRdkk->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $trRdkk->id], ['confirm' => __('Are you sure you want to delete # {0}?', $trRdkk->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>