<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');


$this->start('tb_actions');
?>
<li><?= $this->Html->link(__('Edit Jenis Tanaman'), ['action' => 'edit', $jenisTanaman->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Jenis Tanaman'), ['action' => 'delete', $jenisTanaman->id], ['confirm' => __('Are you sure you want to delete # {0}?', $jenisTanaman->id)]) ?> </li>
<li><?= $this->Html->link(__('List Jenis Tanaman'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Jenis Tanaman'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lahan'), ['controller' => 'Lahan', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
<li><?= $this->Html->link(__('Edit Jenis Tanaman'), ['action' => 'edit', $jenisTanaman->id]) ?> </li>
<li><?= $this->Form->postLink(__('Delete Jenis Tanaman'), ['action' => 'delete', $jenisTanaman->id], ['confirm' => __('Are you sure you want to delete # {0}?', $jenisTanaman->id)]) ?> </li>
<li><?= $this->Html->link(__('List Jenis Tanaman'), ['action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Jenis Tanaman'), ['action' => 'add']) ?> </li>
<li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']) ?> </li>
<li><?= $this->Html->link(__('New Lahan'), ['controller' => 'Lahan', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= h($jenisTanaman->id) ?></h3>
    </div>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
            <td><?= __('Nama') ?></td>
            <td><?= h($jenisTanaman->nama) ?></td>
        </tr>
        <tr>
            <td><?= __('Id') ?></td>
            <td><?= $this->Number->format($jenisTanaman->id) ?></td>
        </tr>
    </table>
</div>

<div class="panel panel-default">
    <!-- Panel header -->
    <div class="panel-heading">
        <h3 class="panel-title"><?= __('Related Lahan') ?></h3>
    </div>
    <?php if (!empty($jenisTanaman->lahan)): ?>
        <table class="table table-striped">
            <thead>
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Alamat') ?></th>
                <th><?= __('Daerah Id') ?></th>
                <th><?= __('Luas') ?></th>
                <th><?= __('Jenis Tanaman Id') ?></th>
                <th><?= __('Petani Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($jenisTanaman->lahan as $lahan): ?>
                <tr>
                    <td><?= h($lahan->id) ?></td>
                    <td><?= h($lahan->alamat) ?></td>
                    <td><?= h($lahan->daerah_id) ?></td>
                    <td><?= h($lahan->luas) ?></td>
                    <td><?= h($lahan->jenis_tanaman_id) ?></td>
                    <td><?= h($lahan->petani_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link('', ['controller' => 'Lahan', 'action' => 'view', $lahan->id], ['title' => __('View'), 'class' => 'btn btn-default glyphicon glyphicon-eye-open']) ?>
                        <?= $this->Html->link('', ['controller' => 'Lahan', 'action' => 'edit', $lahan->id], ['title' => __('Edit'), 'class' => 'btn btn-default glyphicon glyphicon-pencil']) ?>
                        <?= $this->Form->postLink('', ['controller' => 'Lahan', 'action' => 'delete', $lahan->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lahan->id), 'title' => __('Delete'), 'class' => 'btn btn-default glyphicon glyphicon-trash']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <p class="panel-body">no related Lahan</p>
    <?php endif; ?>
</div>
