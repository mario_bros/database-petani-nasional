<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');

$this->start('tb_actions');
?>
    <li><?= $this->Html->link(__('List Jenis Tanaman'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Lahan'), ['controller' => 'Lahan', 'action' => 'add']) ?> </li>
<?php
$this->end();

$this->start('tb_sidebar');
?>
<ul class="nav nav-pills">
    <li><?= $this->Html->link(__('List Jenis Tanaman'), ['action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('List Lahan'), ['controller' => 'Lahan', 'action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Lahan'), ['controller' => 'Lahan', 'action' => 'add']) ?> </li>
</ul>
<?php
$this->end();
?>
<?= $this->Form->create($jenisTanaman); ?>
<fieldset>
    <legend><?= __('Add {0}', ['Jenis Tanaman']) ?></legend>
    <?php
    echo $this->Form->input('nama');
    ?>
</fieldset>
<?= $this->Form->button(__("Add")); ?>
<?= $this->Form->end() ?>
