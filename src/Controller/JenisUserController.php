<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * JenisUser Controller
 *
 * @property \App\Model\Table\JenisUserTable $JenisUser
 */
class JenisUserController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $jenisUser = $this->paginate($this->JenisUser);

        $this->set(compact('jenisUser'));
        $this->set('_serialize', ['jenisUser']);
    }

    /**
     * View method
     *
     * @param string|null $id Jenis User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $jenisUser = $this->JenisUser->get($id, [
            'contain' => []
        ]);

        $this->set('jenisUser', $jenisUser);
        $this->set('_serialize', ['jenisUser']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $jenisUser = $this->JenisUser->newEntity();
        if ($this->request->is('post')) {
            $jenisUser = $this->JenisUser->patchEntity($jenisUser, $this->request->data);
            if ($this->JenisUser->save($jenisUser)) {
                $this->Flash->success(__('The jenis user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The jenis user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('jenisUser'));
        $this->set('_serialize', ['jenisUser']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Jenis User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $jenisUser = $this->JenisUser->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $jenisUser = $this->JenisUser->patchEntity($jenisUser, $this->request->data);
            if ($this->JenisUser->save($jenisUser)) {
                $this->Flash->success(__('The jenis user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The jenis user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('jenisUser'));
        $this->set('_serialize', ['jenisUser']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Jenis User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $jenisUser = $this->JenisUser->get($id);
        if ($this->JenisUser->delete($jenisUser)) {
            $this->Flash->success(__('The jenis user has been deleted.'));
        } else {
            $this->Flash->error(__('The jenis user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
