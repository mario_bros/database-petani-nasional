<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use App\Controller\DaerahController;

/**
 * Service Controller
 *
 * @property \App\Model\Table\EktpTable $Ektp
 */
class ServiceController extends AppController
{
    private $connection;
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Petani','Ektp']
        ];
        $petani = $this->paginate($this->Petani);

        $this->set(compact('Ektp'));
        $this->set('_serialize', ['petani']);
    }

    // public function beforeFilter(Event $event)
    // {
    //     parent::beforeFilter($event);
    //     $this->connection = ConnectionManager::get('default');
    // }

    public function getEktp()
    {
        $data = $this->Petani->Ektp->find('all')->select(['id']);        
        $this->set('data', $data);
        $this->set('_serialize', ['data']);
        
        // $query="select id from ektp";
        // $data = $this->connection->execute($query)->fetchAll('assoc');
        // $this->set('data', $data);
        // $this->set('_serialize', ['data']);

        //echo  $query; exit;
    }

}
