<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * KomoditasPetani Controller
 *
 * @property \App\Model\Table\KomoditasPetaniTable $KomoditasPetani
 */
class KomoditasPetaniController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $komoditasPetani = $this->paginate($this->KomoditasPetani);

        $this->set(compact('komoditasPetani'));
        $this->set('_serialize', ['komoditasPetani']);
    }

    /**
     * View method
     *
     * @param string|null $id Komoditas Petani id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $komoditasPetani = $this->KomoditasPetani->get($id, [
            'contain' => []
        ]);

        $this->set('komoditasPetani', $komoditasPetani);
        $this->set('_serialize', ['komoditasPetani']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $komoditasPetani = $this->KomoditasPetani->newEntity();
        if ($this->request->is('post')) {
            $komoditasPetani = $this->KomoditasPetani->patchEntity($komoditasPetani, $this->request->data);
            if ($this->KomoditasPetani->save($komoditasPetani)) {
                $this->Flash->success(__('The komoditas petani has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The komoditas petani could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('komoditasPetani'));
        $this->set('_serialize', ['komoditasPetani']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Komoditas Petani id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $komoditasPetani = $this->KomoditasPetani->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $komoditasPetani = $this->KomoditasPetani->patchEntity($komoditasPetani, $this->request->data);
            if ($this->KomoditasPetani->save($komoditasPetani)) {
                $this->Flash->success(__('The komoditas petani has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The komoditas petani could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('komoditasPetani'));
        $this->set('_serialize', ['komoditasPetani']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Komoditas Petani id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        //$this->request->allowMethod(['post', 'delete']);
    
        $komoditasPetani = $this->KomoditasPetani->get($id);
        if ($this->KomoditasPetani->delete($komoditasPetani)) {
            $this->Flash->success(__('The komoditas petani has been deleted.'));
        } else {
            $this->Flash->error(__('The komoditas petani could not be deleted. Please, try again.'));
        }
        return $this->redirect($this->referer());
    }
}
