<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Gapoktan Controller
 *
 * @property \App\Model\Table\GapoktanTable $Gapoktan
 */
class GapoktanController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Daerah'],
            'conditions' => ( $this->request->session()->read('Auth.User.id') == 1 ) ? [''] : [
                'Gapoktan.bumdes_id' => $this->request->session()->read('Bumdes.id')
            ]
        ];
        $gapoktan = $this->paginate($this->Gapoktan);
        //debug($gapoktan); exit;

        $this->set(compact('gapoktan'));
        $this->set('_serialize', ['gapoktan']);
    }

    /**
     * View method
     *
     * @param string|null $id Gapoktan id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $gapoktan = $this->Gapoktan->get($id, [
            //'contain' => ['Daerah.ParentDaerah', 'Poktan.KetuaPoktan.Ektp', 'Petani.Ektp', /*'Poktan.Daerah.ParentDaerah'*/]
            'contain' => ['Daerah', 'Poktan.KetuaPoktan.Ektp', 'Petani.Ektp', 'Poktan.Daerah', 'Poktan.Penyuluh.Ektp']
        ]);
        //debug($gapoktan->poktan[0]->penyuluh->ektp->nama); exit;
        $this->loadModel('Daerah');
        //$kecamatan = $this->Daerah->get($gapoktan->daerah_id, ['contain' =>  ['ParentDaerah.ParentDaerah'] ]);
        $provinsi = $this->Daerah->get($gapoktan->daerah->parent_daerah->grand_parent_daerah->third_level_parent_daerah->id);
        //debug($provinsi); exit(' cari provinsi');

        $this->set( compact('gapoktan', 'provinsi'));
        $this->set('_serialize', ['gapoktan']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $gapoktan = $this->Gapoktan->newEntity();
        if ($this->request->is('post')) {
            $gapoktan = $this->Gapoktan->patchEntity($gapoktan, $this->request->data);
            if ($this->Gapoktan->save($gapoktan)) {
                $this->Flash->success(__('The gapoktan has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The gapoktan could not be saved. Please, try again.'));
            }
        }
        $daerah = $this->Gapoktan->Daerah->find('list', ['limit' => 200]);
        $bps = $this->Gapoktan->Bps->find('list', ['keyField' => 'id', 'valueField' => 'ektp.nama'])->contain(['Ektp']);
        $this->set(compact('gapoktan', 'daerah','bps'));
        $this->set('_serialize', ['gapoktan']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Gapoktan id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        
        $gapoktan = $this->Gapoktan->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gapoktan = $this->Gapoktan->patchEntity($gapoktan, $this->request->data);
            
            //debug($gapoktan); exit;
            
            if ($this->Gapoktan->save($gapoktan)) {
                $this->Flash->success(__('The gapoktan has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The gapoktan could not be saved. Please, try again.'));
            }
        }

        $daerah = $this->Gapoktan->Daerah->find('list', ['limit' => 200]);

        $this->loadModel('Petani');
        $petani = $this->Gapoktan->Poktan->Petani->find('list', ['keyField' => 'id', 'valueField' => 'ektp.nama'])->contain(['Ektp']);
        $bps = $this->Gapoktan->Bps->find('list', ['keyField' => 'id', 'valueField' => 'ektp.nama'])->contain(['Ektp']);
        $this->set(compact('gapoktan', 'daerah','bps','petani'));
        $this->set('_serialize', ['gapoktan']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Gapoktan id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $gapoktan = $this->Gapoktan->get($id);
        if ($this->Gapoktan->delete($gapoktan)) {
            $this->Flash->success(__('The gapoktan has been deleted.'));
        } else {
            $this->Flash->error(__('The gapoktan could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
