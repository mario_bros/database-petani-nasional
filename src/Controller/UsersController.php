<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

  public function beforeFilter(Event $event)
  {
      parent::beforeFilter($event);

  }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

        $error = '';
          if ($this->request->is(['patch', 'post', 'put'])) {
            $query = $this->Users->find('all')->where([

              'username LIKE'=>"%".$this->request->data['username']."%",
              'Users.nama LIKE'=>"%".$this->request->data['nama']."%",
              'Users.email LIKE'=>"%".$this->request->data['email']."%"
            ])->contain(['JenisUser','Bumdes.Daerah']);
            try{
              $users = $this->paginate($query);
            }catch (NotFoundException $e) {
              $error = 1;
              $users = $this->Users;
            }

          }else{
            $this->set('users', $this->Users->find('all'));
            $this->paginate = [
                'contain' => ['JenisUser', 'Daerah']
            ];
            $users = $this->paginate($this->Users);
          }


        $this->set(compact('users','error'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['JenisUser', 'Daerah', 'Petani', 'Log', 'Notifications', 'Penyuluh']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            //debug($user); exit;
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $jenisUser = $this->Users->JenisUser->find('list', ['limit' => 200]);
        $daerah = $this->Users->Daerah->find('list', ['limit' => 200]);
        $ketuaPoktan = $this->Users->Petani->Poktan->find('all')->select(['ketua_poktan'])->toArray();
        foreach ( $ketuaPoktan as $item ) {
            $data[] = $item['ketua_poktan'];
        }

        $bps = $this->Users->Bps->find('list', ['keyField' => 'id', 'valueField' => 'ektp.nama'])->contain(['Ektp']);
        $petanis = $this->Users->Petani->find('list', ['keyField' => 'id', 'valueField' => 'ektp.nama'])->contain(['Ektp'])->where([
            'Petani.id IN'=>$data
        ]);

        //debug($petanis); exit;
        $penyuluhs = $this->Users->Penyuluh->find('list', ['keyField' => 'id', 'valueField' => 'ektp.nama'])->contain(['Ektp']);
        $bumdes = $this->Users->Bumdes->find('list');
        $this->set(compact('user', 'jenisUser', 'daerah', 'petanis','penyuluhs','bumdes','bps'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $user = $this->Users->patchEntity($user, $this->request->data);
            //debug($user); exit;
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['controller'=>'Pages','action' => 'display']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $jenisUser = $this->Users->JenisUser->find('list', ['limit' => 200]);
        $daerah = $this->Users->Daerah->find('list', ['limit' => 200]);
        $ketuaPoktan = $this->Users->Petani->Poktan->find('all')->select(['ketua_poktan'])->toArray();
        $bumdes = $this->Users->Bumdes->find('list');
        $bps = $this->Users->Bps->find('list', ['keyField' => 'id', 'valueField' => 'ektp.nama'])->contain(['Ektp']);
        foreach ( $ketuaPoktan as $item ) {
            $data[] = $item['ketua_poktan'];
        }



        $petanis = $this->Users->Petani->find('list', ['keyField' => 'id', 'valueField' => 'ektp.nama'])->contain(['Ektp'])->where([
            'Petani.id IN'=>$data
        ]);
        $penyuluhs = $this->Users->Penyuluh->find('list', ['keyField' => 'id', 'valueField' => 'ektp.nama'])->contain(['Ektp']);
        $this->set(compact('user', 'jenisUser', 'daerah', 'petanis','penyuluhs','bumdes','bps'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function login()
    {
        if ($this->request->session()->read('Auth.User')) {
            $this->redirect($this->Auth->redirectUrl());
        }

        $this->set('title', 'Login');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();

            if ($user) {
                $this->Auth->setUser($user);
                $userData = $this->Users->get( $this->request->session()->read('Auth.User.id'), [
                    'contain' => []
                ]);
                $userData = $this->Users->patchEntity($userData, ['last_login'=>date('Y-m-d H:i:s')]);
                $this->Users->save($userData);
                //echo $this->request->session()->read('Auth'); exit;
                if ( $this->request->session()->read('Auth.User.jenis_user_id') == 3) {
                    $poktan = $this->Users->find('all')
                        ->contain([
                            'Bumdes.Daerah',
                        ])

                        ->where([
                            'Users.id' => $this->request->session()->read('Auth.User.id')
                        ])->first();

                    $data = $poktan->toArray();

                    $this->request->session()->write('Bumdes', $data['bumde']);
                }

                return $this->redirect($this->Auth->redirectUrl());
            }

            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
}
