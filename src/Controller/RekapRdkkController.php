<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * RekapRdkk Controller
 *
 * @property \App\Model\Table\RekapRdkkTable $RekapRdkk
 */
class RekapRdkkController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Daerah']
        ];
        $rekapRdkk = $this->paginate($this->RekapRdkk);

        $this->set(compact('rekapRdkk'));
        $this->set('_serialize', ['rekapRdkk']);
    }

    /**
     * View method
     *
     * @param string|null $id Rekap Rdkk id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $rekapRdkk = $this->RekapRdkk->get($id, [
            'contain' => ['Daerah', 'Aprroves', 'RekapLog']
        ]);

        $this->set('rekapRdkk', $rekapRdkk);
        $this->set('_serialize', ['rekapRdkk']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $rekapRdkk = $this->RekapRdkk->newEntity();
        if ($this->request->is('post')) {
            $rekapRdkk = $this->RekapRdkk->patchEntity($rekapRdkk, $this->request->data);
            if ($this->RekapRdkk->save($rekapRdkk)) {
                $this->Flash->success(__('The rekap rdkk has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The rekap rdkk could not be saved. Please, try again.'));
            }
        }
        $daerah = $this->RekapRdkk->Daerah->find('list', ['limit' => 200]);
        $this->set(compact('rekapRdkk', 'daerah'));
        $this->set('_serialize', ['rekapRdkk']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Rekap Rdkk id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $rekapRdkk = $this->RekapRdkk->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rekapRdkk = $this->RekapRdkk->patchEntity($rekapRdkk, $this->request->data);
            if ($this->RekapRdkk->save($rekapRdkk)) {
                $this->Flash->success(__('The rekap rdkk has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The rekap rdkk could not be saved. Please, try again.'));
            }
        }
        $daerah = $this->RekapRdkk->Daerah->find('list', ['limit' => 200]);
        $this->set(compact('rekapRdkk', 'daerah'));
        $this->set('_serialize', ['rekapRdkk']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Rekap Rdkk id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $rekapRdkk = $this->RekapRdkk->get($id);
        if ($this->RekapRdkk->delete($rekapRdkk)) {
            $this->Flash->success(__('The rekap rdkk has been deleted.'));
        } else {
            $this->Flash->error(__('The rekap rdkk could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
