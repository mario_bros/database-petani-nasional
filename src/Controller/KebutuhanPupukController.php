<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * KebutuhanPupuk Controller
 *
 * @property \App\Model\Table\KebutuhanPupukTable $KebutuhanPupuk
 */
class KebutuhanPupukController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $kebutuhanPupuk = $this->paginate($this->KebutuhanPupuk);

        $this->set(compact('kebutuhanPupuk'));
        $this->set('_serialize', ['kebutuhanPupuk']);
    }

    /**
     * View method
     *
     * @param string|null $id Kebutuhan Pupuk id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $kebutuhanPupuk = $this->KebutuhanPupuk->get($id, [
            'contain' => []
        ]);

        $this->set('kebutuhanPupuk', $kebutuhanPupuk);
        $this->set('_serialize', ['kebutuhanPupuk']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $kebutuhanPupuk = $this->KebutuhanPupuk->newEntity();
        if ($this->request->is('post')) {
            $kebutuhanPupuk = $this->KebutuhanPupuk->patchEntity($kebutuhanPupuk, $this->request->data);
            if ($this->KebutuhanPupuk->save($kebutuhanPupuk)) {
                $this->Flash->success(__('The kebutuhan pupuk has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The kebutuhan pupuk could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('kebutuhanPupuk'));
        $this->set('_serialize', ['kebutuhanPupuk']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Kebutuhan Pupuk id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $kebutuhanPupuk = $this->KebutuhanPupuk->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $kebutuhanPupuk = $this->KebutuhanPupuk->patchEntity($kebutuhanPupuk, $this->request->data);
            if ($this->KebutuhanPupuk->save($kebutuhanPupuk)) {
                $this->Flash->success(__('The kebutuhan pupuk has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The kebutuhan pupuk could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('kebutuhanPupuk'));
        $this->set('_serialize', ['kebutuhanPupuk']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Kebutuhan Pupuk id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $kebutuhanPupuk = $this->KebutuhanPupuk->get($id);
        if ($this->KebutuhanPupuk->delete($kebutuhanPupuk)) {
            $this->Flash->success(__('The kebutuhan pupuk has been deleted.'));
        } else {
            $this->Flash->error(__('The kebutuhan pupuk could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
