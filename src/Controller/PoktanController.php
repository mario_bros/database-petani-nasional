<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\UsersTable as Users;

/**
 * Poktan Controller
 *
 * @property \App\Model\Table\PoktanTable $Poktan
 */
class PoktanController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

        if($this->request->session()->read('Auth.User.bumdes_id') && $this->request->session()->read('Auth.User.jenis_user_id'==2)) {
            $this->paginate = [
                'contain' => ['Daerah', 'Gapoktan','Log'],
                'conditions' => ['Poktan.bumdes_id' => $this->request->session()->read('Auth.User.bumdes_id')]
            ];
        }elseif($this->request->session()->read('Auth.User.bps_id')){
            $bpsId = $this->request->session()->read('Auth.User.bps_id');
            $listPoktan =  $this->Poktan->Gapoktan->Bumdes->Bps->find('all')->select('Poktan.id')->leftJoinWith('Bumdes.Gapoktan.Poktan')
                                                                            ->where(['Bps.id'=>$bpsId]);

            $this->paginate = [
                'contain' => ['Daerah', 'Gapoktan','Log'],
                'conditions'=>['Poktan.id IN'=>$listPoktan]
            ];
           // debug($this->paginate($this->Poktan)); exit;
        }else{

            $this->paginate = [
                'contain' => ['Daerah', 'Gapoktan','Log'],
                //'conditions'=>['penyuluh_id'=>$this->request->session()->read('Auth.User.penyuluh_id')]
            ];
        }
        $poktan = $this->paginate($this->Poktan);
      
        $this->set(compact('poktan'));
        $this->set('_serialize', ['poktan']);
    }

    /**
     * View method
     *
     * @param string|null $id Poktan id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->loadModel('JenisUser');
        $this->loadModel('Log');
        $this->loadModel('Notifications');

        $role = $this->JenisUser->findById($this->request->session()->read('Auth.User.jenis_user_id'))->first();

        $log = $this->Log->newEntity();
        $notifications = $this->Notifications->newEntity();
        
        $penyuluh = $this->Poktan->get($id)->penyuluh;
        $poktan = $this->Poktan->get($id, [
            //'contain' => ['Daerah', 'Gapoktan', 'Log', 'Petani.Ektp']
            //'contain' => ['Daerah', 'Gapoktan', 'Petani.Ektp', 'Petani','Penyuluh','Penyuluh.Ektp','KetuaPoktan.Ektp','KetuaPoktan.Users']
            'contain' => ['Daerah', 'Gapoktan', 'Petani.Ektp', 'KetuaPoktan.Ektp', 'KetuaPoktan.Users', 'Penyuluh.Ektp', 'Penyuluh.Users','Bumdes.Users']
            //'contain' => ['Petani.Ektp']
        ]);


         //debug($poktan); exit;

        $notificationList = [];
        
        $basePath = \Cake\Routing\Router::url('/', true);
        $basePathLength = strlen($basePath) - 1;
        $url = substr($this->referer(), $basePathLength);
        $notificationList = [];
        if(!empty($poktan->bumde->user['id']) && !empty($poktan->penyuluh->user['id'])) {
            $notificationList = $this->Poktan->Log->find('All')->where([
                    //'user_id IN' => [$poktan->bumde->user['id'], $poktan->penyuluh->user['id']],
                    'poktan_id' => $id
                ]

            );
        }
        //debug(array_key_exists('Disapproved',$this->request->data)); exit;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $penyuluhUserId = $poktan->penyuluh->user->id;

            if ( array_key_exists('Approved', $this->request->data) ) {

                $poktan = $this->Poktan->patchEntity($poktan, ['aprroved' => 1, 'approval_request' => 1]);
                $notificationsData = $this->Notifications->patchEntity($notifications, [
                    'title' => "Poktan Disetujui",
                    'url' => $url,
                    'date' => date('Y-m-d H:i:s'),
                    'flag' => 0,
                    'user_id' =>$poktan->bumde->user['id'] ,
                    'send_to_mail' => 0
                ]);

                $message = "Poktan sudah disetujui";
                $logAction = "Approved";

            } elseif ( array_key_exists('Disapproved',$this->request->data) ) {

                $poktan = $this->Poktan->patchEntity($poktan, ['aprroved' => 0, 'approval_request' => 0]);

                $notificationsData = $this->Notifications->patchEntity($notifications, [
                    'title' => "Poktan Perlu Direview Kembali",
                    'url' => $url,
                    'date' => date('Y-m-d H:i:s'),
                    'flag' => 0,
                    'user_id' => $poktan->bumde->user['id'],
                    'send_to_mail' => 0,
                    'notes'=>$this->request->data['alasan']
                ]);

                //var_dump($poktan->bumde); exit;


                $message = "Poktan belum disetujui";
                $logAction = "Disapproved";

            } elseif ( array_key_exists('ApprovalRequest',$this->request->data) ) {

                $poktan = $this->Poktan->patchEntity($poktan, ['approval_request' => 1]);

                $notificationsData = $this->Notifications->patchEntity($notifications, [
                    'title' => "Pengajuan Persetujuan Poktan",
                    'url' => $url,
                    'date' => date('Y-m-d H:i:s'),
                    'flag' => 0,
                    'user_id' => $penyuluhUserId,
                    'send_to_mail' => 0
                ]);
                //debug($notifications); exit;

                $message = "Persetujuan Poktan diajukan";
                $logAction = "ApprovalRequest";
            }

            $log = $this->Log->patchEntity($log, [
                'user_id' => $this->request->session()->read('Auth.User.id'),
                'action' => $logAction,
                'date' => date('Y-m-d H:i:s'),
                'poktan_id' => $id
            ]);

            if ($this->Poktan->save($poktan)) {

                $this->Log->save($log);

                if ( $notifications->dirty('title') )
                {
                    $this->Notifications->save($notificationsData);
                }

                $this->Flash->success(__($message));
                return $this->redirect($this->referer());
                //return $this->redirect(['action' => 'view']);
            }
        }

        $this->set(compact('poktan', 'role', 'notificationList'));
        $this->set('_serialize', ['poktan']);
    }



    public function processReview($id,$status,$idRekap){
        $poktan = $this->Poktan->get($id,[
            'contain'=>['Gapoktan.Bumdes.Bps.Users','Gapoktan.Bumdes.Users','RekapRdkk'=>function($q) use($idRekap) {
                $dataRekap = $q->where(['RekapRdkk.id'=>$idRekap]);
                return $dataRekap;
            }]
        ]);

        //debug($poktan); exit;

        $masaTanam = $poktan->rekap_rdkk[0]->masa_tanam;
        if($status==3) {
            $action = 'rejected';
            $message = "Data RDKK Poktan ".$poktan->nama." masa tanam ".$masaTanam." ditolak ";
            $userId = $poktan->gapoktan->bumde->user['id'];
        }
        elseif($status==1) {
            $action = "Rdkk Approval request, Data RDKK Poktan ".$poktan->nama." masa tanam ".$masaTanam;
            $message = "Data RDKK Poktan ".$poktan->nama." masa tanam ".$masaTanam." Diajukan ";
            $userId = $poktan->gapoktan->bumde->bp->user['id'];
        }
        elseif($status == 2) {
            $action = "Approved Data RDKK Poktan ".$poktan->nama." masa tanam ".$masaTanam;
            $message = "Data RDKK Poktan ".$poktan->nama." masa tanam ".$masaTanam." Diajukan ";
            $userId = $poktan->gapoktan->bumde->user['id'];
        }


        $dataRekapRdkk = [
            'id'=>$idRekap,
            'status'=>$status,
            'rekap_log'=>[0=>[
                'date' => date('Y-m-d H:i:s'),
                'user_id' => $this->request->session()->read('Auth.User.id'),
                'action' => $action,
                'comment' => (!empty($this->request->data) ? $this->request->data['alasan'] : "")
            ]
            ]
        ];

        $dataNotif = [
            'user_id'=>$userId,
            'date'=>date('Y-m-d H:i:s'),
            'title'=>$action,
            'url'=>'/rdkk/rekappoktan/'.$id.'/'.$masaTanam,
            'notes'=>(!empty($this->request->data)?$this->request->data['alasan'] : ""),
            'flag'=>0,

        ];

        $notif  = $this->Poktan->Bumdes->Users->Notifications->newEntity();
        $rekapLog  = $this->Poktan->Petani->Rdkk->RekapRdkk->get($idRekap);

        $notif = $this->Poktan->Bumdes->Users->Notifications->patchEntity($notif,$dataNotif);
        $rekapLog = $this->Poktan->Petani->Rdkk->RekapRdkk->patchEntity($rekapLog,$dataRekapRdkk);

        $poktanSave = $this->Poktan->Petani->Rdkk->RekapRdkk->save($rekapLog);
        $notifSave = $this->Poktan->Bumdes->Users->Notifications->save($notif);

        if($poktanSave != FALSE && $notifSave != FALSE) {
            if($status!=0)
                $this->Flash->success(__($message));
            else
                $this->Flash->error(__($message));
            return $this->redirect([
                'controller'=>'rdkk',
                'action' => 'listrkppoktan',$id
            ]);
        }
        else {
            $this->Flash->error(__('Data RDKK poktan  '.$poktan->nama.' masa tanam '.$masaTanam.' gagal save'));
            return $this->redirect([
                'controller'=>'rdkk',
                'action' => 'rekappoktan',$poktan->id,$masaTanam
            ]);
        }



    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('Users');
        $this->loadModel('Daerah');
        $this->loadModel('Gapoktan');

        $poktan = $this->Poktan->newEntity();

        if ($this->request->is('post')) {
            $poktan = $this->Poktan->patchEntity($poktan, $this->request->data);

            //debug($poktan); exit(' bye');

            if ($this->Poktan->save($poktan)) {
                $this->Flash->success(__('Data poktan sudah tersimpan.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Data poktan tidak bisa disimpan. Mohon coba lagi.'));
            }
        }
        $daerah = $this->Daerah->find('list');
        $gapoktan = $this->Gapoktan->find('list');
        $bumdes = $this->Poktan->Bumdes->find('list');
        $petani = $this->Users->Petani->find('list', ['keyField' => 'id', 'valueField' => 'ektp.nama'])->contain(['Ektp']);
        $penyuluh = $this->Poktan->Penyuluh->find('list', ['keyField' => 'id', 'valueField' => 'ektp.nama'])->contain(['Ektp']);


        $this->set(compact('poktan', 'daerah', 'gapoktan', 'petani','penyuluh','bumdes'));
        $this->set('_serialize', ['poktan']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Poktan id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $poktan = $this->Poktan->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
           // var_dump($poktan);

            $array = $this->request->data;
            $poktan = $this->Poktan->patchEntity($poktan, $this->request->data );

            if ($this->Poktan->save($poktan)) {
                $this->Flash->success(__('The poktan has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The poktan could not be saved. Please, try again.'));
            }
        }
        $this->loadModel('Users');
        $daerah = $this->Poktan->Daerah->find('list');
        $gapoktan = $this->Poktan->Gapoktan->find('list');
        $bumdes = $this->Poktan->Bumdes->find('list');
        $petani = $this->Users->Petani->find('list', ['keyField' => 'id', 'valueField' => 'ektp.nama'])->contain(['Ektp']);
        $penyuluh = $this->Poktan->Penyuluh->find('list', ['keyField' => 'id', 'valueField' => 'ektp.nama'])->contain(['Ektp']);
        $this->set(compact('poktan', 'daerah', 'gapoktan', 'penyuluh', 'petani','bumdes'));
        $this->set('_serialize', ['poktan']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Poktan id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $poktan = $this->Poktan->get($id);
        if ($this->Poktan->delete($poktan)) {
            $this->Flash->success(__('The poktan has been deleted.'));
        } else {
            $this->Flash->error(__('The poktan could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
