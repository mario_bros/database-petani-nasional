<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\DaerahController;
use Cake\Log\Log;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Cake\Collection\Collection;

//use Cake\Routing\

/**
 * Rdkk Controller
 *
 * @property \App\Model\Table\RdkkTable $Rdkk
 */
class RdkkController extends AppController
{
    private $connection;
    private $masaTanamArray = [1=>1,2=>2,3=>3];

    //add function beforeFilter
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->connection = ConnectionManager::get('default');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->helpers[] = 'Custom';
        $rdkk = $this->paginate($this->Rdkk);

        if($this->request->session()->read('Auth.User.jenis_user_id') == 3) {
            //ambil list poktan
            $this->loadModel('Penyuluh');
            $this->loadModel('Poktan');
            $listPoktan = $this->Poktan->find('all')->contain(['Daerah', 'Petani.Rdkk']);
            //debug($listPoktan->all()); exit;
            //$namaPenyuluh = $this->Penyuluh->get($this->request->session()->read('Auth.User.penyuluh_id'), ['contain'=>['Ektp']])->ektp->nama;

        } else if($this->request->session()->read('Auth.User.jenis_user_id') == 2) {
            $bpsId = $this->request->session()->read('Auth.User.bps_id');
            $dataPoktan =  $this->Rdkk->Petani->Poktan->Gapoktan->Bumdes->Bps->find('all')->select('Poktan.id')->leftJoinWith('Bumdes.Gapoktan.Poktan')
                ->where(['Bps.id'=>$bpsId]);
            $listPoktan =  $this->Rdkk->Petani->Poktan->find('all')->where(
                [
                    'Poktan.id IN'=>$dataPoktan
                ]
            )->contain([
                "Daerah","Petani.Rdkk"
            ]);


        } else if($this->request->session()->read('Auth.User.jenis_user_id') == 5 ) {
            //ambil list poktan
            $this->loadModel('Daerah');
            $listDaerah = $this->Daerah->find('All')->where([
                            '`Daerah`.`id`'=> $this->request->session()->read('Auth.User.kepala_daerah_id')
                        ])
                        ->contain([
                            'Gapoktan.Poktan.Petani', 'JenisDaerah'
                        ])
                        ->toArray();
            //debug($listDaerah); exit;
            //$dataPenyuluh = $this->Daerah->where([ 'active' => $this->request->session()->read('Auth.User.penyuluh_id') ]);
        }

        $this->set(compact(['rdkk', 'dataPoktan', 'listPoktan', 'namaPenyuluh', 'listDaerah']));
        $this->set('_serialize', ['rdkk']);
    }

    public function listrkppoktan($poktanID=NULL)
    {
        $this->request->session()->delete('Config.url_referer');

        $this->loadModel('Poktan');
        /*$dataPoktan = $this->Poktan->get($poktanID,[
            'contain'=>['RekapRdkk']
        ]);*/
        if ($this->request->is(['patch', 'post', 'put'])) {
          $this->paginate = [
              'conditions'=>[
                'Petani.bumdes_id'=>$this->request->session()->read('Bumdes.id'),
                'Petani.no_ektp LIKE'=>"%".$this->request->data['NIK']."%",
                'Ektp.nama LIKE'=>"%".$this->request->data['nama']."%"
              ],
                'contain' => ['Ektp', 'Rdkk.Lahan'],
          ];
        }else{
          $this->paginate = [
              'conditions' => [
                  'Petani.bumdes_id' => $this->request->session()->read('Bumdes.id')
              ],
              'contain' => ['Ektp', 'Rdkk.Lahan'],
              //'leftJoinWith' => ['Petani.Rdkk']
          ];
        }


        //$rdkk = $this->Rdkk->find()->innerJoinWith('Petani.Poktan')->innerJoinWith('Petani.Ektp')->leftJoinWith('Petani');
        $this->loadModel('Petani');
        $rdkk = $this->paginate($this->Petani);

        //debug($rdkk); exit;
        $masaTanamArr = $this->masaTanamArray;

        $this->set(compact(['rdkk', 'dataPoktan', 'masaTanamArr']));
        $this->set('_serialize', ['rdkk']);
    }

    /**
     * View method
     *
     * @param string|null $id Rdkk id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        //declare variabel tanpa query builder dengan menggunakan contain table
        $dataHeader = $this->Rdkk->get($id, ['contain' => ['Petani.Ektp', 'Petani.Bumdes', 'Lahan.Komoditas.Subsektor']]);

        $dataItem = $this->Rdkk->TrRdkk->Items->find()
                                ->where(['active = ' => 1])
                                ->contain(['Category'])
                                ->order(['category_id' => 'ASC']);

        $concatItemsTrrdkk = '';
        $concatPricesTrrdkk = '';
        foreach($dataItem as $item) {
            $aliasItemName = str_replace(["-"], "_", $item['name']);

            $concatItemsTrrdkk .= ",SUM(IF(Items_name = '$item[name]' AND Items_active=1, TrRdkk_jumlah, NULL)) as $aliasItemName \n";
            $concatPricesTrrdkk .= ",SUM(IF(Items_name = '$item[name]', Items_harga, NULL)) as Harga_$aliasItemName \n";
        }

        //declare variabel menggunakan row query
        $qry = " SELECT \n Petani_id, \n Lahan_alamat \n"
                        . $concatItemsTrrdkk
                        . $concatPricesTrrdkk
                        . " , SUM(`Total_harga`) as jumlah_harga, lahan_id"
                        . " FROM (
                                SELECT
                                  `TrRdkk`.`id` as `TrRdkk_id`,
                                  `Rdkk`.`petani_id` AS `Petani_id`,
                                  `TrRdkk`.`jumlah` AS `TrRdkk_jumlah`,
                                  `Lahan`.`luas` AS `Lahan_alamat`,
                                  `Items`.`name` AS `Items_name`,
                                  `Items`.`active` AS `Items_active`,
                                  `Items`.`harga` AS `Items_harga`,
                                  (`Items`.`harga` * `TrRdkk`.`jumlah`) as `Total_harga`,
                                  `TrRdkk`.`jumlah` as `Total_jumlah`,
                                  Rdkk.lahan_id,
                                  Rdkk.id

                                FROM `rdkk` `Rdkk`, `items` `Items`, `tr_rdkk` `TrRdkk`, `items_komoditas` `ItemsKomoditas`, lahan `Lahan`
                                WHERE TrRdkk.rdkk_id = Rdkk.id
                                AND Items.id = TrRdkk.item_id
                                AND ItemsKomoditas.item_id = TrRdkk.item_id
                                AND ItemsKomoditas.komoditas_id = Lahan.komoditas_id
                                AND `Rdkk`.`id` in ($id)
                                GROUP BY `TrRdkk_id`
                        ) AS subq
                        GROUP BY id ";
        $detailRdkk = $this->connection->execute($qry)->fetchAll('assoc');

        $collectionDetails = [];
        foreach ($detailRdkk as $key => $detail) {
            $collection = new Collection($detail);
            $detail = $collection->reject(function ($value, $key) {
                return $value === null;
            });
            $collectionDetails[$key] = $detail->toArray();
        }

        $dataItem = $dataCategory = [];
        foreach ( $collectionDetails as $row ) {
            $dataItem = array_slice($row, 2);
            array_pop($dataItem);
            array_pop($dataItem);

            foreach ($dataItem as $key => $val) {

                if( strpos($key,'Harga_') === false ) {
                    $key = str_replace(["_"], "-", $key);
                    $itemObj = $this->Rdkk->TrRdkk->Items->findByName($key)->contain(['Category'])->first();

                    $dataCategory[$itemObj->category->name][] = $itemObj->id;
                }
            }
        }

        $this->set(compact('dataHeader', 'dataItem', 'dataCategory', 'collectionDetails'));
        $this->set('_serialize', ['rdkk']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ( ( $this->request->session()->check('Config.url_referer') !== true) && $this->request->here != $this->referer() )
            $this->request->session()->write('Config.url_referer', $this->referer());

        $this->loadModel('Lahan');
        $alamat_lahan = $this->Lahan->get($this->request->query['lahan_id'])->alamat;
        $komoditas_petani_id = $this->Lahan->get($this->request->query['lahan_id'])->komoditas_petani_id;
        $masa_tanam = 1;
        $lahan_id = $this->request->query['lahan_id'];



        $dataPetani = $this->Rdkk->Petani->get($this->request->params['pass'][0], ['contain' => ['KomoditasPetani'=>
        function($q) use($komoditas_petani_id){
            return $q->where(['KomoditasPetani.id'=>$komoditas_petani_id]);
        }, 'Ektp','KomoditasPetani.Lahan','KomoditasPetani.Komoditas.Subsektor']]);

        $qryCategory = $this->Rdkk->Lahan->find()
                    ->contain(['Komoditas.Items.Category'])
                    ->where(['Lahan.id' => $this->request->query['lahan_id'] ]);

        $dataItem = $dataCategory = [];
        $qryCategory = $qryCategory->toArray();

        if ( !empty($qryCategory) ) {
            foreach ( $qryCategory[0]->komoditas->Items as $key => $Item ) {
                $dataCategory[$Item->category->name][$key] = $Item->id;
                $dataItem[$key]['name'] = $Item->name;
                $dataItem[$key]['id'] = $Item->id;
            }
        }

        $rdkk = $this->Rdkk->newEntity();
        if ($this->request->is('post')) {

            $rdkk = $this->Rdkk->patchEntity($rdkk, $this->request->data);

            if ($this->Rdkk->save($rdkk)) {
                $this->Flash->success(__('The rdkk has been saved.'));

                $redirectPage = $this->request->session()->read('Config.url_referer');
                return $this->redirect($redirectPage);
            } else {

                $this->Flash->error(__('The rdkk could not be saved. Please, try again.'));
            }
        }

        $this->set(compact(['rdkk', 'dataPetani', 'dataCategory', 'alamat_lahan', 'masa_tanam', 'lahan_id', 'dataItem']));
        $this->set('_serialize', ['rdkk']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Rdkk id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($petaniID = null, $id = null, $masa_tanam = null)
    {
        if ( ( $this->request->session()->check('Config.url_referer') !== true) && $this->request->here != $this->referer() )
          $this->request->session()->write('Config.url_referer', $this->referer());

          $rdkk = $this->Rdkk->get($id, [
              'contain' => ['Petani.Ektp','TrRdkk','Lahan.KomoditasPetani.Komoditas']
          ]);

        $komoditas_petani_id = $rdkk->lahan->komoditas_petani['id'];

        $dataPetani = $this->Rdkk->Petani->get($petaniID, ['contain' => ['KomoditasPetani'=>
        function($q) use($komoditas_petani_id){
            return $q->where(['KomoditasPetani.id'=>$komoditas_petani_id]);
        }, 'Ektp','KomoditasPetani.Lahan','KomoditasPetani.Komoditas.Subsektor']]);
        $alamat_lahan = $dataPetani->Ektp['alamat'];
        $masa_tanam = $masa_tanam;
        $lahan_id = $rdkk['lahan_id'];

        $qryCategory = $this->Rdkk->Lahan->find()
                    ->contain(['Komoditas.Items.Category'])
                    ->where(['Lahan.id' => $lahan_id ]);

        $dataItem = $dataCategory = [];
        $qryCategory = $qryCategory->toArray();

        if ( !empty($qryCategory) ) {
            foreach ( $qryCategory[0]->komoditas->Items as $key => $Item ) {
                $dataCategory[$Item->category->name][$key] = $Item->id;
                $dataItem[$key]['name'] = $Item->name;
                $dataItem[$key]['id'] = $Item->id;
            }
        }

        //$rdkkEntity = $this->Rdkk->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {


            $rdkkEntity = $this->Rdkk->patchEntity($rdkk, $this->request->data);
            //debug($rdkkEntity); exit;


            if ($this->Rdkk->save($rdkkEntity)) {
                $this->Flash->success(__('The rdkk has been saved.'));

                $redirectPage = $this->request->session()->read('Config.url_referer');
                return $this->redirect($redirectPage);
            } else {

                $this->Flash->error(__('The rdkk could not be saved. Please, try again.'));
            }
        }

        $this->set(compact(['rdkk', 'dataPetani', 'dataCategory', 'alamat_lahan', 'masa_tanam', 'lahan_id', 'dataItem']));
        $this->set('_serialize', ['rdkk']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Rdkk id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $rdkk = $this->Rdkk->get($id);
        if ($this->Rdkk->delete($rdkk)) {
            $this->Flash->success(__('The rdkk has been deleted.'));
        } else {
            $this->Flash->error(__('The rdkk could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function createRekap($poktanId,$masaTanam)
    {
        //cek apakah data sudah ada

        $dataRekap =  $this->Rdkk->RekapRdkk->find('all')->where(
            [
                'poktan_id'=>$poktanId,
                'masa_tanam'=>$masaTanam
            ]
        )->count();

        if($dataRekap==0) {
            $data = [
                'created_date' => date('Y-m-d H:i:s'),
                'status' => 0,
                'poktan_id' => $poktanId,
                'masa_tanam' => $masaTanam,
                'rekap_log' =>
                    [0 => [
                        'date' => date('Y-m-d H:i:s'),
                        'poktan_id' => $poktanId,
                        'user_id' => $this->request->session()->read('Auth.User.id'),
                        'action' => "Created",
                        'comment' => (!empty($this->request->data) ? $this->request->data['alasan'] : "")
                    ]
                    ]


            ];
            $rekapRdkk = $this->Rdkk->RekapRdkk->newEntity();
            $rekapRdkk = $this->Rdkk->RekapRdkk->patchEntity($rekapRdkk,$data);
            //debug($rekapRdkk); exit;
            $this->Rdkk->RekapRdkk->save($rekapRdkk);
        }

    }

    public function rekapQueryData($rdkkIds){
        $concatItemsTrrdkk = '';
        $concatPricesTrrdkk = '';

        //declare variabel tanpa query builder dengan menggunakan contain table
        $concatRdkkIds = implode(',',$rdkkIds);

        $dataItem = $this->Rdkk->TrRdkk->Items->find()
            ->select(['name', 'category_id'])
            ->where(['active = ' => 1])
            ->order(['category_id' => 'ASC']);

        foreach($dataItem as $item) {
            $concatItemsTrrdkk .= ",SUM(IF(Items_name = '$item[name]' AND Items_active=1, TrRdkk_jumlah, NULL)) as $item[name] \n";
            $concatPricesTrrdkk .= ",SUM(IF(Items_name = '$item[name]', Items_harga, NULL)) as Harga_$item[name] \n";
        }

        //declare variabel menggunakan row query
        $data = "
            SELECT
            \n Petani_id, \n Lahan_alamat \n "
            . $concatItemsTrrdkk
            . $concatPricesTrrdkk
            . " ,
            SUM(`Total_harga`),
             masa_tanam

             "

            . "FROM (
                    SELECT
                      `Rdkk`.`petani_id` AS `Petani_id`,
                      masa_tanam,
                      `TrRdkk`.`jumlah` AS `TrRdkk_jumlah`,
                      `Lahan`.`alamat` AS `Lahan_alamat`,
                      `Items`.`name` AS `Items_name`,
                      `Items`.`active` AS `Items_active`,
                      `Prices`.`harga` AS `Items_harga`,
                      `Prices`.`harga` as `Total_harga`,
                      `TrRdkk`.`jumlah` as `Total_jumlah`
                    FROM `tr_rdkk` `TrRdkk`
                    INNER JOIN `lahan` `Lahan` ON `Lahan`.`id` = (`TrRdkk`.`lahan_id`)
                    INNER JOIN `items` `Items` ON `Items`.`id` = (`TrRdkk`.`item_id`)
                    INNER JOIN `prices` `Prices` ON `Items`.`id` = (`Prices`.`item_id`)
                    INNER JOIN `rdkk` `Rdkk` ON `Rdkk`.`id` = (`TrRdkk`.`rdkk_id`)
                    WHERE `Rdkk`.`id` in ($concatRdkkIds)
                ) AS subq
                GROUP BY masa_tanam";
        $detailRdkk = $this->connection->execute($data)->fetchAll('assoc');

        return $detailRdkk;
    }

    public function checkRekap($daerahId){
        $rekapan = $this->RekapRdkk->find('All')->where([
            'daerah_id'=>$daerahId,
            'YEAR(created_date)'=>date('Y')
        ])
            ->contain([
                'TrRekaprdkk'
            ])
            ->toArray();
        return $rekapan;
    }

    public function rekapRdkk($daerahId)
    {
        //cek dulu apa rekap sudah ada kalo ga ada buat data rekap di DB
        $this->loadModel('RekapRdkk');
        $rekapan = $this->checkRekap($daerahId);
        $daerahCont = new DaerahController();
        if(count($rekapan)){
            $this->Flash->error(__('rdkk rekap sudah ada'));
        }else{
            $data = $daerahCont->getAllChildren($daerahId);
            $this->createRekap($data,$daerahId);
        }

        foreach($rekapan[0]->tr_rekaprdkk as $item) {
            $rdkkIds[] = $item['rdkk_id'];
        }
        $detailRdkk  = $this->rekapQueryData($rdkkIds);
        $dataItem = $this->Rdkk->TrRdkk->Items->find()
            ->select(['name', 'category_id'])
            ->where(['active = ' => 1])
            ->order(['category_id' => 'ASC']);

        $this->set(compact(['rekapan','detailRdkk','dataItem']));
        $this->set('_serialize', ['rekapan']);
    }

    public function getStatusAndClass($statusNum){
        switch($statusNum){
            case 0 : $status = "belum diajukan";
                $class = "info";
                break;
            case 1 : $status = "Sedang Ditinjau";
                $class ="warning";
                break;
            case 2 : $status = "Disetujui";
                $class = "primary";
                break;
            case 3 : $status = "Ditolak";
                    $class = "danger";
                break;

        }

        $statuses = ['status'=>$status,'class'=>$class];
        return $statuses;
    }

    public function rekappoktan($idPoktan,$masaTanam)
    {
        $this->loadModel('Poktan');
        $this->loadModel('TrRdkk');

        $this->createRekap($idPoktan,$masaTanam);

        $dataPoktan = $this->Poktan->find()->where(['Poktan.id' => $idPoktan])
                                        ->contain(['Gapoktan', 'Daerah', 'Bumdes',
                                            'RekapRdkk'=>function($q) use($masaTanam){
                                                return $q->where(['masa_tanam'=>$masaTanam]);
                                            },
                                            'RekapRdkk.RekapLog'
                                        ])->first();

        $dataItems = $this->TrRdkk->Items->find()
                    ->select(['name'])
                    ->where(['active' => 1])
                    ->order(['category_id' => 'ASC']);

        $dataCategory = $this->TrRdkk->Items->Category->find()
                        ->contain(['Items'])
                        ->order(['id' => 'ASC']);

        $aliasNameCollections = [];
        $concatItemsTrrdkk = $totalItemsPerMasaTanamStmnt = '';
        foreach( $dataItems as $index => $item ) {

            $totalItemsStmntCollection = [];

            for ( $x=$masaTanam; $x <= $masaTanam;$x++ ) {
                $sqlComplyAlias = str_replace(["-"], "_", $item['name']);
                $aliasItem = $sqlComplyAlias;
                $aliasItemCollections[] = $aliasItem;

                $totalItemsStmnt = "SUM(IF(nama_items = '$item[name]' AND masa_tanam = $x, jumlah_items, 0))";
                $hargaPerItemsStmnt = "SUM(IF(nama_items = '$item[name]' AND masa_tanam = $x, jumlah_items * harga_items, 0))";
                $concatItemsTrrdkk .= ", $totalItemsStmnt as $aliasItem \n";
                $concatItemsTrrdkk .= ", $hargaPerItemsStmnt as harga_$aliasItem \n";

                $totalItemsStmntCollection[] = $totalItemsStmnt;
                $totalItemsStmntCollection[] = $hargaPerItemsStmnt;
            }

            $aliasItemCollections[] = "harga_" . $aliasItem;
            foreach ($totalItemsStmntCollection as $idx => $stmt) {
                if ( $idx == 0 )
                    $totalItemsPerMasaTanamStmnt = $stmt;
                else
                    $totalItemsPerMasaTanamStmnt .= " + " . $stmt;
            }
            //$concatItemsTrrdkk .= ", (" . $totalItemsPerMasaTanamStmnt . ") as Total$sqlComplyAlias";
        }//var_dump($aliasItem); exit;

        $rekap = "
                SELECT
                  nama_petani
                  $concatItemsTrrdkk,
                  masa_tanam,
                  no_ktp,
                  luas_lahan,
                  alamat_lahan,
                  nama_jenis_lahan,
                  SUM( harga_items) as `Total_harga`
                FROM (
                    SELECT
                          e.id AS no_ktp,
                          e.nama AS nama_petani,
                          f.name AS nama_items,
                          f.harga AS harga_items,
                          b.jumlah AS jumlah_items,
                          a.masa_tanam AS masa_tanam,
                          c.alamat AS alamat_lahan,
                          c.luas AS luas_lahan,
                          g.nama AS nama_jenis_lahan
                    FROM rdkk a
                    inner join tr_rdkk b on b.rdkk_id = a.id
                    inner join lahan c on c.id = b.lahan_id
                    inner join petani d on d.id = a.petani_id
                    inner join ektp e on e.id = d.no_ektp
                    inner join items f on f.id = b.item_id
                    left outer join jenis_lahan g on g.id = c.jenis_lahan_id
                    where d.poktan_id = $idPoktan AND
                    a.masa_tanam = ".$masaTanam."
                ) As SubQuery
                GROUP BY no_ktp";

        $hasilRekap = $this->connection->execute($rekap)->fetchAll('assoc');
        //debug( $hasilRekap ); exit;
        //cek status terakhir
        $statusNum = $dataPoktan->rekap_rdkk[0]->status;
        $statuses = $this->getStatusAndClass($statusNum);
        $status = $statuses['status'];
        $class = $statuses['class'];
        $this->set(compact(['dataPoktan', 'hasilRekap','status','class', 'dataItems', 'aliasItemCollections', 'dataCategory','masaTanam', 'headerBumdes', ]));
        $this->set('_serialize', ['hasilRekap']);
    }

    function checkLahan($data){
      $count = 0;
      foreach ($data as $key => $value) {
        if(!empty($value->lahan)){
          $count++;
          return true;
        }
      }

      return false;

    }



    /**
     * fungsi untuk mengambil masa tanam dan lahan apa saja yang masih available
     * @param $petaniId
     */
    public function getAvailableOptions($petaniId,$returnVar = 0)
    {
        $dataPetani = $this->Rdkk->Petani->get($petaniId, ['contain' => 'Ektp']);

        $namaPetani = $dataPetani->ektp->nama;

        //ambil masa tanam dan lahan yang sudah ada di record
        $dataRdkk = $this->Rdkk->find('all')
        ->where([
           'Rdkk.petani_id' => $petaniId
        ])
        ->contain([
            'Petani.KomoditasPetani', 'Petani.Ektp','Lahan'
        ]);

         //get lahan
        $petaniData = $this->Rdkk->Petani->get($petaniId,['contain'=>['KomoditasPetani.Lahan.Daerah']]);

        if(!$this->checkLahan($petaniData->komoditas_petani)){
            $dataLahan = false;
         }else{
             $dataLahan = true;
         }

        $emptyTrxOpts = true;
        if ( $dataRdkk->count() != 0 ) {

            $emptyTrxOpts = false;

            //kelompokan masa tnam berdasarkan lahan
            $lahanTanam = $masaTanam = [];
            foreach($dataRdkk as $item){
                $masaTanam[$item->lahan_id][] = $item->masa_tanam;

            }

            foreach($petaniData->komoditas_petani as $item){
                //$masaTanam[] = $item->masa_tanam;

                $lahanTanam[$item->lahan['id']] = [
                    'lahan_id'=>$item->lahan['id'],
                    'alamat'=>$item->lahan['alamat'],
                    'masa_tanam'=> $this->masaTanamArray
                ];
            }

            //reset masa tanam
            foreach($lahanTanam as &$item){
                $data = $this->masaTanamArray;
                if(!empty($masaTanam[$item['lahan_id']])){
                  $item['masa_tanam'] = (array_diff($data,$masaTanam[$item['lahan_id']]));
                }
            }



            $lahanTanam = array_values($lahanTanam);
            $lahanTanamCollection = new Collection($lahanTanam);
            $optsLahan = $lahanTanamCollection->reject( function ($option, $key) { return $option['masa_tanam'] === [];} )->combine('lahan_id', 'alamat')->toArray();
            $emptyLahanOpts = false;
            if ( empty($optsLahan) ) {
                $emptyLahanOpts = true;
            }


        }elseif ($emptyTrxOpts==true && $dataLahan == true) {
            $emptyTrxOpts = false;

            foreach($petaniData->komoditas_petani as $item){

                $lahanTanam[$item->lahan['id']] = [
                    'lahan_id'=>$item->lahan['id'],
                    'alamat'=>$item->lahan['alamat'],
                    'masa_tanam'=> $this->masaTanamArray
                ];
            }



            $lahanTanam = array_values($lahanTanam);
            $lahanTanamCollection = new Collection($lahanTanam);
            $optsLahan = $lahanTanamCollection->reject( function ($option, $key) { return $option['masa_tanam'] === [];} )->combine('lahan_id', 'alamat')->toArray();
            $emptyLahanOpts = false;
            if ( empty($optsLahan) ) {
                $emptyLahanOpts = true;
            }
        }

        if($returnVar){
          $dataLahanTanam = NULL;
          foreach ($lahanTanam as $key => $value) {
            $dataLahanTanam[$value['lahan_id']] = $value;
          }
          $allData = ['lahan'=>$dataLahanTanam];
          return $allData;
        }

        //$emptyLahanOpts = true;
        $this->set(compact(['optsLahan', 'petaniId', 'emptyLahanOpts', 'namaPetani', 'emptyTrxOpts']));
        //$this->set('_serialize', ['lahanTanam']);
    }

    public function renewMasaTanamOptions()
    {
        $dataRdkk = $this->Rdkk->find('all')
        ->where([
            'Rdkk.petani_id' => $_POST['petani_id'],
            'Rdkk.lahan_id' => $_POST['lahan_id']
        ]);
        if($dataRdkk->count() !=0){
            $lahanTanam = $masaTanam = [];
            foreach($dataRdkk as $item){
                $masaTanam[$item->masa_tanam] = $item->masa_tanam;
                $lahanTanam[$item->lahan_id] = [
                    'masa_tanam'=> $masaTanam
                ];
            }

            //reset masa tanam
            foreach($lahanTanam as &$item){
                $item['masa_tanam'] = array_diff( $this->masaTanamArray ,$item['masa_tanam']);
            }
        }else{
            $item['masa_tanam'] = $this->masaTanamArray;
        }

        $newDropDownList = $item['masa_tanam'];
        $this->set(compact(['newDropDownList']));
        //$this->set('_serialize', ['newDropDownList']);
    }

    public function rekapkecamatan($idDaerah)
    {
        $this->loadModel('Daerah');
        $headerKecamatan = $this->Daerah->get($idDaerah);

        $dataItems = $this->Rdkk->TrRdkk->Items->find()
                    ->select(['name'])
                    ->where(['active' => 1])
                    ->order(['category_id' => 'ASC']);

        $aliasNameCollections = [];
        $concatItemsTrrdkk = $totalItemsPerMasaTanamStmnt = '';
        foreach( $dataItems as $index => $item ) {

            $totalItemsStmntCollection = [];

            for ( $masaTanam=1; $masaTanam <= 4;$masaTanam++ ) {
                $sqlComplyAlias = str_replace(["-"], "_", $item['name']);
                $aliasItem = $sqlComplyAlias . "_mt$masaTanam";
                $aliasItemCollections[] = $aliasItem;

                $totalItemsStmnt = "SUM(IF(nama_items = '$item[name]' AND masa_tanam = $masaTanam, jumlah_items, 0))";
                $concatItemsTrrdkk .= ", $totalItemsStmnt as $aliasItem \n";

                $totalItemsStmntCollection[] = $totalItemsStmnt;
            }
            $aliasItemCollections[] = "Total" . $sqlComplyAlias;
            foreach ($totalItemsStmntCollection as $idx => $stmt) {
                if ( $idx == 0 )
                    $totalItemsPerMasaTanamStmnt = $stmt;
                else
                    $totalItemsPerMasaTanamStmnt .= " + " . $stmt;
            }
            $concatItemsTrrdkk .= ", (" . $totalItemsPerMasaTanamStmnt . ") as Total$sqlComplyAlias";
        }

        $concatTransaksiPetani = '';
        foreach ( $aliasItemCollections as $idx => $collection ) {
            $aliasItem = $collection . "_poktan";
            $concatTransaksiPetani .= ", SUM( $collection ) as $aliasItem \n";

            $aliasItemCollections[$idx] = $aliasItem;
        }
        //debug($aliasItemCollections); exit;

        $rekap = "
                    SELECT
                        nama_petani
                        $concatTransaksiPetani,
                        luas_lahan,
                        nama_poktan,
                        masa_tanam,
                        poktan_id
                    FROM
                    (
                        SELECT
                          nama_petani
                          $concatItemsTrrdkk,
                          masa_tanam,
                          no_ktp,
                          luas_lahan,
                          poktan_id,
                          nama_poktan
                        FROM (
                            SELECT
                                  f.id AS no_ktp,
                                  f.nama AS nama_petani,
                                  g.name AS nama_items,
                                  b.jumlah AS jumlah_items,
                                  a.masa_tanam AS masa_tanam,
                                  c.alamat AS alamat_lahan,
                                  c.luas AS luas_lahan,
                                  e.id AS poktan_id,
                                  e.nama AS nama_poktan
                            FROM rdkk a
                            inner join tr_rdkk b on b.rdkk_id = a.id
                            inner join lahan c on c.id = b.lahan_id
                            inner join petani d on d.id = a.petani_id
                            inner join poktan e on e.id = d.poktan_id
                            inner join ektp f on f.id = d.no_ektp
                            inner join items g on g.id = b.item_id
                            where e.daerah_id = $idDaerah
                        ) As SubQueryTransaksi
                        GROUP BY no_ktp ) As SubQueryPetani
                    GROUP BY poktan_id
                ";

        $hasilRekap = $this->connection->execute($rekap)->fetchAll('assoc');
        //debug( $aliasItemCollections ); exit;

        $this->set(compact(['headerKecamatan', 'hasilRekap', 'dataItems', 'aliasItemCollections']));
        $this->set('_serialize', ['hasilRekap']);
    }
}
