<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * JenisDaerah Controller
 *
 * @property \App\Model\Table\JenisDaerahTable $JenisDaerah
 */
class JenisDaerahController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $jenisDaerah = $this->paginate($this->JenisDaerah);

        $this->set(compact('jenisDaerah'));
        $this->set('_serialize', ['jenisDaerah']);
    }

    /**
     * View method
     *
     * @param string|null $id Jenis Daerah id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $jenisDaerah = $this->JenisDaerah->get($id, [
            'contain' => ['Daerah']
        ]);

        $this->set('jenisDaerah', $jenisDaerah);
        $this->set('_serialize', ['jenisDaerah']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $jenisDaerah = $this->JenisDaerah->newEntity();
        if ($this->request->is('post')) {
            $jenisDaerah = $this->JenisDaerah->patchEntity($jenisDaerah, $this->request->data);
            if ($this->JenisDaerah->save($jenisDaerah)) {
                $this->Flash->success(__('The jenis daerah has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The jenis daerah could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('jenisDaerah'));
        $this->set('_serialize', ['jenisDaerah']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Jenis Daerah id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $jenisDaerah = $this->JenisDaerah->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $jenisDaerah = $this->JenisDaerah->patchEntity($jenisDaerah, $this->request->data);
            if ($this->JenisDaerah->save($jenisDaerah)) {
                $this->Flash->success(__('The jenis daerah has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The jenis daerah could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('jenisDaerah'));
        $this->set('_serialize', ['jenisDaerah']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Jenis Daerah id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $jenisDaerah = $this->JenisDaerah->get($id);
        if ($this->JenisDaerah->delete($jenisDaerah)) {
            $this->Flash->success(__('The jenis daerah has been deleted.'));
        } else {
            $this->Flash->error(__('The jenis daerah could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
