<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use App\Controller\DaerahController;
use App\Controller\RdkkController;
use Cake\Event\Event;
//use App\Vendor\phpexcel\phpexcel\Classes\PHPExcel;


/**
 * Petani Controller
 *
 * @property \App\Model\Table\PetaniTable $Petani
 */
class PetaniController extends AppController
{
    private $connection;

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */

     public function beforeFilter(Event $event){
        parent::beforeFilter($event);
        $this->Auth->allow('mappingData');
    }

    public function index()
    {
      $error = '';
        if ($this->request->is(['patch', 'post', 'put'])) {
          $query = $this->Petani->find('all')->where([
            'Petani.bumdes_id'=>$this->request->session()->read('Bumdes.id'),
            'no_ektp LIKE'=>"%".$this->request->data['NIK']."%",
            'Ektp.nama LIKE'=>"%".$this->request->data['nama']."%"
          ])->contain(['Ektp']);
          try{
            $petani = $this->paginate($query);
          }catch (NotFoundException $e) {
            $error = 1;
            $petani = $this->Petani;
          }

        }else{
          if($this->request->session()->read('Auth.User.jenis_user_id') == 1){
            $this->paginate = [
                //'conditions'=>['Petani.bumdes_id'=>$this->request->session()->read('Bumdes.id')],
                'contain' => ['Ektp']
            ];
          }else{
            $this->paginate = [
                'conditions'=>['Petani.bumdes_id'=>$this->request->session()->read('Bumdes.id')],
                'contain' => ['Ektp']
            ];
          }


          $petani = $this->paginate($this->Petani);
        }


        $this->set(compact('petani','error'));
        //$this->set('_serialize', ['petani']);
    }

    public function getEktp()
    {
        $subdata = $this->Petani->find('all')
                                ->select(['no_ektp']);
        $data = $this->Petani->Ektp->find('all')
                                ->select(['id'])
                                ->where(['Ektp.id NOT IN' => $subdata]);
                                //SELECT id FROM ektp WHERE ektp.id NOT IN (SELECT no_ektp FROM petani) order by id

        $this->set('data', $data);
        $this->set('_serialize', ['data']);

    }

    public function getEktpDetail($idEktp)
    {
        $this->loadModel('Daerah');
        $data = $this->Petani->Ektp->find("all")
                                ->contain('Daerah')
                                ->where(['Ektp.id'=>$idEktp])->first();

        $lookupKecamatan = $this->Daerah->find()->where( ['id' => $data->daerah->id] )->first();
        $lookupKabupaten = $this->Daerah->find()->where( ['id' => $lookupKecamatan->parent_id] )->first();
        $lookupProvinsi = $this->Daerah->find()->where( ['id' => $lookupKabupaten->parent_id] )->first();

        $data->kecamatan = $lookupKecamatan->nama;
        $data->kabupaten_kota = $lookupKabupaten->nama;

        //debug($data); exit;

        $this->set('data', $data);
        $this->set('_serialize', ['data']);
    }

    /**
     * View method
     *
     * @param string|null $id Petani id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $rkp = new RdkkController();
        $petani = $this->Petani->get($id, [
            'contain' => ['Ektp.Daerah','KomoditasPetani.Lahan.Daerah','KomoditasPetani.Lahan.JenisLahan','Bumdes','KomoditasPetani.Komoditas.Subsektor','KomoditasPetani.JenisPetani','KomoditasPetani.Lahan.Rdkk','KomoditasPetani.Lahan.Assets']
        ]);

        //process review
        if ($this->request->is(['patch', 'post', 'put'])) {
          switch(key($this->request->data)){
            case 'Approved' : $status = 1;
                              $this->request->data['alasan'] = "disetujui";
                              $this->request->data['status_label'] = "Disetujui";
                              break;
            case 'Tolak'   : $status = 2;
                             $this->request->data['status_label'] = "Ditolak";
                             break;
            default        : break;
          }
          $petaniData =  $this->Petani->get($id);
          $dataPetani = [
            'status' => $status
          ];

          $petaniData = $this->Petani->patchEntity($petaniData, $dataPetani);

          if($this->Petani->save($petaniData)){
            $this->Flash->success(__('Data Petani '.  $this->request->data['status_label']));
          }else{
            $this->Flash->error(__('Data Petani sudah gagal disave'));
          }

        }

        $this->loadModel('JenisUser');
        $role = $this->JenisUser->findById($this->request->session()->read('Auth.User.jenis_user_id'))->first();
        //$availableRkp = $rkp->getAvailableOptions($id,1);

        $this->set(compact('petani','role','availableRkp'));
        $this->set('_serialize', ['petani']);
    }

    public function adminIndex(){
      $error = '';
        if ($this->request->is(['patch', 'post', 'put'])) {
          $query = $this->Petani->find('all')->where([
            'Daerah.nama LIKE'=>"%".$this->request->data['desa']."%",
            'no_ektp LIKE'=>"%".$this->request->data['NIK']."%",
            'Ektp.nama LIKE'=>"%".$this->request->data['nama']."%"
          ])->contain(['Ektp.Daerah']);
          try{
            $petani = $this->paginate($query);
          }catch (NotFoundException $e) {
            $error = 1;
            $petani = $this->Petani;
          }

        }else{
          $this->paginate = [
              //'conditions'=>['Petani.bumdes_id'=>$this->request->session()->read('Bumdes.id')],
              'contain' => ['Ektp.Daerah']
          ];
            $petani = $this->paginate($this->Petani);
        }


        $this->set(compact('petani','error'));
    }

    /**
     * mappingdaerah method
     * berfungsi untuk melakukan pemetaan semua nama parent daerah ke data KTP
     *
     *
     */

    function mappingDaerah(&$dataPetani,$dataDaerah){
        //get current data of daerah
        $data = $this->Petani->Ektp->Daerah->get($dataPetani['ektp']['daerah_id']);
        //$dataPetani['ektp']['desa_kelurahan'] = $data->nama;
        //$dataPetani['ektp']['kecamatan'] = $dataDaerah['Kecamatan']['nama'];
        //$dataPetani['ektp']['kabupaten_kota'] = $dataDaerah['Kabupaten']['nama'];
        //$dataPetani['ektp']['propinsi'] = $dataDaerah['Kelurahan']['nama'];

    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($poktanId=NULL)
    {
        $this->loadModel('Poktan');
        $this->loadModel('Subsektor');
        $this->loadModel('JenisPetani');
        $daerahCont = new DaerahController();
        $optsKomoditas = [];
        $jumlahData = 0;
        $optsSubSektor = $this->Subsektor->find('list')->all();
        $subsektors = [];

        /*if ( $userPoktan->aprroved != FALSE ) {
            throw new NotFoundException('Poktan anda sudah disetujui. Tidak bisa menambah jumlah petani lagi.');
        }*/

        $petani = $this->Petani->newEntity();
        $dataKomoditas = [];


        if ($this->request->is('post')) {
          //unset($dataKomoditas[0]);

            $this->request->data['ektp']['id'] = $this->request->data['no_ektp'];
            foreach ($this->request->data['komoditas_petani'] as $key => $value) {
              $this->request->data['komoditas_petani'][$key]['lahan']['komoditas_id'] = $this->request->data['komoditas_petani'][$key]['id_komoditas'];
            }


            $this->request->data['alasan'] = 'Data Created';

            //mapping daerah

            $daerahsData = $daerahCont->getAllParents($this->request->data['ektp']['daerah_id']);
            //filter data jika ada lahan dengan jenis = 3 maka atribut lahan semua dihapus
            if(!empty($this->request->data['komoditas_petani'])){
              foreach ($this->request->data['komoditas_petani'] as $key => $value) {
                //debug($value['id_jenis_petani']);
                if(!empty($value['id_jenis_petani']) && $value['id_jenis_petani']==3){
                  //delete semua atribut lahan
                  unset($this->request->data['komoditas_petani'][$key]['lahan']);
                }
              }

            $petani = $this->Petani->patchEntity($petani, $this->request->data,[
                          'associated' => ['KomoditasPetani', 'KomoditasPetani.Lahan.Assets','Ektp']
                      ]);
             //debug($petani); exit;
            if ($this->Petani->save($petani)) {
                $this->Flash->success(__('Data Petani Berhasil disimpan'));

                return $this->redirect(['controller'=>'petani','action' => 'index']);
            } else {
                $this->Flash->error(__('The petani could not be saved. Please, try again.'));
            }


            $jumlahData = count($this->request->data['komoditas_petani']);
            foreach ($this->request->data['komoditas_petani'] as $key=>$value) {

             $namaSubsektor = $this->Petani->komoditasPetani->Komoditas->Subsektor->get($value['subsektor_id']);
              $value['nama_subsektor'] = $namaSubsektor['nama'];
              $dataKomoditas[$value['subsektor_id']][] = $value;

            }

          }else{

            $this->Flash->error(__('Data Komoditas Petani Masih Kosong, Mohon untuk diperiksa kembali'));
          }
            $subsektors = $petani->subsektor;

         //debug($dataKomoditas); exit;
        }


        $poktans = $this->Petani->Poktan->find('list');
        $bumdes = $this->Petani->Bumdes->find('list')->where([
            'Bumdes.id'=>$this->request->session()->read('Bumdes.id')
        ]);

        $daerahs = $this->Petani->Ektp->Daerah->find('list')->where([
            'jenis_daerah_id'=>'1'
        ]);
        $jenis_petani = $this->JenisPetani->find('list');
        $jenis_lahan = $this->Petani->KomoditasPetani->Lahan->JenisLahan->find('all')->where();
        $komoditas = $this->Petani->KomoditasPetani->Komoditas->find('all');

        foreach ($komoditas as $key => $value) {
          $optsKomoditas[$value['subsektor_id']][$value['id']] = $value['nama'];
        }

        $optsJenisLahan = [];
        foreach ($jenis_lahan as $key => $value) {
          $optsJenisLahan[$value['subsektor_id']][$value['id']] = $value['nama'];
        }


        //debug($optsKomoditas); exit;

        $this->loadModel('Daerah');
        $daerah = $this->Daerah->find('list')->where(['jenis_daerah_id' => 1]);
        $headerDaerah = $daerahCont->getAllParents($this->request->session()->read('Bumdes.daerah.id'));
        $this->set(compact('petani','headerDaerah','jumlahData','subsektors','optsJenisLahan','dataKomoditas','optsKomoditas', 'poktans', 'daerahs', 'jenis_lahan','poktanId', 'daerah', 'optsSubSektor','bumdes','jenis_petani'));
        $this->set('_serialize', ['petani']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Petani id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('Poktan');
        $this->loadModel('Subsektor');
        $this->loadModel('JenisPetani');
        $daerahCont = new DaerahController();
        $optsKomoditas = [];
        $jumlahData = 0;
        $optsSubSektor = $this->Subsektor->find('list')->all();
        $petani = $this->Petani->get($id, [
            'contain' => ['Ektp', 'KomoditasPetani.Lahan.Daerah','KomoditasPetani.Komoditas.Subsektor','KomoditasPetani.Lahan.Assets']
        ]);
        $jumlahData = count($petani->komoditas_petani);
        $editDataKomoditas = $petani->komoditas_petani;
        $subsektors = [];
        $dataKomoditas = [];

        if ($this->request->is(['patch', 'post', 'put'])) {

            $this->request->data['ektp']['id'] = $this->request->data['no_ektp'];
            foreach ($this->request->data['komoditas_petani'] as $key => $value) {
              if(!empty($this->request->data['komoditas_petani'][$key]['lahan']['komoditas_id']))
                $this->request->data['komoditas_petani'][$key]['id_komoditas'] = $this->request->data['komoditas_petani'][$key]['lahan']['komoditas_id'] ;
            }
            $this->request->data['alasan'] = 'Data Edited';
            //mapping daerah
            $daerahCont = new DaerahController();

            $daerahsData = $daerahCont->getAllParents($this->request->data['ektp']['daerah_id']);
            if(isset($this->request->data['komoditas_petani'])){
                foreach ($this->request->data['komoditas_petani'] as $key => $value) {
                  //debug($value['id_jenis_petani']);
                  if($value['id_jenis_petani']==3){
                    //delete lahan jika addLahan

                    if(!empty($value['lahan']['id'])){
                      $lahan = $this->Petani->KomoditasPetani->Lahan->get($value['lahan']['id']);
                      if ($this->Petani->KomoditasPetani->Lahan->delete($lahan)) {
                        echo "sukses delete lahan";
                      }
                    }
                    //delete semua atribut lahan
                    unset($this->request->data['komoditas_petani'][$key]['lahan']);

                  }
                }
            }
            $this->mappingDaerah($this->request->data,$daerahsData);
            //debug($this->request->data);
            $petani = $this->Petani->patchEntity($petani, $this->request->data,[
                          'associated' => ['KomoditasPetani', 'KomoditasPetani.Lahan','Ektp']
                      ]);
            //debug($petani); exit;
            $editDataKomoditas = $petani->komoditas_petani;

            if ($this->Petani->save($petani)) {
                $this->Flash->success(__('The petani has been saved.'));
                return $this->redirect(['action'=>'index']);
            } else {
                $this->Flash->error(__('The petani could not be saved. Please, try again.'));
            }
        }

        $poktans = $this->Petani->Poktan->find('list', ['limit' => 200]);
        $users = $this->Petani->Users->find('list', ['limit' => 200]);
        $daerahs = $this->Petani->Ektp->Daerah->find('list')->where([
            'jenis_daerah_id'=>'1'
        ]);
        $bumdes = $this->Petani->Bumdes->find('list')->where([
            'Bumdes.id'=>$this->request->session()->read('Bumdes.id')
        ]);

        $this->loadModel('Daerah');
        $daerah = $this->Daerah->find('list')->where(['jenis_daerah_id' => 1]);

        $this->loadModel('Komoditas');

        $jenis_tanamans = $this->Petani->KomoditasPetani->Lahan->JenisTanaman->find('list');
        $jenis_petani = $this->Petani->KomoditasPetani->JenisPetani->find('list');
        $jenis_lahan = $this->Petani->KomoditasPetani->Lahan->JenisLahan->find('all')->where(['subsektor_id >'=>0]);
        $headerDaerah = $daerahCont->getAllParents($petani['ektp']->daerah_id);

        $komoditas = $this->Petani->KomoditasPetani->Komoditas->find('all')->toArray();

        foreach ($komoditas as $key => $value) {

          $optsKomoditas[$value['subsektor_id']][$value['id']] = $value['nama'];
        }

        //restructure the data

        foreach ($editDataKomoditas as $value) {
          if(empty($value->komodita['subsektor_id'])){
            $dataKomoditas[$value['subsektor_id']][] = $value;
          }else{
            $dataKomoditas[$value->komodita['subsektor_id']][] = $value;
          }
          $subsektors[] = $value->komodita['subsektor_id'];
        }

        $optsJenisLahan = [];
        foreach ($jenis_lahan as $key => $value) {
          $optsJenisLahan[$value['subsektor_id']][$value['id']] = $value['nama'];
        }

      //  debug($optsJenisLahan); exit;


        $this->set(compact('petani','headerDaerah','dataKomoditas','optsJenisLahan','jumlahData','optsKomoditas','subsektors','optsKomoditas', 'bumdes', 'users','daerahs','jenis_tanamans','jenis_lahan', 'daerah', 'optsSubSektor','jenis_petani'));
        $this->set('_serialize', ['petani']);
    }



    /**
     * Delete method
     *
     * @param string|null $id Petani id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $petani = $this->Petani->get($id);
        if ($this->Petani->delete($petani)) {
            $this->Flash->success(__('The petani has been deleted.'));
        } else {
            $this->Flash->error(__('The petani could not be deleted. Please, try again.'));
        }
        return $this->redirect($this->referer());
    }

    public function array_filter_recursive(&$array) {
         foreach ($array as $key => &$value) {
            if (empty($value)&& $key > 15) {
               unset($array[$key]);
            }
            else {
               if (is_array($value)) {
                  $value = $this->array_filter_recursive($value);
                    if (empty($value) && $key > 15) {
                     unset($array[$key]);
                  }
               }
            }
         }

         return $array;
      }

      public function removeWithoutEktp(&$array){
        //var_dump($array);
        foreach ($array['Sheet1'] as $key => $value) {
          if((empty($value['1']) || $value['1'] == 0) && $key>1){
            unset($array['Sheet1'][$key]);
          }
        }

        $this->array_filter_recursive($array);
      }

    public function MappingData(){
      $this->loadModel('TmpKomoditasLahan');
      $data = $this->TmpKomoditasLahan->find('all');
      //set header
      $dataArray = $data->toArray();
      $dataArray = array_keys($dataArray[0]->toArray());
      foreach ($dataArray as $key => $value) {
          $value = str_replace('assets-',"lahan-assets-",$value);

          $thisKey = str_replace("-",'.',$value);
          $thisKey = explode(".",$thisKey);

            if($thisKey[0]!='subsektor_id'){

              if($thisKey[1]=="assets"){
                $thisKey[2] = "jumlah";
                $dataHeader[$thisKey[0]][$thisKey[1]][][$thisKey[2]]=$value;
              }else{
                if(!empty($thisKey[1]) && $thisKey[1]=='no_ektp'){
                  $thisKey[1] = 'id_petani';
                }

                if($thisKey[0]=='ektp' || $thisKey[0]=='komoditas_petani'){
                    $dataHeader[$thisKey[1]]=$value;
                }else{
                    if($thisKey[1]=='komoditas_id'){
                      $dataHeader['id_komoditas'] = $value;
                    }
                    $dataHeader[$thisKey[0]][$thisKey[1]]=$value;
                }


              }

            }

          }
      //debug($data->toArray()); exit;
      foreach ($data->toArray() as $key => $item) {
        //debug($item); exit;
          foreach ($dataHeader as $key2 => $value) {
            if(!is_array($value)){


              if($key2=='id_petani'){
                //get data petani
                //echo $value;
                //echo $item->$value;
                $dataPetani = $this->Petani->find('all')->where(['no_ektp'=>$item->$value])->first();
                if(empty($dataPetani->id)){
                  //debug($item); exit;
                    echo $item->$value." Ga ada petaninya \n";
                    break;
                }
                $dataSave[$key][$key2] = $dataPetani->id;
              }else{
                $dataSave[$key][$key2] = (int)$item->$value;
              }



            }
            else{
              foreach ($value as $key3 => $valueArray) {
                  if($key3=="tanggal_lahir" || $key3 == "tanggal_terbit"){
                    $item->$valueArray = date("Y-m-d",strtotime($item->$valueArray));
                  }
                  $key3 = ($key3=="id_jenis_lahan")? "jenis_lahan_id" : $key3;
                  if($key3 =='alamat' && empty($item->$valueArray)){
                    $item->$valueArray = "kosong";
                  }

                  if($key3=="assets"){
                    $count=0;
                    foreach ($valueArray as $key4 => $value3) {
                        $value3['jumlah'] = str_replace('lahan-',"",$value3['jumlah']);
                      if($item->$value3['jumlah']>0){

                        //debug($value3['jumlah']); exit;
                        $index = explode("_",$value3['jumlah']);
                        $dataSave[$key][$key2][$key3][$count]["nama_asset"] = "Hewan ".$index[1];
                        $dataSave[$key][$key2][$key3][$count]["jumlah"] = (int)$item->$value3['jumlah'];
                        $count++;
                      }
                    }

                  }else{
                    $dataSave[$key][$key2][$key3] = (is_float($item->$valueArray) && $key3 !="luas")?(int)$item->$valueArray:$item->$valueArray;
                  }

              }
            }

          }
        }

      //debug($dataSave); exit;
      $lahan = $this->Petani->newEntities($dataSave);
      $lahan = $this->Petani->KomoditasPetani->patchEntities($lahan, $dataSave,[
                    'associated' => ['Lahan', 'Lahan.Assets']
                ]);
      //debug($lahan); exit;
      $flag = 0;
      foreach ($lahan as $key=>$item) {
        if(empty($item->id_petani)){
          unset($lahan[$key]);
        }
        if($item->errors() || empty($item->id_petani)){
          $flag = 1;
          foreach($item->errors() as $errors){
              if(is_array($errors)){
                  foreach($errors as $error){
                      $error_msg[]    =   $error;
                  }
              }else{
                  $error_msg[]    =   $errors;
              }
          }

          if(!empty($error_msg) || empty($item->id_petani) ){
              debug($item);
              debug($error_msg);
          }
        }
      }


    if($flag==0){
        if($this->Petani->KomoditasPetani->saveMany($lahan)){
          echo "success";
        }else{
          echo "ada error di dalam field";
        }
      }
      exit;
  }

    /*public function importFile(){

      $inputFileName = WWW_ROOT."csv".DS."sendangagung.xlsx";
      $pathInfo = pathinfo($inputFileName);
      $type = $pathInfo['extension'] == 'xlsx' ? 'Excel2007' : 'Excel5';

      $objReader = \PHPExcel_IOFactory::createReader($type);


      $filterSubset = new MyReadFilter();
      $objReader->setReadFilter($filterSubset);
      //$objReader->setLoadSheetsOnly('CSV');
      $objPHPExcel = $objReader->load($inputFileName);
      foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
           $highestColumn      = $worksheet->getHighestColumn();
           echo $highestColumn;
          $worksheets[$worksheet->getTitle()] = $worksheet->toArray();
      }


      //$this->removeWithoutEktp($worksheets);
      //$data = \PHPExcel_IOFactory::load($inputFileName);
      //$data = new PHPExcel();
      debug($worksheets); exit;
      unset($worksheets['Sheet1'][0][0]);
      foreach ($worksheets['Sheet1'][0] as $key => $value) {
          $thisKey = str_replace("]","",str_replace("[",'.',$value));
          $thisKey = explode(".",$thisKey);
          if($thisKey[0]=='ektp'){
            if($thisKey[1]=='no_ektp'){
              $thisKey[1] = 'id';
              $dataHeader['no_ektp']=$key;
            }
            $dataHeader[$thisKey[0]][$thisKey[1]]=$key;
          }
          else{

            $dataHeader[$thisKey[1]]=$key;

          }
      }

      debug($dataHeader); exit;

      unset($worksheets['Sheet1'][0]);

      foreach ($worksheets['Sheet1'] as $key => $item) {
          foreach ($dataHeader as $key2 => $value) {
            if(!is_array($value)){
              $data[$key][$key2] = $item[$value];
            }
            else{
              foreach ($value as $key3 => $valueArray) {
                  if(
                    ($key3=="tanggal_lahir" || $key3 == "tanggal_terbit" || $key3 == "tanggal_berakhir") && !empty($item[$valueArray]) ){
                    $item[$valueArray] = date("Y-m-d",strtotime($item[$valueArray]));
                  }
                  $data[$key][$key2][$key3] = $item[$valueArray];
              }
            }

          }
      }

      //save data petani
      $petani = $this->Petani->newEntities($data);
      $petani = $this->Petani->patchEntities($petani, $data,[
                              'associated' => ['Ektp']
                          ]);
      debug($petani); exit;
      //$this->Petani->saveMany($petani)
      if($this->Petani->saveMany($petani)){
        $this->saveLahanPetani($worksheets);
      }else{
        echo "gagal maning son";
      }


    }

    public function saveLahanPetani($worksheets){

      foreach ($worksheets['Sheet2'][0] as $key => $value) {
          //$value = str_replace('komoditas_petani',"0.komoditas_petani",$value);
          //$value = str_replace('lahan[',"0.komoditas_petani.lahan[",$value);
          $thisKey = str_replace("]","",str_replace("[",'.',$value));
          $thisKey = explode(".",$thisKey);

            if($thisKey[0]!='subsektor_id'){

              if($thisKey[0]=="0"){
                $dataHeader[$thisKey[0]][$thisKey[1]][$thisKey[2]]=$key;


              }else{
                if(!empty($thisKey[1]) && $thisKey[1]=='no_ektp'){
                  $thisKey[1] = 'id_petani';
                }

                if($thisKey[0]=='ektp' || $thisKey[0]=='komoditas_petani'){
                    $dataHeader[$thisKey[1]]=$key;
                }else{
                    if($thisKey[1]=='komoditas_id'){
                      $dataHeader['id_komoditas'] = $key;
                    }
                    $dataHeader[$thisKey[0]][$thisKey[1]]=$key;
                }


              }

            }

          }


          unset($worksheets['Sheet2'][0]);

          foreach ($worksheets['Sheet2'] as $key => $item) {
              foreach ($dataHeader as $key2 => $value) {
                if(!is_array($value)){


                  if($key2=='id_petani'){
                    //get data petani
                    $dataPetani = $this->Petani->find('all')->where(['no_ektp'=>$item[$value]])->first();
                    $item[$value] = $dataPetani->id;
                  }

                  $data[$key][$key2] = (int)$item[$value];

                }
                else{
                  foreach ($value as $key3 => $valueArray) {
                      if($key3=="tanggal_lahir" || $key3 == "tanggal_terbit"){
                        $item[$valueArray] = date("Y-m-d",strtotime($item[$valueArray]));
                      }
                      $key3 = ($key3=="id_jenis_lahan")? "jenis_lahan_id" : $key3;
                      $data[$key][$key2][$key3] = (is_float($item[$valueArray]) && $key3 !="luas")?(int)$item[$valueArray]:$item[$valueArray];
                  }
                }

              }
            }
            $lahan = $this->Petani->newEntities($data);
            $lahan = $this->Petani->KomoditasPetani->patchEntities($lahan, $data);
            if($this->Petani->KomoditasPetani->saveMany($lahan)){
              echo "masuk";
            }

            exit;
    }

    public function deleteLahan($id = null)
    {
        // $this->request->allowMethod(['post', 'delete']);
        $lahan = $this->Petani->KomoditasPetani->get($id);
        if ($this->Petani->KomoditasPetani->delete($lahan)) {
            $this->Flash->success(__('This Lahan has been deleted.'));
        } else {
            $this->Flash->error(__('This Lahan could not be deleted. Please, try again.'));
        }
        return $this->redirect($this->referer());
    }*/
}
