<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Bumdes Controller
 *
 * @property \App\Model\Table\BumdesTable $Bumdes
 */
class BumdesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Daerah']
        ];
        $bumdes = $this->paginate($this->Bumdes);

        $this->set(compact('bumdes'));
        $this->set('_serialize', ['bumdes']);
    }

    /**
     * View method
     *
     * @param string|null $id Bumde id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bumde = $this->Bumdes->get($id, [
            'contain' => ['Daerah']
        ]);

        $this->set('bumde', $bumde);
        $this->set('_serialize', ['bumde']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bumde = $this->Bumdes->newEntity();
        if ($this->request->is('post')) {
            $bumde = $this->Bumdes->patchEntity($bumde, $this->request->data);
            if ($this->Bumdes->save($bumde)) {
                $this->Flash->success(__('The bumde has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The bumde could not be saved. Please, try again.'));
            }
        }
        $daerah = $this->Bumdes->Daerah->find('list');
        $this->set(compact('bumde', 'daerah'));
        $this->set('_serialize', ['bumde']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Bumde id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bumde = $this->Bumdes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bumde = $this->Bumdes->patchEntity($bumde, $this->request->data);
            if ($this->Bumdes->save($bumde)) {
                $this->Flash->success(__('The bumde has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The bumde could not be saved. Please, try again.'));
            }
        }
        $daerah = $this->Bumdes->Daerah->find('list', ['limit' => 200]);
        $this->set(compact('bumde', 'daerah'));
        $this->set('_serialize', ['bumde']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Bumde id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bumde = $this->Bumdes->get($id);
        if ($this->Bumdes->delete($bumde)) {
            $this->Flash->success(__('The bumde has been deleted.'));
        } else {
            $this->Flash->error(__('The bumde could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
