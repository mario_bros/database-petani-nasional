<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TrRdkk Controller
 *
 * @property \App\Model\Table\TrRdkkTable $TrRdkk
 */
class TrRdkkController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Rdkk', 'Lahan', 'Items']
        ];
        $trRdkk = $this->paginate($this->TrRdkk);

        $this->set(compact('trRdkk'));
        $this->set('_serialize', ['trRdkk']);
    }

    /**
     * View method
     *
     * @param string|null $id Tr Rdkk id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $trRdkk = $this->TrRdkk->get($id, [
            'contain' => ['Rdkk', 'Lahan', 'Items']
        ]);

        $this->set('trRdkk', $trRdkk);
        $this->set('_serialize', ['trRdkk']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $trRdkk = $this->TrRdkk->newEntity();
        if ($this->request->is('post')) {
            $trRdkk = $this->TrRdkk->patchEntity($trRdkk, $this->request->data);
            if ($this->TrRdkk->save($trRdkk)) {
                $this->Flash->success(__('The tr rdkk has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tr rdkk could not be saved. Please, try again.'));
            }
        }
        $rdkk = $this->TrRdkk->Rdkk->find('list', ['limit' => 200]);
        $lahan = $this->TrRdkk->Lahan->find('list', ['limit' => 200]);
        $items = $this->TrRdkk->Items->find('list', ['limit' => 200]);
        $this->set(compact('trRdkk', 'rdkk', 'lahan', 'items'));
        $this->set('_serialize', ['trRdkk']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tr Rdkk id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $trRdkk = $this->TrRdkk->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $trRdkk = $this->TrRdkk->patchEntity($trRdkk, $this->request->data);
            if ($this->TrRdkk->save($trRdkk)) {
                $this->Flash->success(__('The tr rdkk has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tr rdkk could not be saved. Please, try again.'));
            }
        }
        $rdkk = $this->TrRdkk->Rdkk->find('list', ['limit' => 200]);
        $lahan = $this->TrRdkk->Lahan->find('list', ['limit' => 200]);
        $items = $this->TrRdkk->Items->find('list', ['limit' => 200]);
        $this->set(compact('trRdkk', 'rdkk', 'lahan', 'items'));
        $this->set('_serialize', ['trRdkk']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tr Rdkk id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $trRdkk = $this->TrRdkk->get($id);
        if ($this->TrRdkk->delete($trRdkk)) {
            $this->Flash->success(__('The tr rdkk has been deleted.'));
        } else {
            $this->Flash->error(__('The tr rdkk could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
