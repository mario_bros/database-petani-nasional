<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Sektor Controller
 *
 * @property \App\Model\Table\SektorTable $Sektor
 */
class SektorController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $sektor = $this->paginate($this->Sektor);

        $this->set(compact('sektor'));
        $this->set('_serialize', ['sektor']);
    }

    /**
     * View method
     *
     * @param string|null $id Sektor id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sektor = $this->Sektor->get($id, [
            'contain' => ['Subsektor']
        ]);

        $this->set('sektor', $sektor);
        $this->set('_serialize', ['sektor']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sektor = $this->Sektor->newEntity();
        if ($this->request->is('post')) {
            $sektor = $this->Sektor->patchEntity($sektor, $this->request->data);
            if ($this->Sektor->save($sektor)) {
                $this->Flash->success(__('The sektor has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sektor could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('sektor'));
        $this->set('_serialize', ['sektor']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sektor id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sektor = $this->Sektor->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sektor = $this->Sektor->patchEntity($sektor, $this->request->data);
            if ($this->Sektor->save($sektor)) {
                $this->Flash->success(__('The sektor has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sektor could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('sektor'));
        $this->set('_serialize', ['sektor']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sektor id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sektor = $this->Sektor->get($id);
        if ($this->Sektor->delete($sektor)) {
            $this->Flash->success(__('The sektor has been deleted.'));
        } else {
            $this->Flash->error(__('The sektor could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
