<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Pupuk Controller
 *
 * @property \App\Model\Table\PupukTable $Pupuk
 */
class PupukController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $pupuk = $this->paginate($this->Pupuk);

        $this->set(compact('pupuk'));
        $this->set('_serialize', ['pupuk']);
    }

    /**
     * View method
     *
     * @param string|null $id Pupuk id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pupuk = $this->Pupuk->get($id, [
            'contain' => []
        ]);

        $this->set('pupuk', $pupuk);
        $this->set('_serialize', ['pupuk']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pupuk = $this->Pupuk->newEntity();
        if ($this->request->is('post')) {
            $pupuk = $this->Pupuk->patchEntity($pupuk, $this->request->data);
            if ($this->Pupuk->save($pupuk)) {
                $this->Flash->success(__('The pupuk has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pupuk could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pupuk'));
        $this->set('_serialize', ['pupuk']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pupuk id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pupuk = $this->Pupuk->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pupuk = $this->Pupuk->patchEntity($pupuk, $this->request->data);
            if ($this->Pupuk->save($pupuk)) {
                $this->Flash->success(__('The pupuk has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pupuk could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pupuk'));
        $this->set('_serialize', ['pupuk']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pupuk id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pupuk = $this->Pupuk->get($id);
        if ($this->Pupuk->delete($pupuk)) {
            $this->Flash->success(__('The pupuk has been deleted.'));
        } else {
            $this->Flash->error(__('The pupuk could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
