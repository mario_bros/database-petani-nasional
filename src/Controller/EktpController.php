<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\DaerahController;

/**
 * Ektp Controller
 *
 * @property \App\Model\Table\EktpTable $Ektp
 */
class EktpController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Daerah']
        ];
        $ektp = $this->paginate($this->Ektp);

        $this->set(compact('ektp'));
        $this->set('_serialize', ['ektp']);
    }

    /**
     * View method
     *
     * @param string|null $id Ektp id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ektp = $this->Ektp->get($id, [
            'contain' => ['Daerah']
        ]);

        $daerahCont = new DaerahController();
        $daerah = $daerahCont->getAllParents($ektp->daerah_id);
        $this->set(compact('daerah','ektp'));
        $this->set('_serialize', ['ektp']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ektp = $this->Ektp->newEntity();
        if ($this->request->is('post')) {
            $ektp = $this->Ektp->patchEntity($ektp, $this->request->data);
            if ($this->Ektp->save($ektp)) {
                $this->Flash->success(__('The ektp has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ektp could not be saved. Please, try again.'));
            }
        }
        $daerah = $this->Ektp->Daerah->find('list', ['limit' => 200]);
        $this->set(compact('ektp', 'daerah'));
        $this->set('_serialize', ['ektp']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ektp id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ektp = $this->Ektp->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ektp = $this->Ektp->patchEntity($ektp, $this->request->data);
            if ($this->Ektp->save($ektp)) {
                $this->Flash->success(__('The ektp has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ektp could not be saved. Please, try again.'));
            }
        }
        $daerah = $this->Ektp->Daerah->find('list', ['limit' => 200]);
        $this->set(compact('ektp', 'daerah'));
        $this->set('_serialize', ['ektp']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ektp id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ektp = $this->Ektp->get($id);
        if ($this->Ektp->delete($ektp)) {
            $this->Flash->success(__('The ektp has been deleted.'));
        } else {
            $this->Flash->error(__('The ektp could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
