<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Daerah Controller
 *
 * @property \App\Model\Table\DaerahTable $Daerah
 */
class DaerahController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

        $this->paginate = [
            'contain' => ['ParentDaerah', 'JenisDaerah']
        ];

        $daerah = $this->paginate($this->Daerah);

        $this->set(compact('daerah'));
        $this->set('_serialize', ['daerah']);
    }

    /**
     * get all parrents
     */

    function getAllParents($id,&$data=[]){
        $daerah = $this->Daerah->get($id,['contain'=>[
            'JenisDaerah','ParentDaerah.JenisDaerah'
        ]]);


        if($daerah->parent_id) {
            //array_push($data, $daerah->jenis_daerah['nama']=>$daerah->parent_id);
            $data[$daerah->parent_daerah['jenis_daerah']['nama']] = $daerah->parent_daerah;
            $this->getAllParents($daerah->parent_id, $data);
        }else{
            $data[$daerah->jenis_daerah['nama']] = $daerah;
        }
        return $data;

    }

    /**
    * fungsi untuk ambil semua data daerah vs status persetujuan
    **/
    public function daerahApproval(){
      $error = '';
        if ($this->request->is(['patch', 'post', 'put'])) {
          $query = $this->Daerah->find('all')->where([
            'Daerah.jenis_daerah_id'=>1,
            'Daerah.nama LIKE '=>"%".$this->request->data['nama']."%"
          ])->contain(['ParentDaerah.ParentDaerah',
            'JenisDaerah',
            'Ektp',
            'DaerahStatus'=>function($q){
            return $q->where(['YEAR(approval_date)'=>date('Y')]);
          }]);

          try{
            $daerah = $this->paginate($query);
          }catch (NotFoundException $e) {
            $error = 1;
            $daerah = $this->Daerah;
          }

        }else{
          $this->paginate = [
              'contain' => [
                'ParentDaerah.ParentDaerah',
                'JenisDaerah',
                'Ektp',
                'DaerahStatus'=>function($q){
                return $q->where(['YEAR(approval_date)'=>date('Y')]);
              }],
              'conditions'=>['Daerah.jenis_daerah_id'=>1]
          ];

          $daerah = $this->paginate($this->Daerah);
        }



    //  debug($daerah);

      $this->set(compact('daerah'));
      $this->set('_serialize', ['daerah']);
    }

    function rekapBps($idDaerah){
      $this->loadModel('Petani');
      $this->loadModel('Lahan');
      $bumdes_id = $this->Petani->Bumdes->find()->where(['daerah_id'=>$idDaerah])->first()->id;
      $dataTotalLahan = $this->Lahan->find('all')->contain(['KomoditasPetani.Petani'])->where(['Petani.bumdes_id' => $bumdes_id ])->all();
      $totalLuasLahan = $dataTotalLahan->reduce(function ($accumulated, $item) {
          return $accumulated + $item->luas;
      }, 0);

      $dataBumdes = $this->Petani->Bumdes->find('all')->where([
        'Bumdes.id'=>$bumdes_id
      ])
      ->contain([
        'Daerah'
      ])->first();

      $dataTotalPetani = $this->Petani->find('all')->where([
        'bumdes_id'=>$bumdes_id
      ])->count();

      //get data komoditas
      $query = $this->Petani->KomoditasPetani->find();
      $komoditas = $query->select([
        'jumlah_komoditas'=>$query->func()->count('id_komoditas'),
        'komoditas'=>'Komoditas.nama'
      ])
      ->where([
        'bumdes_id'=>$bumdes_id
      ])
      ->group(['id_komoditas'])->contain(['Komoditas','Petani']);

      $this->set(compact(['totalLuasLahan','dataTotalPetani','dataBumdes','komoditas']));
      //$this->set('_serialize', ['dataPoktan']);
    }

    public function getAllChildren($daerahId,&$data=[]){
        //get all children
        $query = $this->Daerah->find('All')->where(['parent_id'=>$daerahId]);
        $dataDaerah = $query->toArray();

        if($query->count() > 0) {
            array_push($data,$dataDaerah[0]['id']);
            $this->getAllChildren($dataDaerah[0]['id'],$data);
        }

        return $data;
    }
    /**
     * View method
     *
     * @param string|null $id Daerah id.
     * @return \Cake\Network\Response|nul`l
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {


        $daerah = $this->Daerah->get($id, [
            'contain' => ['ParentDaerah', 'JenisDaerah', 'Bumdes', 'ChildDaerah', 'Ektp', 'Gapoktan', 'Lahan', 'Poktan', 'Users']
        ]);

        $descendants = $this->getAllParents($id);

        $this->set(compact('daerah','descendants'));
        $this->set('_serialize', ['daerah']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $daerah = $this->Daerah->newEntity();
        if ($this->request->is('post')) {
            $daerah = $this->Daerah->patchEntity($daerah, $this->request->data);
            if ($this->Daerah->save($daerah)) {
                $this->Flash->success(__('The daerah has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The daerah could not be saved. Please, try again.'));
            }
        }
        $parentDaerah = $this->Daerah->ParentDaerah->find('list');
        $jenisDaerah = $this->Daerah->JenisDaerah->find('list');
        $this->set(compact('daerah', 'parentDaerah', 'jenisDaerah'));
        $this->set('_serialize', ['daerah']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Daerah id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $daerah = $this->Daerah->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $daerah = $this->Daerah->patchEntity($daerah, $this->request->data);
            if ($this->Daerah->save($daerah)) {
                $this->Flash->success(__('The daerah has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The daerah could not be saved. Please, try again.'));
            }
        }
        $parentDaerah = $this->Daerah->ParentDaerah->find('list', ['limit' => 200]);
        $jenisDaerah = $this->Daerah->JenisDaerah->find('list', ['limit' => 200]);
        $this->set(compact('daerah', 'parentDaerah', 'jenisDaerah'));
        $this->set('_serialize', ['daerah']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Daerah id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $daerah = $this->Daerah->get($id);
        if ($this->Daerah->delete($daerah)) {
            $this->Flash->success(__('The daerah has been deleted.'));
        } else {
            $this->Flash->error(__('The daerah could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
