<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Komoditas Controller
 *
 * @property \App\Model\Table\KomoditasTable $Komoditas
 */
class KomoditasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Subsektor'],
        ];
        $komoditas = $this->paginate($this->Komoditas);

        $this->set(compact('komoditas'));
        $this->set('_serialize', ['komoditas']);
    }

    /**
     * View method
     *
     * @param string|null $id Komodita id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $komodita = $this->Komoditas->get($id, [
            'contain' => []
        ]);

        $this->set('komodita', $komodita);
        $this->set('_serialize', ['komodita']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $komoditas = $this->Komoditas->newEntity();
        if ($this->request->is('post')) {
            $komoditas = $this->Komoditas->patchEntity($komoditas, $this->request->data);
            if ($this->Komoditas->save($komoditas)) {
                $this->Flash->success(__('The komodita has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The komodita could not be saved. Please, try again.'));
            }
        }

        $subsektor = $this->Komoditas->Subsektor->find('list');
        $this->set(compact('komoditas', 'subsektor'));
        $this->set('_serialize', ['komodita']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Komodita id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $komoditas = $this->Komoditas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $komoditas = $this->Komoditas->patchEntity($komoditas, $this->request->data);
            if ($this->Komoditas->save($komoditas)) {
                $this->Flash->success(__('The komodita has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The komodita could not be saved. Please, try again.'));
            }
        }

        $subsektor = $this->Komoditas->Subsektor->find('list');
        //debug($subsektor->first()); exit;

        $this->set(compact('komoditas', 'subsektor'));
        $this->set('_serialize', ['komoditas']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Komodita id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $komodita = $this->Komoditas->get($id);
        if ($this->Komoditas->delete($komodita)) {
            $this->Flash->success(__('The komodita has been deleted.'));
        } else {
            $this->Flash->error(__('The komodita could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function getKomoditas()
    {
        $post = $this->request->data;
        $inputNumber = 0;

        if ( isset($post['inpNumber']) ) {
            $inputNumber = $post['inpNumber'];
        }


        $optsKomoditas = $this->Komoditas->find('list')->where([ 'subsektor_id' => $post['subsektor_id'] ])->toArray();
        $resp['options'] = $optsKomoditas;
        $resp['inputNumber'] = $inputNumber;
      
        //debug($resp); exit;

        $this->set(compact(['resp']));
        //$this->set('_serialize', ['lahanTanam']);
    }
}
