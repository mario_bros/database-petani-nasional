<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Subsektor Controller
 *
 * @property \App\Model\Table\SubsektorTable $Subsektor
 */
class SubsektorController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $subsektor = $this->paginate($this->Subsektor);

        $this->set(compact('subsektor'));
        $this->set('_serialize', ['subsektor']);
    }

    /**
     * View method
     *
     * @param string|null $id Subsektor id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $subsektor = $this->Subsektor->get($id, [
            'contain' => []
        ]);

        $this->set('subsektor', $subsektor);
        $this->set('_serialize', ['subsektor']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $subsektor = $this->Subsektor->newEntity();
        if ($this->request->is('post')) {
            $subsektor = $this->Subsektor->patchEntity($subsektor, $this->request->data);
            if ($this->Subsektor->save($subsektor)) {
                $this->Flash->success(__('The subsektor has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The subsektor could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('subsektor'));
        $this->set('_serialize', ['subsektor']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Subsektor id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $subsektor = $this->Subsektor->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $subsektor = $this->Subsektor->patchEntity($subsektor, $this->request->data);
            if ($this->Subsektor->save($subsektor)) {
                $this->Flash->success(__('The subsektor has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The subsektor could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('subsektor'));
        $this->set('_serialize', ['subsektor']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Subsektor id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $subsektor = $this->Subsektor->get($id);
        if ($this->Subsektor->delete($subsektor)) {
            $this->Flash->success(__('The subsektor has been deleted.'));
        } else {
            $this->Flash->error(__('The subsektor could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Get Jenis Lahan method
     *
     * @param string|null $id Subsektor id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

     public function getJenisLahan(){

       $post = $this->request->data;
       $inputNumber = 0;

       if ( isset($post['inpNumber']) ) {
           $inputNumber = $post['inpNumber'];
       }

       $label = "Jenis Lahan";
       if($post['subsektor_id'] == 11){
         $label = "Jenis Kapal";
       }else if ($post['subsektor_id']==10 || $post['subsektor_id']== 12) {
          $label = "Jenis Usaha";
       }

       //get jenis lahan
       $optsKomoditas = $this->Subsektor->JenisLahan->find('list')->where([ 'subsektor_id' => $post['subsektor_id'] ])->toArray();
       $resp['options'] = $optsKomoditas;
       $resp['inputNumber'] = $inputNumber;
         $resp['label'] = $label;
       //debug($resp); exit;

       $this->set(compact(['resp']));
     }

}
