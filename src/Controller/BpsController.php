<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Bps Controller
 *
 * @property \App\Model\Table\BpsTable $Bps
 */
class BpsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Daerah']
        ];
        $bps = $this->paginate($this->Bps);

        $this->set(compact('bps'));
        $this->set('_serialize', ['bps']);
    }

    /**
     * View method
     *
     * @param string|null $id Bp id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bp = $this->Bps->get($id, [
            'contain' => ['Daerah']
        ]);

        $this->set('bp', $bp);
        $this->set('_serialize', ['bp']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bp = $this->Bps->newEntity();
        if ($this->request->is('post')) {
            $bp = $this->Bps->patchEntity($bp, $this->request->data);
            if ($this->Bps->save($bp)) {
                $this->Flash->success(__('The bp has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The bp could not be saved. Please, try again.'));
            }
        }
        $daerah = $this->Bps->Daerah->find('list', ['limit' => 200]);
        $this->set(compact('bp', 'daerah'));
        $this->set('_serialize', ['bp']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Bp id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bp = $this->Bps->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bp = $this->Bps->patchEntity($bp, $this->request->data);
            if ($this->Bps->save($bp)) {
                $this->Flash->success(__('The bp has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The bp could not be saved. Please, try again.'));
            }
        }
        $daerah = $this->Bps->Daerah->find('list', ['limit' => 200]);
        $this->set(compact('bp', 'daerah'));
        $this->set('_serialize', ['bp']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Bp id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bp = $this->Bps->get($id);
        if ($this->Bps->delete($bp)) {
            $this->Flash->success(__('The bp has been deleted.'));
        } else {
            $this->Flash->error(__('The bp could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
