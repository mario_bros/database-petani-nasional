<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * JenisTanaman Controller
 *
 * @property \App\Model\Table\JenisTanamanTable $JenisTanaman
 */
class JenisTanamanController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $jenisTanaman = $this->paginate($this->JenisTanaman);

        $this->set(compact('jenisTanaman'));
        $this->set('_serialize', ['jenisTanaman']);
    }

    /**
     * View method
     *
     * @param string|null $id Jenis Tanaman id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $jenisTanaman = $this->JenisTanaman->get($id, [
            'contain' => ['Lahan']
        ]);

        $this->set('jenisTanaman', $jenisTanaman);
        $this->set('_serialize', ['jenisTanaman']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $jenisTanaman = $this->JenisTanaman->newEntity();
        if ($this->request->is('post')) {
            $jenisTanaman = $this->JenisTanaman->patchEntity($jenisTanaman, $this->request->data);
            if ($this->JenisTanaman->save($jenisTanaman)) {
                $this->Flash->success(__('The jenis tanaman has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The jenis tanaman could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('jenisTanaman'));
        $this->set('_serialize', ['jenisTanaman']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Jenis Tanaman id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $jenisTanaman = $this->JenisTanaman->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $jenisTanaman = $this->JenisTanaman->patchEntity($jenisTanaman, $this->request->data);
            if ($this->JenisTanaman->save($jenisTanaman)) {
                $this->Flash->success(__('The jenis tanaman has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The jenis tanaman could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('jenisTanaman'));
        $this->set('_serialize', ['jenisTanaman']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Jenis Tanaman id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $jenisTanaman = $this->JenisTanaman->get($id);
        if ($this->JenisTanaman->delete($jenisTanaman)) {
            $this->Flash->success(__('The jenis tanaman has been deleted.'));
        } else {
            $this->Flash->error(__('The jenis tanaman could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
