<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\DaerahController;
use App\Controller\PetaniController;

/**
 * Penyuluh Controller
 *
 * @property \App\Model\Table\PenyuluhTable $Penyuluh
 */
class PenyuluhController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Ektp']
        ];
        $penyuluh = $this->paginate($this->Penyuluh);

        $this->set(compact('penyuluh'));
        $this->set('_serialize', ['penyuluh']);
    }

    /**
     * View method
     *
     * @param string|null $id Penyuluh id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $penyuluh = $this->Penyuluh->get($id, [
            'contain' => ['Ektp']
        ]);

        $this->set('penyuluh', $penyuluh);
        $this->set('_serialize', ['penyuluh']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $penyuluh = $this->Penyuluh->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['ektp']['id'] = $this->request->data['no_ektp'];

            //mapping daerah
            $daerahCont = new DaerahController();
            $mapper = new PetaniController();
            $daerahsData = $daerahCont->getAllParents($this->request->data['ektp']['daerah_id']);
            $mapper->mappingDaerah($this->request->data,$daerahsData);


            $penyuluh = $this->Penyuluh->patchEntity($penyuluh, $this->request->data);
            //debug($penyuluh); exit(' exit ');

            if ($this->Penyuluh->save($penyuluh)) {
                $this->Flash->success(__('The penyuluh has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The penyuluh could not be saved. Please, try again.'));
            }
        }
        $user = $this->Penyuluh->Users->find('list', ['limit' => 200]);
        $daerahs = $this->Penyuluh->Ektp->Daerah->find('list', ['limit' => 200])->where([
            'jenis_daerah_id'=>'2'
        ]);
        $this->set(compact('penyuluh', 'user','daerahs'));
        $this->set('_serialize', ['penyuluh']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Penyuluh id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $penyuluh = $this->Penyuluh->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $penyuluh = $this->Penyuluh->patchEntity($penyuluh, $this->request->data);
            if ($this->Penyuluh->save($penyuluh)) {
                $this->Flash->success(__('The penyuluh has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The penyuluh could not be saved. Please, try again.'));
            }
        }
        $user = $this->Penyuluh->Users->find('list', ['limit' => 200]);
        $this->set(compact('penyuluh', 'user'));
        $this->set('_serialize', ['penyuluh']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Penyuluh id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $penyuluh = $this->Penyuluh->get($id);
        if ($this->Penyuluh->delete($penyuluh)) {
            $this->Flash->success(__('The penyuluh has been deleted.'));
        } else {
            $this->Flash->error(__('The penyuluh could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
