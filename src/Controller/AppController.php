<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Routing\RequestActionTrait;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authorize' => ['Controller'], // Added this line
            'loginRedirect' => [
                'controller' => 'Pages',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Pages',
                'action' => 'display',
                'home'
            ]
        ]);


    }

    public function isAuthorized($user)
    {
        // Admin can access every action
        $controller = $this->request->controller;
        $action = $this->request->action;
        $restrictAction = [];

        if (isset($user['jenis_user_id']) && $user['jenis_user_id'] == 1) {
            return true;
        }
        elseif (isset($user['jenis_user_id']) && $user['jenis_user_id'] == 3) {
            //debug($this->request->controller);
            switch($controller){
                case 'poktan' : //$restrictAction = [];
                                $restrictAction = ['add','edit','delete'];
                                break;
                case 'Petani' : $restrictAction = ['delete'];
                                break;

                case 'Users' : $restrictAction = ['edit','index','delete','add'];
                                break;
                case 'Lahan' : $restrictAction = ['index'];
                                break;
                case 'Ektp' : $restrictAction = ['delete'];
                                break;
                case 'Rdkk' : $restrictAction = ['index'];
                                break;
                case 'KomoditasPetani' : $restrictAction = ['view','index'];
                                         break;
                default :   $restrictAction = ['add','edit','delete','view','index'];
                            break;
            }
            //cuma bsa edit dia punya saja
            if(
                      !empty($this->request->params['pass'][0])
                      && $this->request->session()->read('Auth.User.id') ==  $this->request->params['pass'][0]
                      && $this->request->controller == 'Users'
            ){
               $key = array_search('edit',$restrictAction);
               unset($restrictAction[$key]);
               //debug($restrictAction); exit;
             }


        }
        elseif (isset($user['jenis_user_id']) && $user['jenis_user_id'] == 2) {
            switch($controller){
                case 'poktan' : $restrictAction = ['add','edit','delete'];
                                break;
                case 'petani' : $restrictAction = ['delete'];
                                break;

                default :   $restrictAction = ['add','edit','delete','view','index'];
                            break;
            }
        }else{

          switch($controller){

              case 'users' : $restrictAction = ['add','edit','delete','index'];
                              break;
              case 'Petani' : $restrictAction = ['delete'];

                              break;
              default :   $restrictAction = ['add','edit','delete','view','index'];
                          break;
          }
        }

        if(in_array($action, $restrictAction)){

            $this->Flash->error(__('Anda tidak mempunyai hak akses untuk mengakes halaman yang dimaksud'));
            return false;
        }else {
            return true;
        }

        // Default deny
        return false;
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    public function beforeFilter(Event $event)
    {
        $controller = $this->request->controller;

        if($controller=='Users')
          $this->Auth->allow('login','logout');
        else
          $this->Auth->allow('home','login',"MappingData");


        $this->response->header('Access-Control-Allow-Origin','*');
    }
}
