<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;

/**
 * Lahan Controller
 *
 * @property \App\Model\Table\LahanTable $Lahan
 */
class LahanController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Daerah', 'KomoditasPetani.Petani.Ektp','JenisLahan','Komoditas'],
            'conditions'=>['Daerah.id'=>$this->request->session()->read('Bumdes.daerah.id')]
        ];
        $lahan = $this->paginate($this->Lahan)->toArray();

        //debug($lahan[0]); exit;

        $this->set(compact('lahan'));
        $this->set('_serialize', ['lahan']);
    }

    /**
     * View method
     *
     * @param string|null $id Lahan id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $lahan = $this->Lahan->get($id, [
            'contain' => ['Daerah']
        ]);

        $this->set('lahan', $lahan);
        $this->set('_serialize', ['lahan']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $lahan = $this->Lahan->newEntity();
        if ($this->request->is('post')) {
            $lahan = $this->Lahan->patchEntity($lahan, $this->request->data);
            if ($this->Lahan->save($lahan)) {
                $this->Flash->success(__('The lahan has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The lahan could not be saved. Please, try again.'));
            }
        }
        $daerah = $this->Lahan->Daerah->find('list', ['limit' => 200]);
        $this->loadModel('JenisTanaman');
        $this->loadModel('JenisTanah');
        $jenisTanaman = $this->JenisTanaman->find('list', ['limit' => 200]);
        $jenisTanah = $this->JenisTanah->find('list', ['limit' => 200]);

        $this->loadModel('Petani');
        $petani = $this->Petani->listPetani($this->Petani->find());

        $this->set(compact('lahan', 'daerah', 'petani', 'jenisTanaman', 'jenisTanah'));
        $this->set('_serialize', ['lahan']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Lahan id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $lahan = $this->Lahan->get($id, [
            'contain' => ['Petani']
        ]);

        //debug($lahan->petani); exit;

        if ($this->request->is(['patch', 'post', 'put'])) {
            $lahan = $this->Lahan->patchEntity($lahan, $this->request->data);
            if ($this->Lahan->save($lahan)) {
                $this->Flash->success(__('The lahan has been saved.'));

                $redirectPage = ( $this->request->session()->read('Bumdes.id') ) ? ['controller' => 'petani', 'action' => 'view', $lahan->petani->id]: ['action' => 'index'];
                //debug($redirectPage); exit;

                return $this->redirect($redirectPage);
            } else {
                $this->Flash->error(__('The lahan could not be saved. Please, try again.'));
            }
        }
        $this->loadModel('JenisTanaman');
        $this->loadModel('Daerah');
        $this->loadModel('Petani');
        $petani = $this->Petani->listPetani($this->Petani->find());
        $jenisTanaman = $this->JenisTanaman->find('list', ['limit' => 200]);
        $dukuh = $this->Daerah->find('list')->where(['jenis_daerah_id' => 1]);
        $jenisLahan = $this->Lahan->JenisLahan->find('list', ['limit' => 200]);
        $this->set(compact('lahan', 'dukuh', 'jenisTanaman', 'petani','jenisLahan'));
        $this->set('_serialize', ['lahan']);
    }

    public function updateLuasLahan(){
      //get id lahan yang diinput oleh User
      $conn = ConnectionManager::get('default');

      $query = "select lahan.id,(luas*10000) as luas from lahan, komoditas_petani where lahan.komoditas_petani_id = komoditas_petani.id and id_petani in ( select petani_id from petani_log) AND luas <= 1";
      $data = $conn->execute($query);
      $dataArrayIdLahan = $data->fetchAll('assoc');
      $entity = $this->Lahan->newEntities($dataArrayIdLahan);
      $entity = $this->Lahan->patchEntities($entity, $dataArrayIdLahan);
      $update = $this->Lahan->saveMany($entity);

      debug($update); exit;
    }

    /**
     * Delete method
     *
     * @param string|null $id Lahan id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $lahan = $this->Lahan->get($id);
        if ($this->Lahan->delete($lahan)) {
            $this->Flash->success(__('The lahan has been deleted.'));
        } else {
            $this->Flash->error(__('The lahan could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
