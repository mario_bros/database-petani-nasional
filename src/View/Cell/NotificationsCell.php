<?php
/**
 * Created by PhpStorm.
 * User: Mario
 * Date: 1/27/2016
 * Time: 2:50 PM
 */
namespace App\View\Cell;

use Cake\View\Cell;

class NotificationsCell extends Cell
{

    public function display()
    {
        $this->loadModel('Notifications');
        $unread_count = $this->Notifications->find('all')
            ->where([
            'user_id' => $this->request->session()->read('Auth.User.id'),
            'flag'=>0
        ]);
        $unread_count = $unread_count->count();
        //debug($unread_count); exit;
        $this->set(compact('unread_count'));
    }
}