<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

class CustomHelper extends Helper
{

    public function arrayUnique($array, $preserveKeys = false)
    {
        // Unique Array for return
        $arrayRewrite = [];
        // Array with the md5 hashes
        $arrayHashes = [];

        foreach ($array as $key => $item) {
            // Serialize the current element and create a md5 hash
            $hash = md5(serialize($item));

            // If the md5 didn't come up yet, add the element to
            // to arrayRewrite, otherwise drop it
            if (!isset($arrayHashes[$hash])) {

                // Save the current element hash
                $arrayHashes[$hash] = $hash;

                // Add element to the unique Array
                if ($preserveKeys) {
                    $arrayRewrite[$key] = $item;
                } else {
                    $arrayRewrite[] = $item;
                    //debug($arrayRewrite); exit(' helpers');
                }
            }
        }

        return $arrayRewrite;
    }
    
    public function arrayUniqueByValue($array, $key='')
    {
        // Unique Array for return
        $arrayRewrite = [];

        foreach($array as $element) {
            $hash = $element[$key];
            $arrayRewrite[$hash] = $element;
        }
        //debug($arrayRewrite); exit(' helpers');

        return $arrayRewrite;
    }

}
