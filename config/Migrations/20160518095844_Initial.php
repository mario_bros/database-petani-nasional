<?php
use Migrations\AbstractMigration;

class Initial extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('aprroves');
        $table
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('date', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('note', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('rekap_rdkk_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'rekap_rdkk_id',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $table = $this->table('bumdes');
        $table
            ->addColumn('nama', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('alamat', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('lat', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('lon', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('daerah_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'daerah_id',
                ]
            )
            ->create();

        $table = $this->table('category');
        $table
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->create();

        $table = $this->table('daerah');
        $table
            ->addColumn('nama', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('lat', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('lon', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('note', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('parent_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('jenis_daerah_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('lft', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('rght', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addIndex(
                [
                    'jenis_daerah_id',
                ]
            )
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->create();

        $table = $this->table('ektp', ['id' => false, 'primary_key' => ['id']]);
        $table
            ->addColumn('id', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('nama', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('alamat', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('tanggal_lahir', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('rt_rw', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('desa_kelurahan', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('kecamatan', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('kabupaten_kota', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('pekerjaan', 'string', [
                'default' => null,
                'limit' => 150,
                'null' => false,
            ])
            ->addColumn('kewarganegaraan', 'string', [
                'default' => null,
                'limit' => 150,
                'null' => false,
            ])
            ->addColumn('status_perkawinan', 'string', [
                'default' => null,
                'limit' => 150,
                'null' => false,
            ])
            ->addColumn('gol_darah', 'string', [
                'default' => null,
                'limit' => 150,
                'null' => false,
            ])
            ->addColumn('tanggal_terbit', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('daerah_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('tanggal_berakhir', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'daerah_id',
                ]
            )
            ->create();

        $table = $this->table('gapoktan');
        $table
            ->addColumn('nama', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('alamat', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('daerah_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('ketua_gapoktan', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addIndex(
                [
                    'daerah_id',
                ]
            )
            ->create();

        $table = $this->table('items');
        $table
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('category_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('desc', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->addColumn('active', 'boolean', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'category_id',
                ]
            )
            ->create();

        $table = $this->table('jenis_daerah');
        $table
            ->addColumn('nama', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->create();

        $table = $this->table('jenis_tanah');
        $table
            ->addColumn('nama', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->create();

        $table = $this->table('jenis_tanaman');
        $table
            ->addColumn('nama', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->create();

        $table = $this->table('jenis_user');
        $table
            ->addColumn('jenis', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->create();

        $table = $this->table('kepala_daerah');
        $table
            ->addColumn('nomor_induk', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('nama', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('jabatan', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('alamat', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('daerah_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'daerah_id',
                ]
            )
            ->create();

        $table = $this->table('komoditas');
        $table
            ->addColumn('nama', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('keterangan', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->create();

        $table = $this->table('lahan');
        $table
            ->addColumn('alamat', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('daerah_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('luas', 'decimal', [
                'default' => null,
                'null' => false,
                'precision' => 8,
                'scale' => 2,
            ])
            ->addColumn('jenis_tanaman_id', 'integer', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('petani_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('jenis_tanah_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'daerah_id',
                ]
            )
            ->addIndex(
                [
                    'jenis_tanaman_id',
                ]
            )
            ->addIndex(
                [
                    'petani_id',
                ]
            )
            ->addIndex(
                [
                    'jenis_tanah_id',
                ]
            )
            ->create();

        $table = $this->table('log');
        $table
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('action', 'string', [
                'default' => null,
                'limit' => 150,
                'null' => false,
            ])
            ->addColumn('date', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('poktan_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'poktan_id',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $table = $this->table('notifications');
        $table
            ->addColumn('title', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('url', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('flag', 'decimal', [
                'default' => null,
                'limit' => 3,
                'null' => true,
            ])
            ->addColumn('date', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('send_to_mail', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 8,
                'scale' => 2,
            ])
            ->addColumn('notes', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $table = $this->table('penyuluh');
        $table
            ->addColumn('no_ektp', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('photo', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addIndex(
                [
                    'no_ektp',
                ]
            )
            ->create();

        $table = $this->table('petani');
        $table
            ->addColumn('no_tlp', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('photo', 'string', [
                'default' => null,
                'limit' => 150,
                'null' => false,
            ])
            ->addColumn('no_ektp', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('poktan_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'no_ektp',
                ]
            )
            ->addIndex(
                [
                    'poktan_id',
                ]
            )
            ->create();

        $table = $this->table('poktan');
        $table
            ->addColumn('nama', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('alamat', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('daerah_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('gapoktan_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('lat', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('lon', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('aprroved', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 2,
                'scale' => 2,
            ])
            ->addColumn('ketua_poktan', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('penyuluh_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('approval_request', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('bumdes_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'gapoktan_id',
                ]
            )
            ->addIndex(
                [
                    'daerah_id',
                ]
            )
            ->addIndex(
                [
                    'ketua_poktan',
                ]
            )
            ->addIndex(
                [
                    'penyuluh_id',
                ]
            )
            ->create();

        $table = $this->table('prices');
        $table
            ->addColumn('item_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('harga', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('tanggal', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'item_id',
                ]
            )
            ->create();

        $table = $this->table('rdkk');
        $table
            ->addColumn('id_poktan', 'decimal', [
                'default' => null,
                'null' => false,
                'precision' => 8,
                'scale' => 2,
            ])
            ->addColumn('tahun', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('id_subsektor', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('id_komoditas', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'id_komoditas',
                ]
            )
            ->addIndex(
                [
                    'id_subsektor',
                ]
            )
            ->create();

        $table = $this->table('rekap_log');
        $table
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('action', 'string', [
                'default' => null,
                'limit' => 150,
                'null' => false,
            ])
            ->addColumn('date', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('rekap_rdkk_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('comment', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addIndex(
                [
                    'rekap_rdkk_id',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $table = $this->table('rekap_rdkk');
        $table
            ->addColumn('created_date', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('status', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('approved_date', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('daerah_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'daerah_id',
                ]
            )
            ->create();

        $table = $this->table('subsektor');
        $table
            ->addColumn('nama', 'string', [
                'default' => null,
                'limit' => 2000,
                'null' => true,
            ])
            ->create();

        $table = $this->table('tr_rdkk');
        $table
            ->addColumn('rdkk_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('lahan_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('item_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('jumlah', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('masa_tanam', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'item_id',
                ]
            )
            ->addIndex(
                [
                    'lahan_id',
                ]
            )
            ->addIndex(
                [
                    'rdkk_id',
                ]
            )
            ->create();

        $table = $this->table('users');
        $table
            ->addColumn('username', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('password', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('created_date', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('jenis_user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('active', 'boolean', [
                'default' => 1,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('daerah_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('petani_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('penyuluh_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('kepala_daerah_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addIndex(
                [
                    'daerah_id',
                ]
            )
            ->addIndex(
                [
                    'jenis_user_id',
                ]
            )
            ->addIndex(
                [
                    'petani_id',
                ]
            )
            ->addIndex(
                [
                    'petani_id',
                ]
            )
            ->addIndex(
                [
                    'kepala_daerah_id',
                ]
            )
            ->create();

        $this->table('aprroves')
            ->addForeignKey(
                'rekap_rdkk_id',
                'rekap_rdkk',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'user_id',
                'rekap_rdkk',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('bumdes')
            ->addForeignKey(
                'daerah_id',
                'daerah',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('daerah')
            ->addForeignKey(
                'jenis_daerah_id',
                'jenis_daerah',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('ektp')
            ->addForeignKey(
                'daerah_id',
                'daerah',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('gapoktan')
            ->addForeignKey(
                'daerah_id',
                'daerah',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('items')
            ->addForeignKey(
                'category_id',
                'category',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('kepala_daerah')
            ->addForeignKey(
                'daerah_id',
                'daerah',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('lahan')
            ->addForeignKey(
                'daerah_id',
                'daerah',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'jenis_tanaman_id',
                'jenis_tanaman',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'petani_id',
                'petani',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('log')
            ->addForeignKey(
                'poktan_id',
                'poktan',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('notifications')
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('penyuluh')
            ->addForeignKey(
                'no_ektp',
                'ektp',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('petani')
            ->addForeignKey(
                'no_ektp',
                'ektp',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'poktan_id',
                'poktan',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('prices')
            ->addForeignKey(
                'item_id',
                'items',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('rdkk')
            ->addForeignKey(
                'id_komoditas',
                'komoditas',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'id_subsektor',
                'subsektor',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('rekap_log')
            ->addForeignKey(
                'rekap_rdkk_id',
                'rekap_rdkk',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('rekap_rdkk')
            ->addForeignKey(
                'daerah_id',
                'daerah',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('tr_rdkk')
            ->addForeignKey(
                'item_id',
                'items',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'lahan_id',
                'lahan',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'rdkk_id',
                'rdkk',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('users')
            ->addForeignKey(
                'daerah_id',
                'daerah',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'jenis_user_id',
                'jenis_user',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

    }

    public function down()
    {
        $this->table('aprroves')
            ->dropForeignKey(
                'rekap_rdkk_id'
            )
            ->dropForeignKey(
                'user_id'
            );

        $this->table('bumdes')
            ->dropForeignKey(
                'daerah_id'
            );

        $this->table('daerah')
            ->dropForeignKey(
                'jenis_daerah_id'
            );

        $this->table('ektp')
            ->dropForeignKey(
                'daerah_id'
            );

        $this->table('gapoktan')
            ->dropForeignKey(
                'daerah_id'
            );

        $this->table('items')
            ->dropForeignKey(
                'category_id'
            );

        $this->table('kepala_daerah')
            ->dropForeignKey(
                'daerah_id'
            );

        $this->table('lahan')
            ->dropForeignKey(
                'daerah_id'
            )
            ->dropForeignKey(
                'jenis_tanaman_id'
            )
            ->dropForeignKey(
                'petani_id'
            );

        $this->table('log')
            ->dropForeignKey(
                'poktan_id'
            )
            ->dropForeignKey(
                'user_id'
            );

        $this->table('notifications')
            ->dropForeignKey(
                'user_id'
            );

        $this->table('penyuluh')
            ->dropForeignKey(
                'no_ektp'
            );

        $this->table('petani')
            ->dropForeignKey(
                'no_ektp'
            )
            ->dropForeignKey(
                'poktan_id'
            );

        $this->table('prices')
            ->dropForeignKey(
                'item_id'
            );

        $this->table('rdkk')
            ->dropForeignKey(
                'id_komoditas'
            )
            ->dropForeignKey(
                'id_subsektor'
            );

        $this->table('rekap_log')
            ->dropForeignKey(
                'rekap_rdkk_id'
            )
            ->dropForeignKey(
                'user_id'
            );

        $this->table('rekap_rdkk')
            ->dropForeignKey(
                'daerah_id'
            );

        $this->table('tr_rdkk')
            ->dropForeignKey(
                'item_id'
            )
            ->dropForeignKey(
                'lahan_id'
            )
            ->dropForeignKey(
                'rdkk_id'
            );

        $this->table('users')
            ->dropForeignKey(
                'daerah_id'
            )
            ->dropForeignKey(
                'jenis_user_id'
            );

        $this->dropTable('aprroves');
        $this->dropTable('bumdes');
        $this->dropTable('category');
        $this->dropTable('daerah');
        $this->dropTable('ektp');
        $this->dropTable('gapoktan');
        $this->dropTable('items');
        $this->dropTable('jenis_daerah');
        $this->dropTable('jenis_tanah');
        $this->dropTable('jenis_tanaman');
        $this->dropTable('jenis_user');
        $this->dropTable('kepala_daerah');
        $this->dropTable('komoditas');
        $this->dropTable('lahan');
        $this->dropTable('log');
        $this->dropTable('notifications');
        $this->dropTable('penyuluh');
        $this->dropTable('petani');
        $this->dropTable('poktan');
        $this->dropTable('prices');
        $this->dropTable('rdkk');
        $this->dropTable('rekap_log');
        $this->dropTable('rekap_rdkk');
        $this->dropTable('subsektor');
        $this->dropTable('tr_rdkk');
        $this->dropTable('users');
    }
}
