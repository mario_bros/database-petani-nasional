<?php
use Migrations\AbstractMigration;

class AlterGapoktanAddRegionsField extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('gapoktan');
        $table->addColumn('desa_kelurahan', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('kecamatan', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('kabupaten_kota', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->update();
    }
}
