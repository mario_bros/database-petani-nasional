<?php
use Migrations\AbstractMigration;

class CreateBPS extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('bps');
        $table->addColumn('no_ektp', 'string', [
                    'limit' => 20,
                    'null' => false,
                ])
                ->addColumn('no_telp', 'string', [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ])
                ->addColumn('daerah_id', 'integer', [
                    'null' => false,
                ])
                ->create();
    }
}
