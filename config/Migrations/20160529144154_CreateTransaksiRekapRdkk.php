<?php
use Migrations\AbstractMigration;

class CreateTransaksiRekapRdkk extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('tr_rekaprdkk');
        $table->addColumn('rdkk_id', 'integer', [
			'null' => false,
		])
                ->addColumn('rekap_rdkk_id', 'integer', [
			'null' => false,
		])
                ->create();
    }
}
