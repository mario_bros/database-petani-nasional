<?php
use Migrations\AbstractMigration;

class AlterUsersBumdesID extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $tableUsers = $this->table('users');
        $tableUsers->addColumn('bumdes_id', 'integer', [
			'default' => null,
			'null' => true,
		]);
        $tableUsers->update();
    }
}
