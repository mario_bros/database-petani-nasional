<?php
use Migrations\AbstractMigration;

class CreateJenisLahan extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('jenis_lahan');
        $table->addColumn('nama', 'string', [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]);
        $table->create();
    }
}
