<?php
use Migrations\AbstractMigration;

class AddLahanToRdkk extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('ALTER TABLE `rdkk` DROP FOREIGN KEY FK_rdkk_subsektor02;');
        $this->execute('ALTER TABLE `rdkk` DROP COLUMN id_subsektor;');
        
        $this->execute('ALTER TABLE `rdkk` DROP FOREIGN KEY rdkk_ibfk_1;');
        $this->execute('ALTER TABLE `rdkk` DROP COLUMN id_komoditas;');

        $table = $this->table('rdkk');
        $table->addColumn('lahan_id', 'integer', [
                    'default' => 0,
                    'limit' => 11,
                    'null' => false,
                ]);
        
        $table->update();
    }
}
