<?php
use Migrations\AbstractMigration;

class AlterGapoktanChangeUserIdToBumdesId extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('gapoktan');
        $table->addColumn('bumdes_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true
            ])
            ->removeColumn('user_id')
            ->update();
    }
}