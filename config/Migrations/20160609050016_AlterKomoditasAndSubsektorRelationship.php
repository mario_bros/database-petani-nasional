<?php
use Migrations\AbstractMigration;

class AlterKomoditasAndSubsektorRelationship extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        /*$this->execute('DELETE FROM `subsektor`');
        $this->execute('ALTER TABLE `subsektor` AUTO_INCREMENT = 1');*/

        $subsektorTable = $this->table('subsektor');
        
        $subsektorRowsInsert = [
            [ 'nama' => "Perkebunan"],
            [ 'nama' => "Peternak"],
            [ 'nama' => "Petambak"]
        ];
        foreach ($subsektorRowsInsert as $row) {
            $subsektorTable->insert($row); 
        }
        $subsektorTable->saveData();

        $komoditasTable = $this->table('komoditas');
        $komoditasTable->addColumn('subsektor_id', 'integer', [
                            'null' => false,
                        ])
                        ->update();
    }
}
