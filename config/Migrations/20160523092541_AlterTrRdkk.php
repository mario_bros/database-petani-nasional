<?php
use Migrations\AbstractMigration;

class AlterTrRdkk extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('tr_rdkk');
        $table->changeColumn('masa_tanam', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->update();
        $table2 = $this->table('rdkk');
        $table2->changeColumn('id_komoditas', 'integer', [
                'default' => null,
                'null' => true,
            ]);
        $table2->changeColumn('id_subsektor', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table2->update();
    }
}
