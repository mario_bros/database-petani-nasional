<?php
use Migrations\AbstractMigration;

class AlterRdkkTanggal extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docacs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('rdkk');
		$table->removeColumn('tahun');
		$table->addColumn('tanggal', 'date', [
			'default' => null,
			'null' => true,
		]);
        $table->update();
    }
}
