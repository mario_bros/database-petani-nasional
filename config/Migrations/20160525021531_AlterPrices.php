<?php
use Migrations\AbstractMigration;

class AlterPrices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('prices');
		$table->changeColumn('id', 'integer', [
			'identity' => true
		]);
        $table->update();
    }
}
