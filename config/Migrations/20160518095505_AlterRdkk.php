<?php
use Migrations\AbstractMigration;

class AlterRdkk extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('rdkk');
        $table->renameColumn('id_poktan', 'petani_id')->update();
        //$table->update();
    }
}
