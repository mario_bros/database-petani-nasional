<?php
use Migrations\AbstractMigration;

class AlterCategory extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('category');
        $table->changeColumn('id', 'integer', [
            'identity' => true,
        ]);
        $table->update();
    }
}
