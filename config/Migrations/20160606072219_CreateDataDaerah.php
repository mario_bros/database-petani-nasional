<?php
use Migrations\AbstractMigration;

class CreateDataDaerah extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('daerah'); 
       
        // inserting multiple rows 

        $rows1 = [ 'nama' => 'JAWA TENGAH', 'lat' => -7.13683, 'lon' => 110.145, 'jenis_daerah_id' => 4 ];
        $rows2 = [ 'nama' => 'SRAGEN', 'lat' => -7.42894, 'lon' => 111.011, 'jenis_daerah_id' => 3 ]; 
        $rows3 = [ 'nama' => 'SIDOHARJO', 'lat' => -7.42354, 'lon' => 110.968, 'jenis_daerah_id' => 2 ]; 
        $rows4 = [ 'nama' => 'PURWOSUMAN', 'lat' => -7.44996, 'lon' => 110.952, 'jenis_daerah_id' => 1 ]; 
        // this is a handy shortcut 
        $table->insert($rows1); 
        $table->insert($rows2); 
        $table->insert($rows3); 
        $table->insert($rows4); 
        $table->saveData(); 
        
    }
}
