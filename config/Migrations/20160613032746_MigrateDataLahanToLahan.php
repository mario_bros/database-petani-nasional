<?php
use Migrations\AbstractMigration;

class MigrateDataLahanToLahan extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('SET foreign_key_checks = 0;');
        $this->execute('DELETE FROM `lahan`');
        $this->execute('ALTER TABLE `lahan` AUTO_INCREMENT = 1');
        $this->execute('SET foreign_key_checks = 1;');

        $table = $this->table('lahan'); 

        $dataLahanRows = $this->fetchAll('SELECT * FROM `data_lahan` WHERE `petani_id` NOT IN (0, 134)');
        foreach ($dataLahanRows as $insertRow) {
            //debug( $insertRow['petani_id'] ); exit;
            $rows = [ 'alamat' => $insertRow['alamat'], 'daerah_id' => $insertRow['daerah_id'], 'luas' => $insertRow['luas'], 'jenis_tanaman_id' => $insertRow['jenis_tanaman_id'], 'petani_id' => $insertRow['petani_id'], 'komoditas_id' => $insertRow['jenis_tanaman_id'],  'jenis_lahan_id' => $insertRow['jenis_lahan_id']];
            $table->insert($rows);
        }

        $table->saveData();
    }
}
