<?php
use Migrations\AbstractMigration;

class DeleteUsersAttempt1 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('DELETE FROM `tr_rdkk`');
        $this->execute('DELETE FROM `ektp`');
    }
}
