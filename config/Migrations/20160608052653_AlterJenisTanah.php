<?php
use Migrations\AbstractMigration;

class AlterJenisTanah extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('jenis_tanah');
        $table->changeColumn('nama', 'string');
        $table->update();
    }
}
