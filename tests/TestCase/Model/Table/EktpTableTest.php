<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EktpTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EktpTable Test Case
 */
class EktpTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EktpTable
     */
    public $Ektp;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ektp',
        'app.daerah',
        'app.jenis_daerah',
        'app.bumdes',
        'app.gapoktan',
        'app.poktan',
        'app.log',
        'app.users',
        'app.jenis_user',
        'app.petanis',
        'app.notifications',
        'app.penyuluh',
        'app.petani',
        'app.lahan',
        'app.jenis_tanaman'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Ektp') ? [] : ['className' => 'App\Model\Table\EktpTable'];
        $this->Ektp = TableRegistry::get('Ektp', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Ektp);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
