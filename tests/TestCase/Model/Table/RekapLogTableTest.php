<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RekapLogTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RekapLogTable Test Case
 */
class RekapLogTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RekapLogTable
     */
    public $RekapLog;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.rekap_log',
        'app.users',
        'app.jenis_user',
        'app.bumdes',
        'app.daerah',
        'app.jenis_daerah',
        'app.ektp',
        'app.petani',
        'app.poktan',
        'app.gapoktan',
        'app.penyuluh',
        'app.log',
        'app.rekap_rdkk',
        'app.aprroves',
        'app.tr_rekaprdkk',
        'app.rdkk',
        'app.komoditas',
        'app.subsektor',
        'app.tr_rdkk',
        'app.lahan',
        'app.jenis_tanaman',
        'app.jenis_lahan',
        'app.items',
        'app.category',
        'app.prices',
        'app.ketua_poktan',
        'app.bps',
        'app.notifications'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RekapLog') ? [] : ['className' => 'App\Model\Table\RekapLogTable'];
        $this->RekapLog = TableRegistry::get('RekapLog', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RekapLog);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
