<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PoktanTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PoktanTable Test Case
 */
class PoktanTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PoktanTable
     */
    public $Poktan;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.poktan',
        'app.daerah',
        'app.jenis_daerah',
        'app.bumdes',
        'app.ektp',
        'app.gapoktan',
        'app.lahan',
        'app.user',
        'app.log',
        'app.petani',
        'app.users',
        'app.jenis_user',
        'app.notifications',
        'app.penyuluh'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Poktan') ? [] : ['className' => 'App\Model\Table\PoktanTable'];
        $this->Poktan = TableRegistry::get('Poktan', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Poktan);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
