<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BumdesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BumdesTable Test Case
 */
class BumdesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BumdesTable
     */
    public $Bumdes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bumdes',
        'app.daerah',
        'app.jenis_daerah',
        'app.ektp',
        'app.gapoktan',
        'app.poktan',
        'app.log',
        'app.user',
        'app.petani',
        'app.lahan',
        'app.users',
        'app.jenis_user',
        'app.notifications',
        'app.penyuluh'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Bumdes') ? [] : ['className' => 'App\Model\Table\BumdesTable'];
        $this->Bumdes = TableRegistry::get('Bumdes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Bumdes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
