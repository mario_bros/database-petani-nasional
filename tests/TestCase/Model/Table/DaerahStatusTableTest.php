<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DaerahStatusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DaerahStatusTable Test Case
 */
class DaerahStatusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DaerahStatusTable
     */
    public $DaerahStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.daerah_status',
        'app.daerahs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DaerahStatus') ? [] : ['className' => 'App\Model\Table\DaerahStatusTable'];
        $this->DaerahStatus = TableRegistry::get('DaerahStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DaerahStatus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
