<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TrRdkkTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TrRdkkTable Test Case
 */
class TrRdkkTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TrRdkkTable
     */
    public $TrRdkk;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tr_rdkk',
        'app.rdkk',
        'app.petani',
        'app.poktan',
        'app.daerah',
        'app.jenis_daerah',
        'app.bumdes',
        'app.ektp',
        'app.penyuluh',
        'app.users',
        'app.jenis_user',
        'app.log',
        'app.notifications',
        'app.gapoktan',
        'app.lahan',
        'app.jenis_tanaman',
        'app.ketua_poktan',
        'app.items',
        'app.category',
        'app.prices'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TrRdkk') ? [] : ['className' => 'App\Model\Table\TrRdkkTable'];
        $this->TrRdkk = TableRegistry::get('TrRdkk', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TrRdkk);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
