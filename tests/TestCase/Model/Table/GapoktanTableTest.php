<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GapoktanTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GapoktanTable Test Case
 */
class GapoktanTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GapoktanTable
     */
    public $Gapoktan;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.gapoktan',
        'app.daerah',
        'app.jenis_daerah',
        'app.bumdes',
        'app.ektp',
        'app.lahan',
        'app.poktan',
        'app.log',
        'app.user',
        'app.petani',
        'app.users',
        'app.jenis_user',
        'app.notifications',
        'app.penyuluh'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Gapoktan') ? [] : ['className' => 'App\Model\Table\GapoktanTable'];
        $this->Gapoktan = TableRegistry::get('Gapoktan', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Gapoktan);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
