<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PetaniTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PetaniTable Test Case
 */
class PetaniTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PetaniTable
     */
    public $Petani;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.petani',
        'app.poktan',
        'app.daerah',
        'app.jenis_daerah',
        'app.bumdes',
        'app.ektp',
        'app.gapoktan',
        'app.lahan',
        'app.user',
        'app.log',
        'app.users',
        'app.jenis_user',
        'app.notifications',
        'app.penyuluh'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Petani') ? [] : ['className' => 'App\Model\Table\PetaniTable'];
        $this->Petani = TableRegistry::get('Petani', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Petani);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
