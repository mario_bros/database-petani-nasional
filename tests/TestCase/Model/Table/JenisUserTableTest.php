<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\JenisUserTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\JenisUserTable Test Case
 */
class JenisUserTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\JenisUserTable
     */
    public $JenisUser;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.jenis_user'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('JenisUser') ? [] : ['className' => 'App\Model\Table\JenisUserTable'];
        $this->JenisUser = TableRegistry::get('JenisUser', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->JenisUser);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
