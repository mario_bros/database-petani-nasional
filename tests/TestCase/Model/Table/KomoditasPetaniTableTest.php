<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\KomoditasPetaniTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\KomoditasPetaniTable Test Case
 */
class KomoditasPetaniTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\KomoditasPetaniTable
     */
    public $KomoditasPetani;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.komoditas_petani'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('KomoditasPetani') ? [] : ['className' => 'App\Model\Table\KomoditasPetaniTable'];
        $this->KomoditasPetani = TableRegistry::get('KomoditasPetani', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->KomoditasPetani);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
