<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubsektorTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubsektorTable Test Case
 */
class SubsektorTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SubsektorTable
     */
    public $Subsektor;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.subsektor',
        'app.sektors',
        'app.data_lahan',
        'app.komoditas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Subsektor') ? [] : ['className' => 'App\Model\Table\SubsektorTable'];
        $this->Subsektor = TableRegistry::get('Subsektor', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Subsektor);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
