<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SektorTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SektorTable Test Case
 */
class SektorTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SektorTable
     */
    public $Sektor;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sektor',
        'app.subsektor',
        'app.komoditas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Sektor') ? [] : ['className' => 'App\Model\Table\SektorTable'];
        $this->Sektor = TableRegistry::get('Sektor', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Sektor);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
