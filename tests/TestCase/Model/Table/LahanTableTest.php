<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LahanTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LahanTable Test Case
 */
class LahanTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LahanTable
     */
    public $Lahan;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.lahan',
        'app.daerah',
        'app.jenis_daerah',
        'app.bumdes',
        'app.ektp',
        'app.gapoktan',
        'app.poktan',
        'app.log',
        'app.user',
        'app.petani',
        'app.users',
        'app.jenis_user',
        'app.notifications',
        'app.penyuluh'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Lahan') ? [] : ['className' => 'App\Model\Table\LahanTable'];
        $this->Lahan = TableRegistry::get('Lahan', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Lahan);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
