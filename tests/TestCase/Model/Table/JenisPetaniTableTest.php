<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\JenisPetaniTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\JenisPetaniTable Test Case
 */
class JenisPetaniTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\JenisPetaniTable
     */
    public $JenisPetani;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.jenis_petani'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('JenisPetani') ? [] : ['className' => 'App\Model\Table\JenisPetaniTable'];
        $this->JenisPetani = TableRegistry::get('JenisPetani', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->JenisPetani);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
