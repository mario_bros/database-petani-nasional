<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BpsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BpsTable Test Case
 */
class BpsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BpsTable
     */
    public $Bps;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bps',
        'app.daerah',
        'app.jenis_daerah',
        'app.bumdes',
        'app.users',
        'app.jenis_user',
        'app.petani',
        'app.poktan',
        'app.gapoktan',
        'app.penyuluh',
        'app.ektp',
        'app.log',
        'app.ketua_poktan',
        'app.lahan',
        'app.jenis_tanaman',
        'app.jenis_lahan',
        'app.tr_rdkk',
        'app.rdkk',
        'app.komoditas',
        'app.subsektor',
        'app.items',
        'app.category',
        'app.prices',
        'app.notifications'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Bps') ? [] : ['className' => 'App\Model\Table\BpsTable'];
        $this->Bps = TableRegistry::get('Bps', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Bps);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
