<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\JenisLahanTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\JenisLahanTable Test Case
 */
class JenisLahanTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\JenisLahanTable
     */
    public $JenisLahan;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.jenis_lahan'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('JenisLahan') ? [] : ['className' => 'App\Model\Table\JenisLahanTable'];
        $this->JenisLahan = TableRegistry::get('JenisLahan', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->JenisLahan);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
