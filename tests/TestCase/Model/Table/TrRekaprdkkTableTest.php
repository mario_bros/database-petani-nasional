<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TrRekaprdkkTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TrRekaprdkkTable Test Case
 */
class TrRekaprdkkTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TrRekaprdkkTable
     */
    public $TrRekaprdkk;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tr_rekaprdkk',
        'app.rdkks',
        'app.rdkk_rekaps'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TrRekaprdkk') ? [] : ['className' => 'App\Model\Table\TrRekaprdkkTable'];
        $this->TrRekaprdkk = TableRegistry::get('TrRekaprdkk', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TrRekaprdkk);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
