<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AssetsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AssetsTable Test Case
 */
class AssetsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AssetsTable
     */
    public $Assets;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.assets',
        'app.lahan',
        'app.daerah',
        'app.jenis_daerah',
        'app.bumdes',
        'app.petani',
        'app.poktan',
        'app.gapoktan',
        'app.penyuluh',
        'app.ektp',
        'app.users',
        'app.jenis_user',
        'app.bps',
        'app.log',
        'app.notifications',
        'app.rekap_rdkk',
        'app.aprroves',
        'app.rekap_log',
        'app.tr_rekaprdkk',
        'app.rdkk',
        'app.komoditas',
        'app.subsektor',
        'app.jenis_lahan',
        'app.items',
        'app.category',
        'app.prices',
        'app.tr_rdkk',
        'app.items_komoditas',
        'app.ketua_poktan',
        'app.komoditas_petani',
        'app.jenis_petani',
        'app.daerah_status',
        'app.daerahs',
        'app.jenis_tanaman'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Assets') ? [] : ['className' => 'App\Model\Table\AssetsTable'];
        $this->Assets = TableRegistry::get('Assets', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Assets);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
