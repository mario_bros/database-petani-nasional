<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\JenisTanamanTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\JenisTanamanTable Test Case
 */
class JenisTanamanTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\JenisTanamanTable
     */
    public $JenisTanaman;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.jenis_tanaman',
        'app.lahan',
        'app.daerah',
        'app.jenis_daerah',
        'app.bumdes',
        'app.ektp',
        'app.gapoktan',
        'app.poktan',
        'app.log',
        'app.user',
        'app.petani',
        'app.users',
        'app.jenis_user',
        'app.notifications',
        'app.penyuluh'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('JenisTanaman') ? [] : ['className' => 'App\Model\Table\JenisTanamanTable'];
        $this->JenisTanaman = TableRegistry::get('JenisTanaman', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->JenisTanaman);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
