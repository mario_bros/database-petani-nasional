<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\KomoditasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\KomoditasTable Test Case
 */
class KomoditasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\KomoditasTable
     */
    public $Komoditas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.komoditas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Komoditas') ? [] : ['className' => 'App\Model\Table\KomoditasTable'];
        $this->Komoditas = TableRegistry::get('Komoditas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Komoditas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
