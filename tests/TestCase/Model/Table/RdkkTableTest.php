<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RdkkTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RdkkTable Test Case
 */
class RdkkTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RdkkTable
     */
    public $Rdkk;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.rdkk',
        'app.petani',
        'app.poktan',
        'app.daerah',
        'app.jenis_daerah',
        'app.bumdes',
        'app.ektp',
        'app.penyuluh',
        'app.users',
        'app.jenis_user',
        'app.log',
        'app.notifications',
        'app.gapoktan',
        'app.lahan',
        'app.jenis_tanaman',
        'app.ketua_poktan',
        'app.subsektor',
        'app.komoditas',
        'app.tr_rdkk',
        'app.item'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Rdkk') ? [] : ['className' => 'App\Model\Table\RdkkTable'];
        $this->Rdkk = TableRegistry::get('Rdkk', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Rdkk);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
