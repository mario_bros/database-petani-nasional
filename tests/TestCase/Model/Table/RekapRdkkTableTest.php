<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RekapRdkkTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RekapRdkkTable Test Case
 */
class RekapRdkkTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RekapRdkkTable
     */
    public $RekapRdkk;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.rekap_rdkk',
        'app.daerah',
        'app.jenis_daerah',
        'app.bumdes',
        'app.bps',
        'app.ektp',
        'app.petani',
        'app.poktan',
        'app.gapoktan',
        'app.penyuluh',
        'app.users',
        'app.jenis_user',
        'app.log',
        'app.notifications',
        'app.ketua_poktan',
        'app.lahan',
        'app.jenis_tanaman',
        'app.jenis_lahan',
        'app.tr_rdkk',
        'app.rdkk',
        'app.komoditas',
        'app.subsektor',
        'app.items',
        'app.category',
        'app.prices',
        'app.aprroves',
        'app.rekap_log',
        'app.tr_rekaprdkk'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RekapRdkk') ? [] : ['className' => 'App\Model\Table\RekapRdkkTable'];
        $this->RekapRdkk = TableRegistry::get('RekapRdkk', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RekapRdkk);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
