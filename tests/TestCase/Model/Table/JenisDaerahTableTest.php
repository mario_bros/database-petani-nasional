<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\JenisDaerahTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\JenisDaerahTable Test Case
 */
class JenisDaerahTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\JenisDaerahTable
     */
    public $JenisDaerah;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.jenis_daerah',
        'app.daerah',
        'app.bumdes',
        'app.ektp',
        'app.gapoktan',
        'app.poktan',
        'app.log',
        'app.user',
        'app.petani',
        'app.lahan',
        'app.users',
        'app.jenis_user',
        'app.notifications',
        'app.penyuluh'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('JenisDaerah') ? [] : ['className' => 'App\Model\Table\JenisDaerahTable'];
        $this->JenisDaerah = TableRegistry::get('JenisDaerah', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->JenisDaerah);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
