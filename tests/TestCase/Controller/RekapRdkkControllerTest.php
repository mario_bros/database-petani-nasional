<?php
namespace App\Test\TestCase\Controller;

use App\Controller\RekapRdkkController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\RekapRdkkController Test Case
 */
class RekapRdkkControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.rekap_rdkk',
        'app.daerah',
        'app.jenis_daerah',
        'app.bumdes',
        'app.poktan',
        'app.gapoktan',
        'app.penyuluh',
        'app.ektp',
        'app.petani',
        'app.lahan',
        'app.jenis_tanaman',
        'app.users',
        'app.jenis_user',
        'app.log',
        'app.notifications',
        'app.ketua_poktan',
        'app.aprroves',
        'app.rekap_log'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
