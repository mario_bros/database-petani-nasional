<?php
namespace App\Test\TestCase\Controller;

use App\Controller\PoktanController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\PoktanController Test Case
 */
class PoktanControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.poktan',
        'app.daerah',
        'app.jenis_daerah',
        'app.bumdes',
        'app.ektp',
        'app.gapoktan',
        'app.lahan',
        'app.user',
        'app.log',
        'app.petani',
        'app.users',
        'app.jenis_user',
        'app.notifications',
        'app.penyuluh'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
