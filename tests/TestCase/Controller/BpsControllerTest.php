<?php
namespace App\Test\TestCase\Controller;

use App\Controller\BpsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\BpsController Test Case
 */
class BpsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bps',
        'app.daerah',
        'app.jenis_daerah',
        'app.bumdes',
        'app.users',
        'app.jenis_user',
        'app.petani',
        'app.poktan',
        'app.gapoktan',
        'app.penyuluh',
        'app.ektp',
        'app.log',
        'app.ketua_poktan',
        'app.lahan',
        'app.jenis_tanaman',
        'app.jenis_lahan',
        'app.tr_rdkk',
        'app.rdkk',
        'app.komoditas',
        'app.subsektor',
        'app.items',
        'app.category',
        'app.prices',
        'app.notifications'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
