<?php
namespace App\Test\TestCase\Controller;

use App\Controller\CategoryController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\CategoryController Test Case
 */
class CategoryControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.category',
        'app.items',
        'app.prices',
        'app.tr_rdkk',
        'app.rdkk',
        'app.petani',
        'app.poktan',
        'app.daerah',
        'app.jenis_daerah',
        'app.bumdes',
        'app.ektp',
        'app.penyuluh',
        'app.users',
        'app.jenis_user',
        'app.log',
        'app.notifications',
        'app.gapoktan',
        'app.lahan',
        'app.jenis_tanaman',
        'app.ketua_poktan',
        'app.subsektor',
        'app.komoditas'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
