<?php
namespace App\Test\TestCase\Controller;

use App\Controller\GapoktanController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\GapoktanController Test Case
 */
class GapoktanControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.gapoktan',
        'app.daerah',
        'app.jenis_daerah',
        'app.bumdes',
        'app.ektp',
        'app.lahan',
        'app.poktan',
        'app.log',
        'app.user',
        'app.petani',
        'app.users',
        'app.jenis_user',
        'app.notifications',
        'app.penyuluh'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
