<?php
namespace App\Test\TestCase\Controller;

use App\Controller\BumdesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\BumdesController Test Case
 */
class BumdesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bumdes',
        'app.daerah',
        'app.jenis_daerah',
        'app.ektp',
        'app.gapoktan',
        'app.poktan',
        'app.log',
        'app.user',
        'app.petani',
        'app.lahan',
        'app.users',
        'app.jenis_user',
        'app.notifications',
        'app.penyuluh'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
