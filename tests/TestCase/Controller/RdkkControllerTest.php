<?php
namespace App\Test\TestCase\Controller;

use App\Controller\RdkkController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\RdkkController Test Case
 */
class RdkkControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.rdkk',
        'app.tr_rdkk',
        'app.lahan',
        'app.daerah',
        'app.jenis_daerah',
        'app.bumdes',
        'app.poktan',
        'app.gapoktan',
        'app.penyuluh',
        'app.ektp',
        'app.petani',
        'app.users',
        'app.jenis_user',
        'app.log',
        'app.notifications',
        'app.ketua_poktan',
        'app.jenis_tanaman',
        'app.items',
        'app.category',
        'app.prices'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
