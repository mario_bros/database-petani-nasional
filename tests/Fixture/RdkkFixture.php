<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RdkkFixture
 *
 */
class RdkkFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'rdkk';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'petani_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tanggal' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'id_subsektor' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'id_komoditas' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'IXFK_RDKK_Subsektor' => ['type' => 'index', 'columns' => ['id_subsektor'], 'length' => []],
            'id_komoditas' => ['type' => 'index', 'columns' => ['id_komoditas'], 'length' => []],
            'petani_id' => ['type' => 'index', 'columns' => ['petani_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'FK_rdkk_subsektor02' => ['type' => 'foreign', 'columns' => ['id_subsektor'], 'references' => ['subsektor', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'rdkk_ibfk_1' => ['type' => 'foreign', 'columns' => ['id_komoditas'], 'references' => ['komoditas', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'rdkk_ibfk_2' => ['type' => 'foreign', 'columns' => ['petani_id'], 'references' => ['petani', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'petani_id' => 1,
            'tanggal' => '2016-05-23',
            'id_subsektor' => 1,
            'id_komoditas' => 1
        ],
    ];
}
