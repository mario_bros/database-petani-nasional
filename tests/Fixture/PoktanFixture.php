<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PoktanFixture
 *
 */
class PoktanFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'poktan';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'nama' => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'alamat' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'daerah_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'gapoktan_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'lat' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'lon' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'aprroved' => ['type' => 'decimal', 'length' => 2, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'ketua_poktan' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'IXFK_poktan_gapoktan' => ['type' => 'index', 'columns' => ['gapoktan_id'], 'length' => []],
            'IXFK_poktan_daerah' => ['type' => 'index', 'columns' => ['daerah_id'], 'length' => []],
            'ketua_poktan' => ['type' => 'index', 'columns' => ['ketua_poktan'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'FK_poktan_daerah' => ['type' => 'foreign', 'columns' => ['daerah_id'], 'references' => ['daerah', 'id'], 'update' => 'restrict', 'delete' => 'cascade', 'length' => []],
            'FK_poktan_gapoktan' => ['type' => 'foreign', 'columns' => ['gapoktan_id'], 'references' => ['gapoktan', 'id'], 'update' => 'restrict', 'delete' => 'cascade', 'length' => []],
            'poktan_ibfk_1' => ['type' => 'foreign', 'columns' => ['ketua_poktan'], 'references' => ['petani', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'nama' => 'Lorem ipsum dolor sit amet',
            'alamat' => 'Lorem ipsum dolor sit amet',
            'daerah_id' => 1,
            'gapoktan_id' => 1,
            'lat' => 1,
            'lon' => 1,
            'aprroved' => 1.5,
            'ketua_poktan' => 1
        ],
    ];
}
