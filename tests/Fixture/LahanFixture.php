<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * LahanFixture
 *
 */
class LahanFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'lahan';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'alamat' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'daerah_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'luas' => ['type' => 'decimal', 'length' => 8, 'precision' => 2, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'jenis_tanaman_id' => ['type' => 'integer', 'length' => 50, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'petani_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'IXFK_lahan_petani' => ['type' => 'index', 'columns' => ['petani_id'], 'length' => []],
            'IXFK_lahan_daerah' => ['type' => 'index', 'columns' => ['daerah_id'], 'length' => []],
            'jenis_tanaman_id' => ['type' => 'index', 'columns' => ['jenis_tanaman_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'lahan_ibfk_1' => ['type' => 'foreign', 'columns' => ['jenis_tanaman_id'], 'references' => ['jenis_tanaman', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'FK_lahan_daerah' => ['type' => 'foreign', 'columns' => ['daerah_id'], 'references' => ['daerah', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'FK_lahan_daerah02' => ['type' => 'foreign', 'columns' => ['petani_id'], 'references' => ['daerah', 'id'], 'update' => 'restrict', 'delete' => 'cascade', 'length' => []],
            'FK_lahan_petani' => ['type' => 'foreign', 'columns' => ['petani_id'], 'references' => ['petani', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'alamat' => 'Lorem ipsum dolor sit amet',
            'daerah_id' => 1,
            'luas' => 1.5,
            'jenis_tanaman_id' => 1,
            'petani_id' => 1
        ],
    ];
}
