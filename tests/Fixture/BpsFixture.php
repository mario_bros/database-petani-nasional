<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BpsFixture
 *
 */
class BpsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'no_ktp' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'no_tlp' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'daerah_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'daerah_id' => ['type' => 'index', 'columns' => ['daerah_id'], 'length' => []],
            'no_ktp' => ['type' => 'index', 'columns' => ['no_ktp'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'bps_ibfk_2' => ['type' => 'foreign', 'columns' => ['daerah_id'], 'references' => ['daerah', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'bps_ibfk_1' => ['type' => 'foreign', 'columns' => ['no_ktp'], 'references' => ['ektp', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'no_ktp' => 'Lorem ipsum dolor sit amet',
            'no_tlp' => 'Lorem ipsum dolor sit amet',
            'daerah_id' => 1
        ],
    ];
}
