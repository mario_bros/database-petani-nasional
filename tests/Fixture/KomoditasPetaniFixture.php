<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * KomoditasPetaniFixture
 *
 */
class KomoditasPetaniFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'komoditas_petani';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'id_petani' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'id_komoditas' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'id_jenis_petani' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'id_petani' => ['type' => 'index', 'columns' => ['id_petani', 'id_komoditas'], 'length' => []],
            'id_komoditas' => ['type' => 'index', 'columns' => ['id_komoditas'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'komoditas_petani_ibfk_1' => ['type' => 'foreign', 'columns' => ['id_petani'], 'references' => ['petani', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'komoditas_petani_ibfk_2' => ['type' => 'foreign', 'columns' => ['id_komoditas'], 'references' => ['komoditas', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'id_petani' => 1,
            'id_komoditas' => 1,
            'id_jenis_petani' => 1
        ],
    ];
}
