<?php
/* @var $this \Cake\View\View */
use Cake\Core\Configure;

$this->Html->css('BootstrapUI.dashboard', ['block' => true]);
$this->Html->css('bootstrap/dashboard', ['block' => true]);
$this->prepend('tb_body_attrs', ' class="' . implode(' ', [$this->request->controller, $this->request->action]) . '" ');
$this->start('tb_body_start');
?>
<body <?= $this->fetch('tb_body_attrs') ?>>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><?= Configure::read('App.title') ?></a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right visible-xs">
                    <?php // $this->fetch('tb_actions') ?>
                    <li><?= $this->Html->link(__('Poktan Saya'), ['controller'=>'poktan','action' => 'index']); ?></li>
                    <li><?= $this->Html->link(__('Tambah Data Poktan'), ['controller'=>'petani','action' => 'add']); ?></li>
                    <li><?= $this->Html->link(__('Tambah Data Gapoktan'), ['controller'=>'petani','action' => 'add']); ?></li>
                    <li><?= $this->Html->link(__('Tambah Data Petani'), ['controller'=>'petani','action' => 'add']); ?></li>
                    <li><?= $this->Html->link(__('Tambah Data Petani'), ['controller'=>'petani','action' => 'add']); ?></li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li class="nav-divider"></li>
                    <li><?php echo $this->Html->link(__('Home'),['controller'=>'pages','action'=>'home']) ?></li>
                    <li><a href="#">Settings</a></li>
                    <li><?php echo $this->Html->link(__('Logout'),['controller'=>'user','action'=>'logout']) ?></li>

                </ul>
                <form class="navbar-form navbar-right">
                    <input type="text" class="form-control" placeholder="Search...">
                </form>

            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-pills">
                    <li><?= $this->Html->link(__('Poktan Saya'), ['controller'=>'poktan','action' => 'index']); ?></li>
                    <li><?= $this->Html->link(__('Tambah Data Poktan'), ['controller'=>'petani','action' => 'add']); ?></li>
                    <li><?= $this->Html->link(__('Tambah Data Gapoktan'), ['controller'=>'petani','action' => 'add']); ?></li>
                    <li><?= $this->Html->link(__('Tambah Data Petani'), ['controller'=>'petani','action' => 'add']); ?></li>
                    <li><?= $this->Html->link(__('Tambah Data Petani'), ['controller'=>'petani','action' => 'add']); ?></li>
                </ul>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <h1 class="page-header"><?= ($this->request->controller=='Pages') ? "Dashboard" : $this->request->controller; ?></h1>
            <div class="row">
                <div class="col-md-12 pull-right" id="secondNav">
                    <?= $this->fetch('tb_sidebar') ?>
                </div>
            </div>
<?php
/**
 * Default `flash` block.
 */
if (!$this->fetch('tb_flash')) {
    $this->start('tb_flash');
    if (isset($this->Flash))
        echo $this->Flash->render();
    $this->end();
}
$this->end();

$this->start('tb_body_end');
echo '</body>';
$this->end();

$this->append('content', '</div></div></div>');
echo $this->fetch('content');
